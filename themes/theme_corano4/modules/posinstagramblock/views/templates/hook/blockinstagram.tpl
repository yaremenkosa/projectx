<!--- Author : Posthemes.com -->
<div id="instagram_block">
{foreach from=$instagrams item=instagram}<a class="fancybox col-xs-{12/$row}" href="{$instagram['image']}" data-fancybox="gallery" style="padding:3px;"><img class="instagram_image" src="{$instagram['thumbnail']}" alt="" style="max-width: 100%;"/></a>{/foreach}<div class="our_instagram"><a href="https://www.instagram.com/{$username}/" target="_blank"  ><span>{l s='Follow our instagram' d='Shop.Theme.Catalog'}</span></a></div>
</div>
<script>    
    $(document).ready(function () {			
	$('#instagram_block [data-fancybox="gallery"]').fancybox({
		// Options will go here
	});
});
</script>


