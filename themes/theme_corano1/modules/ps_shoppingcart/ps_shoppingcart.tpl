<div id="_desktop_cart_block">
	<div class="blockcart cart-preview" data-refresh-url="{$refresh_url}" data-cartitems="{$cart.products_count}">
		<div class="button_cart">
			{* <a rel="nofollow" href="{$cart_url}" class="desktop hidden-md-down">
				<i class="pe-7s-shopbag"></i>
				<span class="item_count">{$cart.products_count}</span>
			</a> *}
			{*<a rel="nofollow" href="{$cart_url}" class="mobile hidden-lg-up">
				<i class="pe-7s-shopbag"></i>
				<span class="item_count">{$cart.products_count}</span>
			</a> *}
			<a class="header_cart" rel="nofollow" href="{$cart_url}">
				<div class="header_cart-left">
					<svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M23.0139 4.00649H4.24886L3.96992 0.899045C3.9243 0.390878 3.49841 0.00150299 2.98823 0.00150299H0.98564C0.441285 0.00150299 0 0.442788 0 0.987143C0 1.5315 0.441285 1.97278 0.98564 1.97278H2.08712C2.68906 8.67898 1.13335 -8.65406 3.22074 14.6025C3.30119 15.5128 3.79302 16.5005 4.6403 17.1717C3.1127 19.1225 4.50846 21.9985 6.99335 21.9985C9.05578 21.9985 10.5104 19.9415 9.80317 17.9933H15.1977C14.4913 19.939 15.9429 21.9985 18.0075 21.9985C19.6553 21.9985 20.9958 20.658 20.9958 19.0103C20.9958 17.3626 19.6553 16.022 18.0075 16.022H7.00001C6.2513 16.022 5.59886 15.5703 5.31725 14.9122L21.0691 13.9865C21.4991 13.9612 21.8629 13.6595 21.9675 13.2416L23.9702 5.23117C24.1255 4.61003 23.6553 4.00649 23.0139 4.00649V4.00649ZM6.99335 20.0272C6.43266 20.0272 5.97645 19.571 5.97645 19.0103C5.97645 18.4495 6.43266 17.9933 6.99335 17.9933C7.55408 17.9933 8.0103 18.4495 8.0103 19.0103C8.0103 19.571 7.55408 20.0272 6.99335 20.0272ZM18.0075 20.0272C17.4468 20.0272 16.9906 19.571 16.9906 19.0103C16.9906 18.4495 17.4468 17.9933 18.0075 17.9933C18.5682 17.9933 19.0244 18.4495 19.0244 19.0103C19.0244 19.571 18.5682 20.0272 18.0075 20.0272ZM20.2306 12.0611L5.05192 12.9531L4.4258 5.97772H21.7516L20.2306 12.0611Z" fill="white" />
					</svg>
					<span>{$cart.products_count}</span>
				</div>
				<div class="header_cart-right">
					{if $cart.products_count > 0}
						<span>{$cart.products_count} товаров</span>
					{else}
						<span>Корзина</span>
					{/if}

					{if $cart.totals.total.value > 0}
						<span>{$cart.totals.total.value}</span>
					{else}
						<span>Пусто</span>
					{/if}
				</div>

				{* <span>{$cart.totals.total.value}</span> *}

			</a>
		</div>
		<div class="popup_cart">
			<div class="content-cart">
				<div class="mini_cart_arrow"></div>
				<ul>
					{foreach from=$cart.products item=product}
						<li>{include 'module:ps_shoppingcart/ps_shoppingcart-product-line.tpl' product=$product}</li>
					{/foreach}
				</ul>
				<div class="price_content">
					{* <div class="cart-subtotals">
						{foreach from=$cart.subtotals item="subtotal"}
							<div class="{$subtotal.type} price_inline">
								<span class="label">{$subtotal.label}</span>
								<span class="value">{$subtotal.value}</span>
							</div>
						{/foreach}
					</div> *}
					<div class="cart-total price_inline">
						<span class="label">{$cart.totals.total.label}</span>
						<span class="value">{$cart.totals.total.value}</span>
					</div>
				</div>
				<div class="checkout">
					<a onclick="ym(74564365,'reachGoal','showcart') ; return true;" href="{$cart_url}" class="btn btn-primary">{l s='Checkout' d='Shop.Theme.Actions'}</a>
				</div>
			</div>
		</div>
	</div>
</div>