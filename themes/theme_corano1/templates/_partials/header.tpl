{**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License 3.0 (AFL-3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* https://opensource.org/licenses/AFL-3.0
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2018 PrestaShop SA
* @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
{block name='header_banner'}
<div class="header-banner">
	{hook h='displayBanner'}
</div>
{/block}

{block name='header_nav'}
<nav class="header-nav">
	<div class="container header_nav-head">
		<nav class="hidden-md-down pwblockfavpage">
			{hook h='displayNav'}
		</nav>
		<div class="hidden-lg-up mobile" id="header_mobile">
			<div class="mobile-bottom">

					<div id="menu-icon">
						<span></span>
						<span></span>
						<span></span>
						<!--<i class="pe-7s-menu"></i>-->
					</div>
					<div id="mobile_top_menu_wrapper" class="row hidden-lg-up" style="display:none;">
						{* <div class="top-header-mobile">
							<div id="_mobile_static"></div>
						</div>
						<div id="_mobile_currency_selector"></div>
						<div id="_mobile_language_selector"></div> *}
						<div class="menu-close">
							{l s='menu' d='Shop.Theme.Global'} <i class="material-icons float-xs-right">arrow_back</i>
						</div>
						<div class="menu-tabs">
							<div class="js-top-menu-bottom">
								<div id="_mobile_catalog">
									<div class="itemCatalog">
										<a href="/2-glavnaya">
											<span>Каталог товаров</span>
										</a>
										<span class="icon-drop-mobile">
											<i class="material-icons add active">add</i>
											<i class="material-icons remove">remove</i>
										</span>
									</div>
								</div>
								<div id="_mobile_megamenu"></div>
							</div>

						</div>
					</div>
					<div id="_mobile_logo"></div>
					<div id="_mobile_search_block"></div>
					<div id="_mobile_user_info"></div>
					<div id="_mobile_compare"></div>
					<div id="_mobile_favorite"></div>
					<div id="_mobile_wishtlist"></div>
					<div id="_mobile_cart_block"></div>
			</div>
		</div>
	</div>
</nav>
{/block}

{block name='header_top'}
<div class="header_top hidden-md-down">
	<div class="container">
		<div class="header_top-row">
			<div class="header_top-logo" id="_desktop_logo">
				{if $page.page_name == 'index'}
				<a href="{$urls.base_url}">
				<img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
				</a>
				{else}
				<a href="{$urls.base_url}">
				<img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
				</a>
				{/if}
			</div>
			<div class="header_top-middle">
				{hook h='displayTop'}
			</div>

		</div>
	</div>
</div>

{if $smarty.get.controller != 'cart' || $page.page_name != 'cart'}
<div class="navigation_megamenu">
	<div class="container">
		{hook h='displayCat'}
		{hook h='displaymegamenu'}
	</div>
</div>
{/if}

{hook h='displayNavFullWidth'}
{/block}
