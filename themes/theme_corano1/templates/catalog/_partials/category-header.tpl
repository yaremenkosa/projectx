{**
 * 2007-2018 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{* <div id="js-product-list-header">
    {if $listing.pagination.items_shown_from == 1}
        <div class="block-category card card-block {if $category.image.large.url}category_img{/if}">
			{if $category.image.large.url}
                <div class="category-cover">
                    <img src="{$category.image.large.url}" alt="{if !empty($category.image.legend)}{$category.image.legend}{else}{$category.name}{/if}">
                </div>
            {/if}
			{if $category.description}
			<div class="block-desc">
				<h1 class="h1">{$category.name}</h1>
                <div id="category-description" class="text-muted">{$category.description nofilter}</div>
			</div>
			 {/if}


        </div>
    {/if}
*}
{if isset($category) && $category.name}
	{if $category.level_depth < 3}
		<h1 class="h1">{$category.name}</h1>
	{else}
		<h1 class="h1">
			{foreach Category::getCategoryInformation([$category.id_parent]) item=cat}
				{$cat.name} 
			{/foreach}
		- {$category.name}</h1>
	{/if}
{/if}

{if $postheme.cate_show_subcategories == 1}
{if isset($subcategories)}
	<!-- Subcategories -->
{assign var="loop_middle" value=$subcategories|@count/2}
<div id="subcategories" style="margin-top:20px;">
	<ul class="clearfix subcategory-content" style="display: block;width: max-content;">
		{foreach from=$subcategories item=subcategory name="subcats"}
			{if $smarty.foreach.subcats.iteration == $loop_middle|ceil}
				<div style="display: block;width: max-content;">
			{/if}
			<li>
				<div class="subcategory-image">
					<a href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}" title="{$subcategory.name|escape:'html':'UTF-8'}" class="img">
					</a>
					<h5 style="margin-bottom: 4px;"><a class="subcategory-name" href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}">{$subcategory.name|truncate:25:'...'|escape:'html':'UTF-8'}</a></h5>
				</div>
			</li>
			{if $smarty.foreach.var.iteration == $loop_middle|ceil}
				</div>
			{/if}
		{/foreach}
	</ul>
</div>
{/if}
{/if}


