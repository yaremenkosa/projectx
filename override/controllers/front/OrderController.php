<?php

class OrderController extends OrderControllerCore 
{
    protected function restorePersistedData(CheckoutProcess $process)
    {
        $cart = $this->context->cart;
        $customer = $this->context->customer;
        $rawData = Db::getInstance()->getValue(
            'SELECT checkout_session_data FROM ' . _DB_PREFIX_ . 'cart WHERE id_cart = ' . (int) $cart->id
        );
        $data = json_decode($rawData, true);

        if (!is_array($data)) {
            $data = array();
        }

        $addressValidator = new AddressValidator();
        $invalidAddressIds = $addressValidator->validateCartAddresses($cart);

        // Build the currently selected address' warning message (if relevant)
        if (!$customer->isGuest() && !empty($invalidAddressIds)) {
            $this->checkoutWarning['address'] = array(
                'id_address' => (int) reset($invalidAddressIds),
                'exception' => $this->trans(
                    'Your address is incomplete, please update it.',
                    array(),
                    'Shop.Notifications.Error'
                ),
            );

            $checksum = null;
        } else {
            $checksum = Tools::getValue('checksum');
        }

        // Prepare all other addresses' warning messages (if relevant).
        // These messages are displayed when changing the selected address.
        $allInvalidAddressIds = $addressValidator->validateCustomerAddresses($customer, $this->context->language);
        $this->checkoutWarning['invalid_addresses'] = $allInvalidAddressIds;

        if (isset($data['checksum']) && $data['checksum'] === $checksum) {
            $process->restorePersistedData($data);
        }
    }
    protected function bootstrap()
    {
        $translator = $this->getTranslator();

        $session = $this->getCheckoutSession();

        $this->checkoutProcess = new CheckoutProcess(
            $this->context,
            $session
        );

        $step_personal = new CheckoutPersonalInformationStep(
            $this->context,
            $translator,
            $this->makeLoginForm(),
            $this->makeCustomerForm()
        );
        $step_address = new CheckoutAddressesStep(
                $this->context,
                $translator,
                $this->makeAddressForm()
            );
        $step_personal->setComplete(true)->setReachable(true);
        $step_address->setComplete(true)->setReachable(true);
        $this->checkoutProcess
            ->addStep($step_personal)
            ->addStep($step_address);
        if (!$this->context->cart->isVirtualCart()) {
            $checkoutDeliveryStep = new CheckoutDeliveryStep(
                $this->context,
                $translator
            );

            $checkoutDeliveryStep
                ->setRecyclablePackAllowed((bool) Configuration::get('PS_RECYCLABLE_PACK'))
                ->setGiftAllowed((bool) Configuration::get('PS_GIFT_WRAPPING'))
                ->setIncludeTaxes(
                    !Product::getTaxCalculationMethod((int) $this->context->cart->id_customer)
                    && (int) Configuration::get('PS_TAX')
                )
                ->setDisplayTaxesLabel((Configuration::get('PS_TAX') && !Configuration::get('AEUC_LABEL_TAX_INC_EXC')))
                ->setGiftCost(
                    $this->context->cart->getGiftWrappingPrice(
                        $checkoutDeliveryStep->getIncludeTaxes()
                    )
                );
            // if(Configuration::get('PW_EO_CARRIER') == PwExpressOrder::_CARRIER_ADV_){
                    //$checkoutDeliveryStep->setCurrent(true);
            // }else{
                  $checkoutDeliveryStep->setComplete(true);
            // }
            $checkoutDeliveryStep->setReachable(true);
            $this->checkoutProcess->addStep($checkoutDeliveryStep);
        }

        $this->checkoutProcess
            ->addStep(new CheckoutPaymentStep(
                $this->context,
                $translator,
                new PaymentOptionsFinder(),
                new ConditionsToApproveFinder(
                    $this->context,
                    $translator
                )
            ));
    }
}