<?php

namespace PriceParser\Export;

interface Runnable {
	
	public function run(Array $data);
	
}
