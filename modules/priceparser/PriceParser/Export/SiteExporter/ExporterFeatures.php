<?php

namespace PriceParser\Export\SiteExporter;

use PriceParser\Core\Object\Object;
use PriceParser\Export\Exportable;
use PriceParser\Core\PriceParserIds;
use PriceParser\Export\Exceptions\ExporterException;
use PriceParser\Core\FeatureSearch;
use PriceParser\Core\Tools AS PTools;

class ExporterFeatures extends Object implements Exportable {
	
	var $id_lang;
	
	var $features_created = 0;
	var $features_updated = 0;
	
	var $features_values_created = 0;
	var $features_values_updated = 0;

	var $brands_created = 0;
	
	function __construct(array $params = array()) {
		parent::__construct($params);
		
		$this->id_lang = \Configuration::get('PS_LANG_DEFAULT');
	}
	
	public function export(Array $data) {
		
		if(!empty($data['features'])) {
			$this->log(1, 'Экспортируем характеристики. Всего: ' . count($data['features']));
			
			foreach($data['features'] as $filter_row) {
//				if(isset($filter_row['vendor_id'])) {
//					$id_feature = PriceParserIds::getSiteIdByVendorId($filter_row['vendor_id'], PriceParserIds::FILTER_TYPE_ID);
//				} elseif(isset($filter_row['site_id']) && $filter_row['site_id']) {
//					$id_feature = $filter_row['site_id'];
//				} else {
//					throw new \PriceParser\Export\Exceptions\ExporterException('Отсутствует id характеристики, строка: ' . print_r($filter_row, true));
//				}
//				
//				if($id_feature) {
//					$this->log(2, 'Фильтр ' . $filter_row['id'] . ' найден на сайте, id = ' . $id_feature);
//					
//					$feature = new \Feature($id_feature);
//				} else {
//					$this->log(2, 'Фильтр ' . $filter_row['id'] . ' отсутствует на сайте');
//					$feature = new \Feature();
//				}
				
				$feature = $this->initFeature($filter_row);
				
				if(!$feature->id || $feature->name[$this->id_lang] != $filter_row['name']) {
				
					$feature->name[$this->id_lang] = PTools::clearString($filter_row['name']);

					$is_new = !$feature->id;
					
					$save_result = $feature->save();

					if($save_result) {
						if($is_new && isset($filter_row['vendor_id'])) {
							$this->features_created++;
							PriceParserIds::addSiteIdByVendorId($filter_row['vendor_id'], $feature->id, PriceParserIds::FILTER_TYPE_ID);
						} else {
							$this->features_updated++;
						}

						$this->log(2, 'Фильтр успешно сохранён (id на сайте = ' . $feature->id . ')');
					} else {
						$this->log(2, 'Фильтр - ОШИБКА при сохранении ');
					}
				} else {
					$this->log(2, 'Фильтр не изменён (id на сайте = ' . $feature->id . ')');
				}
				
				if(isset($filter_row['values'])) {
					foreach($filter_row['values'] as $value_row) {
//						$id_feature_value = PriceParserIds::getSiteIdByVendorId($value_row['id'], PriceParserIds::FILTER_VALUE_TYPE_ID);
//						
//						if($id_feature_value) {
//							$this->log(2, 'Значение фильтра ' . $value_row['id'] . ' найдено на сайте, id = ' . $id_feature_value);
//					
//							$featureValue = new \FeatureValue($id_feature_value);
//						} else {
//							$this->log(2, 'Значение фильтра ' . $value_row['id'] . ' отсутствует на сайте');
//					
//							$featureValue = new \FeatureValue();
//						}
//						
//						if(!$id_feature_value) {
//							$featureValue->id_feature = $feature->id;
//						}
						
						$featureValue = $this->initFeatureValue($value_row);
						
						if(!$featureValue->id) {
							$featureValue->id_feature = $feature->id;
						}
						
						if(!$featureValue->id || $featureValue->value[$this->id_lang] != $value_row['name']) {
						
							$featureValue->value[$this->id_lang] = PTools::clearString($value_row['name']);
							
							$is_new = !$featureValue->id;
							
							$save_result = $featureValue->save();

							if($save_result) {
								if($is_new && isset($value_row['vendor_id'])) {
									$this->features_values_created++;
									PriceParserIds::addSiteIdByVendorId($value_row['vendor_id'], $featureValue->id, PriceParserIds::FILTER_VALUE_TYPE_ID);
								} else {
									$this->features_values_updated++;
								}

								$this->log(2, 'Значение характеритики успешно сохранено (id на сайте = ' . $featureValue->id . ')');
							} else {
								$this->log(2, 'Значение характеритики - ОШИБКА при сохранении');
							}
						} else {
							$this->log(2, 'Значение характеритики не изменено');
						}
					}
				}
			}
		}

		if(!empty($data['brands'])) {
			$this->log(1, 'Экспортируем бренды');

			foreach($data['brands'] as $brand_row) {
				$id_brand = PriceParserIds::getSiteIdByVendorId($brand_row['vendor_id'], PriceParserIds::BRAND_VALUE_ID);

				if(!$id_brand) {
					$this->log(2, 'Производитель ' . $brand_row['name'] . ' отсутствует, добавляем');
					$manufacturer = new \Manufacturer();
					$manufacturer->name = $brand_row['name'];
					$manufacturer->active = true;

					$save_result = $manufacturer->save();

					if($save_result) {
						$this->brands_created++;
						PriceParserIds::addSiteIdByVendorId($brand_row['vendor_id'], $manufacturer->id, PriceParserIds::BRAND_VALUE_ID);
					} else {
						$this->log(2, 'Фильтр - ОШИБКА при сохранении ');
					}
				}
			}
		}
		
		$this->log(1, 'Загрузка фильтров завершена');
	}
	
	/**
	 * Инициализация модели характеристики из строки выгрузки
	 * @param Array $row
	 * @return \Feature
	 */
	function initFeature($filter_row) {
		$feature = null;
		
		if(isset($filter_row['vendor_id'])) {
			$id_feature = PriceParserIds::getSiteIdByVendorId($filter_row['vendor_id'], PriceParserIds::FILTER_TYPE_ID);
			
			if($id_feature) {
				$feature = new \Feature($id_feature);
				
				if(!\Validate::isLoadedObject($feature)) {
					$this->log(2, 'Не удалось загрузить характеристику. id_vendor = ' . $filter_row['vendor_id']);
					PriceParserIds::deleteSiteId($id_feature, PriceParserIds::FILTER_TYPE_ID);
				}
				
				$this->log(2, 'Характеристика ' . $filter_row['vendor_id'] . ' найдена на сайте, id = ' . $id_feature);
				
			}
			
			if(!$id_feature || !\Validate::isLoadedObject($feature)) {
				$fSearch = FeatureSearch::_instance();
				
				if(($f_id = $fSearch->searchFeature($filter_row['name'])) !== false) {
					$feature = new \Feature($f_id);
					
					$this->log(2, 'Характеристика ' . $filter_row['vendor_id'] . ' найдеа на сайте по названию');
					
					PriceParserIds::addSiteIdByVendorId($filter_row['vendor_id'], $feature->id, PriceParserIds::FILTER_TYPE_ID);
					
				} else {
					$feature = new \Feature();
					$this->log(2, 'Характеристика ' . $filter_row['vendor_id'] . ' отсутствует на сайте');
				}
			}
		} elseif(isset($filter_row['site_id']) && $filter_row['site_id']) {
			$feature = new \Feature($filter_row['site_id']);
			
			if(!\Validate::isLoadedObject($feature)) {
				throw new ExporterException('Не удалось загрузить характеристику по site_id. Строка: ' . print_r($filter_row, true));
			}
			
			$this->log(2, 'Характеристика по site_id ' . $filter_row['site_id'] . ' загружена');
		}
		
		if(!is_object($feature)) {
			throw new ExporterException('Не удалось инициализировать характеристику. Строка: ' . print_r($filter_row, true));
		}
		
		return $feature;
	}
	
	/**
	 * Инициализация модели значения характеристики из строки-значения х-ки выгрузки 
	 * @param Array $filter_value_row
	 * @return \FeatureValue
	 */
	function initFeatureValue($value_row) {
		$featureValue = null;
		
		if(isset($value_row['vendor_id'])) {
			$id_feature_value = PriceParserIds::getSiteIdByVendorId($value_row['vendor_id'], PriceParserIds::FILTER_VALUE_TYPE_ID);
			
			if($id_feature_value) {
				$featureValue = new \FeatureValue($id_feature_value);
				
				if(!\Validate::isLoadedObject($featureValue)) {
					$this->log(2, 'Не удалось загрузить значение характеристики. vendor_id: ' . $value_row['vendor_id']);
					PriceParserIds::deleteSiteId($id_feature_value, PriceParserIds::FILTER_VALUE_TYPE_ID);
				} else {
				
					$this->log(2, 'Значение характеристики ' . $value_row['vendor_id'] . ' загружено, id = ' . $id_feature_value);
				}
				
			}
			
			if(!$id_feature_value || !\Validate::isLoadedObject($featureValue)) {
				$fSearch = FeatureSearch::_instance();
				
				if(($fv_id = $fSearch->searchFeatureValue($value_row['name'])) !== false) {
					$this->log(2, 'Значение характеристики ' . $value_row['vendor_id'] . ' найдено на сайте по названию');
					$featureValue = new \FeatureValue($fv_id);
					PriceParserIds::addSiteIdByVendorId($value_row['vendor_id'], $featureValue->id, PriceParserIds::FILTER_VALUE_TYPE_ID);
				} else {
					$this->log(2, 'Значение характеристики ' . $value_row['vendor_id'] . ' отсутствует на сайте');
					$featureValue = new \FeatureValue();
				}
			}
		} elseif(isset($value_row['site_id']) && $value_row['site_id']) {
			$featureValue = new \FeatureValue($value_row['site_id']);
			
			if(!\Validate::isLoadedObject($featureValue)) {
				throw new ExporterException('Не удалось загрузить значение характеристики по site_id. Строка: ' . print_r($value_row, true));
			}
			
			$this->log(2, 'Значение арактеристики по site_id ' . $value_row['site_id'] . ' загружено');
		}
		
		if(!is_object($featureValue)) {
			throw new ExporterException('Не удалось инициализировать значение характеристики. Строка: ' . print_r($value_row, true));
		}
		
		return $featureValue;
	}
	
	public function getSummary() {
		return [
			'Фильтров создано: '					. $this->features_created,
			'Фильтров обновлено: '				. $this->features_updated,
			'Значений фильтров создано: '		. $this->features_values_created,
			'Значений фильтров обновлено: '		. $this->features_values_updated,
		];
	}
}
