<?php

namespace PriceParser\Export\SiteExporter;

use PriceParser\Core\Object\Object;
use PriceParser\Export\Processable;
use PriceParser\Core\Tools;

class ProcessorCatalogue extends Object implements Processable {
	
	/**
	 * id категории вендора, дочерние ветки которой будут выгружены
	 * @var int 
	 */
	var $root_vendor_category_id;
	
	public function process(array $data) {
		$this->log(1, 'Обработка категорий: сборка дерева');
		
		return [
			'catalogue' => Tools::genTree($data['catalogue'], $this->root_vendor_category_id)
		];
	}
	
}
