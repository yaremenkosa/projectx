<?php

namespace PriceParser\Export\SiteExporter;

use PriceParser\Core\Object\Object;
use PriceParser\Export\Processable;
use PriceParser\Core\Tools;

class ProcessorCatalogueTree extends Object implements Processable {
	
	/**
	 * id категории вендора, дочерние ветки которой будут выгружены
	 * @var int 
	 */
	var $root_vendor_category_id;
	
	public function process(array $data) {
		return Tools::genTree($data['catalogue'], $this->root_vendor_category_id);
	}
}
