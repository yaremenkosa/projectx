<?php

namespace PriceParser\Export\SiteExporter;

use PriceParser\Core\Object\Object;
use PriceParser\Core\PriceParserIds;
use PriceParser\Export\Exportable;

class ExporterStock extends Object implements Exportable {

	var $products_stock_updated = 0;
	
	const SHOP_ID = 1;

	public function export(Array $data) {
		$this->log(1, 'Загружаем остатки');

		$not_found_products = 0;

		if(!empty($data['stock'])) {
			foreach($data['stock'] as $row) {
				$id_product = PriceParserIds::getSiteIdByVendorId($row['product_id'], PriceParserIds::PRODUCT_ITEM_TYPE_ID);

				if ($id_product) {
					$this->saveProduct($id_product, $row);
				}

				$id_combination = PriceParserIds::getSiteIdByVendorId($row['product_id'], PriceParserIds::COMBINATION_TYPE_ID);

				if ($id_combination) {
					$this->saveCombination($id_combination, $row);
				}
			}
		}

		$this->log(1, 'Загрузка завершена. Обновлены остатки у ' . $this->products_stock_updated . ' товаров. Не найдено товаров: ' . $not_found_products);
	}

	/**
	 * Сохранение остатков по товару.
	 *
	 * @param int   $productId Идентификатор товара
	 * @param array $row       Строка данных для остатков из загруженных данных
	 */
	protected function saveProduct($productId, $row) {
		$this->log(2, 'Назначаем товару site id = ' . $productId . ' остатки: ' . $row['quantity']);

		\StockAvailable::setQuantity($productId, 0, $row['quantity'], static::SHOP_ID);

		$product = new \Product($productId);

		if(!\Validate::isLoadedObject($product)) {
			$this->log(1, 'Товар с vendor id = ' . $productId . ' не найден');

			return;
		}

		$product->quantity = $row['quantity'];
		try {
			$product->save();
		}
		catch (\Exception $e) {
			$this->log(1, 'Не удалось сохранить остатки по товару: ' . $e->getMessage());
		}

		$this->products_stock_updated++;
	}

	/**
	 * Сохранение остатков по комбинации.
	 *
	 * @param int   $combinationId Идентификатор комбинации
	 * @param array $row           Строка данных для остатков из загруженных данных
	 */
	protected function saveCombination($combinationId, $row) {
		$combination = new \Combination($combinationId);

		if(!\Validate::isLoadedObject($combination)) {
			$this->log(1, 'Комбинация с vendor id = ' . $combinationId . ' не найдена');

			return;
		}

		$this->log(1, 'Найдена комбинация по id = ' . $row['product_id']);
		$this->log(2, 'Назначаем комбинации site id = ' . $combination->id . ' остатки: ' . $row['quantity']);

		\StockAvailable::setQuantity($combination->id_product, $combination->id, $row['quantity'], static::SHOP_ID);
		$combination->quantity = $row['quantity'];

		try {
			$combination->save();
		}
		catch (\Exception $e) {
			$this->log(1, 'Не удалось сохранить остатки по комбинации: ' . $e->getMessage());
		}

		$this->products_stock_updated++;
	}

	public function getSummary() {
		return [
			'Остатки обновлены у товаров: ' . $this->products_stock_updated,
		];
	}

}
