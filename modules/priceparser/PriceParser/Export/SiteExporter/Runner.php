<?php

namespace PriceParser\Export\SiteExporter;

use PriceParser\Export\Runnable;
use PriceParser\Core\Exceptions\RunnerException;
use PriceParser\Core\Object\Object;
use PriceParser\Export\Exportable;
use PriceParser\Export\Processable;
use PriceParser\Core\Object\ObjectFactory;
use PriceParser\Core\Exceptions\ObjectFactoryException;
use PriceParser\Controller\Env;

class Runner extends Object implements Runnable {
	
	protected $exporter;
	protected $processor;
	protected $data;
	
	/**
	 * @var Exportable
	 */
	protected $_Exporter;
	
	/**
	 * @var Processable
	 */
	protected $_Processor;
	
	/**
	 * 
	 * @param array $config
	 * @throws RunnerException
	 */
	function run(Array $data) {
		//читаем конфиг
		
		try {
			$this->setExporter(ObjectFactory::getObject($this->exporter))
				 ->setProcessor(ObjectFactory::getObject($this->processor));
			
		} catch(ObjectFactoryException $ex) {
			throw new RunnerException('Error on preparing runner components: ' . $ex->getMessage(), $ex->getCode(), $ex);
		}
		
		$processed_data = $this->_Processor->process($data);
		
		$export_result = $this->_Exporter->export($processed_data);
		
		$export_summary = $this->_Exporter->getSummary();
		
		if(is_object(Env::$summary) && !empty($export_summary)) {
			foreach($export_summary as $s_item) {
				Env::$summary->log(0, $s_item);
			}
		}
		
		//что с ней делать?
		//$export_summary

        // отправим email уведомление
        $id_vendor = \PriceParser\Controller\Env::$config['id_vendor'];
        $em = \PriceParser\Controller\Env::$config['email'];
        $file_log = \PriceParser\Controller\Env::$config['runtime']['summary']['params']['log_file'];

        if(file_exists($file_log)) {
            $file_attachment = array();
            $file_attachment['content'] = file_get_contents($file_log);
            $file_attachment['name'] = basename($file_log);
            $file_attachment['mime'] = 'text/plain';
        } else {
            $file_attachment = null;
        }

        $emails = explode(',' , $em);

        foreach($emails as $email) {
            if(\Validate::isEmail($email))
                \Mail::Send(
                    (int)\Configuration::get('PS_LANG_DEFAULT'),
                    'alert',
                    'Отчет о синхронизации с ' . $id_vendor,
                    array(),
                    $email,
                    null,
                    null,
                    null,
                    $file_attachment,
                    null,
                    _PS_MODULE_DIR_.'priceparser/mails/',
                    true);
        }


		
		return $export_result;
	}
	
	protected function setExporter(Exportable $exporter) {
		$this->_Exporter = $exporter;
		
		return $this;
	}
	
	protected function setProcessor(Processable $processor) {
		$this->_Processor = $processor;
		
		return $this;
	}
}
