<?php
namespace PriceParser\Export\SiteExporter;

use Category;
use PriceParser\Core\Object\Object;
use PriceParser\Export\Exportable;
use PriceParser\Controller\Env;
use PriceParser\Export\Exceptions\ExporterException;
use PriceParser\Core\PriceParserIds;
use Validate;

class ExporterCatalogue extends Object implements Exportable {
	
	/**
	 * Id категории на сайте, в которую будут выгружены все категории вендора
	 * @var int 
	 */
	var $export_category_id;
	
	var $categories_created;
	var $categories_updated;
	
	var $id_lang;
	
	/**
	 *
	 * @var \PriceParser\Export\Components\Interfaces\LoaderCategoryInterface
	 */
	var $loaderCategory;
	
	/**
	 *
	 * @var \PriceParser\Export\Components\AbstractComponentsFactory
	 */
	var $componentsFactory;
	
	/**
	 *
	 * @var Array
	 */
	var $vendor_row;

	/**
	 * Корневая категория каталога для вендора
	 * @var Category
	 */
	protected $vendorRootCategory;
	
	function __construct(array $params = array()) {
		parent::__construct($params);
		
		$this->vendor_row = Env::getDb()->getRow('SELECT id_root_category,id_supplier'
				. ' FROM ' . _DB_PREFIX_ . 'price_parser_vendor'
				. ' WHERE id_vendor_str = "' . \pSQL(Env::$config['id_vendor']) . '"');
		
		if(!$this->vendor_row || empty($this->vendor_row)) {
			throw new ExporterException('Не удалось загрузить информацию о вендоре: ' . Env::$config['id_vendor']);
		}
		
		$this->export_category_id = $this->vendor_row['id_root_category'];
		
		$this->id_lang = \Configuration::get('PS_LANG_DEFAULT');
		
		$this->componentsFactory = \PriceParser\Export\Components\AbstractComponentsFactory::getFactory();
		
		$this->loaderCategory = $this->componentsFactory->createLoaderCategory([
			'id_lang' => $this->id_lang,
		]);

		$this->vendorRootCategory = new Category($this->export_category_id);

		if(!Validate::isLoadedObject($this->vendorRootCategory)) {
			throw new ExporterException('Указана некорретная корневая категория для экспорта каталога: ' . $this->export_category_id);
		}
	}
	
	public function export(array $data) {
		if(!$this->export_category_id) {
			throw new ExporterException('Не указана корневая категория для экспорта каталога');
		}
		
		if(!isset($data['catalogue']) || empty($data['catalogue'])) {
			throw new ExporterException('Категории отсутствуют');
		}
		
//		$vendor_cat_id = (int)Env::getDb()->getValue('SELECT c.id_category FROM ' . _DB_PREFIX_ . 'category AS c'
//				. ' LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category`'.\Shop::addSqlRestrictionOnLang('cl').')'
//				. ' WHERE id_parent = ' . (int)$exportCategory->id
//				. ' AND cl.name = "' . \pSQL(Env::$config['id_vendor']) . '"');
//		
//		if(!$vendor_cat_id) {
//			$vendorCategory = new \Category();
//			$vendorCategory->id_parent = $this->export_category_id;
//			$vendorCategory->name[$this->id_lang] = Env::$config['id_vendor'];
//			$vendorCategory->link_rewrite[$this->id_lang] = \Tools::str2url(Env::$config['id_vendor']);
//			
//			$save_result = $vendorCategory->save();
//			
//			if(!$save_result) {
//				throw new ExporterException('Ошибка при сохранении категории для вендора');
//			}
//			
//			$vendor_cat_id = $vendorCategory->id;
//		}
		
		$this->exportCategory($data['catalogue'], $this->vendorRootCategory->id);
		
		$this->log(1, 'Регенерация дерева каталога');
		Category::regenerateEntireNtree();
		
		return true;
	}
	
	protected function exportCategory($cat_rows, $parent_id) {
		if(!empty($cat_rows)) {
			
			foreach($cat_rows as $cat_row) {
				$this->exportCategoryItem($cat_row, $parent_id);
			}
		}
		
	}
	
	protected function exportCategoryItem($cat_row, $parent_id) {
		$id_category = PriceParserIds::getSiteIdByVendorId($cat_row['id'], PriceParserIds::CATEGORY_ITEM_TYPE_ID);

		$category = $this->loaderCategory->loadCategory($id_category, $cat_row, $parent_id);

		if (!$id_category) {
			$this->categories_created++;
			PriceParserIds::addSiteIdByVendorId($cat_row['id'], $category->id, PriceParserIds::CATEGORY_ITEM_TYPE_ID);
		}
		else {
			$this->categories_updated++;
		}
		$this->log(2, 'категория ' . $cat_row['id'] . ' успешно сохранена (id на сайте: ' . $category->id . ')');

		if (isset($cat_row['children']) && !empty($cat_row['children'])) {
			$this->exportCategory($cat_row['children'], $category->id);
		}

		return true;
	}

	public function getSummary() {
		return [
			'Категорий создано: ' . $this->categories_created,
			'Категорий обновлено: ' . $this->categories_updated,
		];
	}
	
}
