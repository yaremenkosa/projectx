<?php

namespace PriceParser\Export\SiteExporter;

use PriceParser\Core\Object\Object;
use PriceParser\Export\Exportable;
use PriceParser\Core\PriceParserIds;
use PriceParser\Export\Exceptions\ExporterDataException;

class ExporterPrices extends Object implements Exportable {

	var $prices_updated = 0;

	public function export(array $data) {

		if(!empty($data['prices'])) {
			foreach($data['prices'] as $row) {
				try {
					$result = $this->loadProductPrice($row);
				} catch (ExporterDataException $ex) {
					$this->log(2, 'Ошибка: ' . $ex->getMessage());

					$result = false;
				}

				if($result) {
					$this->prices_updated++;
				}
			}
		}
	}

	/**
	 *
	 * @param array $row
	 * @return boolean
	 * @throws ExporterDataException
	 */
	private function loadProductPrice($row) {
		$id_product = PriceParserIds::getSiteIdByVendorId($row['product_id'], PriceParserIds::PRODUCT_ITEM_TYPE_ID);

		if(!$id_product) {
			throw new ExporterDataException('Товар на сайте не найден по id вендора ' . $row['product_id']);
		}

		$product = new \Product($id_product);

		if(!\Validate::isLoadedObject($product)) {
			throw new ExporterDataException('Не получилось загрузить товар по site id = ' . $id_product);
		}

		if(isset($row['wholesale_price'])) {
			$product->wholesale_price			= $row['wholesale_price'];
		} else {
			$product->wholesale_price			= $row['price'];
		}

		$product->price							= $row['price'];

		$this->log(2, 'Для товара ' . $row['product_id'] . ' (site id = ' . $product->id . ') назначаем цену ' . $row['price']);

		return $product->save();
	}

	public function getSummary() {
		return [
			'Цен обновлено: ' . $this->prices_updated,
		];
	}

}
