<?php

namespace PriceParser\Export\SiteExporter;

use PriceParser\Core\Object\Object;
use PriceParser\Export\Processable;
use PriceParser\Core\PriceParserIds;
use PriceParser\Controller\Env;

class ProcessorProducts extends Object implements Processable {
	
	/**
	 *
	 * @var \PriceParser\Export\Components\AbstractComponentsFactory
	 */
	var $componentsFactory;
	
	/**
	 *
	 * @var \PriceParser\Export\Components\Interfaces\ProcessorImageInterface
	 */
	var $imageProcessor;
	
	function __construct(array $params = array()) {
		parent::__construct($params);
		
		$this->componentsFactory = \PriceParser\Export\Components\AbstractComponentsFactory::getFactory();
		
		$this->imageProcessor = $this->componentsFactory->createProcessorImage();
	}
	
	public function process(Array $data) {
		$this->log(1, 'Начинаем обработку данных');
		
		if(!empty($data)) {
			foreach($data['products'] as $i => &$row) {
				$this->processRow($row);
			}
		}
		
		$this->log(1, 'Обработка данных завершена');
		
		return $data;
	}
	
	protected function processRow(&$row) {
		$row['code'] = trim($row['code']);
		$this->imageProcessor->process($row);
	}
	
}
