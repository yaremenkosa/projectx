<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PriceParser\Export\SiteExporter\DirectGifts;

use PriceParser\Export\SiteExporter\ExporterCatalogue as OrigExporterCatalogue;

class ExporterCatalogue extends OrigExporterCatalogue {

	protected function exportCategoryItem($cat_row, $parent_id) {
		$found_categories = \Category::searchByName($this->id_lang, $cat_row['name'], true);
		if(!empty($found_categories)) {
			//если существуют категории с таким названием, то добавляем к имени название поставщика
			if($this->vendorRootCategory->alternate_name[$this->id_lang] !== '') {
				$cat_row['meta_title'] = $cat_row['name'] . ' от ' . $this->vendorRootCategory->alternate_name[$this->id_lang];
			}
			else {
				$cat_row['meta_title'] = $cat_row['name'] . ' от ' . $this->vendorRootCategory->name[$this->id_lang];
			}
		} else {
			$cat_row['meta_title'] = $cat_row['name'];
		}
		
		return parent::exportCategoryItem($cat_row, $parent_id);
	}
	
}
