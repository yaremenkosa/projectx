<?php

namespace PriceParser\Export\SiteExporter\DirectGifts;


use PriceParser\Controller\Env;
use PriceParser\Core\Object\Object;
use PriceParser\Core\PriceParserIds;
use PriceParser\Export\Exportable;
use Product;
use Validate;

class ExporterProductsChildrenCleaner extends Object implements Exportable {

	public function export(Array $data) {
		$count = 0;
		if (!empty($data['products'])) {

			foreach ($data['products'] as $row) {

				if (isset($row['children']) && !empty($row['children'])) {
					$this->log(2, 'Товар ' . $row['id'] . ' имеет дочерние товары');

					foreach ($row['children'] as $child) {
//						$this->log(2, 'Вычищаем товар vendor id = ' . $child['product_id']);
						//ищем товар
						$id_product = PriceParserIds::getSiteIdByVendorId($child['product_id'],
							PriceParserIds::PRODUCT_ITEM_TYPE_ID);

						if (!$id_product) {
							$this->log(2, 'Товар vendor id = ' . $child['product_id'] . ' отсутствует');
							continue;
						}

						//выбираем картинки
//						$images = Env::getDb()->executeS('
//							SELECT `id_image`
//							FROM `' . _DB_PREFIX_ . 'image`
//							WHERE `id_product` = ' . (int)$id_product
//						);

						//удаляем товар
						$product = new Product($id_product);

						if (Validate::isLoadedObject($product)) {
//							$product->delete();
							$count++;
						}
						else {
							$this->log(2, 'Товар на сайте остутствует');
						}
						continue;

						//удаляем товар из регистра
//						PriceParserIds::deleteSiteId($id_product, PriceParserIds::PRODUCT_ITEM_TYPE_ID);

						//удаляем картинки из регистра
						if (!empty($images)) {
							$this->log(2, 'Удаляем картинки из регистра: ' . count($images) . ' шт');
//							foreach ($images as $image_row) {
//								$this->log(2, 'Удаляем картинку site id = ' . $image_row['id_image']);
//								PriceParserIds::deleteSiteId($image_row['id_image'],
//									PriceParserIds::IMAGE_ITEM_TYPE_ID);
//							}
						}

//						$this->log(2, 'Товар site id = ' . $child['product_id'] . ' вычищен');
//						++$count
						$this->log(2, 'Обработано товаров: ' . ++$count);
						//						$child['product_id']
					}
				}
			}
		}

		$this->log(1, 'Всего товаров для удаления: ' . $count);
	}

	public function getSummary() {
	}

}