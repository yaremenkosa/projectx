<?php

namespace PriceParser\Export\SiteExporter\DirectGifts;

use PriceParser\Export\SiteExporter\ProcessorProducts as OrigProcessorProducts;
use PriceParser\Export\Processable;
use PriceParser\Controller\Env;

class ProcessorProducts extends OrigProcessorProducts implements Processable {
	
	var $id_supplier;
	var $id_manufacturer;
	
	function __construct(array $params = array()) {
		parent::__construct($params);
		
		$this->id_supplier = (int)Env::getDb()->getValue('SELECT id_supplier'
				. ' FROM ' . _DB_PREFIX_ . 'price_parser_vendor'
				. ' WHERE id_vendor_str = "' . \pSQL(Env::$config['id_vendor']) . '"');
	}
	
	protected function processRow(&$row) {
		parent::processRow($row);
		
		if($this->id_supplier) {
//			$this->log(2, 'Назначаем товару ' . $row['id'] . ' id_supplier = ' . $this->id_supplier);
			$row['id_supplier']	= $this->id_supplier;
			$row['code']		= $this->id_supplier . '_' . $row['code'];
		}
		
		if($this->id_manufacturer) {
			$row['id_manufacturer'] = $this->id_manufacturer;
		}
	}
	
}
