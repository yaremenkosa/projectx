<?php

namespace PriceParser\Export\SiteExporter;

use PriceParser\Core\Object\Object;
use PriceParser\Export\Processable;

/**
 * "Пустая" обработка товаров
 */
class ProcessorProductsNull extends Object implements Processable {

	public function process(Array $data) {
		return $data;
	}

	
}
