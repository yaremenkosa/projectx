<?php

namespace PriceParser\Export\SiteExporter\Derr;

use Attribute;
use AttributeCore;
use AttributeGroup;
use AttributeGroupCore;
use Configuration;
use FeatureCore;
use FeatureValue;
use FeatureValueCore;
use PriceParser\Core\Object\Object;
use PriceParser\Export\Exportable;
use PriceParser\Core\PriceParserIds;
use PriceParser\Export\Exceptions\ExporterException;
use PriceParser\Export\Exceptions\ExporterImageException;
use PriceParser\Export\Exceptions\ExporterDataException;

use \Image;
use PriceParser\Export\SiteExporter\Notifies\ClearOldNotify;
use Product;
use Feature;
use Manufacturer;
use \Tools;
use \Shop;
use \ImageManager;
use \ImageType;
use \Hook;
use \Tag;
use \SpecificPrice;


use PriceParser\Controller\Env;

class ExporterProducts extends Object implements Exportable {

    var $id_lang;

    var $categories_bind = [];

    var $download;

    var $connect_timeout = 5;
    var $timeout = 30;

    //привязывать товары к импортированным категориям вендора
    var $bind_to_imported_vendor_cats = false;

    var $max_image_filesize = 5000000;

    var $images_ext_accept = ['jpg', 'jpeg'];

    var $products_created = 0;
    var $products_updated = 0;

    var $images_created = 0;
    var $images_deleted = 0;

    var $features_added = 0;
    var $features_deleted = 0;

    var $attachment_created = 0;

    var $errors;

    /**
     *
     * @var \PriceParser\Export\Components\AbstractComponentsFactory
     */
    var $componentsFactory;

    /**
     *
     * @var \PriceParser\Export\Components\Interfaces\LoadInterface;
     */
    var $loadImageComponent;

    /**
     *
     * @var \PriceParser\Export\Components\Interfaces\LoadInterface;
     */
    var $loadFileComponent;

    /**
     *
     * @var \Category
     */
    var $vendorRootCategory;

    var $id_supplier;

    const SHOP_ID = 1;

    function __construct(array $params = array()) {
        parent::__construct($params);

        $this->componentsFactory = \PriceParser\Export\Components\AbstractComponentsFactory::getFactory();

        $this->id_lang = Configuration::get('PS_LANG_DEFAULT');

        $id_vendor = \PriceParser\Controller\Env::$config['id_vendor'];

        if(!$id_vendor) {
            throw new ExporterException('Отсутсвует id поставщика');
        }

        $result = Env::getDb()->executeS('SELECT id_vendor_category, id_category'
            . ' FROM ' . _DB_PREFIX_ . 'price_parser_vendor_categories_bind'
            . ' WHERE id_vendor = "' . pSQL($id_vendor) . '"');

        if(!empty($result)) {
            foreach($result as $row) {
                if(!isset($this->categories_bind[$row['id_vendor_category']])) {
                    $this->categories_bind[$row['id_vendor_category']] = [];
                }

                $this->categories_bind[$row['id_vendor_category']][] = $row['id_category'];
            }
        }

        try {
            $downloader = \PriceParser\Core\Object\ObjectFactory::getObject([
                'class'	 => '\PriceParser\Core\Downloader\\' . $this->download['type'] . 'Downloader',
                'params' => $this->download['downloader_params'],
            ]);
            $downloader->setAutoOpen(true);
        } catch (\PriceParser\Core\Exceptions\ObjectFactoryException $ex) {
            $this->log(1, 'Ошибка инициализации загрузчика: ' . $ex->getMessage());

            throw new \PriceParser\Export\Exceptions\ExporterImageDownloadException('Ошибка инициализации загрузчика: ' . $ex->getMessage(), $ex->getCode(), $ex);
        }

        $this->loadImageComponent = $this->componentsFactory->createLoaderImage([
            'downloader' => $downloader,
            'tmp_path'	 => $this->download['tmp_path']
        ]);

        $this->loadFileComponent = $this->componentsFactory->createLoaderFile([
            'downloader' => $downloader,
            'tmp_path'	 => $this->download['tmp_path']
        ]);

        $vendorRow = Env::getDb()->getRow('SELECT id_root_category, id_supplier'
            . ' FROM ' . _DB_PREFIX_ . 'price_parser_vendor'
            . ' WHERE id_vendor_str = "' . \pSQL(Env::$config['id_vendor']) . '"');

        if(!$vendorRow) {
            $vendorRow['id_root_category'] = Configuration::get('PS_HOME_CATEGORY');
            $vendorRow['id_supplier'] = null;
        }

        $this->vendorRootCategory = new \Category($vendorRow['id_root_category']);
        $this->id_supplier = $vendorRow['id_supplier'];

        if(!\Validate::isLoadedObject($this->vendorRootCategory)) {
            throw new ExporterException('Не удалось загрузить корневой каталог вендора: ' . Env::$config['id_vendor']);
        }
    }

    public function export(Array $data) {

        if(!$this->bind_to_imported_vendor_cats && empty($this->categories_bind)) {
            //если не нужно привязывать к вендорским импортированным категориям и привязки категорий отсутствуют, то импортировать нечего
            $this->log('1', 'Привязки категорий отсутствуют.');
            return false;
        }

        if(!empty($data['products'])) {

            $loaderProduct = $this->componentsFactory->createLoaderProduct([
                'id_lang' => $this->id_lang,
            ]);

            $this->log(9, 'Начало обработки товаров');
            $this->log(9, 'Всего товаров к обработке: ' . count($data['products']));

            $count_products = count($data['products']);
            $i = 0;

            foreach($data['products'] as $row) {
                $i++;

                $this->log(9, '==========================' . $i . ' из ' . $count_products . '==========================');
                $this->log(9, 'Начинаем обрабатывать товар "' . $row['name'] . '" (IDp: ' . $row['id'] . ')');

                $this->log(2, '--------------------------' . $i . ' из ' . $count_products . '--------------------------');
                $this->log(2, 'Товар ' . $row['id']);


                //если задана привязка, а данный товар не входит в категории привязывания, то игнорируем его

                if(!$this->bind_to_imported_vendor_cats) {
                    //если не нужно привязвывать товары ко всем импортированным категориям, то фильтруем непривязанные товары
                    if(!isset($row['category_id']) && !isset($row['categories_ids'])) {
                        //если никакие привязки к категориям не указаны, то пропускаем
                        $this->log(2, 'Отсутствует привязка к категориями - пропускаем');
                        continue;
                    }

                    if(isset($row['category_id']) && $row['category_id'] && !isset($this->categories_bind[$row['category_id']])) {
                        $this->log(2, 'Привязываемая категория отсутствует среди категорий связывания');
                        continue;
                    }

                    //грёбаный php 5.4-

                    if(isset($row['categories_ids'])) {
                        $intersect_res = array_intersect($row['categories_ids'], array_keys($this->categories_bind));
                        if(empty($intersect_res)) {
                            $this->log(2, 'Привязываемые категории отсутствуют среди категорий связывания');
                            continue;
                        }
                    }
                }

                Env::getDb()->execute('START TRANSACTION');

                try {
                    $id_product = PriceParserIds::getSiteIdByVendorId($row['id'], PriceParserIds::PRODUCT_ITEM_TYPE_ID);


                    if($id_product) {
                        $this->log(2, 'по товару ' . $row['id'] . ' найден товар на сайте: ' . $id_product);
                        $this->log(9, 'Товар найден на сайте ID:' . $id_product);
                        $product = new Product($id_product);

                        if(!\Validate::isLoadedObject($product)) {
                            //если товар на сайте не существует, то удаляем связку этого вендорского id с несуществующим товаром
                            $this->log(2, 'товар на сайте: ' . $id_product . ' отсутствует, удаляем привязку вендора');
                            $_res = PriceParserIds::deleteByVendorId($row['id'], PriceParserIds::PRODUCT_ITEM_TYPE_ID);
                            $this->log(2, '...' . ($_res ? 'успешно' : 'ошибка'));

                            if(!$_res) {
                                throw new ExporterException('Не удалось удалить неактуальную связку товара для вендорского товара id = ' . $row['id']);
                            }

                            $id_product = null;
                            $product->id = null;
                        }
                    } else {
                        $this->log(2, 'по товару ' . $row['id'] . ' товар на сайте отсутствует');
                        $this->log(9, 'Товар не найден на сайте, создаем новый.');
                        $product = new Product();
                    }


                    try {
                        $this->log(9, 'Что обновили у товара:');
                        $load_result = $loaderProduct->load($product, $row);

                        $update_product = PriceParserIds::getUpdateDesc($product->id, 1);

                        // сохраним xml id
                        //$this->saveXMLID($product, $row);

                        // обновим остатки
                        $this->loadStock($product, $row);

                        $this->loadImages($product, $row);


                        if($update_product == 0)
                            $this->loadFeatures($product->id, $row);

                        $this->loadBrand($product, $row);

                        // добавим видео
                        //$this->loadVideo($product, $row);

                        // добавим файлы
                        $this->attachFiles($product, $row);

                        // Добавим теги
                        //$this->loadTags($product, $row);

                        // добавим спец цену
                        $this->addSpecificPrice($product, $row);

                        if (isset($row['children']) && !empty($row['children'])) {
                            $this->parseChildren($product, $row, $row['children']);
                        }

                    } catch(ExporterDataException $ex) {
                        $this->log(2, 'Ошибка при загрузке товара (vendor id = ' . $row['id'] . '): ' . $ex->getMessage());

                        throw new ExporterException('Ошибка при загрузке товара (vendor id = ' . $row['id'] . '): ' . $ex->getMessage());
                    }

                    if($load_result) {
                        if(!$id_product) {
                            $this->products_created++;
                            PriceParserIds::addSiteIdByVendorId($row['id'], $product->id, PriceParserIds::PRODUCT_ITEM_TYPE_ID);
                        } else {
                            $this->products_updated++;
                        }
                        $this->log(2, 'товар ' . $row['id'] . ' успешно сохранён (id на сайте: ' . $product->id . ')');
                    } else {
                        $this->log(2, 'товар ' . $row['id'] . ' ОШИБКА при сохранении');
                    }

                    /*Env::getDb()->delete('price_parser_product_log',
                        'id_vendor = "' . pSQL(\PriceParser\Controller\Env::$config['id_vendor']) . '"'
                        . ' AND id_product = ' . (int) $product->id);

                    Env::getDb()->insert('price_parser_product_log', [
                        'id_vendor' => \PriceParser\Controller\Env::$config['id_vendor'],
                        'id_product' => $product->id,
                        'data' => json_encode($row, JSON_UNESCAPED_UNICODE),
                    ]);*/

					if(!$id_product) {
                    //если товар новый, то обрабатываем его категории
                    $this->loadCategory($product, $row);
					}


                } catch (\Exception $ex) {
                    $this->log(1, 'Исключение: ' . $ex->getMessage());
                    Env::getDb()->execute('ROLLBACK');

                    throw $ex;
                }

                Env::getDb()->execute('COMMIT');

            }
        }

        //$this->clearOldProducts();
        $this->removeGarbage();

        $this->log(1, 'Загрузка данных завершена');
        $this->log(1, 'Товаров создано: ' . $this->products_created);
        $this->log(1, 'Товаров обновлено: ' . $this->products_updated);
        $this->log(9, '=================================================================================');
        $this->log(9, 'Загрузка данных завершена');
    }



    protected function loadStock(Product $product, $row) {
        $old_stock = \StockAvailable::getQuantityAvailableByProduct($product->id, null, static::SHOP_ID);

        \StockAvailable::setQuantity($product->id, 0, $row['quantity'], static::SHOP_ID);

        if($product->id != null) {
            if($old_stock != $row['quantity'])
                $this->log(9, '- обновили кол-во товара (' . $old_stock . ' -> ' . $row['quantity'] . ')');
        } else {
            $this->log(9, '- установили кол-во товара (' . $row['quantity'] . ')');
        }

        $product->quantity = $row['quantity'];
        $result = $product->save();
        if($result)
            $this->log(2, 'Кол-во у товара ' . $product->id . ', изменено на: ' . $row['quantity']);
        else
            $this->log(2, 'Кол-во у товара ' . $product->id . ', не обновлено, произошла ошибка.');
    }

    protected function loadImages(Product $product, $row) {
        $this->log(2, 'Загружаем изображения для товара site_id = ' . $product->id . ', количество: ' . count($row['images']));
        $this->log(9, 'Загружаем изображения для товара, количество: ' . count($row['images']));
        var_dump($row['images']);
        if(!empty($row['images'])) {
            foreach($row['images'] as $img_row) {
                try {
                    $this->loadImage($product, $img_row);
                } catch (ExporterDataException $ex) {
                    $this->errors = 'Ошибка загрузки изображения: ' . $img_row['url'] . ' для товара ' . $product->id . ': ' . $ex->getMessage();
                    continue;
                }

            }
        }

        $this->log(9, 'Новых изображений добавлено: ' . $this->images_created);
    }

    /**
     *
     * @param array $img_row
     * @return boolean
     * @throws ExporterDataException
     */
    protected function loadImage(Product $product, Array $img_row) {
        if($img_row['is_new']) {
            $this->log(2, 'Новое изображение ' . $img_row['url']);
            $this->log(9, 'Новое изображение ' . $img_row['url']);
            if(!$this->checkImage($img_row['url'])) {
                $this->log(2, 'изображение ' . $img_row['url'] . ' имеет неверное имя или расширение, пропускаем');

                throw new ExporterDataException('Ошибка загрузки изображения: изображение ' . $img_row['url'] . ' имеет неверное имя или расширение');
            }

            try {

                $image = $this->loadImageComponent->load($product, $img_row);

                $this->loadImageComponent->assignParams($image, $img_row);

            } catch(ExporterImageException $ex) {
                $this->log(2, 'Не удалось обработать изображение ' . $img_row['url'] . '. Ошибка: ' . $ex->getMessage() . ', код: ' . $ex->getCode());

                throw new ExporterDataException('Ошибка обработки изображения: ' . $ex->getMessage(), $ex->getCode(), $ex);

            } catch(ExporterDataException $ex) {
                $this->log(2, 'Не удалось загрузить изображение ' . $img_row['url'] . '. Ошибка: ' . $ex->getMessage() . ', код: ' . $ex->getCode());

                throw new ExporterDataException('Ошибка загрузки изображения: ' . $ex->getMessage(), $ex->getCode(), $ex);
            } catch(ExporterException $ex) {
                $this->log(2, 'Не удалось загрузить изображение ' . $img_row['url'] . '. Ошибка: ' . $ex->getMessage() . ', код: ' . $ex->getCode());

                throw new ExporterDataException('Ошибка загрузки изображения: ' . $ex->getMessage(), $ex->getCode(), $ex);
            }


            if($image) {
                $this->log(2, 'изображение ' . $img_row['url'] . ' добавляем к товару ' . $product->id);
                PriceParserIds::addSiteIdByVendorId($img_row['url'], $image->id, PriceParserIds::IMAGE_ITEM_TYPE_ID);
                $this->images_created++;

            }
        } elseif($img_row['is_changed']) {
            $this->log(2, 'Изменённое изображение ' . $img_row['url']);
            $image = new Image($img_row['id_site']);

            $this->loadImageComponent->assignParams($image, $img_row);

            $image->save();

        } elseif($img_row['is_deleted']) {
            /*$this->log(2, 'изображение ' . $img_row['id_site'] . ' было удалено в выгрузке, удаляем из ' . $product->id);
            $image = new Image($img_row['id_site']);

            $this->loadImageComponent->deleteImage($image);

            PriceParserIds::deleteSiteId($img_row['id_site'], PriceParserIds::IMAGE_ITEM_TYPE_ID);

            $this->images_deleted++;*/
        }

        return true;

    }

    protected function checkImage($image_path) {
        $info = pathinfo($image_path);
        $ext = explode('?', $info['extension']);
        if(isset($info['extension'])) {
            return in_array(mb_strtolower($ext[0]), $this->images_ext_accept);
        }

        return false;
    }

    /**
     * Переделанный метод \Product::getFeaturesStatic() с добавленной фильтрацией только по импортированным характеристикам и значеням
     * @param int $id_product
     * @return array
     */
    private function getProductFeatures($id_product) {
        return Env::getDb()->executeS('SELECT fp.id_feature, fp.id_product, fp.id_feature_value, custom
				FROM `'._DB_PREFIX_.'feature_product` fp
INNER JOIN '._DB_PREFIX_.'price_parser_ids AS f_ids on f_ids.id_item_type = 4 AND f_ids.id_vendor = "' . pSQL(Env::$config['id_vendor']) . '" AND f_ids.id_item_site = fp.id_feature
				LEFT JOIN `'._DB_PREFIX_.'feature_value` fv ON (fp.id_feature_value = fv.id_feature_value)
INNER JOIN '._DB_PREFIX_.'price_parser_ids AS fv_ids ON fv_ids.id_item_type = 5 AND fv_ids.id_vendor = "' . pSQL(Env::$config['id_vendor']) . '" AND fv_ids.id_item_site = fv.id_feature_value
				WHERE `id_product` = ' . (int)$id_product);
    }

    private function loadBrand(Product $product, $row) {
        if(!isset($row['brand_id'])) return;

        $this->log(2, 'Определяем бренд для товара ' . $product->id);

        $id_brand = PriceParserIds::getSiteIdByVendorId($row['brand_id'], PriceParserIds::BRAND_VALUE_ID);
        if(!$id_brand) { // создаем новый
            $id_brand = Manufacturer::getIdByName($row['brand_id']);
            if(!$id_brand) {
                $manufacturer = new Manufacturer();
                $manufacturer->name = $row['brand_id'];
                $manufacturer->active = 1;
                $manufacturer->add();
                $id_brand = $manufacturer->id;
                PriceParserIds::addSiteIdByVendorId($row['brand_id'], $id_brand, PriceParserIds::BRAND_VALUE_ID);
            } else { // с похожим названием есть на сайте, создадим связь
                PriceParserIds::addSiteIdByVendorId($row['brand_id'], $id_brand, PriceParserIds::BRAND_VALUE_ID);
            }
        }
        $this->log(2, 'Бренд ' . $row['brand_id'] . ' успешно сохранен. [SiteID: ' . $id_brand . ']');
        $product->id_manufacturer = $id_brand;
        $product->save();
    }

    private function loadFeatures($id_product, $product_row) {
        $this->log(2, 'Загружаем фильтра по товару ' . $id_product);

        //получаем имеющиеся характеристики и значения
        $exists_features = $this->getProductFeatures($id_product);

        $exists_ids = [];

        if(!empty($exists_features)) {
            foreach($exists_features as $row) {
                if(!isset($exists_ids[$row['id_feature']])) {
                    $exists_ids[$row['id_feature']] = [];
                }

                $exists_ids[$row['id_feature']][] = $row['id_feature_value'];
            }
        }

        //получаем id загруженных характеристик и значений по справочникам сайта
        $loaded_ids = [];

        if(!empty($product_row['features'])) {
            foreach($product_row['features'] as $filter_row) {
                if(isset($filter_row['vendor_id'])) {
                    $id_feature = PriceParserIds::getSiteIdByVendorId($filter_row['vendor_id'], PriceParserIds::FILTER_TYPE_ID);
                } elseif(isset($filter_row['site_id'])) {
                    $id_feature = $filter_row['site_id'];
                }

                if(isset($filter_row['vendor_value_id'])) {
                    $id_feature_value = PriceParserIds::getSiteIdByVendorId($filter_row['vendor_value_id'], PriceParserIds::FILTER_VALUE_TYPE_ID);
                } elseif(isset($filter_row['site_value_id'])) {
                    $id_feature_value = $filter_row['site_value_id'];
                }

                // если не найдены характеристики - добавляем
                if(!$id_feature) {
                    $feature = new Feature();
                    $feature->name[$this->id_lang] = $filter_row['vendor_id'];
                    $feature->save();
                    PriceParserIds::addSiteIdByVendorId($filter_row['vendor_id'], $feature->id, PriceParserIds::FILTER_TYPE_ID);
                    $id_feature = $feature->id;
                }
                if(!$id_feature_value) {
                    $feature_value = new FeatureValue();
                    $feature_value->id_feature = $id_feature;
                    $feature_value->value[$this->id_lang] = $filter_row['vendor_value_id'];
                    $feature_value->save();
                    PriceParserIds::addSiteIdByVendorId($filter_row['vendor_value_id'], $feature_value->id, PriceParserIds::FILTER_VALUE_TYPE_ID);
                    $id_feature_value = $feature_value->id;
                }

                if(!isset($loaded_ids[$id_feature])) {
                    $loaded_ids[$id_feature] = [];
                }

                $loaded_ids[$id_feature][$id_feature_value] = true;
            }
        }

        $this->log(2, 'Загружено характеристик: ' . count($loaded_ids));

        //перебираем загруженные характеристики и значения, чтобы определить, какие нужно добавить

        $to_add_items = [];
        if(!empty($loaded_ids)) {
            foreach($loaded_ids as $loaded_id => $loaded_vals) {
                foreach($loaded_vals as $loaded_val_id => $_val) {
                    //пока на сайте не работают составные (мульти-) характеристики (косяк престы), то берём только первое значение, а остальные игнорим
                    if(!isset($exists_ids[$loaded_id]) || !in_array($loaded_val_id, $exists_ids[$loaded_id])) {
                        $to_add_items[] = [
                            'id' => $loaded_id,
                            'val_id' => $loaded_val_id,
                        ];
                    }

                    break;

                    //но когда престу пропатчим, то должно быть так:
                    /*if(!isset($exists_ids[$loaded_id]) || !isset($exists_ids[$loaded_id][$loaded_val_id])) {
                        $to_add_items[] = [
                            'id' => $loaded_id,
                            'val_id' => $loaded_val_id,
                        ];
                    }*/
                }
            }
        }

        //перебираем имеющиеся характеристики и значения, чтобы определить, какие нужно удалить

        $to_del_items = [];
        if(!empty($exists_ids)) {
            foreach($exists_ids as $exists_id => $exists_vals) {
                foreach($exists_vals as $exists_val_id) {
                    if(!isset($loaded_ids[$exists_id]) || !isset($loaded_ids[$exists_id][$exists_val_id])) {
                        $to_del_items[] = [
                            'id' => $exists_id,
                            'val_id' => $exists_val_id,
                        ];
                    }
                }
            }
        }

        $this->log(2, 'Для добавления кол-во значений характеристик: ' . count($to_add_items));
        if(!empty($to_add_items)) {

            foreach($to_add_items as $row) {
                $result = Product::addFeatureProductImport($id_product, $row['id'], $row['val_id']);

                $this->log(2, 'Характеристика ' . $row['id'] . ' ' . $row['val_id'] . ($result ? ' добавлена' : ' НЕ добавлена'));

                $this->features_added++;
            }
        }

        $this->log(2, 'Для удаления кол-во значений характеристик: ' . count($to_del_items));
        if(!empty($to_del_items)) {
            foreach($to_del_items as $row) {
                Env::getDb()->execute('DELETE FROM ' . _DB_PREFIX_ . 'feature_product WHERE id_feature = ' . (int)$row['id']
                    . ' AND id_feature_value = ' . (int)$row['val_id']
                    . ' AND id_product = ' . (int)$id_product);
                $this->features_deleted++;
            }
        }

        $this->log(2, 'Характеристики загружены');
    }

    /**
     * Привязка товара к категориям
     * @param Product $product
     * @param type $row
     * @throws ExporterException
     */
    private function loadCategory(Product $product, $row) {
        //массив всех вендорских категорий, к которым привязан товар
        /*$categories_vendor_ids = [];

        if(isset($row['category_id'])) {
            $categories_vendor_ids[] = $row['category_id'];
        } elseif(isset($row['categories_ids'])) {
            $categories_vendor_ids = $row['categories_ids'];
        }

        //списик категорий сайта, к которым нужно привязать товар
        $categories_ids = [];

        if(!empty($categories_vendor_ids)) {
            foreach($categories_vendor_ids as $cat_v_id) {
                if(isset($this->categories_bind[$cat_v_id]) && !empty($this->categories_bind[$cat_v_id])) {
                    $categories_ids = array_merge($categories_ids, $this->categories_bind[$cat_v_id]);
                }

                if($this->bind_to_imported_vendor_cats) {
                    //если нужно привязать к импортированным категориям вендора, то добавляем их id на сайте
                    $_cat_id = PriceParserIds::getSiteIdByVendorId($cat_v_id, PriceParserIds::CATEGORY_ITEM_TYPE_ID);
                    if($_cat_id) {
                        $categories_ids[] = $_cat_id;
                    }
                }
            }
        }*/

        // pw-pl уже в $row['categories_ids'] передаем id категорий сайта, поэтому...
        $categories_ids = $row['categories_ids'];

        $this->log(2, 'У товара (id сайта: ' . $product->id . ') ' . count($categories_ids) . ' категорий (' . implode(', ' , $categories_ids) . '), обновляем');

        $this->log(9, 'Товар привязан с категориям: (' . implode(', ' , $categories_ids) . ')');

//		if(!empty($categories_ids)) {
//		}

        if(!empty($categories_ids)) {
            if(!$product->id_category_default) {
                //указываем дефолтную категорию, только если она либо пустая, либо 0 (не указана ранее)
                $product->id_category_default = $categories_ids[0];

                $save_result = $product->save();

                if(!$save_result) {
                    throw new ExporterException('Ошибка при сохранении товара. Результат: false');
                }

                $this->log(2, 'У товара (id сайта: ' . $product->id . ') ' . ' категория по умолчанию: ' . $product->id_category_default . ' ' . ($save_result ? 'сохранена' : 'не сохранена'));
                $this->log(9, 'По умолчанию установим: ' . $product->id_category_default);
            }

            $categories_ids = array_unique($categories_ids);
            try {

//				$this->vendorRootCategory
                //выбираем все родительские категории до вендорской
                /*$categories_result = Env::getDb()->ExecuteS('select c.id_category
					FROM ps_category AS c
					INNER JOIN (
					select c_.nleft, c_.nright
					from ps_category AS c_
					WHERE c_.id_category IN (' . implode(',', $categories_ids) . ')
					) AS p_inf ON c.nleft <= p_inf.nleft AND c.nright >= p_inf.nright AND c.level_depth > ' . $this->vendorRootCategory->level_depth);

                if(!empty($categories_result)) {
                    $categories_ids = [];
                    foreach($categories_result as $row) {
                        $categories_ids[] = (int)$row['id_category'];
                    }
                }*/

                $categories_ids = array_unique($categories_ids);

                $this->log(2, 'Все категории, к которым привязывается товар: ' . implode(',', $categories_ids));

                /*foreach($categories_ids as $id_category) {
                    $return = \Db::getInstance()->delete('category_product', 'id_product = '.(int)$product->id.' AND id_category = '.(int)$id_category);
                }

                foreach ($categories_ids as $new_id_categ) {

                    \Db::getInstance()->insert('category_product', [
                        'id_category'	 => (int) $new_id_categ,
                        'id_product'	 => (int) $product->id,
                    ]);
                }*/

                $update_result = $product->updateCategories($categories_ids);

                $this->log(2, $update_result ? 'Категории успешно обновлены' : 'Категории не обновлены - ошибка');
            } catch(\PrestaShopException $ex) {
                $this->log(1, 'Exception: ' . $ex->getMessage());
                throw new ExporterException('Ошибка при сохранении товара: ' . $ex->getMessage(), 0, $ex);
            }
        }

        //можно убрать (???), т.к. updateCategories
//		if(!empty($categories_ids)) {
//			foreach($categories_ids as $cat_id) {
//				$product->addToCategories($cat_id);
//			}
//		}
    }

    public function getSummary() {
        return [
            'Товаров создано: ' . $this->products_created,
            'Товаров обновлено: ' . $this->products_updated,
            'Изображений добавлено: ' . $this->images_created,
            'Изображений удалено: ' . $this->images_deleted,
            'Характеристик добавлено: ' . $this->features_added,
            'Характеристик удалено: ' . $this->features_deleted,
            'Ошибки: ' . $this->errors,
        ];
    }

    protected function clearOldProducts() {
        $id_vendor = \PriceParser\Controller\Env::$config['id_vendor'];

        if(!$id_vendor) {
            return false;
        }

        $days = (int) Configuration::get('PRICE_PARSER_OLD_PRODUCTS_DAYS_EXPIRE');

        $emails = explode(',', Configuration::get('PRICE_PARSER_OLD_PRODUCTS_NOTIFY_EMAILS'));

        if(!$days || empty($emails)) {
            return false;
        }

        $result = Env::getDb()->executeS('SELECT p.id_product, reference FROM ps_product p'
            . ' INNER JOIN ps_price_parser_ids ids ON ids.id_item_site = p.id_product AND ids.id_item_type=1 AND id_vendor = "' . pSQL($id_vendor) . '"'
            . ' WHERE date_upd < \'' . date('Y-m-d', strtotime('-' . $days . ' days')) . '\'');

        if(!empty($result)) {

            $n = new ClearOldNotify();
            $n->sendNotify($result, $emails);

            foreach($result as $row) {
                $product = new \Product($row['id_product']);
                $product->delete();
            }
        }
    }

    protected function parseChildren(Product $product, Array $product_row, Array $children) {

        $this->log(2, 'Обрабатываем комбинации для товара site id = ' . $product->id);
        $this->log(2, 'Дочерних товаров: ' . count($children));
        foreach($children as $child) {
            $this->log(2, 'Дочерний товар vendor id = ' . $child['product_id']);

            //если дочерний товар не является основным товаром
            if ($child['product_id'] !== $product_row['id']) {
                //сначала ищем обычный товар (если он был ранее создан вместо комбинации)
                $productId = PriceParserIds::getSiteIdByVendorId($child['product_id'],
                    PriceParserIds::PRODUCT_ITEM_TYPE_ID);

                if ($productId) {
                    $productComb = new \Product($productId);

                    if (\Validate::isLoadedObject($productComb)) {
                        $this->log(2, 'Удаляем товар из комбинации: ' . $productId);

                        $productComb->delete();
                    }

                    PriceParserIds::deleteSiteId($productId, PriceParserIds::PRODUCT_ITEM_TYPE_ID);
                }
            }

            $id_combination = PriceParserIds::getSiteIdByVendorId($child['product_id'], PriceParserIds::COMBINATION_TYPE_ID);

            if($id_combination) {
                $combination = new \Combination($id_combination);

                if(!\Validate::isLoadedObject($combination)) {
                    $id_combination = null;
                }
            }

            if (!$id_combination) {
                $combination = new \Combination();
                $combination->id_product = $product->id;
            }

            $combination->reference = /*$this->id_supplier . '_' . */$child['code'];
            //приходится использовать number_format, чтобы отрезать 2 символа после запятой

            $combination->price = (float) number_format($child['price'] - $product->price, 2, '.', '');
            $this->log(2, 'Цена: ' . $combination->price);

            $combination->save();
            if(!$id_combination) {
                PriceParserIds::addSiteIdByVendorId($child['product_id'], $combination->id, PriceParserIds::COMBINATION_TYPE_ID);
            }

            foreach($product_row['comb_attributes'] as $product_comb_row) {
                foreach($child['comb_attributes'] as $child_comb_row) {
                    if ($product_comb_row['name'] == $child_comb_row['name'])  {
                        $this->log(2, 'Характеристика: ' . $child_comb_row['name'] . ', значение товара: ' . $product_comb_row['value'] . ', значение дочернего товара: ' . $child_comb_row['value']);
//						if($product_comb_row['value'] == $child_comb_row['value']) {
//							$this->log(2, 'Значения одинаковые, пропускаем');
//							continue;
//						}

                        $id_attribute = PriceParserIds::getSiteIdByVendorId($child_comb_row['value'], PriceParserIds::ATTRIBUTE_TYPE_ID);

                        if (!$child_comb_row['value']) {
                            $this->log(2, 'Значение аттрибута ' . $product_comb_row['name'] . ' пустое - пропускаем');

                            if ($id_attribute) {
                                //удаляем аттрибут из связки комбинации, если он уже был загружен
                                Env::getDb()->execute('DELETE FROM ' . _DB_PREFIX_ . 'product_attribute_combination'
                                    . ' where id_attribute = ' . (int)$id_attribute
                                    . ' and id_product_attribute = ' . (int)$combination->id);
                            }

                            continue;
                        }

                        $this->log(2, 'Значения отличаются, обрабатываем');
                        //одинаковая характеристика, но разные значения
                        $id_attribute_group = PriceParserIds::getSiteIdByVendorId($child_comb_row['name'], PriceParserIds::ATTRIBUTE_GROUP_TYPE_ID);

                        if (!$id_attribute_group) {
                            /* @var $attributeGroup AttributeGroupCore */
                            $attributeGroup = new AttributeGroup();
                            /* @var $feature FeatureCore */

//							$id_feature = PriceParserIds::getSiteIdByVendorId($child_feature_row['vendor_id'], PriceParserIds::FILTER_TYPE_ID);
//
//							$feature = new \Feature($id_feature);
                            $attributeGroup->name[$this->id_lang] = $child_comb_row['name'];
                            $attributeGroup->public_name[$this->id_lang] = $child_comb_row['name'];
                            $attributeGroup->group_type = 'select';
                            $attributeGroup->save();
                            $id_attribute_group = $attributeGroup->id;
                            PriceParserIds::addSiteIdByVendorId($child_comb_row['name'], $id_attribute_group, PriceParserIds::ATTRIBUTE_GROUP_TYPE_ID);
                        }

                        if(!$id_attribute) {
                            /* @var $attribute AttributeCore */
                            $attribute = new Attribute();
//							$id_feature_value = PriceParserIds::getSiteIdByVendorId($child_feature_row['vendor_value_id'], PriceParserIds::FILTER_VALUE_TYPE_ID);

                            /* @var $featureValue FeatureValueCore */
//							$featureValue = new FeatureValue($id_feature_value);
                            $attribute->name[$this->id_lang] = $child_comb_row['value'];
                            $attribute->id_attribute_group = $id_attribute_group;
                            $attribute->save();
                            $id_attribute = $attribute->id;
                            PriceParserIds::addSiteIdByVendorId($child_comb_row['value'], $id_attribute, PriceParserIds::ATTRIBUTE_TYPE_ID);
                        }

                        Env::getDb()->execute('INSERT IGNORE INTO ' . _DB_PREFIX_ . 'product_attribute_combination (id_attribute, id_product_attribute) VALUES '
                            . ' (' . (int)$id_attribute . ', ' . (int) $combination->id . ')');
                    }
                }
            }

            if(!empty($child['images'])) {
                foreach($child['images'] as $imageUrl) {

                    $image_id = null;
                    //перебираем изображения товара, чтобы найти его ID
                    foreach ($product_row['images'] as $productImgRow) {
                        if ($productImgRow['url'] === $imageUrl) {
                            $image_id = PriceParserIds::getSiteIdByVendorId($productImgRow['url'], PriceParserIds::IMAGE_ITEM_TYPE_ID);
                            if($productImgRow['id_site']) $image_id = $productImgRow['id_site'];
                            break;
                        }
                    }



                    if(!$image_id) {
                        $this->log(2, 'Не удалось привзяать картинку ' . $imageUrl . ' к комбинации: картинка на сайте не найдена');
                        continue;
                    }
                    Env::getDb()->execute('INSERT IGNORE INTO ' . _DB_PREFIX_ . 'product_attribute_image (id_product_attribute, id_image) VALUES '
                        . ' (' . (int) $combination->id . ',' . $image_id . ')');
                }
            }
        }
    }

    /**
     * Удаление мусора (id сущностей, которые есть в индексе, но в сущностях).
     */
    protected function removeGarbage() {
        $this->log(2, 'Очищаем мусорные записи по изображениям');
        Env::getDb()->execute('delete ids
from ' . _DB_PREFIX_ . 'price_parser_ids ids
left join ' . _DB_PREFIX_ . 'image i on i.id_image = ids.id_item_site 
where ids.id_item_type = 2 and i.id_image IS NULL and id_vendor = \'' . PriceParserIds::$vendor_id . '\'');
    }


    // метод добавления видео к товару, работает с модулями shopimultitabs и pwsimaland
    private function loadVideo(Product $product, $row) {
        // если ссылки на видео нет, пропускаем
        if(!isset($row['video_url']) || $row['video_url'] == false || $row['video_url'] == '')
            return;

        // если нужный нам модуль выключен, пропускаем
        if(\Module::isEnabled('shopimultitabs') == false || \Module::isEnabled('pwsimaland') == false)
            return;

        // если в настройках pwsimaland не указан tab видео, пропускаем
        if(!Configuration::get('PWSIMALAND_TABVIDEO'))
            return;

        $this->log(9, 'Найдена ссылка на видео, ' . $row['video_url']);

        $sql = "SELECT COUNT(*) FROM `" . _DB_PREFIX_ . "shopimultitab` WHERE `id_shopimultitab` = '" . Configuration::get('PWSIMALAND_TABVIDEO') . "' AND `active` = '1'";
        $n = Env::getDb()->getValue($sql);
        if($n <= 0) return; // если нет такого таба, пропускаем

        $sql = "SELECT `id_shopimultitab_content` FROM `" . _DB_PREFIX_ . "shopimultitab_content` WHERE `id_product` = '" . $product->id . "' AND `id_shopimultitab` = '" . Configuration::get('PWSIMALAND_TABVIDEO') . "'";
        $id_content = Env::getDb()->getValue($sql);

        // обновим контент, добавив видео
        $content = '<p><iframe src="' . $row['video_url'] . '" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>';
        $sql = "UPDATE `" . _DB_PREFIX_ . "shopimultitab_content_lang` SET `content_text` = '" . $content . "', `id_lang` = '" . $this->id_lang . "' WHERE `id_shopimultitab_content` = '" . $id_content . "'";
        if(Env::getDb()->execute($sql)) {
            $this->log(2, 'Видео добавлено к товару. ');
            $this->log(9, 'Видео добавлено к товару. ');
        } else {
            $this->log(2, 'Видео не добавлено к товару. ');
            $this->log(9, 'Видео не добавлено к товару. ');
        }

    }


    private function saveXMLID(Product $product, $row) {
        // генерируем CommerceML ID для МойСклад для derr.su
        $xml = substr(md5($row['id']), 0, 22);
        $sql = "UPDATE `" . _DB_PREFIX_ . "product` SET `xml` = '" . $xml . "' WHERE `id_product` = '" . $product->id . "'";
        if(Env::getDb()->execute($sql))
            $this->log(9, 'Товару присвоен CommerceML ID: ' . $xml);

    }

    // работа с вложениями
    private function attachFiles(Product $product, $row) {
        // если файлов нет, пропускаем
        if(!isset($row['files']) || $row['files'] == null || count($row['files']) <= 0)
            return;

        $this->log(9, 'Начинаем обработку вложений. Всего: ' . count($row['files']));
        foreach ($row['files'] as $file)
            $this->attachFile($product, $file);

        $this->log(9, 'Обработано вложений ' . $this->attachment_created . ' из ' . count($row['files']) . '. ');


    }

    private function attachFile(Product $product, $file) {
        try {

            $attachment = $this->loadFileComponent->load($product, $file);

        } catch(ExporterImageException $ex) {
            $this->log(2, 'Не удалось обработать файл ' . $file['url'] . '. Ошибка: ' . $ex->getMessage() . ', код: ' . $ex->getCode());

            throw new ExporterDataException('Ошибка обработки файла: ' . $ex->getMessage(), $ex->getCode(), $ex);

        } catch(ExporterDataException $ex) {
            $this->log(2, 'Не удалось загрузить файл ' . $file['url'] . '. Ошибка: ' . $ex->getMessage() . ', код: ' . $ex->getCode());

            throw new ExporterDataException('Ошибка загрузки файла: ' . $ex->getMessage(), $ex->getCode(), $ex);
        } catch(ExporterException $ex) {
            $this->log(2, 'Не удалось загрузить файл ' . $file['url'] . '. Ошибка: ' . $ex->getMessage() . ', код: ' . $ex->getCode());

            throw new ExporterDataException('Ошибка загрузки файла: ' . $ex->getMessage(), $ex->getCode(), $ex);
        }


        if($attachment) {
            $attachment->attachProduct($product->id);
            $this->log(9, 'файл ' . $file['url'] . ' добавляем к товару ' . $product->id);
            //PriceParserIds::addSiteIdByVendorId($file['url'], $attachment->id, PriceParserIds::PRODUCT_ATTACHMENT_TYPE_ID);
            $this->attachment_created++;

        }
    }

    private function loadTags(Product $product, $row) {

        $tags_product = Tag::getProductTags($product->id);
        if($tags_product != false) {

        } else {
            $name = explode('-', $row['code']);
            if(count($name) > 1) { // если указан префикс (напрм. sim-XXXXXX), делаем несколько тегов
                $names = $name[0] . '-' . $name[1] . ',' . $name[0] . $name[1] . ',' . $name[1];
                $result = Tag::addTags($this->id_lang, $product->id, $names);
            } else { // если нет префикса, добавляем тегом артикул
                $names = $row['code'];
                $tag = new Tag();
                $tag->name = $name;
                $tag->id_lang = $this->id_lang;
                $tag->add();
                $result = $tag->setProducts(array($product->id));
            }


            if($result) {
                $this->log(2, 'Добавлены теги: ' . $names);
                $this->log(9, 'Добавлены теги: ' . $names);
            } else {
                $this->log(2, 'Теги не добавлены: ' . $names);
                $this->log(9, 'Теги не добавлены: ' . $names);
            }

        }
        //die(var_dump($tags_product));

    }

    private function addSpecificPrice(Product $product, $row) {

        // если нет цены, пропускаем
        if(!isset($row['price']['special_price']))
            return;



        // цены не отличаются, спец цену не добавляем
        if(round($row['price']['special_price'], 1) == round($row['price']['price'], 1))
            return;

        $price = round($row['price']['price'], 1) - round($row['price']['special_price'], 1);

        //var_dump(array('price' => $row['price']['price'], 'special_price' => $row['price']['special_price'], $price));
 
        // удалим старые цены
        SpecificPrice::deleteByProductId($product->id);

        $specific_price = new SpecificPrice();
        $specific_price->id_product = $product->id;
        $specific_price->id_shop = (int)static::SHOP_ID;
        $specific_price->id_product_attribute = 0;
        $specific_price->id_currency = 0;
        $specific_price->id_country = 0;
        $specific_price->id_group = 0;
        $specific_price->id_customer = 0;
        $specific_price->price = (float)(-1);
        $specific_price->from_quantity = 1;
        $specific_price->reduction = (float)$price;
        $specific_price->reduction_type = 'amount';
        $specific_price->from = '0000-00-00 00:00:00';
        $specific_price->to = '0000-00-00 00:00:00';
        if ($specific_price->add()) {
            $this->log(2, 'Установлена скидка: ' . $price . ' руб.');
            $this->log(9, 'Установлена скидка: ' . $price . ' руб.');
        } else {
            $this->log(2, 'Cкидка: ' . $price . ' руб. не установлена');
        }

    }
}
