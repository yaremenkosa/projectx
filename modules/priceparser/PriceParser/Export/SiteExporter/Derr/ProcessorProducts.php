<?php

namespace PriceParser\Export\SiteExporter\Derr;

use PriceParser\Export\SiteExporter\ProcessorProducts as OrigProcessorProducts;
use PriceParser\Export\Processable;
use PriceParser\Controller\Env;

class ProcessorProducts extends OrigProcessorProducts implements Processable {
	
	var $id_supplier;
	
	function __construct(array $params = array()) {
		parent::__construct($params);
		
		$this->id_supplier = (int)Env::getDb()->getValue('SELECT id_supplier'
				. ' FROM ' . _DB_PREFIX_ . 'price_parser_vendor'
				. ' WHERE id_vendor_str = "' . \pSQL(Env::$config['id_vendor']) . '"');
	}
	
	protected function processRow(&$row) {
		parent::processRow($row);
		
		if($this->id_supplier) {
			$row['id_supplier']	= $this->id_supplier;
			$row['code']		= $this->id_supplier . '_' . $row['code'];
		}
	}
	
}
