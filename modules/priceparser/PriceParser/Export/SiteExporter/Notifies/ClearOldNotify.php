<?php

namespace PriceParser\Export\SiteExporter\Notifies;

include_once(_PS_SWIFT_DIR_.'Swift.php');
include_once(_PS_SWIFT_DIR_.'Swift/Connection/SMTP.php');
include_once(_PS_SWIFT_DIR_.'Swift/Connection/NativeMail.php');
include_once(_PS_SWIFT_DIR_.'Swift/Plugin/Decorator.php');

use \Configuration;
use \Swift_Connection_NativeMail;
use \Swift;
use \Swift_Message;
use \Swift_Message_Part;
use \Swift_Address;
use \Context;

/**
 * Отправщик уведомлений по устаревшим товарам, которые будут удалены
 */
class ClearOldNotify {

	/**
	 * @param array $products строки товаров
	 * @param array $emails адреса для отправки
	 */
	public function sendNotify($products, $emails) {
		$body = $this->generateBody($products);

		foreach ($emails as $emailName => $email) {
			$this->sendMail('info@directgifts.ru', $email, $emailName, 'Уведомление об удалении устаревших товаров от поставщиков', $body);
		}
	}

	/**
	 * @param array $products строки товаров
	 * @return string текст
	 */
	protected function generateBody(Array $products) {
		$smarty = Context::getContext()->smarty;

		$smarty->assign([
			'products' => $products,
		]);

		return $smarty->fetch(dirname(__FILE__) . '/clear_old_notify.tpl');

	}

	protected function sendMail($fromEmail, $toEmail, $toName, $subject, $body) {
		$id_shop = Configuration::get('PS_SHOP_DEFAULT');

		$connection = new Swift_Connection_NativeMail();

		$swift = new Swift($connection, Configuration::get('PS_MAIL_DOMAIN', null, null, $id_shop));

		$message = new Swift_Message($subject);

		$message->setCharset('utf-8');

		/* Set Message-ID - getmypid() is blocked on some hosting */
		//$message->setId(Mail::generateId());

		$message->headers->setEncoding('Q');

		$message->attach(new Swift_Message_Part($body, 'text/plain', '8bit', 'utf-8'));

		$send = $swift->send($message, $toEmail, new Swift_Address($fromEmail, $toName));
		$swift->disconnect();
	}



}