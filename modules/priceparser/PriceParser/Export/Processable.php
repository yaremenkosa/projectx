<?php

namespace PriceParser\Export;

interface Processable {
	
	public function process(Array $data);
	
}
