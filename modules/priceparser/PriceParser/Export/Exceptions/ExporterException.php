<?php

namespace PriceParser\Export\Exceptions;

use PriceParser\Core\Exceptions\PriceParserException;

class ExporterException extends PriceParserException {}
