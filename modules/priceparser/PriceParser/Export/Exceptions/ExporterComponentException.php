<?php

namespace PriceParser\Export\Exceptions;

class ExporterComponentException extends ExporterException {}
