<?php

namespace PriceParser\Export\Exceptions;

class ExporterImageSizeException extends ExporterImageException {}
