<?php

namespace PriceParser\Export\Exceptions;

class ExporterImageDownloadException extends ExporterImageException {}
