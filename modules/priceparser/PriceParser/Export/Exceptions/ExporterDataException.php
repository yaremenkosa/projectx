<?php

namespace PriceParser\Export\Exceptions;

class ExporterDataException extends ExporterException {}
