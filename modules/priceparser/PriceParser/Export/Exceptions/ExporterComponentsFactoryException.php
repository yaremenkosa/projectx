<?php

namespace PriceParser\Export\Exceptions;

class ExporterComponentsFactoryException extends ExporterException {}
