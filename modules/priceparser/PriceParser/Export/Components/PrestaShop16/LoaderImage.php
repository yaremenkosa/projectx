<?php

namespace PriceParser\Export\Components\PrestaShop16;

use PriceParser\Export\Components\Interfaces\LoaderImageInterface;

use PriceParser\Core\Object\Object;

use \Image;
use PriceParser\Export\Exceptions\ExporterException;
use PriceParser\Export\Exceptions\ExporterImageException;
use PriceParser\Export\Exceptions\ExporterDataException;
use PriceParser\Export\Exceptions\ExporterComponentException;
use PriceParser\Core\Downloader\Exceptions\FileExistsException;
use PriceParser\Core\Downloader\Exceptions\FileCheckException;
use \ImageManager;
use \ImageType;
use \Hook;
use \Shop;
use Validate;

class LoaderImage extends Object implements LoaderImageInterface {

    /**
     *
     * @var \PriceParser\Core\Downloader\AbstractDownloader
     */
    var $downloader;

    var $tmp_path;

    /**
     *
     * @param \Product $product
     * @param array $image_row
     * @return Image
     * @throws ExporterException
     * @throws ExporterDataException
     * @throws ExporterImageException
     * @throws type
     */
    public function load(\Product $product, Array $image_row) {

        $this->log(3, 'Загружаем изображение ' . $image_row['url']);

        try {
            $file = $this->downloader->download($image_row['url'], $this->tmp_path, null, \PriceParser\Core\Downloader\AbstractDownloader::RETURN_TYPE_FILE_PATH);
        } catch(\PriceParser\Core\Downloader\Exceptions\ConnectException $ex) {
            throw new ExporterException('Ошибка при загрузке файла: ' . $ex->getMessage(), $ex->getCode(), $ex);
        } catch(FileExistsException $ex) {
            throw new ExporterDataException('Ошибка проверки файла: ' . $ex->getMessage(), $ex->getCode(), $ex);
        } catch(FileCheckException $ex) {
            throw new ExporterDataException('Файл не найден: ' . $ex->getMessage(), $ex->getCode(), $ex);
        }

        if(!$file || !file_exists($file)) {
            throw new ExporterImageException('Изображение ' . $file . ' не удалось загрузить');
        }

        if (!$this->checkJpeg($file)) {
            throw new ExporterImageException('Изображение ' . $file . ' корявое');
        }

        if(function_exists('exif_imagetype')) {
            if(!exif_imagetype($file)) {
                throw new ExporterImageException('Изображение не является некорректным');
            }
        } elseif(@getimagesize($file) === false) {
            throw new ExporterImageException('Изображение не является некорректным');
        }

        $this->log(1, 'Файл ' . $file);

        $image = new Image();
        $image->id_product = (int)($product->id);

        $this->assignParams($image, $image_row);

        if (!$image->add()) {
            throw new ExporterException('Error while creating additional image');
        }
        else {
            if (!$new_path = $image->getPathForCreation()) {
                throw new ExporterException('An error occurred during new folder creation');
            }

            $error = 0;

            if (!ImageManager::resize($file, $new_path.'.'.$image->image_format, null, null, 'jpg', false, $error)) {
                switch ($error) {
                    case ImageManager::ERROR_FILE_NOT_EXIST :
                        throw new ExporterImageException('An error occurred while copying image, the file does not exist anymore.', $error);
                        break;

                    case ImageManager::ERROR_FILE_WIDTH :
                        throw new ExporterImageException('An error occurred while copying image, the file width is 0px.', $error);
                        break;

                    case ImageManager::ERROR_MEMORY_LIMIT :
                        throw new ExporterImageException('An error occurred while copying image, check your memory limit.', $error);
                        break;

                    default:
                        throw new ExporterImageException('An error occurred while copying image.', $error);
                        break;
                }
            }
            else {
                $imagesTypes = ImageType::getImagesTypes('products');
                foreach ($imagesTypes as $imageType) {
                    if (!ImageManager::resize($file, $new_path.'-'.stripslashes($imageType['name']).'.'.$image->image_format, $imageType['width'], $imageType['height'], $image->image_format)) {
                        throw new ExporterException('An error occurred while copying image:').' '.stripslashes($imageType['name']);
                    }
                }
            }

            Hook::exec('actionWatermark', array('id_image' => $image->id, 'id_product' => $product->id));

            // обновим название
            $id_lang = \Configuration::get('PS_LANG_DEFAULT');
            if(isset($image_row['legend']))
                $image->legend[$id_lang] = $image_row['legend'];

            if (!$image->update()) {
                throw new ExporterException('Error while updating image status');
            }

            // Associate image to shop from context
            $shops = Shop::getContextListShopID();
            $image->associateTo($shops);
//			$json_shops = array();
//
//			foreach ($shops as $id_shop) {
//				$json_shops[$id_shop] = true;
//			}
//			
//			$file['status']   = 'ok';
//			$file['id']       = $image->id;
//			$file['position'] = $image->position;
//			$file['cover']    = $image->cover;
//			$file['legend']   = $image->legend;
//			$file['path']     = $image->getExistingImgPath();
//			$file['shops']    = $json_shops;

//			@unlink(_PS_TMP_IMG_DIR_.'product_'.(int)$product->id.'.jpg');
//			@unlink(_PS_TMP_IMG_DIR_.'product_mini_'.(int)$product->id.'_'.$this->context->shop->id.'.jpg');

            $this->log(3, 'изображение сохранено успешно. id = ' . $image->id);
        }

        if(file_exists($file)) {
            $unlink_result = unlink($file);
            if($unlink_result) {
                $this->log(2, 'временный файл ' . $file . ' был удалён');
            }
            else {
                $this->log(2, 'Ошибка при удалении временного файла ' . $file);
            }
        }

        return $image;
    }

    protected function checkJpeg($f, $fix=false )
    {

        # check for jpeg file header and footer - also try to fix it
        if ( false !== (@$fd = fopen($f, 'r+b' )) ){
            if ( fread($fd,2)==chr(255).chr(216) ){
                fseek ( $fd, -2, SEEK_END );
                if ( fread($fd,2)==chr(255).chr(217) ){
                    fclose($fd);
                    return true;
                }else{
                    if ( $fix && fwrite($fd,chr(255).chr(217)) ){return true;}
                    fclose($fd);
                    return false;
                }
            }else{fclose($fd); return false;}
        }else{
            return false;
        }
    }

    /**
     *
     * @param Image $image
     * @param array $image_row
     * @return boolean
     * @throws ExporterComponentException
     */
    public function assignParams(Image $image, array $image_row) {
        if(!isset($image_row['position'])) {
            throw new ExporterComponentException('Отсутствует параметр position');
        }

        if(!isset($image_row['is_cover'])) {
            throw new ExporterComponentException('Отсутствует параметр is_cover');
        }

        $image->position = $image_row['position'];
        $image->cover	 = $image_row['is_cover'];

        return true;
    }

    /**
     *
     * @param Image $image
     * @throws ExporterComponentException
     */
    public function deleteImage(Image $image) {
        if(!Validate::isLoadedObject($image)) {
            throw new ExporterComponentException('Изображение не загружено');
        }
        $image->deleteImage();
        $image->delete();
    }
}
