<?php

namespace PriceParser\Export\Components\PrestaShop16;

use PriceParser\Export\Components\AbstractComponentsFactory;

class PrestaShop16ComponentsFactory extends AbstractComponentsFactory {

    /**
     *
     * @return \PriceParser\Export\Components\PrestaShop16\LoaderFile
     */
    public function createLoaderFile(Array $params = []) {
        return new LoaderFile($params);
    }

	/**
	 * 
	 * @return \PriceParser\Export\Components\PrestaShop16\LoaderImage
	 */
	public function createLoaderImage(Array $params = []) {
		return new LoaderImage($params);
	}
	
	/**
	 * 
	 * @param array $params
	 * @return \PriceParser\Export\Components\PrestaShop16\ProcessorImage
	 */
	public function createProcessorImage(array $params = array()) {
		return new ProcessorImage($params);
	}
	
	/**
	 * 
	 * @param array $params
	 * @return \PriceParser\Export\Components\PrestaShop16\LoaderProduct
	 */
	public function createLoaderProduct(array $params = array()) {
		return new LoaderProduct($params);
	}
	
	/**
	 * 
	 * @param array $params
	 * @return \PriceParser\Export\Components\PrestaShop16\LoaderCategory
	 */
	public function createLoaderCategory(array $params = array()) {
		return new LoaderCategory($params);
	}
}
