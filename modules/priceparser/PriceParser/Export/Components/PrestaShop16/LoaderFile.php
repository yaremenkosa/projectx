<?php

namespace PriceParser\Export\Components\PrestaShop16;

use PriceParser\Export\Components\Interfaces\LoaderFileInterface;

use PriceParser\Core\Object\Object;

use \Attachment;
use PriceParser\Export\Exceptions\ExporterException;
use PriceParser\Export\Exceptions\ExporterImageException;
use PriceParser\Export\Exceptions\ExporterDataException;
use PriceParser\Export\Exceptions\ExporterComponentException;
use PriceParser\Core\Downloader\Exceptions\FileExistsException;
use PriceParser\Core\Downloader\Exceptions\FileCheckException;
use Validate;
use Tools;

use PriceParser\Controller\Env;

class LoaderFile extends Object implements LoaderFileInterface {
	
	/**
	 *
	 * @var \PriceParser\Core\Downloader\AbstractDownloader
	 */
	var $downloader;
	
	var $tmp_path;
	
	/**
	 * 
	 * @param \Product $product
	 * @param array $file_row
	 * @return File
	 * @throws ExporterException
	 * @throws ExporterDataException
	 * @throws ExporterImageException
	 * @throws type
	 */
	public function load(\Product $product, Array $file_row) {

		$this->log(3, 'Загружаем файл ' . $file_row['url']);
		
		try {
			$file = $this->downloader->download($file_row['url'], $this->tmp_path, null, \PriceParser\Core\Downloader\AbstractDownloader::RETURN_TYPE_FILE_PATH);
		} catch(\PriceParser\Core\Downloader\Exceptions\ConnectException $ex) {
			throw new ExporterException('Ошибка при загрузке файла: ' . $ex->getMessage(), $ex->getCode(), $ex);
		} catch(FileExistsException $ex) {
			throw new ExporterDataException('Ошибка проверки файла: ' . $ex->getMessage(), $ex->getCode(), $ex);
		} catch(FileCheckException $ex) {
			throw new ExporterDataException('Файл не найден: ' . $ex->getMessage(), $ex->getCode(), $ex);
		}
		
		if(!$file || !file_exists($file)) {
			throw new ExporterImageException('Файл ' . $file . ' не удалось загрузить');
		}
		
		$this->log(1, 'Файл ' . $file);

		$id_atachment = $this->checkAttachment($file_row, $file);

        /*if (filesize($file) > (\Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE') * 1024 * 1024)) {
            $this->log(2, 'Загружаемый файл превышает максимальный допустимый размер ');
            @unlink($file);
            return false;
        }
        else
        {*/
        if($id_atachment == null) {
            do $uniqid = sha1(microtime());
            while (file_exists(_PS_DOWNLOAD_DIR_ . $uniqid));
            if (!copy($file, _PS_DOWNLOAD_DIR_ . $uniqid)) {
                $this->log(2, 'Файл не скопирован.');
                @unlink($file);
                return false;
            }
            //}
            $file_row['uniqid'] = $uniqid;
            $file_row['mime'] = mime_content_type($file);

            $attachment = new \Attachment();

		
            $this->assignParams($attachment, $file_row);

            if (!$attachment->add()) {
                throw new ExporterException('Error while creating additional image');
            } else {
                $this->log(2, 'Файл сохранен успешно. id = ' . $attachment->id);
            }
        } else {
            $this->log(2, 'Файл с таким именем существует. id = ' . $id_atachment);
            $attachment = new \Attachment($id_atachment);
        }


		if(file_exists($file)) {
			$unlink_result = unlink($file);
			if($unlink_result) {
				$this->log(2, 'временный файл ' . $file . ' был удалён');
			}
			else {
				$this->log(2, 'Ошибка при удалении временного файла ' . $file);
			}
		}
		
		return $attachment;
	}

	/**
	 * 
	 * @param Attachment $attachment
	 * @param array $file_row
	 * @return boolean
	 * @throws ExporterComponentException
	 */
	public function assignParams(Attachment $attachment, array $file_row) {
		if(!isset($file_row['uniqid'])) {
			throw new ExporterComponentException('Отсутствует параметр uniqid');
		}
		
		if(!isset($file_row['mime'])) {
			throw new ExporterComponentException('Отсутствует параметр mime');
		}

        if(!isset($file_row['filename'])) {
            throw new ExporterComponentException('Отсутствует параметр name');
        }

        $id_lang = \Configuration::get('PS_LANG_DEFAULT');

        $attachment->name[$id_lang]	    = \Tools::substr($file_row['title'], 0, 30);
        $attachment->file               = $file_row['uniqid'];
        $attachment->mime               = $file_row['mime'];
        $attachment->file_name          = $file_row['filename'];

        if (empty($attachment->mime) || Tools::strlen($attachment->mime) > 128)
            throw new ExporterComponentException('Invalid file extension');
        if (!Validate::isGenericName($attachment->file_name))
            throw new ExporterComponentException('Invalid file name');
        if (Tools::strlen($attachment->file_name) > 128)
            throw new ExporterComponentException('The file name is too long.');

		return true;
	}
	
	/**
	 * 
	 * @param Attachment $attachment
	 * @throws ExporterComponentException
	 */
	public function deleteFile(Attachment $attachment) {
		if(!Validate::isLoadedObject($attachment)) {
			throw new ExporterComponentException('Файл не загружен');
		}
        $attachment->delete();
	}

    private function checkAttachment($file_row, $file) {

        $mime = mime_content_type($file);

        $sql = "SELECT `id_attachment` 
        FROM `" . _DB_PREFIX_ . "attachment` 
        WHERE `file_name` = '" . $file_row['filename'] . "' 
        AND `mime` = '" . $mime . "' AND `file_size` = '" . filesize($file) . "'";
        $attachment = Env::getDb()->getValue($sql);


        if($attachment)
            return $attachment;
        else
            return null;
    }
}