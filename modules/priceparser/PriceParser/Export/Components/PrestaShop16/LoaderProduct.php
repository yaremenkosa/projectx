<?php
namespace PriceParser\Export\Components\PrestaShop16;

use PriceParser\Core\Object\Object;
use PriceParser\Export\Components\Interfaces\LoaderProductInterface;
use PriceParser\Export\Exceptions\ExporterDataException;
use PriceParser\Core\PriceParserIds;

class LoaderProduct extends Object implements LoaderProductInterface {

	var $id_lang;

	/**
	 *
	 * @param \Product $product
	 * @param array $row
	 * @return boolean
	 * @throws ExporterDataException
	 */
	public function load(\Product $product, Array $row) {

	    $updated_product = false;

		$clean_description = $row['description'];

		$row['name'] = \Tools::substr(preg_replace('/[<>;=#{}]/u', '', html_entity_decode($row['name'])), 0, 120);


		$product->link_rewrite[$this->id_lang]	= \Tools::substr(\Tools::str2url($row['name']), 0, 120);

		$product->id_supplier				= (int)$row['id_supplier'];

		$product->reference					= $row['code'];

        $product->active                    = $row['is_active'];

		// pw-pl доработал для derr.su, обновляем описание в том случае, если не изменялось в админке
        $update_desc = PriceParserIds::getUpdateDesc($product->id, 1);

        if($update_desc == 0) {
            if($product->id != null) {
                if ($product->name[$this->id_lang] != $row['name']) {
                    $this->log(9, '- обновили название');
                    $updated_product = true;
                }

                if ($product->description[$this->id_lang] != $clean_description) {
                    $this->log(9, '- обновили описание');
                    $updated_product = true;
                }
            }

            $product->name[$this->id_lang]			= $row['name'];
            $product->description = $clean_description;
            $product->description_short = $clean_description;
        } else {
            $this->log(9, 'Товар редактировался в ручную, поэтому не обновляем описание. ');
        }
		//???
		$product->status					= 0;

		//проверяем лимит для description_short
		$limit = (int)\Configuration::get('PS_PRODUCT_SHORT_DESC_LIMIT') - 100;
		if ($limit <= 0) {
			$limit = 300;
		}

		if(\Tools::strlen($product->description_short) > $limit) {
			//если превышение лимита, то обрезаем (
			$product->description_short = \Tools::substr($product->description_short, 0, ($limit)) . '...';
		}

		if(isset($row['attributes']['weight'])) {
			$product->weight					= $row['attributes']['weight'];
		}

		if(isset($row['attributes']['sizex'])) {
			$product->width						= $row['attributes']['sizex'];
		}

		if(isset($row['attributes']['sizey'])) {
			$product->height					= $row['attributes']['sizey'];
		}

		if(isset($row['attributes']['sizez'])) {
			$product->depth						= $row['attributes']['sizez'];
		}

        // pw-pl доработал для derr.su, обновляем цену в том случае, если не изменялось в админке
        $update_price = PriceParserIds::getUpdatePrice($product->id, 1);
        if($update_price == 0) {
            if (isset($row['price']['price_rr'])) {
                if (isset($row['price']['wholesale_price'])) {
                    $product->wholesale_price = $row['price']['wholesale_price'];
                } else {
                    $product->wholesale_price = $row['price']['price_rr'];
                }

                if ($product->id != null) {
                    if ($product->price != $row['price']['price_rr']) {
                        $this->log(9, '- обновили цену (' . $product->price . ' -> ' . $row['price']['price_rr'] . ')');
                        $updated_product = true;
                    }
                } else {
                    $this->log(9, '- установили цену (' . $row['price']['price_rr'] . ')');
                }

                $product->price = $row['price']['price_rr'];
            }
        } else {
            $this->log(9, 'Цена изменялась в ручную, не обновляем. ');
        }

		if($updated_product == false)
            $this->log(9, '- ничего не обновили');

		if(isset($row['brand_id'])) {
			$brand_id = PriceParserIds::getSiteIdByVendorId($row['brand_id'], PriceParserIds::BRAND_VALUE_ID);

			if($brand_id) {
				$product->id_manufacturer = $brand_id;
			}
		} elseif(isset($row['id_manufacturer'])) {
			$product->id_manufacturer = $row['id_manufacturer'];
		}

        // надпись доступности
        if(isset($row['available_now']))
            $product->available_now = $row['available_now'];

        if(isset($row['available_later']))
            $product->available_later = $row['available_later'];

		if(($valid_result = $product->validateFields(true, true)) !== true) {
			throw new ExporterDataException('Некорректные данные при сохранении товара: ' . $valid_result);
		}

		if($product->id == null) { var_dump('qweqweqe321');
            $save_result = $product->add();
        } else {
            $save_result = $product->save();
        }

		$product->addSupplierReference((int)$row['id_supplier'], 0);

		return $save_result;
	}

}
