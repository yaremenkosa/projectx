<?php

namespace PriceParser\Export\Components\Interfaces;

interface LoaderFileInterface extends ComponentInterface {
	
	/**
	 * 
	 * @param \Product $product
	 * @param array $file_row
	 */
	public function load(\Product $product, Array $file_row);
	
	/**
	 * 
	 * @param \Attachment $file
	 * @param array $file_row
	 */
	public function assignParams(\Attachment $file, Array $file_row);
	
	/**
	 * 
	 * @param \Attachment $file
	 */
	public function deleteFile(\Attachment $file);
	
}
