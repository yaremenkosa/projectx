<?php

namespace PriceParser\Export\Components\Interfaces;

interface LoaderCategoryInterface extends ComponentInterface {
	
	/**
	 * 
	 * @param type $id_category
	 * @param type $cat_row
	 * @param type $id_parent
	 * @return \Category Description
	 */
	public function loadCategory($id_category, $cat_row, $parent_id);
	
}
