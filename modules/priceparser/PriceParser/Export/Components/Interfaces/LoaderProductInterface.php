<?php

namespace PriceParser\Export\Components\Interfaces;

interface LoaderProductInterface extends ComponentInterface {
	
	/**
	 * 
	 * @param \Product $product
	 * @param array $row
	 * @return bool
	 */
	public function load(\Product $product, Array $row);
	
}
