<?php

namespace PriceParser\Export\Components\Interfaces;

interface LoaderImageInterface extends ComponentInterface {
	
	/**
	 * 
	 * @param \Product $product
	 * @param array $image_row
	 */
	public function load(\Product $product, Array $image_row);
	
	/**
	 * 
	 * @param \Image $image
	 * @param array $image_row
	 */
	public function assignParams(\Image $image, Array $image_row);
	
	/**
	 * 
	 * @param \Image $imageS
	 */
	public function deleteImage(\Image $image);
	
}
