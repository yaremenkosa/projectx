<?php

namespace PriceParser\Export\Components\Interfaces;

interface ProcessorImageInterface extends ComponentInterface {
	
	/**
	 * 
	 * @param array $row
	 * @return bool Description
	 */
	public function process(array &$row);
	
}
