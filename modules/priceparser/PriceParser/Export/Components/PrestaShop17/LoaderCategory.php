<?php

namespace PriceParser\Export\Components\PrestaShop17;

use PriceParser\Core\Object\Object;
use PriceParser\Core\PriceParserIds;
use PriceParser\Export\Components\Interfaces\LoaderCategoryInterface;
use PriceParser\Export\Exceptions\ExporterComponentException;
use PriceParser\Controller\Env;
use PriceParser\Export\Exceptions\ExporterDataException;

class LoaderCategory extends Object implements LoaderCategoryInterface {
	
	var $id_lang;
	
	/**
	 * 
	 * @param type $id_category
	 * @param type $cat_row
	 * @param type $id_parent
	 * @return \Category
	 */
	public function loadCategory($id_category, $cat_row, $parent_id) {
		if($id_category) {
			$this->log(2, 'по категории вендора ' . $cat_row['id'] . ' найдена категория на сайте: ' . $id_category);

			$category = new \Category($id_category);

		} else {
			$this->log(2, 'по категории вендора ' . $cat_row['id'] . ' отсутствует категория на сайте');
            PriceParserIds::deleteByVendorId($cat_row['id'], PriceParserIds::CATEGORY_ITEM_TYPE_ID);

			$category = new \Category();
		}
        var_dump($this->id_lang);
		$category->id_parent = $parent_id;
		$category->active = 1;
		$category->name[$this->id_lang] = $cat_row['name'];

		if(isset($cat_row['uri'])) {
			$category->link_rewrite[$this->id_lang] = $cat_row['uri'];
		} else {
			$category->link_rewrite[$this->id_lang] = \Tools::str2url($cat_row['name']);
		}
		
		if(isset($cat_row['meta_title'])) {
			$category->meta_title[$this->id_lang] = $cat_row['meta_title'];
		}

		$category->doNotRegenerateNTree = true;
		
		/*if(!$category->id_image) {
			$this->log(2, 'У категории ' . $category->getName($this->id_lang) . '(id = ' . $category->id . ') нет изображения');
			try {
				$this->assignCategoryImage($category);
			} catch(\PriceParser\Export\Exceptions\ExporterDataException $ex) {
				$this->log(1, 'Ошибка привязки изображения к категории: ' . $ex->getMessage());
			}
		} else {
			$this->log(2, 'У категории ' . $category->getName($this->id_lang) . '(id = ' . $category->id . ') есть изображение');
		}*/

		//валидируем модель
		$validationResult = $category->validateFields(false, true);
		$validationLangResult = $category->validateFieldsLang(false, true);

		if (true !== $validationResult) {
			//если невалидна
			throw new ExporterDataException('Модель каталога невалидна, сохранение невозможно. Ошибка: ' . $validationResult);
		}

		if (true !== $validationLangResult) {
			//если невалидна
			throw new ExporterDataException('Модель каталога невалидна, сохранение невозможно. Ошибка: ' . $validationLangResult);
		}

		//если сущность не загружена (например, ктото удалил её), то задаём ей принудительно её id
		if(!\Validate::isLoadedObject($category) && $id_category) {
			$this->log(2, 'Категория ' . $id_category . ' оказалась удалена, нужно пересоздать');
            $save_result = $category->add();
            PriceParserIds::deleteByVendorId($cat_row['id'], PriceParserIds::CATEGORY_ITEM_TYPE_ID);
            PriceParserIds::addSiteIdByVendorId($cat_row['id'], $category->id, PriceParserIds::CATEGORY_ITEM_TYPE_ID);
		} else {
			$save_result = $category->save();
		}

		// если задан магазин, привяжем только к нему
		if(isset($cat_row['shop'])) {
            $this->log(1, 'Создаем привязку категории ' . $category->getName($this->id_lang) . ' к магазину ' . $cat_row['shop']);
		    $id_shop = \Shop::getIdByName($cat_row['shop']);
		    $shop = new \Shop($id_shop);
		    if(\Validate::isLoadedObject($shop)) {
                // удалим привязки к магазинам
                Env::getDb()->execute('
                DELETE FROM `' . _DB_PREFIX_ . 'category_shop`
                WHERE id_category = ' . (int) $category->id);

                // добавим к выбранному магазину
                $category->addShop($shop->id);
                $this->log(1, 'Привязка категории ' . $category->getName($this->id_lang) . ' с магазином ' . $cat_row['shop'] .'(id: ' . $shop->id . ') создана');
            } else {
                $this->log(1, 'Магазин с названием ' . $cat_row['shop'] . ' не найден, привяжем ко всем магазинам.');
            }
        }
		
		if(!$save_result) {
			throw new ExporterComponentException('Не удалось сохранить категорию. Название: ' . $cat_row['name']);
		}

		return $category;
	}
	
	function assignCategoryImage(\Category $category) {
		$id_image = Env::getDb()->getValue('SELECT i.id_image'
				. ' FROM ' . _DB_PREFIX_ . 'image AS i'
				. ' INNER JOIN ' . _DB_PREFIX_ . 'category_product AS cp ON cp.id_product = i.id_product AND cp.id_category = ' . (int)$category->id
				. ' ORDER BY cp.position ASC, i.position ASC, cp.id_product ASC');
		
		if($id_image) {
			$this->log(2, 'Для категории ' . $category->getName($this->id_lang) . '(id = ' . $category->id . ') найдено изображение ' . $id_image);
			$result = true;
			
			$image = new \Image($id_image);
			
			$img_file_path = $image->getPathForCreation() . '.' . $image->image_format;
			
			if(!file_exists($img_file_path)) {
				throw new \PriceParser\Export\Exceptions\ExporterDataException('Файл изображения ' . $id_image . ' не найден');
			}
			
			$images_types = \ImageType::getImagesTypes('categories');
			
			copy($img_file_path, _PS_CAT_IMG_DIR_.$category->id.'.jpg');
			
			foreach ($images_types as $k => $image_type) {
				$result &= \ImageManager::resize(
					_PS_CAT_IMG_DIR_.$category->id.'.jpg',
					_PS_CAT_IMG_DIR_.$category->id.'-'.stripslashes($image_type['name']).'.jpg',
					(int)$image_type['width'], (int)$image_type['height']
				);
			}
			
			return $result;
		} else {
			$this->log(2, 'Для категории ' . $category->getName($this->id_lang) . '(id = ' . $category->id . ') изображения по товарам не найдено');
		}
		
		return false;
	}
	
}
