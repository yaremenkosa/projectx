<?php
namespace PriceParser\Export\Components\PrestaShop17;

use PriceParser\Core\Object\Object;
use PriceParser\Export\Components\Interfaces\LoaderProductInterface;
use PriceParser\Export\Exceptions\ExporterDataException;
use \Shop;
use PriceParser\Core\PriceParserIds;

class LoaderProduct extends Object implements LoaderProductInterface {
	
	var $id_lang;
	
	/**
	 * 
	 * @param \Product $product
	 * @param array $row
	 * @return boolean
	 * @throws ExporterDataException
	 */
	public function load(\Product $product, Array $row) {
		$clean_description = nl2br(strip_tags(str_replace('\'', '`', $row['description'])));

		$row['name'] = preg_replace('/[<>;=#{}]/u', '', html_entity_decode($row['name']));
		
		$product->name[$this->id_lang]			= $row['name'];
        
		$product->link_rewrite[$this->id_lang]	= \Tools::str2url($row['name']);
        
		$product->id_supplier				= (int)$row['id_supplier'];
		
		$product->reference					= $row['code'];
		//$product->on_sale					= $row['on_sale'];

		//???
		$product->status					= 0;
        // раз мы загружаем товар, то он будет активный
		$product->active					= 1;
		
		//проверяем лимит для description_short
		$limit = (int)\Configuration::get('PS_PRODUCT_SHORT_DESC_LIMIT');
		if ($limit <= 0) {
			$limit = 800;
		}

        // pw-pl доработал для derr.su, обновляем описание в том случае, если не изменялось в админке
        $update_desc = PriceParserIds::getUpdateDesc($product->id, 1);

        if($update_desc == 0) {
            if($product->id != null) {
                if ($product->description[$this->id_lang] != $clean_description) {
                    $this->log(2, '- обновили описание');
                    $updated_product = true;
                }
            }


            $product->description = $clean_description;
            $product->description_short = substr($clean_description, 0, $limit);
        } else {
            $this->log(2, 'Товар редактировался в ручную, поэтому не обновляем описание. ');
        }

		if(isset($row['attributes']['weight'])) {
			$product->weight					= $row['attributes']['weight'];
		}
		
		if(isset($row['attributes']['sizex'])) {
			$product->width						= $row['attributes']['sizex'];
		}
		
		if(isset($row['attributes']['sizey'])) {
			$product->height					= $row['attributes']['sizey'];
		}
		
		if(isset($row['attributes']['sizez'])) {
			$product->depth						= $row['attributes']['sizez'];
		}

		if(isset($row['price']['price'])) {
			if(isset($row['price']['wholesale_price'])) {
				$product->wholesale_price			= $row['price']['wholesale_price'];
			} else {
				$product->wholesale_price			= $row['price']['price'];
			}
			$product->price							= $row['price']['price'];
		}

		//валидируем модель
		$validationResult = $product->validateFields(false, true);
		$validationLangResult = $product->validateFieldsLang(false, true);

		if (true !== $validationResult) {
			//если невалидна
			throw new ExporterDataException('Модель товара невалидна, сохранение невозможно. Ошибка result: ' . $validationResult);
		}

		if (true !== $validationLangResult) {
			//если невалидна
			throw new ExporterDataException('Модель товара невалидна, сохранение невозможно. Ошибка lang: ' . $validationLangResult);
		}

		if(isset($row['shop_id']) && $row['shop_id'] != false) {
		    // определим магазин по названию
            $this->log(1, 'Создаем привязку к магазину ' . $row['shop_id']);
            $id_shop = \Shop::getIdByName($row['shop_id']);
            $shop = new \Shop($id_shop);
            if(\Validate::isLoadedObject($shop)) {

                $this->log(1, 'Привязка  с магазином ' . $row['shop_id'] .'(id: ' . $shop->id . ') создана');
                $shops_id = array($shop->id);
            } else {
                $this->log(1, 'Магазин с названием ' . $row['shop_id'] . ' не найден, привяжем ко всем магазинам.');
                $shops_id = Shop::getShops(true, null, true);
            }
            $product->id_shop_list = $shops_id;
        } else {
            // для мультимагазина, добавим ко всем
            $product->id_shop_list = Shop::getShops(true, null, true);
        }


		$save_result = $product->save();
		
		$product->addSupplierReference((int)$row['id_supplier'], 0);
		
		return true;
	}
	
}
