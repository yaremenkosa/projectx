<?php

namespace PriceParser\Export\Components\PrestaShop17;

use PriceParser\Export\Components\Interfaces\ProcessorImageInterface;
use PriceParser\Core\Object\Object;
use PriceParser\Core\PriceParserIds;
use PriceParser\Controller\Env;

class ProcessorImage extends Object implements ProcessorImageInterface {
	
	public function process(array &$row) {
		$site_images = [];
		$id_site_product = PriceParserIds::getSiteIdByVendorId($row['id'], PriceParserIds::PRODUCT_ITEM_TYPE_ID);

		if($id_site_product) {
			//список имеющихся загруженны картинок для товара
			$site_images = Env::getDb()->executeS('
				SELECT i.`id_image`, i.position, i.cover, ids.id_item_vendor
				FROM `'._DB_PREFIX_.'image` i
				INNER JOIN ' . _DB_PREFIX_ . 'price_parser_ids ids ON ids.id_item_site = i.id_image AND ids.id_item_type = ' . (int)PriceParserIds::IMAGE_ITEM_TYPE_ID . '
				WHERE i.`id_product` = '.(int)$id_site_product.'
				ORDER BY `position`'
			);
		}

		//var_dump($site_images);

		//просматриваем выгружаемые картинки
		if(!empty($row['images'])) {
			foreach($row['images'] as $key => &$img) {
                if(count($site_images) > 0) {
                    $img['is_cover'] = 0;
                }
			    if(!isset($img['url'])) {
			        unset($row['images'][$key]);
			        continue;
                }

				if(!isset($img['id_vendor'])) {
					$img['id_vendor'] = $img['url'];
				}

				//ищем, не загружена ли такая картинка
				$is_loaded = false;
				if(!empty($site_images)) {
					foreach($site_images as $site_img_row) {
						if($site_img_row['id_item_vendor'] == $img['id_vendor']) {
							$this->log(1, 'Изображение ' . $img['url'] . ' (id на сайте = ' . $site_img_row['id_image'] . ') уже существует');
							$img['id_site'] = $site_img_row['id_image'];
							$img['is_new']	 = false;
							
							if(($site_img_row['position'] != $img['position']) || ($site_img_row['cover'] != $img['is_cover'])) {
								$img['is_changed'] = true;
							}
							else {
								$img['is_changed'] = true;
							}
							
							$img['is_deleted'] = false;

							$is_loaded = true;
							break;
						}
					}
				}

				if(!$is_loaded) {
					$img['is_new']		 = true;
					$img['is_changed']	 = false;
					$this->log(1, 'Изображение ' . $img['url'] . ' будет загружено');
				}
			}
		}

		//перебираем все загруженные картинки на сайте, чтобы найти удалённые из выгрузки
		if(!empty($site_images)) {
			foreach($site_images as $site_img_row) {
				$is_deleted = true;

				foreach($row['images'] as &$img) {
					if(isset($img['id_vendor']) && $img['id_vendor'] == $site_img_row['id_item_vendor']) {
						$is_deleted = false;
					}
				}

				if($is_deleted) {
					$this->log(1, 'Изображение ' . $site_img_row['id_item_vendor'] . ' (id на сайте = ' . $site_img_row['id_image'] . ') отмечено для удаления');
					$row['images'][$site_img_row['id_image']] = [
						'id_site'		=> $site_img_row['id_image'],
						'url'			=> false,
						'is_new'		=> false,
						'is_changed'	=> false,
						'is_deleted'	=> true,
					];
				}
			}
		}
		
		$row['id_supplier'] = 0;
		
		return true;
	}
	
}
