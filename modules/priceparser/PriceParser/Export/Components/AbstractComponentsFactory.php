<?php

namespace PriceParser\Export\Components;

abstract class AbstractComponentsFactory {
	
	/**
	 * 
	 * @return \PriceParser\Export\Components\AbstractComponentsFactory
	 * @throws \PriceParser\Export\Exceptions\ExporterComponentsFactoryException
	 */
	public static final function getFactory() {
		$version_prefix = \PriceParser\Core\Tools::getPrestaShopVersionPrefix();
		
		if(!$version_prefix) {
			throw new \PriceParser\Export\Exceptions\ExporterComponentsFactoryException('Не удалось определить версионный префикс');
		}
		
		$factory_class_name = '\PriceParser\Export\Components\\' . $version_prefix . '\\' . $version_prefix . 'ComponentsFactory';
		
		if(!class_exists($factory_class_name)) {
			throw new \PriceParser\Export\Exceptions\ExporterComponentsFactoryException('Отсутствует класс фабрики ' . $factory_class_name);
		}
		
		return new $factory_class_name;
	}

    /**
     * @return Interfaces\LoaderFileInterface
     */
    abstract public function createLoaderFile(Array $params = []);
	
	/**
	 * @return Interfaces\LoaderImageInterface
	 */
	abstract public function createLoaderImage(Array $params = []);
	
	/**
	 * @return Interfaces\ProcessorImageInterface
	 */
	abstract public function createProcessorImage(Array $params = []);
	
	/**
	 * @return Interfaces\LoaderProductInterface
	 */
	abstract public function createLoaderProduct(Array $params = []);
	
	/**
	 * @return Interfaces\LoaderCategoryInterface
	 */
	abstract public function createLoaderCategory(Array $params = []);
	
}
