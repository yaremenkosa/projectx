<?php

namespace PriceParser\Export\Components\PrestaShop14;

use PriceParser\Export\Components\Interfaces\LoaderImageInterface;
use PriceParser\Core\Object\Object;
use \Image;
use PriceParser\Export\Exceptions\ExporterException;
use PriceParser\Export\Exceptions\ExporterImageException;
use PriceParser\Export\Exceptions\ExporterDataException;
use PriceParser\Core\Downloader\Exceptions\ConnectException;
use PriceParser\Core\Downloader\Exceptions\FileExistsException;
use PriceParser\Core\Downloader\Exceptions\FileCheckException;
use \ImageManager;
use \ImageType;
use \Module;
use \Validate;

require_once(_PS_ROOT_DIR_ . '/images.inc.php');

class LoaderImage extends Object implements LoaderImageInterface {
	
	/**
	 *
	 * @var \PriceParser\Core\Downloader\AbstractDownloader
	 */
	var $downloader;
	
	var $tmp_path;
	
	/**
	 * 
	 * @param \Product $product
	 * @param array $image_path
	 * @return Image
	* @throws ExporterException
	 * @throws ExporterDataException
	 * @throws ExporterImageException
	 */
	public function load(\Product $product, Array $image_row) {
		
		$this->log(3, 'Загружаем изображение ' . $image_path);
		
		try {
			$file = $this->downloader->download($image_row['url'], $this->tmp_path, null, \PriceParser\Core\Downloader\AbstractDownloader::RETURN_TYPE_FILE_PATH);
		} catch(ConnectException $ex) {
			throw new ExporterException('Ошибка при загрузке файла: ' . $ex->getMessage(), $ex->getCode(), $ex);
		} catch(FileExistsException $ex) {
			throw new ExporterDataException('Ошибка проверки файла: ' . $ex->getMessage(), $ex->getCode(), $ex);
		} catch(FileCheckException $ex) {
			throw new ExporterDataException('Файл не найден: ' . $ex->getMessage(), $ex->getCode(), $ex);
		}
		
		if(!$file || !file_exists($file)) {
			throw new ExporterImageException('Изображение ' . $file . ' не удалось загрузить');
		}
		
		if(function_exists('exif_imagetype') && !exif_imagetype($file)) {
			throw new ExporterImageException('Изображение не является некорректным');
		}
		
		$image = new Image();
		$image->id_product = (int)($product->id);
		
		$this->assignParams($image, $image_row);

		if (!$image->add()) {
			throw new ExporterException('Error while creating additional image');
		}
		else {
			if (!$new_path = $image->getPathForCreation()) {
				throw new ExporterException('An error occurred during new folder creation');
			}
			
			if (!$new_path = $image->getPathForCreation()) {
				throw new ExporterImageException('An error occurred during new folder creation');
			}
			elseif (!\imageResize($file, $new_path.'.'.$image->image_format)) {
				throw new ExporterImageException('An error occurred while copying image.');
			}
			else {
				$imagesTypes = ImageType::getImagesTypes('products');
				foreach ($imagesTypes AS $k => $imageType) {
					if (!\imageResize($file, $new_path . '-' . stripslashes($imageType['name']) . '.' . $image->image_format, $imageType['width'], $imageType['height'], $image->image_format)) {
						throw new ExporterImageException(('An error occurred while copying image:') . ' ' . stripslashes($imageType['name']));
					}
				}
			}
			
			Module::hookExec('watermark', array('id_image' => $image->id, 'id_product' => $product->id));

			if (!$image->update()) {
				throw new ExporterException('Error while updating image status');
			}
			
			$this->log(3, 'изображение сохранено успешно. id = ' . $image->id);
		}
		
		if(file_exists($file)) {
			$unlink_result = unlink($file);
			if($unlink_result) {
				$this->log(2, 'временный файл ' . $file . ' был удалён');
			}
			else {
				$this->log(2, 'Ошибка при удалении временного файла ' . $file);
			}
		}
		
		return $image;
	}
	
	/**
	 * 
	 * @param Image $image
	 * @param array $image_row
	 * @return boolean
	 * @throws ExporterComponentException
	 */
	public function assignParams(Image $image, array $image_row) {
		if(!isset($image_row['position'])) {
			throw new ExporterComponentException('Отсутствует параметр position');
		}
		
		if(!isset($image_row['is_cover'])) {
			throw new ExporterComponentException('Отсутствует параметр is_cover');
		}
		
		$image->position = $image_row['position'];
		$image->cover	 = $image_row['is_cover'];
		
		return true;
	}
	
	/**
	 * 
	 * @param Image $image
	 * @throws ExporterComponentException
	 */
	public function deleteImage(Image $image) {
		if(!Validate::isLoadedObject($image)) {
			throw new ExporterComponentException('Изображение не загружено');
		}
		$image->deleteImage();
		$image->delete();
	}
}
