<?php

namespace PriceParser\Export\Components\PrestaShop14;

use PriceParser\Core\Object\Object;
use PriceParser\Export\Components\Interfaces\LoaderCategoryInterface;
use PriceParser\Core\Tools as PTools;
use PriceParser\Export\Exceptions\ExporterComponentException;

class LoaderCategory extends Object implements LoaderCategoryInterface {
	
	var $id_lang;
	
	/**
	 * 
	 * @param type $id_category
	 * @param type $cat_row
	 * @param type $id_parent
	 * @return \Category
	 * @throws ExporterComponentException
	 */
	public function loadCategory($id_category, $cat_row, $parent_id) {
		if($id_category) {
			$this->log(2, 'по категории вендора ' . $cat_row['id'] . ' найдена категория на сайте: ' . $id_category);

			$category = new \Category($id_category);

		} else {
			$this->log(2, 'по категории вендора ' . $cat_row['id'] . ' отсутствует категория на сайте');
			$category = new \Category();
		}

		$category->id_parent = $parent_id;
		$category->name[$this->id_lang] = $cat_row['name'];

		if(isset($cat_row['uri'])) {
			$category->link_rewrite[$this->id_lang] = PTools::correctIrregSymb($cat_row['uri']);
		} else {
			$category->link_rewrite[$this->id_lang] = PTools::str2url($cat_row['name']);
		}

		$category->doNotRegenerateNTree = true;
		$save_result = $category->save();
		
		if(!$save_result) {
			throw new ExporterComponentException('Не удалось сохранить категорию. Название: ' . $cat_row['name']);
		}
		
		return $category;
	}
	
}
