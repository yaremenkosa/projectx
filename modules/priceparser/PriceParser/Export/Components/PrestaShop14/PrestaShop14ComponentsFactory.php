<?php

namespace PriceParser\Export\Components\PrestaShop14;

use PriceParser\Export\Components\AbstractComponentsFactory;

class PrestaShop14ComponentsFactory extends AbstractComponentsFactory {
	
	/**
	 * 
	 * @return \PriceParser\Export\Components\PrestaShop14\LoaderImage
	 */
	public function createLoaderImage(Array $params = []) {
		return new LoaderImage($params);
	}
	
	/**
	 * 
	 * @param array $params
	 * @return \PriceParser\Export\Components\PrestaShop14\ProcessorImage
	 */
	public function createProcessorImage(array $params = array()) {
		return new ProcessorImage($params);
	}
	
	/**
	 * 
	 * @param array $params
	 * @return \PriceParser\Export\Components\PrestaShop14\LoaderProduct
	 */
	public function createLoaderProduct(array $params = array()) {
		return new LoaderProduct($params);
	}
	
	public function createLoaderCategory(array $params = array()) {
		return new LoaderCategory($params);
	}
}
