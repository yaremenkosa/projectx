<?php

namespace PriceParser\Export\Components\PrestaShop14;

use PriceParser\Core\Object\Object;
use PriceParser\Export\Components\Interfaces\LoaderProductInterface;
use PriceParser\Core\Tools as PTools;
use PriceParser\Core\PriceParserIds;

class LoaderProduct extends Object implements LoaderProductInterface {
	
	var $id_lang;
	
	/**
	 * 
	 * @param \Product $product
	 * @param array $row
	 * @return boolean
	 */
	public function load(\Product $product, Array $row) {
		$clean_description = strip_tags(nl2br($row['description']));
		
		$product->name[$this->id_lang]			= PTools::CutString($row['name'], 128);
		
		$product->link_rewrite[$this->id_lang]	= PTools::CutString(PTools::str2url($row['name']), 128);
		
		$product->id_supplier				= (int)$row['id_supplier'];
		$product->reference					= $row['code'];
		
		$product->description				= $clean_description;
		$product->description_short			= PTools::CutString($clean_description, 800);
		//???
		$product->status					= 0;
		
		if(isset($row['attributes']['weight'])) {
			$product->weight					= $row['attributes']['weight'];
		}
		
		if(isset($row['attributes']['sizex'])) {
			$product->width						= $row['attributes']['sizex'];
		}
		
		if(isset($row['attributes']['sizey'])) {
			$product->height					= $row['attributes']['sizey'];
		}
		
		if(isset($row['attributes']['sizez'])) {
			$product->depth						= $row['attributes']['sizez'];
		}
		
		if(isset($row['price']['wholesale_price'])) {
			$product->wholesale_price			= $row['price']['wholesale_price'];
		} else {
			$product->wholesale_price			= $row['price']['price'];
		}

		if(isset($row['brand_id'])) {
			$brand_id = PriceParserIds::getSiteIdByVendorId($row['brand_id'], PriceParserIds::BRAND_VALUE_ID);

			if($brand_id) {
				$product->id_manufacturer = $brand_id;
			}
		} elseif(isset($row['id_manufacturer'])) {
			$product->id_manufacturer = $row['id_manufacturer'];
		}

		$product->price							= $row['price']['price'];
		
		return $product->save();
	}
	
}
