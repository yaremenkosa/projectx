<?php

namespace PriceParser\Export;

interface Exportable {
	
	public function export(Array $data);
	
	public function getSummary();
	
}
