<?php

namespace PriceParser;

class Autoloader {
	
	private static $_instance;
	
	private $_root_path;
	
	public static function getInstance() {
		if (self::$_instance === null) {
			self::$_instance = new Autoloader();
		}
		return self::$_instance;
	}
	
	private function __construct() {
		$this->_root_path = dirname(__FILE__);
	}
	
	public static function __callStatic($name, $arguments) {
		$object = self::getInstance();
		if (method_exists($object, $name)) {
			return call_user_func_array(array($object, $name),$arguments);
		}
		throw new \Exception('Method ' . $name . ' not found');
	}
	
	protected function init() {

		spl_autoload_register(array($this, 'autoloader'), true, true);
	}
	
	/**
	 * Загрузка класса без неймспейсов
	 * @param type $className имя класса 
	 * @param type $path путь в рамках PriceParser
	 */
	protected function load($className, $path) {
		$path = $this->_root_path . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . $className . '.php';
		if(is_file($path)) {
			include_once $path;
		}
	}
	
	protected function autoloader($className) {
		$namespaces = explode("\\", $className);
		
		//убираем свой собственный namespace
		array_shift($namespaces);
		
		$className = end($namespaces);
		array_pop($namespaces);
		
		$path = $this->_root_path . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $namespaces);
		
		if(is_file($path . DIRECTORY_SEPARATOR . $className . '.php')) {
			include_once $path . DIRECTORY_SEPARATOR . $className . '.php';
		}
	}
}

Autoloader::init();