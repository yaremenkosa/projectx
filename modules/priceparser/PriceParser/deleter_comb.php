<?php
if(!defined('STDIN')) {
	die('Run only in CLI mode');
}

$configs_path = 'Configs' . DIRECTORY_SEPARATOR;

declare(ticks=1);
//pcntl_signal_dispatch();

//специальный костыль для smarty
define('_PS_SMARTY_FAST_LOAD_', true);

require(dirname(__FILE__).'/../../../config/config.inc.php');
ini_set('max_execution_time', 0);
ini_set('memory_limit', '512M');
error_reporting(E_ALL);
require_once 'autoloader.php';

$combinations = Db::getInstance()->executeS('SELECT id_item_site'
	. ' FROM ps_price_parser_ids ids WHERE ids.id_item_type = "' . \PriceParser\Core\PriceParserIds::COMBINATION_TYPE_ID . '"');

echo 'Удаляем комбинации: ' . count($combinations) . PHP_EOL;

foreach($combinations as $comb_row) {
	echo 'Удаление ' . $comb_row['id_item_site'] . PHP_EOL;
	$combination = new Combination($comb_row['id_item_site']);

	if(Validate::isLoadedObject($combination) === false) {
		echo 'Объект не загружен' . PHP_EOL;
		continue;
	}

	$combination->delete();

	Db::getInstance()->execute('DELETE FROM ps_product_attribute_combination WHERE id_product_attribute = ' . (int)$combination->id);
	Db::getInstance()->execute('DELETE FROM ps_product_attribute_image WHERE id_product_attribute = ' . (int)$combination->id);

	Db::getInstance()->execute('DELETE FROM ps_price_parser_ids WHERE id_item_site = ' . (int)$comb_row['id_item_site'] . ' AND id_item_type = ' . \PriceParser\Core\PriceParserIds::COMBINATION_TYPE_ID);

}

$attributes = Db::getInstance()->executeS('SELECT id_item_site'
	. ' FROM ps_price_parser_ids ids WHERE ids.id_item_type = "' . \PriceParser\Core\PriceParserIds::ATTRIBUTE_TYPE_ID . '"');

echo 'Удаляем аттрибуты: ' . count($attributes) . PHP_EOL;
foreach($attributes as $att_row) {
	echo 'Удаление ' . $att_row['id_item_site'] . PHP_EOL;
	$attribute = new Attribute($att_row['id_item_site']);

	if(Validate::isLoadedObject($attribute) === false) {
		echo 'Объект не загружен' . PHP_EOL;
		continue;
	}

	$attribute->delete();

	Db::getInstance()->execute('DELETE FROM ps_price_parser_ids WHERE id_item_site = ' . (int)$att_row['id_item_site'] . ' AND id_item_type = ' . \PriceParser\Core\PriceParserIds::ATTRIBUTE_TYPE_ID);;
}

$attributesGroups = Db::getInstance()->executeS('SELECT id_item_site'
	. ' FROM ps_price_parser_ids ids WHERE ids.id_item_type = "' . \PriceParser\Core\PriceParserIds::ATTRIBUTE_GROUP_TYPE_ID . '"');
echo 'Удаляем группы аттрибутов: ' . count($attributesGroups) . PHP_EOL;
foreach($attributesGroups as $att_g_row) {
	echo 'Удаление ' . $att_g_row['id_item_site'] . PHP_EOL;
	$attributeGroup = new AttributeGroup($att_g_row['id_item_site']);

	if(Validate::isLoadedObject($attributeGroup) === false) {
		echo 'Объект не загружен' . PHP_EOL;
		continue;
	}

	$attributeGroup->delete();

	Db::getInstance()->execute('DELETE FROM ps_price_parser_ids WHERE id_item_site = ' . (int)$att_g_row['id_item_site'] . ' AND id_item_type = ' . \PriceParser\Core\PriceParserIds::ATTRIBUTE_GROUP_TYPE_ID);;
}