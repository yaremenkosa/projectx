<?php
if(!defined('STDIN')) {
	die('Run only in CLI mode');
}
 

declare(ticks=1);
//pcntl_signal_dispatch();

require(dirname(__FILE__).'/../../../config/config.inc.php');
//ini_set('max_execution_time', 1);
//set_time_limit(1);
error_reporting(E_ALL);
require_once 'autoloader.php';

$cronController = new PriceParser\Cron\CronController();
echo date('d.m.Y H:i:s');
$cronController->run();