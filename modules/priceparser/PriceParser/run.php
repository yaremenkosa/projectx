<?php

if(!defined('STDIN')) {
	die('Run only in CLI mode');
}

phpinfo();

$configs_path = 'Configs' . DIRECTORY_SEPARATOR;
error_reporting(E_ALL);
declare(ticks=1);
//pcntl_signal_dispatch();

//специальный костыль для smarty
define('_PS_SMARTY_FAST_LOAD_', true);

require(dirname(__FILE__).'/../../../config/config.inc.php');
//ini_set('max_execution_time', 1);
//set_time_limit(1);
ini_set('memory_limit', '-1');
error_reporting(E_ALL);
require_once 'autoloader.php';

$options = getopt('', ['runconfig:','force', 'log_id:', 'start:']);
var_dump($options);
//$force = getopt('', ['force:']);

if(isset($options['start'])) {
    define('_START_IMPORT_', $options['start']);
}


if(isset($options['runconfig'])) {
	$filename = $options['runconfig'];
	$runconfig = \PriceParser\Core\Tools::loadConfig($filename);
//	if(file_exists($configs_path . $filename)) {
//		$runconfig = include($configs_path . $options['runconfig']);
//	}
}


if(!isset($runconfig) || empty($runconfig)) {
	die('Config hasnt been loaded!');
}

if(isset($options['force'])) {
	$runconfig['is_force'] = true;
}

if(isset($options['log_id'])) {
	$runconfig['log_id'] = $options['log_id'];
}

$executor = new \PriceParser\Core\Executor($runconfig);
$executor->start($runconfig);