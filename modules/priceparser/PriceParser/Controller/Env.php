<?php

namespace PriceParser\Controller;

class Env {
	/**
	 *
	 * @var \PriceParser\Core\Logger\AbstractLogger
	 */
	public static $logger;
	
	/**
	 *
	 * @var \PriceParser\Core\Logger\AbstractLogger
	 */
	public static $summary;
	
	public static $timer;
	
	/**
	 *
	 * @var \PriceParser\Core\RunSession
	 */
	public static $runSession;
	
	/**
	 *
	 * @var \PriceParser\Core\PriceParserStatus
	 */
	public static $status;
	
	/**
	 *
	 * @var array
	 */
	public static $config;
	
	/**
	 *
	 * @var \PriceParser\Core\Db\DbInterface
	 */
	private static $db;
	
	/**
	 * 
	 * @return \PriceParser\Core\Adapters\Db\DbInterface
	 * @throws \PriceParser\Core\Exceptions\PriceParserException
	 */
	public static function getDb() {
		if(static::$db instanceof \PriceParser\Core\Db\DbInterface) {
			return static::$db;
		}
		
		$db_class_name = 'PriceParser\Core\Adapters\Db\\' . \PriceParser\Core\Tools::getPrestaShopVersionPrefix() . 'Db';
		
		if(class_exists($db_class_name)) {
			static::$db = $db_class_name::getInstance();
			
		}
		if(!(static::$db instanceof \PriceParser\Core\Adapters\Db\DbInterface)) {
			throw new \PriceParser\Core\Exceptions\PriceParserException('Не удалось получить адаптер прослойки базы данных');
		}
		
		return static::$db;
	}
}
