<?php
namespace PriceParser\Controller;

use PriceParser\Controller\Env;
use PriceParser\Core\Object\ObjectFactory;
use PriceParser\Core\RunSession;
use PriceParser\Core\PriceParserStatus;
use PriceParser\Core\PriceParserIds;

abstract class AbstractController extends \PriceParser\Core\Object\Object {
	
	protected $config;
	
	public function init(Array $config) {
		$this->config = $config;
		
		$this->runtimeInit();
		
		return $this;
	}
	
	public function run() {
		$result = $this->_run();
		
		Env::$status->saveOnFinish();
		Env::$runSession->clear();
		
		return $result;
	}
	
	abstract public function _run();
	
	public function __destruct() {
		$this->log(1, 'выполнение завершено.');
		if(Env::$timer) {
			$this->log(1, 'Время работы: ' . Env::$timer->stop()->formatTime() . ' с');
		}
	}
	
	public function runtimeInit() {
		Env::$config = $this->config;
		
		if($this->config['runtime']) {
			if(isset($this->config['runtime']['logger'])) {
				$_logger = ObjectFactory::getObject($this->config['runtime']['logger']);

				if($_logger instanceof \PriceParser\Core\Logger\AbstractLogger) {
					Env::$logger = $_logger;
				} else {
					throw new \PriceParser\Core\Exceptions\PriceParserException('Logger is wrong type');
				}
			}
			if(isset($this->config['runtime']['summary'])) {
				$_summary = ObjectFactory::getObject($this->config['runtime']['summary']);

				if($_summary instanceof \PriceParser\Core\Logger\AbstractLogger) {
					Env::$summary = $_summary;
				} else {
					throw new \PriceParser\Core\Exceptions\PriceParserException('Logger is wrong type');
				}
			}
			
			if(isset($this->config['runtime']['execution_timer'])) {
				Env::$timer = new \PriceParser\Core\Timer();
				Env::$timer->start();
			}
		}
		
		Env::$runSession = RunSession::_instance(sha1(serialize($this->config)));
		
		Env::$status = PriceParserStatus::_instance($this->config['id_vendor']);
		
		PriceParserIds::setVendorId($this->config['id_vendor']);
	}
}
