<?php

namespace PriceParser\Controller;

use PriceParser\Core\Object\ObjectFactory;
use PriceParser\Controller\Env;

class VendorSiteController extends AbstractController {
	
	var $clear_on_finish_path;
	
	public function _run() {
		
		$import_result = ObjectFactory::getObject($this->config['import'])->run();
		
		$export = ObjectFactory::getObject($this->config['export'])->run($import_result);
		
		if($this->clear_on_finish_path) {
			\PriceParser\Core\Tools::clearDirectory($this->clear_on_finish_path);
		}
		
//		var_dump($import);
//		var_dump($export);
		
		return $export;
	}
	
}
