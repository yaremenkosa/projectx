<?php

namespace PriceParser\Core\Exceptions;

class ObjectFactoryException extends PriceParserException {}
