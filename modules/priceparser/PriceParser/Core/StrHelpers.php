<?php

namespace PriceParser\Core;

class StrHelpers {
	static function formatFileSize($size) {
		return number_format($size, 0, '', ' ');
	}
	
	static function getClassName($classname) {
		$namespaces = explode('\\', $classname);
		
		return end($namespaces);
	}
}
