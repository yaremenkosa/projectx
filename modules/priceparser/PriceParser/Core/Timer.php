<?php
namespace PriceParser\Core;

use PriceParser\Core\Object;

class Timer {
	
	protected $start_time;
	protected $total_time;
	
	function start() {
		$this->start_time = microtime(true);
		
		return $this;
	}
	
	function stop() {
		$this->total_time = microtime(true) - $this->start_time;
		
		return $this;
	}
	
	function getTime() {
		return $this->total_time;
	}
	
	function formatTime() {
		return number_format($this->total_time, 3, '.', ' ');
	}
	
}
