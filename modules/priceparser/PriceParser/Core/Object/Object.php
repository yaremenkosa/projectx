<?php

namespace PriceParser\Core\Object;

use PriceParser\Core\Exceptions\ObjectException;
use PriceParser\Controller\Env;

abstract class Object {
	
	protected $params;
	
	public function __construct(Array $params = []) {
		$this->setParams($params);
	}
	
	public function setParams(Array $params) {
		if(!empty($params)) {
			foreach($params as $key => $val) {
				$this->$key = $val;
			}
		}
		$this->params = $params;
	}
	
	public function __set($name, $value) {
		if(!property_exists($this, $name)) {
			throw new ObjectException('Property ' . $name . ' doesnt exists in ' . get_called_class());
		}
		
		$this->$name = $val;
	}
	
	protected function log($verb_mode, $message) {
//		$message = \PriceParser\Core\StrHelpers::getClassName(get_called_class()) . ': ' . $message;

        if($verb_mode == 9) {
            Env::$summary->log($verb_mode, $message);
        } else {
            $message = get_called_class() . ': ' . $message;
            Env::$logger->log($verb_mode, $message);
        }
	}
	
}
