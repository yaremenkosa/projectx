<?php

namespace PriceParser\Core\Object;

use PriceParser\Core\Exceptions\ObjectFactoryException;

class ObjectFactory {
	
//	static $objects = [];
	
	/**
	 * 
	 * @param type $classname
	 * @return \PriceParser\Core\classname
	 * @throws Exceptions\ObjectFactoryException
	 */
	public static function getObject($config) {

		if(!isset($config['class'])) {
			throw new Exceptions\ObjectFactoryException('Class of object is not isset!');
		}
		
		if(!class_exists($config['class'])) {
			throw new ObjectFactoryException('Class ' . $config['class'] . ' not found!');
		}
		
		$class_name_clear = \PriceParser\Core\StrHelpers::getClassName($config['class']);
		
		try {
			$object = new $config['class'](isset($config['params']) ? $config['params'] : []);
		} catch(Exceptions\ObjectException $ex) {

			throw new ObjectFactoryException('Error on creating object ' . $config['class'] . ': ' . $ex->getMessage(), $ex->getCode(), $ex);
		}
		
//		static::$objects[$class_name_clear] = $object;
		
		return $object;
	}
	
}
