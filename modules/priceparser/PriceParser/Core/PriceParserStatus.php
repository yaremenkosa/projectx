<?php

namespace PriceParser\Core;

use PriceParser\Controller\Env;

class PriceParserStatus {
	
	private static $_instances = [];
	
	private $save_on_finish;
	
	private $id_vendor;
	
	public static function _instance($id_vendor) {
		if(isset(static::$_instances[$id_vendor])) {
			return static::$_instances[$id_vendor];
		} else {
			static::$_instances[$id_vendor] = new static($id_vendor);
		}
		
		return static::$_instances[$id_vendor];
	}
	
	private function __construct($id_vendor) {
		if(!$id_vendor) {
			throw new Exceptions\PriceParserException('Not assigned id_vendor');
		}
		
		$this->id_vendor = $id_vendor;
	}
	
	function saveOnFinish() {
		if(!empty($this->save_on_finish)) {
			foreach($this->save_on_finish as $item) {
				$this->saveDb($item['name'], $item['value']);
			}
		}
	}
	
	function get($name) {
		return Env::getDb()->getValue('SELECT value FROM ' . _DB_PREFIX_ . 'price_parser_status'
				. ' WHERE name = "' . \pSQL($name) . '"'
				. ' AND id_vendor = "' . \pSQL($this->id_vendor) . '"');
	}
	
	function save($name, $value, $on_finish = false) {
		if($on_finish) {
			$this->save_on_finish[] = [
				'name' => $name,
				'value' => $value,
			];
		} else {
			$this->saveDb($name, $value);
		}
	}
	
	private function saveDb($name, $value) {
		Env::getDb()->execute('INSERT INTO ' . _DB_PREFIX_ . 'price_parser_status (name, value, id_vendor)'
				. ' VALUES ("' . \pSQL($name) . '", "' . \pSQL($value) . '","' . \pSQL($this->id_vendor) . '")'
				. ' ON DUPLICATE KEY UPDATE value = "' . \pSQL($value) . '"');
	}
	
}
