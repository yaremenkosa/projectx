<?php

namespace PriceParser\Core\Downloader;

use Exceptions\DownloaderException;
use PriceParser\Core\Object\Object;

abstract class AbstractDownloader extends Object {
	
	const RETURN_TYPE_RESULT		 = 1;
	const RETURN_TYPE_FILE_PATH		 = 2;
	const RETURN_TYPE_FILE_CONENT	 = 3;

	protected $max_filesize;
	protected $save_file_path;
	protected $auto_open = false;

	public function getMaxFilesize() {
		return $this->max_filesize;
	}
	
	public function getSaveFilePath() {
		return $this->save_file_path;
	}
	
	function getAutoOpen() {
		return $this->auto_open;
	}

	/**
	 * 
	 * @param int $max_filesize
	 * @return AbstractDownloader
	 */
	public function setMaxFilesize($max_filesize) {
		$this->max_filesize = $max_filesize;
		
		return $this;
	}
	
	/**
	 * 
	 * @param string $save_file_path
	 * @return AbstractDownloader
	 */
	public function setSaveFilePath($save_file_path) {
		$this->save_file_path = $save_file_path;
		
		return $this;
	}

	function setAutoOpen($auto_open) {
		$this->auto_open = $auto_open;
	}
		
	/**
	 * Открытие сессии
	 * @return AbstractDownloader
	 */
	abstract public function open();
	
	/**
	 * Закрытие сессии
	 * @return AbstractDownloader
	 */
	abstract public function close();
	
	/**
	 * Проверка на открытость сессии
	 */
	abstract public function isOpened();
	
	public final function download($url, $path = null, $save_full_path = null, $return_type) {
		if(!$this->isOpened()) {
			if($this->getAutoOpen()) {
				$this->open();;
			} else {
				throw new Exceptions\SessionException('Сессия не открыта');
			}
		}
		
		if(!$save_full_path) {
			if($path) {
				$download_path = $path;
			} else {
				$download_path = $this->getSaveFilePath();
			}

			if(!$download_path) {
				throw new Exceptions\ParametersException('Не указан каталог для загрузки файлов');
			}

			if(!file_exists($download_path)) {
				throw new Exceptions\ParametersException('Указанный каталог для загрузки не существует, либо недоступен: ' . $download_path);
			}

			//проверки
			if(!$this->validateLoadingFile($url)) {
				throw new Exceptions\DownloaderException('Неизвестная ошибка при загрузке файла: ' . $url);
			}
			
			//если имя файла для сохранения не указано, то пытаемся получить его из урла
			if(preg_match('/([^\/^\.]+\.[^\/^\.]+)$/i', $url, $res)) {
				$filename = $res[1];

				$save_full_path = $download_path . $filename;
			} else {
				throw new Exceptions\ParametersException('Не указано имя файла для сохранения и не удалось получить имя файла из url для скачивания');
			}
		}
		
		return $this->_download($url, $save_full_path, $return_type);
	}
	
	abstract protected function validateLoadingFile($url);
		
	abstract protected function _download($url, $save_file_name, $return_content);
	
	function __destruct() {
		if($this->isOpened()) {
			$this->close();
		}
		
		return true;
	}

	public function searchByPattern($filename_pattern, $path) {
		return [];
	}
	
}
