<?php

/**
 * Исключение проверки скачиваемого файла по параметрам
 */

namespace PriceParser\Core\Downloader\Exceptions;

class FileCheckException extends DownloaderException {
	CONST SIZE_EXCEED = 1;
	CONST FILE_NOT_EXISTS = 2;
	
	
}
