<?php

namespace PriceParser\Core\Downloader\Exceptions;

class SessionException extends DownloaderException {}
