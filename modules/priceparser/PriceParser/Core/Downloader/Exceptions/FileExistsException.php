<?php

namespace PriceParser\Core\Downloader\Exceptions;

class FileExistsException extends DownloaderException {}
