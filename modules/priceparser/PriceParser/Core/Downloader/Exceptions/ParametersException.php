<?php

namespace PriceParser\Core\Downloader\Exceptions;

class ParametersException extends DownloaderException {}
