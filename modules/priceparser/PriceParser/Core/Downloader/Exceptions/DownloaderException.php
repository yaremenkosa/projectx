<?php

namespace PriceParser\Core\Downloader\Exceptions;

class DownloaderException extends \PriceParser\Core\Exceptions\PriceParserException {}
