<?php

namespace PriceParser\Core\Downloader\Exceptions;

class ConnectException extends DownloaderException {}
