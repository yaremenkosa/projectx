<?php

namespace PriceParser\Core\Downloader;

class CurlDownloader extends AbstractDownloader {
	
	protected $userpwd;
	protected $headers;
	protected $timeout = 300;
	protected $connect_timeout = 10;
	
	protected $params = ['userpwd','headers'];
	
//	public function setConnectionParams(array $params) {
//		foreach($this->params as $param_name) {
//			if(isset($params[$param_name])) {
//				if(property_exists($this, $param_name)) {
//					$this->$param_name = $params[$param_name];
//				} else {
//					throw new Exceptions\ParametersException('Параметр ' . $param_name . ' не существует');
//				}
//			}
//		}
//		
//		return $this;
//	}
	
	/**
	 * 
	 * @return CurlDownloader
	 */
	public function open() {
		return $this;
	}
	
	/**
	 * 
	 * @return CurlDownloader
	 */
	public function close() {
		return $this;
	}
	
	public function isOpened() {
		return true;
	}
	
	/**
	 * 
	 * @param type $url
	 * @param type $save_file_name
	 * @param type $return_content
	 * @return boolean
	 * @throws Exceptions\DownloaderException
	 */
	protected function _download($url, $save_file_name, $return_type) {
		$curl = $this->initCurl($url);
		
		$result = curl_exec($curl);
		
		$err_no = curl_errno($curl);

		curl_close($curl);
		
		if($err_no !== 0) {
			throw new Exceptions\ConnectException('Curl error, code: ' . $err_no . ', url: ' . $url);
		}
		
		file_put_contents($save_file_name, $result);
		
		switch($return_type) {
			case static::RETURN_TYPE_FILE_PATH:
				return $save_file_name;
				break;
			
			case static::RETURN_TYPE_FILE_CONENT:
				return $result;
				break;
			
			case static::RETURN_TYPE_RESULT:
			default:
				return true;
				break;
		}
		
		return true;
	}
	
	protected function validateLoadingFile($url) {
		$curl = $this->initCurl($url);

		curl_setopt($curl,	CURLOPT_NOBODY, true);
		curl_setopt($curl,	CURLOPT_HEADER, true);

		$result = curl_exec($curl);
		
		$err_no = curl_errno($curl);

		$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);
		
		if($err_no !== 0) {
			throw new Exceptions\ConnectException('Curl error, code: ' . $err_no . ', url: ' . $url);
		}
		
		if($http_code !== 200) {
			throw new Exceptions\FileCheckException('Ошибка при загрузке файла ' . $url . ', http code: ' . $http_code);
		}
		
		$max_size = $this->getMaxFilesize();
		
		if($max_size) {
			$size = false;

			if(preg_match('/Content-Length: ([0-9]+)/im',$result, $vals)) {
				$file_size = (int)$vals[1];
				
				if($file_size > $max_size) {
					throw new Exceptions\FileCheckException('Загружаемый файл ' . $url . ' превышает разрешённый лимит. Размер файла: ' . $file_size . ', лимит: ' . $max_size, Exceptions\FileCheckException::SIZE_EXCEED);
				}
			}
		}
		
		return true;
	}
	
	protected function initCurl($url) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL,				$url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,	true);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT,	$this->connect_timeout);
		curl_setopt($curl, CURLOPT_TIMEOUT,			$this->timeout);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION,	true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		if($this->userpwd) {
			curl_setopt($curl,	CURLOPT_USERPWD,		$this->userpwd);
		}
		
		if(is_array($this->headers) && !empty($this->headers)) {
			curl_setopt($curl,	CURLOPT_HTTPHEADER,		$this->headers);
		}
		
		return $curl;
	}
	
}
