<?php


namespace PriceParser\Core\Downloader;


class FtpDownloader extends AbstractDownloader {
	
//	CONST DEFAULT_PORT = 21;
	
	protected $conn_id;
	
	protected $host;
	protected $login;
	protected $password;
	protected $pasv = false;
//	protected $port;
	
//	public function setConnectionParams(array $params) {
//		if(!isset($params['host']) || !$params['host']) {
//			throw new Exceptions\ParametersException('Некорректные параметры соединения: не указан host');
//		}
//		
//		$this->host = $params['host'];
//		
//		if(!isset($params['login']) || !$params['login']) {
//			throw new Exceptions\ParametersException('Некорректные параметры соединения: не указан login');
//		}
//		
//		$this->login = $params['login'];
//		
//		if(!isset($params['password'])) {
//			throw new Exceptions\ParametersException('Некорректные параметры соединения: не указан пароль');
//		}
//		
//		$this->password = $params['password'];
//		
////		$this->port = (isset($params['port']) ? $params['port'] : static::DEFAULT_PORT);
//		
//		return $this;
//	}
	
	/**
	 * 
	 * @return \PriceParser\Core\Downloader\FtpDownloader
	 * @throws Exceptions\ParametersException
	 * @throws Exceptions\ConnectException
	 */
	public function open() {
		if(!$this->host || !$this->login) {
			throw new Exceptions\ParametersException('Не заданы параметры соединения');
		}
		
		$conn_id = ftp_connect($this->host);
		
		if(!$conn_id) {
			throw new Exceptions\ConnectException('Ошибка при подключении к ftp-серверу: ' . $this->host);
		}
		
		$this->conn_id = $conn_id;
		
		$login_result = ftp_login($this->conn_id, $this->login, $this->password);

        ftp_pasv($conn_id, true);
		
		if(!$login_result) {
			throw new Exceptions\ConnectException('Ошибка при авторизации на ftp-сервере: ' . $this->host);
		}
		
		if($this->pasv) {
			ftp_pasv($this->conn_id, true);
		}
		
		return $this;
	}
	
	/**
	 * 
	 * @return \PriceParser\Core\Downloader\FtpDownloader
	 * @throws Exceptions\ConnectException
	 */
	public function close() {
		if(!$this->isOpened()) {
			throw new Exceptions\ConnectException('Соединение отсутствует');
		}
		
		ftp_close($this->conn_id);
		
		return $this;
	}
	
	/**
	 * 
	 * @return bool
	 */
	public function isOpened() {
		return is_resource($this->conn_id);
	}
	
	protected function _download($url, $save_file_name, $return_type) {
		if(!$this->isOpened()) {
			throw new Exceptions\ConnectException('Соединение отсутствует');
		}
		
		$parse_result = parse_url($url);
		
		if(!$parse_result) {
			throw new Exceptions\DownloaderException('Некорректный url для загрузки: ' . $url);
		}
		
		$get_result = ftp_get($this->conn_id, $save_file_name, $url, FTP_BINARY);
		
		if($get_result) {
			
			switch($return_type) {
				case static::RETURN_TYPE_FILE_PATH:
					return $save_file_name;
					break;

				case static::RETURN_TYPE_FILE_CONENT:
					return file_get_contents($save_file_name);
					break;

				case static::RETURN_TYPE_RESULT:
				default:
					return $get_result;
					break;
			}
		}
		
		return $get_result;
	}
	
	protected function validateLoadingFile($url) {
		$file_size = $this->getFileSize($url);
		
		if($file_size === -1) {
			throw new Exceptions\FileCheckException('Загружаемый файл ' . $url . ' отсутствует', Exceptions\FileCheckException::FILE_NOT_EXISTS);
		}
		
		$max_size = $this->getMaxFilesize();
		if($max_size) {
			if($file_size > $max_size) {
				throw new Exceptions\FileCheckException('Загружаемый файл ' . $url . ' превышает разрешённый лимит. Размер файла: ' . $file_size . ', лимит: ' . $max_size, Exceptions\FileCheckException::SIZE_EXCEED);
			}
		}
		
		return true;
	}
	
	public function getFileSize($url) {
		if(!$this->isOpened()) {
			throw new Exceptions\ConnectException('Соединение отсутствует');
		}
		
		return ftp_size($this->conn_id, $url);
	}

	public function searchByPattern($filename_pattern, $path) {
		if(!$this->isOpened()) {
			throw new Exceptions\ConnectException('Соединение отсутствует');
		}

		$list = ftp_nlist($this->conn_id, $path);

		if($list === false) {
			throw new Exceptions\DownloaderException('Ошибка при получении списка файлов в дирекории ftp:' . $path);
		}

		$result = [];

		if(!empty($list)) {
			foreach($list as $item) {
				if(preg_match('/' . $filename_pattern . '/', $item)) {
					$result[] = $item;
				}
			}
		}
		
		return $result;
	}
	
}
