<?php

namespace PriceParser\Core\Adapters\Db;

class PrestaShop14Db implements DbInterface {
	
	static protected $_instance;
	
	/**
	 *
	 * @var \Db
	 */
	protected $db;

	protected function __construct($master = true) {
		$this->db = \Db::getInstance($master);
	}

	/**
	 * 
	 * @param boolean $master
	 * @return DbInterface
	 */
	public static function getInstance($master = true) {
		if(static::$_instance === null) {
			static::$_instance = new static($master = true);
		}
		
		return static::$_instance;
	}

	public function insert($table, $data, $null_values = false, $use_cache = true, $type = null, $add_prefix = true) {
		if (!$data && !$null_values)
			return true;

		if ($add_prefix)
			$table = _DB_PREFIX_.$table;
		
		$insert_keyword = 'INSERT';
		

		// Check if $data is a list of row
		$current = current($data);
		if (!is_array($current) || isset($current['type']))
			$data = array($data);

		$keys = array();
		$values_stringified = array();
		foreach ($data as $row_data)
		{
			$values = array();
			foreach ($row_data as $key => $value)
			{
				if (isset($keys_stringified))
				{
					// Check if row array mapping are the same
					if (!in_array("`$key`", $keys))
						throw new PrestaShopDatabaseException('Keys form $data subarray don\'t match');
				}
				else
					$keys[] = '`'.bqSQL($key).'`';

				if (!is_array($value))
					$value = array('type' => 'text', 'value' => $value);
				if ($value['type'] == 'sql')
					$values[] = $value['value'];
				else
					$values[] = $null_values && ($value['value'] === '' || is_null($value['value'])) ? 'NULL' : "'{$value['value']}'";
			}
			$keys_stringified = implode(', ', $keys);
			$values_stringified[] = '('.implode(', ', $values).')';
		}

		$sql = $insert_keyword.' INTO `'.$table.'` ('.$keys_stringified.') VALUES '.implode(', ', $values_stringified);
		
		return (bool)$this->db->execute($sql, $use_cache);
	}
	
	public function delete($table, $where = '', $limit = 0, $use_cache = true, $add_prefix = true) {
		return $this->db->delete($table, $where, $limit, $use_cache, $add_prefix);
	}
	
	public function getValue($sql, $use_cache = true) {
		return $this->db->getValue($sql, $use_cache);
	}
	
	public function execute($sql, $use_cache = true) {
		return $this->db->execute($sql, $use_cache);
	}
	
	public function executeS($sql, $array = true, $use_cache = true) {
		return $this->db->executeS($sql, $array = true, $use_cache = true);
	}
	
	public function getRow($sql, $use_cache = true) {
		return $this->db->getRow($sql, $use_cache);
	}
	
}
