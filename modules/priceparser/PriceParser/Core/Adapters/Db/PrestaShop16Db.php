<?php

namespace PriceParser\Core\Adapters\Db;

class PrestaShop16Db implements DbInterface {
	
	static protected $_instance;
	
	/**
	 *
	 * @var \Db
	 */
	protected $db;

	protected function __construct($master = true) {
		$this->db = \Db::getInstance($master);
	}

	/**
	 * 
	 * @param boolean $master
	 * @return DbInterface
	 */
	public static function getInstance($master = true) {
		if(static::$_instance === null) {
			static::$_instance = new static($master = true);
		}
		
		return static::$_instance;
	}

	public function insert($table, $data, $null_values = false, $use_cache = true, $type = \Db::INSERT, $add_prefix = true) {
		return $this->db->insert($table, $data, $null_values, $use_cache, $type, $add_prefix);
	}
	
	public function delete($table, $where = '', $limit = 0, $use_cache = true, $add_prefix = true) {
		return $this->db->delete($table, $where, $limit, $use_cache, $add_prefix);
	}
	
	public function getValue($sql, $use_cache = true) {
		return $this->db->getValue($sql, $use_cache);
	}
	
	public function execute($sql, $use_cache = true) {
		return $this->db->execute($sql, $use_cache);
	}
	
	public function executeS($sql, $array = true, $use_cache = true) {
		return $this->db->executeS($sql, $array = true, $use_cache = true);
	}
	
	public function getRow($sql, $use_cache = true) {
		return $this->db->getRow($sql, $use_cache);
	}
	
}
