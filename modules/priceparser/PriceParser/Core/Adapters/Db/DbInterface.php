<?php

namespace PriceParser\Core\Adapters\Db;

interface DbInterface {
	
	public static function getInstance($master = true);

	public function insert($table, $data, $null_values = false, $use_cache = true, $type = Db::INSERT, $add_prefix = true);
	
	public function delete($table, $where = '', $limit = 0, $use_cache = true, $add_prefix = true);
	
	public function getValue($sql, $use_cache = true);
	
	public function execute($sql, $use_cache = true);
	
	public function executeS($sql, $array = true, $use_cache = true);
	
	/**
	 * @param mixed $sql the select query (without "LIMIT 1")
	 * @param bool $use_cache find it in cache first
	 * @return array associative array of (field=>value)
	 */
	public function getRow($sql, $use_cache = true);
	
}
