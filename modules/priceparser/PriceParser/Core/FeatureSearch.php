<?php
/**
 * Поиск характеристик и их значений в базе prestashop
 */

namespace PriceParser\Core;

use PriceParser\Controller\Env;

class FeatureSearch {
	
	private static $_instances = [];
	
	private $found_features;
	
	private $id_lang;
	
	private function __construct($id_lang = null) {
		$this->id_lang = $id_lang;
	}
	
	/**
	 * 
	 * @param type $id_lang
	 * @return FeatureSearch
	 */
	public static function _instance($id_lang = null) {
		if(!$id_lang) {
			$id_lang = \Configuration::get('PS_LANG_DEFAULT');
		}
		if(!isset(static::$_instances[$id_lang])) {
			static::$_instances[$id_lang] = new static($id_lang);
		}
		
		return static::$_instances[$id_lang];
	}
	
	public function searchFeature($name, $use_cache = true) {
		return $this->getData($name, $use_cache, $this->found_features, function($search_name) {
			return Env::getDb()->getValue('SELECT id_feature FROM ' . _DB_PREFIX_ . 'feature_lang WHERE name = "' . \pSQL($search_name) . '" AND id_lang = ' . ($this->id_lang));
		});
	}
	
	public function searchFeatureValue($name, $use_cache = true) {
		return $this->getData($name, $use_cache, $this->found_features, function($search_name) {
			return Env::getDb()->getValue('SELECT id_feature_value FROM ' . _DB_PREFIX_ . 'feature_value_lang WHERE value = "' . \pSQL($search_name) . '" AND id_lang = ' . ($this->id_lang));
		});
	}
	
	private function getData($name, $use_cache, &$cache_array, $search_func) {
		if($use_cache) {
			$key = base64_encode($name);

			if(isset($cache_array[$key])) {
				return $cache_array[$key];
			}
		}
		
		$result = $search_func($name);
		
		if($use_cache) {
			$cache_array[base64_encode($name)] = $result;
		}
		
		return $result;
	}
	
	
	
}
