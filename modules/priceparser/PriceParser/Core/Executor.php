<?php

namespace PriceParser\Core;

use PriceParser\Controller\Env;

class Executor {
	var $config;
	
	var $lock_resource;
	
	var $running = false;
	
	//костыль((
	var $destructed = false;
	
	function __construct($config) {
		$this->config = $config;
		
		$this->registerSignals();
		
		if(!isset($config['id_vendor'])) {
			die('Not assigned id_vendor');
		}
	}
	
	function start() {
		$this->registerErrorHandler();
		
		$this->running = true;
		
		echo 'Execution started. Pid: ' . getmypid() . PHP_EOL;
		
		$result = null;
		
		if(isset($this->config['controller'])) {
			$controller = new $this->config['controller']['class']();
			
			try {
				$result = $controller->init($this->config)->run();
			} catch(\Exception $ex) {
				echo 'Caught exception: ' . $ex->getMessage() . ', code: ' . $ex->getCode()  . ', trace: ' . $ex->getTraceAsString() . PHP_EOL;
				
//				throw new \PriceParser\Core\Exceptions\PriceParserException('Runtime error: ' . $ex->getMessage(), $ex->getCode(), $ex);
				die(1);
			}
		} else {
//			throw new Exceptions\RunnerException('Controller is not defined');
			echo 'Controller is not defined' . PHP_EOL;
			die(1);
		}
		
//		Env::$runSession->clear();
		
//		echo 'Saving status' . PHP_EOL;
//		Env::$status->saveOnFinish();
//		
//		echo 'Clearing session' . PHP_EOL;
//		Env::$runSession->clear();
		
		return $result;
	}
	
	protected function registerErrorHandler() {
		set_error_handler(array($this, 'errorHandler'));
	}
	
	public function errorHandler($errno, $errstr, $errfile, $errline) {
		if (error_reporting() === 0) {
			return false;
		}
		
		throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
	}
	
	protected function registerSignals() {
		pcntl_signal_dispatch();

		pcntl_signal(SIGTERM, array($this, 'signalHandler'));
		pcntl_signal(SIGCHLD, array($this, 'signalHandler'));
		pcntl_signal(SIGALRM, array($this, 'signalHandler'));
		pcntl_signal(SIGINT, array($this, 'signalHandler'));
		
		register_shutdown_function(array($this, 'shutdownHandler'));
	}
	
	/**
	 * Обработчик сигналов
	 * @param int $signo
	 * @param int $pid
	 * @param int $status
	 * @return boolean
	 */
	public function signalHandler($signo) {
		if($signo === SIGCHLD) {
			//на сообщения от "детей" не реагируем
			return;
		}
		echo 'Execution aborted. Signal no: ' . $signo . ' Pid: ' . getmypid() . PHP_EOL;
		
		echo 'Saving session' . PHP_EOL;
		Env::$runSession->save();
		
		exit(1);
	}
	
	public function shutdownHandler() {
		$this->__destruct();
		echo 'Execution shutdown.' . PHP_EOL;
		
//		ObjectFactory::$objects['ParserXml']->__destruct();
		
		exit;
	}
	
	function __destruct() {
		if($this->destructed) {
			return ;
		}
		
		echo 'Desctructing executor' . PHP_EOL;
		
		$this->destructed = true;
		
		if($this->running) {
			if(isset($this->config['clear_tmp_path']) && $this->config['clear_tmp_path']) {
				$cleared_count = Tools::clearDirectory($this->config['clear_tmp_path']);
				echo 'Cleared temp directory ' . $this->config['clear_tmp_path'] . '. Deleted ' . $cleared_count . ' files';
			}
		}
	}
}
