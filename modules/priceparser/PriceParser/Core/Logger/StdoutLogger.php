<?php

namespace PriceParser\Core\Logger;

class StdoutLogger extends AbstractLogger {
	
	var $log_file;
	
	var $log_file_resource;
	
	function __construct(array $params = array()) {
		parent::__construct($params);
		
		if($this->log_file) {
			$this->log_file_resource = fopen($this->log_file, 'a');
		}
	}
	
	protected function logSpawn($message) {
		echo $message . PHP_EOL;
		
		if($this->log_file_resource) {
			fwrite($this->log_file_resource, $message . PHP_EOL);
		}
	}
	
	function __destruct() {
		echo 'destructing logger!' . PHP_EOL;
		if($this->log_file_resource) {
			fclose($this->log_file_resource);
		}
	}
	
}
