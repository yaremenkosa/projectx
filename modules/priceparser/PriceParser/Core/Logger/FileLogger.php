<?php

namespace PriceParser\Core\Logger;
use PriceParser\Controller\Env;

class FileLogger extends AbstractLogger {
	
	var $log_file;
	
	var $log_file_resource;
	
	var $filename_is_date = false;
	
	function __construct(array $params = array()) {
		parent::__construct($params);
		
		if($this->filename_is_date) {
			$filename = strftime($this->log_file);
		} elseif(isset(Env::$config['log_id']) && Env::$config['log_id']) {
			$filename = str_replace('%log_id%', Env::$config['log_id'], $this->log_file);
		} else {
			$filename = $this->log_file;
		}
		
		if($this->log_file) {
			$this->log_file_resource = fopen($filename, 'w');
		}
	}
	
	protected function logSpawn($message) {
		if($this->log_file_resource) {
			fwrite($this->log_file_resource, $message . PHP_EOL);
		}
		return true;
	}
	
	function __destruct() {
		if($this->log_file_resource) {
			fclose($this->log_file_resource);
		}
	}
	
}
