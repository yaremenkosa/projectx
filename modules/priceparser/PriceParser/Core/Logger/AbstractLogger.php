<?php

namespace PriceParser\Core\Logger;

use PriceParser\Core\Object\Object;
use PriceParser\Core\StrHelpers;

abstract class AbstractLogger extends Object {
	var $verb_mode = 2;
	
	var $datetime = true;
	var $memory_usage = false;
	
	var $datetime_format = 'd.m.Y H:i:s';
	
	public function log($verb_mode, $message) {
	    if($verb_mode <= $this->verb_mode) {
			$log_message = '';
			
			if($this->datetime) {
				$log_message .= date($this->datetime_format) . "\t";
			}
			
			if($this->memory_usage) {
				$log_message .= StrHelpers::formatFileSize(memory_get_usage()) . " байт \t";
			}
			
			$log_message .= $message;
			
			$this->logSpawn($log_message);
		}
	}
	
	abstract protected function logSpawn($message);
}
