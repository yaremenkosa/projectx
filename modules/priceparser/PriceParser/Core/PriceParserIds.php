<?php

namespace PriceParser\Core;

use PriceParser\Controller\Env;

class PriceParserIds {
	
	CONST PRODUCT_ITEM_TYPE_ID = 1;
	CONST IMAGE_ITEM_TYPE_ID = 2;
	CONST CATEGORY_ITEM_TYPE_ID = 3;
	CONST FILTER_TYPE_ID = 4;
	CONST FILTER_VALUE_TYPE_ID = 5;
	CONST BRAND_VALUE_ID = 6;
	CONST ATTRIBUTE_GROUP_TYPE_ID = 7;
	CONST ATTRIBUTE_TYPE_ID = 8;
	CONST COMBINATION_TYPE_ID = 9;
	CONST PRODUCT_ATTACHMENT_TYPE_ID = 10;

	static $vendor_id;
	
	static function setVendorId($vendor_id) {
		static::$vendor_id = $vendor_id;
	}
	
	static function getSiteIdByVendorId($id_item_vendor, $id_item_type, $version_image = false) {
		return Env::getDb()->getValue('SELECT id_item_site'
				. ' FROM ' . _DB_PREFIX_ . 'price_parser_ids'
				. ' WHERE id_item_type = ' . (int)$id_item_type
				. ' AND id_item_vendor = "' . \pSQL($id_item_vendor) . '"'
                . ($version_image ? ' AND version_image = "' . \pSQL($version_image) . '"' : '')
				. ' AND id_vendor = "' . \pSQL(static::$vendor_id) . '"');
	}
	
	static function addSiteIdByVendorId($id_item_vendor, $id_site, $id_item_type, $version_image = '') {
		return Env::getDb()->insert('price_parser_ids',
				[
					'id_item_site'		=> $id_site,
					'id_item_type'		=> $id_item_type,
					'id_item_vendor'	=> \pSQL($id_item_vendor),
					'id_vendor'			=> \pSQL(static::$vendor_id),
                    'version_image'     => \pSQL($version_image)
				], false, true, \Db::INSERT_IGNORE);
	}
	
	static function deleteByVendorId($id_item_vendor, $id_item_type) {
		return Env::getDb()->delete('price_parser_ids',
				'id_item_vendor = "'		. \pSQL($id_item_vendor) . '"'
				. ' AND id_item_type = '	. (int)$id_item_type
				. ' AND id_vendor = "'		. \pSQL(static::$vendor_id) . '"'
		);
	}
	
	static function deleteSiteId($id_site, $id_item_type) {
		return Env::getDb()->delete('price_parser_ids',
				'id_item_site = ' . (int)$id_site
				. ' AND id_item_type = '	. (int)$id_item_type
		);
	}

	// получаем обновлялось описание
	static function getUpdateDesc($id_item_site, $id_item_type) {
        return Env::getDb()->getValue('SELECT update_desc'
            . ' FROM ' . _DB_PREFIX_ . 'price_parser_ids'
            . ' WHERE id_item_type = ' . (int)$id_item_type
            . ' AND id_item_site = "' . \pSQL($id_item_site) . '"'
            . ' AND id_vendor = "' . \pSQL(static::$vendor_id) . '"');
    }
    static function getUpdatePrice($id_item_site, $id_item_type) {
        return Env::getDb()->getValue('SELECT update_price'
            . ' FROM ' . _DB_PREFIX_ . 'price_parser_ids'
            . ' WHERE id_item_type = ' . (int)$id_item_type
            . ' AND id_item_site = "' . \pSQL($id_item_site) . '"'
            . ' AND id_vendor = "' . \pSQL(static::$vendor_id) . '"');
    }
	
}
