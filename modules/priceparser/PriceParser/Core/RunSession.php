<?php

namespace PriceParser\Core;

use PriceParser\Controller\Env;

class RunSession {
	
	private static $_instances = [];

	var $params;
	
	var $hash;
	
	var $is_changed = false;
	
	public static function _instance($hash) {
		if(isset(static::$_instances[$hash])) {
			return static::$_instances[$hash];
		} else {
			static::$_instances[$hash] = new static($hash);
			static::$_instances[$hash]->load();
		}
		
		return static::$_instances[$hash];
	}
	
	private function __construct($hash) {
		$this->hash = $hash;
	}
	
	public function load() {
		$result = Env::getDb()->executeS('SELECT param, value'
				. ' FROM ' . _DB_PREFIX_ . 'price_parser_run_session'
				. ' WHERE session_hash = "' . \pSQL($this->hash) . '"');
		
		
		if(!empty($result)) {
			foreach($result as $row) {
				$this->params[$row['param']] = $row['value'];
			}
		} else {
			$this->params = array();
		}
	}
	
	public function save() {
		if(!$this->is_changed) {
			return ;
		}
		
		if(empty($this->params)) {
			return ;
		}
		
		$this->clear();
		
		if(!empty($this->params)) {
			foreach($this->params as $name => $value) {
				Env::getDb()->insert('price_parser_run_session', array(
					'session_hash' => \pSQL($this->hash),
					'param' => \pSQL($name),
					'value' => \pSQL($value),
				));
			}
		}
	}
	
	public function clear() {
		if(!empty($this->params)) {
			return Env::getDb()->delete('price_parser_run_session',
					'session_hash = "' . \pSQL($this->hash) . '"');
		}
	}
	
	public function set($name, $value) {
		$this->params[$name] = $value;
		$this->is_changed = true;
	}
	
	public function get($name) {
		if(isset($this->params[$name])) {
			return $this->params[$name];
		} else {
			return false;
		}
	}
	
}
