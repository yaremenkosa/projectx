<?php

return [
    'id_vendor' => 'intimmoll',
    'email' => null, // email для уведомления на почту о завершении синхронизации
	'clear_tmp_path' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../tmp' . DIRECTORY_SEPARATOR . 'intimmoll' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR,
    'instance_lock' => true,
    'controller' => [
        'class' => 'PriceParser\Controller\VendorSiteController',
        'params' => [],
    ],
    'import' => [
        'class' => '\PriceParser\Import\intimmoll\Runner',
        'params' => [
            'loader'		=> [ // настройки загрузчика
                'class' => '\PriceParser\Import\RemoteLoader',
                'params' => [
                    'items'  => [
                        'products' => [
                            'url' => 'https://intimmoll.ru/personal/custom-export/upload/?user=13968&upload_type=1&encoding=1&rrc=Y&markup=2',
                            'save_name' => 'products.xml',
                        ],
                    ],
                    'type'				 => 'Curl', // тип подключения
                    'downloader_params'	 => [],
                    'save_path' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../tmp' . DIRECTORY_SEPARATOR . 'intimmoll' . DIRECTORY_SEPARATOR, // куда сохраняем выгрузки
                    'local_store_actual' => 60*60*24, // как долго считать выгрузку актуальной
                ],
            ],
            'parser'		=> [ // настройка парсера
                'class' => '\PriceParser\Import\intimmoll\ParserProducts',
                'params' => [],
            ],
            'processor'		=> [ // настройка обработки
                'class' => '\PriceParser\Import\intimmoll\ProcessorProducts',
                'params' => [],
            ],
        ],
    ],
    'export' => [
        'class' => '\PriceParser\Export\SiteExporter\Runner',
        'params' => [
            'processor' => [
                'class' => '\PriceParser\Export\SiteExporter\ProcessorProducts',
                'params' => [],
            ],
            'exporter' => [
                'class' => '\PriceParser\Export\SiteExporter\Derr\ExporterProducts',
                'params' => [
                    'bind_to_imported_vendor_cats' => true,
                    'download' => [
                        'tmp_path' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../tmp' . DIRECTORY_SEPARATOR . 'intimmoll' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR,
                        'type'				 => 'Curl',
                        'downloader_params'	 => [
                            'max_filesize'	 => 50000000,
                        ],
                    ],
                ],
            ],
        ],
    ],
    'runtime' => [
        'logger' => [
            'class' => '\PriceParser\Core\Logger\StdoutLogger',
            'params' => [
                'verb_mode' => 9,
                'datetime' => false,
                'memory_usage' => false,
                'log_file' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../logs/execution/intimmoll.log',
            ],
        ],
        'summary' => [
            'class' => '\PriceParser\Core\Logger\FileLogger',
            'params' => [
                'verb_mode' => 9,
                'datetime' => true,
                'memory_usage' => false,
                'log_file' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../logs/summary/intimmoll.log',
            ],
        ],
        'execution_timer' => true,
    ],
];
