<?php

/**
 * Контроллер выполнения через cron-демона
 */

namespace PriceParser\Cron;

use \DbQuery;
use PriceParser\Controller\Env;
use PriceParser\Core\Executor;
use PriceParser\Cron\Exceptions\CronControllerException;

class CronController {
	
	var $configs_dir;
	
	var $logs_dir;
	
	var $task_pid;
	
	var $log;
	var $task;
	
	function __construct() {
		$this->configs_dir = dirname(__FILE__) . '/../Configs/';
		$this->logs_dir = dirname(__FILE__) . '/../logs/execution/';
		
		pcntl_signal(SIGTERM, array($this, 'signalHandler'));
		pcntl_signal(SIGCHLD, array($this, 'signalHandler'));
		pcntl_signal(SIGALRM, array($this, 'signalHandler'));
		pcntl_signal(SIGINT, array($this, 'signalHandler'));
		
		$this->instanceLockCheck();
	}
	
	public function signalHandler($signo) {
		if($signo === SIGCHLD) {
			echo 'Child finished' . PHP_EOL;
			
		} else {
			echo 'Execution aborted. Signal no: ' . $signo . ' Pid: ' . getmypid() . PHP_EOL;

			if($this->task_pid) {
				echo 'Killing task process, pid: ' . $this->task_pid . PHP_EOL;
				posix_kill($this->task_pid, SIGTERM);
				$this->stopTask(-1);
			}

			exit(1);
		}
	}
	
	/**
	 * Запуск контроллера
	 */
	public function run() {
		$tasks = $this->findTasks();
		
		if(!empty($tasks)) {
			foreach($tasks as $task) {
				$this->startTask($task['id_cron_schedule'], $task['run_config'], (bool)$task['manual_run']);
			}
		} else {
			echo date('d.m.Y H:i:s') . ':' . "\t" . 'Задач для выполнения не найдено' . PHP_EOL;
		}
	}
	
	protected function instanceLockCheck() {
//		chdir(dirname(__FILE__));
		$this->lock_resource = fopen('cron_instance.lock', 'w');
		if (!flock($this->lock_resource, LOCK_EX | LOCK_NB)) {
			die('Price parser is already running');
		}
	}
	
	/**
	 * Поиск актуальных задач на данный момент
	 * @return Array массив задач
	 */
	protected function findTasks() {
		
		return Env::getDb()->executeS('SELECT id_cron_schedule, title, run_config, manual_run'
				. ' FROM ' . _DB_PREFIX_ . 'price_parser_cron_schedule'
				. ' WHERE is_enabled = 1 AND status = 0'
				. ' AND (manual_run = 1) OR ((period = \'hourly\' AND last_run_date < "' . date('Y-m-d H:i:s', strtotime('-1 hour')) . '")'
				. 'OR (period = \'daily\' AND last_run_date < "' . date('Y-m-d H:i:s', strtotime('-1 day')) . '"))');
	}
	
	/**
	 * Запуск задачи
	 * @param type $config конфиг запускаемой задачи
	 * @return int статус завершения выполнения
	 */
	protected function startTask($id, $config_name, $force = false) {
		\PriceParser\Autoloader::load('CronRunLog', 'Cron/Model');
		\PriceParser\Autoloader::load('CronSchedule', 'Cron/Model');
		$this->log = new \CronRunLog();
		$this->log->id_cron_schedule = $id;
		$this->log->start_date = date('Y-m-d H:i:s');
		$this->log->status = 'started';
		$this->log->pid = getmypid();
		$this->log->save();
		
		$this->task = new \CronSchedule($id);
		$this->task->status = 1;
		$this->task->save();
		
		$log_filename = 'log_' . $this->log->id . '.log';
		
		$descriptorspec = array(
			0 => array("pipe", "r"),  // stdin - канал, из которого дочерний процесс будет читать
			1 => array("file", $this->logs_dir . $log_filename, "a"),  // stdout - канал, в который дочерний процесс будет записывать
			2 => array("file", $this->logs_dir . $log_filename, "a") // stderr - файл для записи
		);
		
		$process = proc_open('php run.php --runconfig ' . $config_name . ($force ? ' --force' : '') . ' --log_id ' . $this->log->id, $descriptorspec, $pipes);
		
		$proc_statuses = proc_get_status($process);
		
		$this->task_pid = $proc_statuses['pid'];
		
		if(is_resource($process)) {
			fclose($pipes[0]);
			$proc_result = proc_close($process);
			
			$this->stopTask($proc_result);

//			chdir($this->logs_dir);

			exec('cd ' . $this->logs_dir . ' && zip ' . $log_filename . '.zip ' . $log_filename);
			unlink($this->logs_dir . $log_filename);
		}

		return true;
	}
	
	function stopTask($result) {
		$this->task_pid = null;
		if($result === 0) {
			$this->log->status = 'finished';
			
			
		} elseif($result == -1) {
			$this->log->status = 'killed';
		} else {
			$this->log->status = 'error';
		}
		
		if($this->task->manual_run) {
			$this->task->manual_run = false;
		}
		
		$this->log->finish_date = date('Y-m-d H:i:s');
		$this->task->last_run_date = date('Y-m-d H:i:s');
		$this->task->status = 0;
		
		$this->task->save();
		$this->log->save();
		
		return true;
	}
}