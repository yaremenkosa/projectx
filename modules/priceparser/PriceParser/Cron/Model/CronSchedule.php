<?php

/**
 * В этом классе пришлось обойтись без неймспейсов, т.к. преста не умеет с ними работать (
 */
//namespace PriceParser\Cron\Model;

//use \ObjectModel;

class CronSchedule extends ObjectModel {
	
	var $title;
	var $run_config;
	var $period;
	var $status;
	var $manual_run;
	var $last_run_date;
	var $is_enabled;
	var $date_add;
	var $date_upd;
	
	public static $definition = array(
		'table' => 'price_parser_cron_schedule',
		'primary' => 'id_cron_schedule',
		'fields' => array(
			'title'				=> array('type' => self::TYPE_STRING),
			'run_config'		=> array('type' => self::TYPE_STRING),
			'period'			=> array('type' => self::TYPE_STRING),
			'status'			=> array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'manual_run'		=> array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'last_run_date'		=> array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
			'is_enabled'		=> array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'date_add'			=> array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
			'date_upd'			=> array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
		),
	);
	
}
