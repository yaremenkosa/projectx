<?php

//namespace PriceParser\Cron\Model;

//use \ObjectModel;
use PriceParser\Controller\Env;

class CronRunLog extends ObjectModel {
	
	var $id_cron_schedule;
	var $start_date;
	var $finish_date;
	var $status;
	var $pid;
	var $date_add;
	var $date_upd;
	
	public static $definition = array(
		'table' => 'price_parser_cron_run_log',
		'primary' => 'id_cron_run_log',
		'fields' => array(
			'id_cron_schedule'		=> array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'start_date'			=> array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
			'finish_date'			=> array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
			'status'				=> array('type' => self::TYPE_STRING),
			'pid'					=> array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'date_add'				=> array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
			'date_upd'				=> array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
		),
	);
	
	/**
	 * 
	 * @param type $id_cron_schedule
	 * @return \CronRunLog|boolean
	 */
	public static function findLastItemBySchedule($id_cron_schedule) {
		$id = Env::getDb()->getValue('SELECT id_cron_run_log FROM ' . _DB_PREFIX_ . 'price_parser_cron_run_log'
				. ' WHERE id_cron_schedule = ' . (int)$id_cron_schedule
				. ' ORDER BY id_cron_run_log DESC');
		
		if($id) {
			return new CronRunLog($id);
		}
		
		return false;
	}
}
