<?php

namespace PriceParser\Import;

interface Processable {
	
	public function process(Array $data);
	
}
