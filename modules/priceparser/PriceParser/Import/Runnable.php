<?php

namespace PriceParser\Import;

interface Runnable {
	
	public function run();
	
}
