<?php

namespace PriceParser\Import\intimmoll;

use PriceParser\Import\Processable;
use PriceParser\Core\Object\Object;
use PriceParser\Controller\Env;

class ProcessorProducts extends Object implements Processable {

	var $part_length;

	var $categories_ids;

	public function process(array $data) {

		$this->log(1, 'Начинаем обработку данных');

		$products = [];

		if(!empty($data)) {
            //$i = 0;
			foreach($data['products'] as $row) {
                //if($i > 0) continue;
			    // фильтр товаров для теста
                if(\Configuration::get('PRICE_PARSER_TEST_PRODUCT')) {
                    $test_products = explode(',', \Configuration::get('PRICE_PARSER_TEST_PRODUCT'));
                    if (count($test_products) > 0 && !in_array($row['product_id'], $test_products)) continue;
                }
                //$i++;
                //if($row['product_id'] != 246702) continue;

                $categories = array();
                foreach($row['categories_ids'] as $category_vendor) {
                    $sql = "SELECT * FROM `" . _DB_PREFIX_ . "price_parser_vendor_categories_bind` 
                    WHERE `id_vendor` = '" . Env::$config['id_vendor'] . "' AND `id_vendor_category` = '" . $category_vendor . "'";
                    $bind = Env::getDb()->executeS($sql);
                    foreach($bind as $b) {
                        $categories_bind = explode(',', $b['id_category']);
                        $categories = array_merge($categories, $categories_bind);
                    }
                }

                if(count($categories) <= 0) $categories = array(\Configuration::get('PS_HOME_CATEGORY'));

                // Обработаем картинки
                $is_cover = 0;
                foreach($row['images'] as &$image) {
                    if($is_cover == 0) {
                        $image['is_cover'] = $is_cover = 1;
                    } else {
                        $image['is_cover'] = 0;
                    }
                }

				$name = $row['name'];

				$products[] = [
					'id'			 => $row['product_id'],
					'code'			 => $row['code'],
					'name'			 => $name,
					'description'	 => $row['description'],
                    'is_active'		 => $row['active'],
					'price' => [
						//закупочная
						'wholesale_price'	 => $row['wholesale_price'],
						'price'				 => $row['price'],
					],
					'images'		 => $row['images'],
					'features'		 => $row['features'],
					'categories_ids' => $categories,
					'attributes'	 => $row['attributes'],
                    'quantity'       => $row['quantity'],
                    'brand_id'       => (isset($row['brand_id']) ? $row['brand_id'] : false)

                    /*'available_now' => $row['available_now'],
                    'available_later' => $row['available_later'],*/
					//'children'       => $row['children'],
					//'comb_attributes'=> $row['comb_attributes'],
				];
			}
		}

		$this->log(1, 'Обработано: ' . count($products) . ' товаров');

		return [
			'products' => $products,
		];
	}

}
