<?php

namespace PriceParser\Import\intimmoll;

use PriceParser\Core\Object\Object;
use PriceParser\Import\Exceptions\ImportException;
use PriceParser\Controller\Env;
use PriceParser\Core\StrHelpers;
use PriceParser\Import\XmlParserUtils;

class ParserProducts extends Object implements \PriceParser\Import\Parsable {

	//не проверять stamp файла выгрузки
	var $stamp_force = false;
    var $features_name = array(); // список характеристик
    var $features_name_not = array();

    function __construct(array $params = []) {
		parent::__construct($params);

		if (isset(Env::$config['is_force']) && Env::$config['is_force']) {
			$this->stamp_force = true;
		}

        $this->features_name = array(
            'material' => 'Материал',
            'dlina' => 'Длина',
            'diametr' => 'Диаметр',
            'vodonepronicaemost' => 'Водопроницаемость',
            'tsvet' => 'Цвет',
            'razmer' => 'Размер',
            'tipbatareek' => 'Тип батареек',
            'osnova' => 'Основа',
            'marka' => 'Бренд'
        );

	}

	public function parse($data) {
		$this->log(1, 'Парсим xml');

		$file = Env::$config['import']['params']['loader']['params']['save_path'] . Env::$config['import']['params']['loader']['params']['items']['products']['save_name'];
        $data = simplexml_load_file($file);

        $start = (isset($_GET['start']) ? $_GET['start'] : false);
        $limit = 2000;

        $i=0;
        $products = [];
        if(!empty($data->shop->offers->offer)) {
            foreach($data->shop->offers->offer as $products_parsed) {
                if (!empty($products_parsed)) {
                    $i++;

                    if(defined('_START_IMPORT_')) {
                        if($i >= _START_IMPORT_ && $i<= (_START_IMPORT_ + $limit))
                            $this->parseProductRow($products_parsed, $products);
                    } else {
                        $this->parseProductRow($products_parsed, $products);
                    }

                }
            }
        }
//        die(var_dump($this->features_name_not));

		return [
			'products' => $products,
		];
	}

	private function getChildren($products)
    {
        $children = [];

        foreach ($products as $product) {
            if($product['product_id'] == $product['group_id'])
                continue;

            $children[] = $product;
        }

        return $children;
    }

    private function getDefaultProduct($products)
    {
        $defaultProduct = null;
        foreach ($products as $product) {
            if ($product['product_id'] == $product['group_id']) {
                $defaultProduct = $product;
            }
        }
        if(!$defaultProduct)
            $defaultProduct = $products[0];

        return $defaultProduct;
    }

	private function parseProductRow($row, &$result) {

		$images = [];
        if (!empty($row->picture)) {
            $images[] = array('position' => 0, 'url' => (string)$row->picture);
        }
		if (!empty($row->images->image)) {
			foreach ($row->images->image as $image) {
				if (isset($image)) {
					$images[] = array('position' => count($images),'url' => (string)$image);
				}
			}
		}

		//категории, к которым привязан товар
		$categories_ids = [];

		if (isset($row->categoryId)) {
			$categories_ids[] = (int)$row->categoryId;
		} else {
            $categories_ids[] = 2;
        }



		$features = [];

		if(isset($row->properties)) {
		    foreach((Array)$row->properties as $key => $property) {

		        if(isset($this->features_name[$key])) {
                    $features[] = [
                        'vendor_id'       => $this->features_name[$key],
                        'vendor_value_id' => (string)$property,
                    ];
                } else { // для теста, отбираем неопределенные характеристики
		            if(!in_array($key, $this->features_name_not))
                    $this->features_name_not[] = $key;
                }

            }
        }

        $brand_id = false;
		if(isset($row->properties->marka) && $row->properties->marka != '') {
            $brand_id = (string)$row->properties->marka;
        }

		$attributes = [];

		/*if (isset($row['attributes']) && !empty($row['attributes'])) {
			foreach ($row['attributes'] as $att_row) {

				switch ($att_row['name']) {
					case 'Размер товара':

					    $att_row['value'] = str_replace(' ', '', $att_row['value']);
						$att_sizes = $this->parseSize($att_row);

						if (!empty($att_sizes)) {
							$attributes = array_merge($attributes, $att_sizes);
						}

						break;

					case 'Вес':
						$att_weight = $this->parseSize($att_row);

						if (!empty($att_weight)) {
							$attributes = array_merge($attributes, $att_weight);
						}
						break;

					case 'Объем единицы':
						$attributes['volume'] = $att_row['value'];
						break;

					default:

						$features[] = [
							'vendor_id'       => $att_row['name'],
							'vendor_value_id' => $att_row['value'],
						];

						break;
				}
			}
		}*/

		$description = isset($row->description) ? (string)$row->description : '';
        // определим включать товар или нет
        $active = 1;
        if(!isset($row->quantity)) $row->quantity = ($active == 1 ? 555 : 0); // если товар включен и не указано кол-во, задаем кастомное
        $active = ((int)$row->quantity <= 0 ? 0 : 1); // если товар закончился


        // делаем надбавку из модуля pwrcstore
        if(\Module::isEnabled('pwrcstore')) {
            $category_bind = \Module::getInstanceByName('pwrcstore')->getCategoriesBind($categories_ids);
            if($category_bind && $category_bind[0]['price'] > 0)
                $row->price = (float)$row->price + (((float)$row->price * $category_bind[0]['price']) / 100);
            else
                $row->price = (float)$row->price + (((float)$row->price * \Configuration::get('PWRCSTORE_PRICE')) / 100);
        }

		$data = [
			'product_id'     => (int)$row->attributes()->id,
			'code'           => (isset($row->vendorCode) ? (string)$row->vendorCode : (int)$row->attributes()->id),
			'name'           => (string)$row->name,
			'description'    => html_entity_decode($description),
            'price'          => (float)$row->price,
            'wholesale_price' => (float)$row->price,
			'images'         => $images,
			'features'       => $features,
			'categories_ids' => $categories_ids,
            'active'         => $active,
			'attributes'     => $attributes,
            'quantity'       => (int)$row->quantity,
            'brand_id'       => $brand_id

            /*'available_now' => \Configuration::get('PWSIMALAND_AVAILABLE_NOW'),
            'available_later' => \Configuration::get('PWSIMALAND_AVAILABLE_LATER'),*/
		];

        $result[] = $data;

	}

	private function parseSize($att_row) {
		$result = [];
		$sizes  = explode('x', $att_row['value']);

		if (!empty($sizes) && count($sizes) === 3) {
			foreach ($sizes as &$size) {
				$size = (float)trim($size);
				if (isset($att_row['dim'])) {
					switch ($att_row['dim']) {
						case 'мм.':
							$size = $size / 1000;

							break;
					}
				}
			}

			$result['sizex'] = $sizes[0];
			$result['sizey'] = $sizes[1];
			$result['sizez'] = $sizes[2];
		}

		return $result;
	}

	private function parseWeight($att_row) {
		$weight = (float)$att_row['value'];

		if (isset($att_row['dim'])) {
			switch ($att_row['dim']) {
				case 'г.':
					$weight = $weight / 1000;
			}
		}

		return [
			'weight' => $weight,
		];
	}
}
