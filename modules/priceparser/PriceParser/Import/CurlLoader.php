<?php

namespace PriceParser\Import;

use PriceParser\Import\Loadable;
use PriceParser\Import\RemoteLoader;

use PriceParser\Import\Exceptions\ImportException;
use PriceParser\Controller\Env;

class CurlLoader extends RemoteLoader implements Loadable {
	
	var $url;
	
	protected function downloadFile($link, $save_path = false) {
		 if(!$this->url) {
			 throw new ImportException('Url are not assigned');
		 }
		
		$curl = curl_init();
		curl_setopt($curl,	CURLOPT_URL,				$this->url . $link);
		curl_setopt($curl,	CURLOPT_RETURNTRANSFER,		true);
		curl_setopt($curl,	CURLOPT_CONNECTTIMEOUT,		$this->connect_timeout);
		curl_setopt($curl,	CURLOPT_TIMEOUT,			$this->timeout);
		
		$result = curl_exec($curl);
		
		$err_no = curl_errno($curl);
		
		curl_close($curl);
		
		if($err_no !== 0) {
			throw new ImportException('File download error. Code: ' . $err_no);
		}
		
		$this->log(2, 'Файл ' . $this->url . $link . ' загружен успешно. Размер: ' . \PriceParser\Core\StrHelpers::formatFileSize(mb_strlen($result)) . ' байт');
		
		if($save_path) {
			$this->log(1, 'Сохраняем файл ' . $save_path);
			file_put_contents($save_path, $result);
		}
		
		return $result;
	}
	
}
