<?php

namespace PriceParser\Import;

interface Loadable {
	
	public function loadData();
	
}
