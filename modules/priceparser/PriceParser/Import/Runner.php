<?php

namespace PriceParser\Import\VestAlpha;

use PriceParser\Import\Runnable;
use PriceParser\Core\Object\Object;
use PriceParser\Import\Loadable;
use PriceParser\Import\Parsable;
use PriceParser\Import\Processable;
use PriceParser\Import\Downloadable;
use PriceParser\Core\Object\ObjectFactory;
use PriceParser\Core\Exceptions\ObjectFactoryException;
use PriceParser\Core\Exceptions\RunnerException;

class Runner extends Object implements Runnable {
	
	protected $loader;
	protected $parser;
	protected $processor;
	protected $downloader;
	
	protected $_Loader;
	protected $_Parser;
	protected $_Downloader;
	protected $_Processor;
	
	/**
	 * 
	 * @param array $config
	 * @throws RunnerException
	 */
	function run() {
		//читаем конфиг
		
		try {
			$this->setLoader(ObjectFactory::getObject($this->loader))
				 ->setParser(ObjectFactory::getObject($this->parser))
				 ->setProcessor(ObjectFactory::getObject($this->processor));
			
		} catch(ObjectFactoryException $ex) {
			throw new RunnerException('Error on preparing runner components: ' . $ex->getMessage(), $ex->getCode(), $ex);
		}
		
		$loaded_data = $this->_Loader->loadData();
		
		$parsed_data = $this->_Parser->parse($loaded_data);
		
		$final_result = $this->_Processor->process($parsed_data);
		
		unset($loaded_data);
		unset($parsed_data);
		
		return $final_result;
		
	}
	
	protected function setLoader(Loadable $loader) {
		$this->_Loader = $loader;
		
		return $this;
	}
	
	protected function setParser(Parsable $parser) {
		$this->_Parser = $parser;
		
		return $this;
	}
	
	protected function setProcessor(Processable $processor) {
		$this->_Processor = $processor;
		
		return $this;
	}
}
