<?php

namespace PriceParser\Import;

interface Parsable {
	
	public function parse($data);
	
}
