<?php

namespace PriceParser\Import;

use PriceParser\Import\Loadable;
use PriceParser\Core\Object\Object;
use PriceParser\Import\Exceptions\ImportException;
use PriceParser\Controller\Env;

class RemoteLoader extends Object implements Loadable {

	//список путей к файлу
	var $items;
	var $connect_timeout	 = 10;
	var $timeout			 = 30;
	var $save_path;
	var $type;
	/**
	 *
	 * @var \PriceParser\Core\Downloader\AbstractDownloader
	 */
	var $downloader;
	var $downloader_params;
	//срок актуальности локального хранения (в секундах)
	var $local_store_actual = false;
	
	var $filename_pattern;
	var $files_path;

	function __construct(array $params = array()) {
		parent::__construct($params);

		$this->downloader = $this->getDownloader()->open();
	}

	function __destruct() {
		if($this->downloader->isOpened()) {
			$this->downloader->close();
		}
	}

	function loadData() {
		$data = [];

		$this->initItems();

		if(empty($this->items)) {
			throw new ImportDownloadException('Файлы для скачивания не определены');
		}

		foreach ($this->items as $name => $item) {

			$data[$name] = $this->loadItem($item);
		}

		return $data;
	}

	protected function initItems() {
		if($this->filename_pattern && $this->files_path) {
			$_items = $this->downloader->searchByPattern($this->filename_pattern, $this->files_path);

			$items = [];

			if(!empty($_items)) {
				foreach($_items as $_item) {
					$items[basename($_item)] = [
						'url'		 => $_item,
						'save_name'	 => basename($_item),
					];
				}
			}

			$this->items = $items;

			return $items;
		}

		return [];
	}

	protected function loadItem($item) {
		$local_stored_path = $this->save_path . $item['save_name'];
		
		if ((!isset(Env::$config['is_force']) || !Env::$config['is_force']) && $this->local_store_actual && file_exists($local_stored_path)) {
			$file_stamp = filectime($local_stored_path);
			$this->log(1, 'Файл ' . $item['save_name'] . ' найден сохранённый, дата изменения: ' . date('d.m.Y H:i:s', $file_stamp));

			if ((time() - $file_stamp) <= $this->local_store_actual) {
				$this->log(1, 'Файл ' . $item['save_name'] . ' актуален, загружаем его');

				return file_get_contents($local_stored_path);
			}

			$this->log(1, 'Файл ' . $item['save_name'] . ' устарел, загружаем свежую версию');
		}

		$this->log(1, 'Начинаем загрузку файла ' . $item['url']);

		try {
			$data = $this->downloadFile($item['url'], $this->save_path . $item['save_name']);
			
			$this->log(1, 'Файл загружен, размер: ' . \PriceParser\Core\StrHelpers::formatFileSize(mb_strlen($data)) . ' байт');
			

			if (!$data) {
				throw new ImportException('Ошибка при загрузке: данные не получены.');
			}
		} catch (\PriceParser\Import\Exceptions\ImportDownloadException $ex) {
			$this->log(1, 'Ошибка при загрузке файла: ' . $ex->getMessage());

			throw new ImportException('Ошибка при загрузке файла: ' . $ex->getMessage(), $ex->getCode(), $ex);
		}

		return $data;
	}

	protected function downloadFile($link, $save_path) {
		if (!$this->type) {
			throw new Exceptions\ImportDownloadException('Не указан тип загрузки');
		}
		
		return $this->downloader->download($link, null, $save_path, \PriceParser\Core\Downloader\AbstractDownloader::RETURN_TYPE_FILE_CONENT);
	}
	
	/**
	 * Получение загрузчика
	 * @return \PriceParser\Core\Downloader\AbstractDownloader
	 * @throws Exceptions\ImportDownloadException
	 */
	protected function getDownloader() {
		try {
			$downloader = \PriceParser\Core\Object\ObjectFactory::getObject([
						'class'	 => '\PriceParser\Core\Downloader\\' . $this->type . 'Downloader',
						'params' => $this->downloader_params,
			]);
		} catch (\PriceParser\Core\Exceptions\ObjectFactoryException $ex) {
			$this->log(1, 'Ошибка инициализации загрузчика: ' . $ex->getMessage());

			throw new Exceptions\ImportDownloadException('Ошибка инициализации загрузчика: ' . $ex->getMessage(), $ex->getCode(), $ex);
		}
		
		return $downloader;
	}

}
