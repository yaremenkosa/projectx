<?php

namespace PriceParser\Import;

use PriceParser\Import\Loadable;
use PriceParser\Core\Object\Object;
use PriceParser\Import\Exceptions\ImportException;

class FileLoader extends Object implements Loadable  {
	
	var $items;
	
	function loadData() {
		if(empty($this->items)) {
			throw new ImportException('Nothing to load!');
		}
		
		$data = [];
		
		foreach($this->items as $item_name => $item_path) {
			$data[$item_name] = $this->loadFile($item_path);
		}
		
		return $data;
	}
	
	private function loadFile($filename) {
		if(!file_exists($filename)) {
			throw new ImportException('Loader: file to load ' . $filename . ' not found');
		}
		
		$this->log(1, 'Начинаем загрузку файла ' . $filename);
		
		$data = file_get_contents($filename);
		
		$this->log(1, 'Загружено ' . \PriceParser\Core\StrHelpers::formatFileSize(mb_strlen($data)) . ' байт');
		
		return $data;
	}
	
}
