<?php

namespace PriceParser\Import;

interface Downloadable {
	
	public function download($data);
	
}
