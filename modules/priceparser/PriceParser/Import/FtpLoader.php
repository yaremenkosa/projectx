<?php

namespace PriceParser\Import;

use PriceParser\Import\Loadable;
use PriceParser\Import\RemoteLoader;

use PriceParser\Import\Exceptions\ImportException;
use PriceParser\Controller\Env;

class FtpLoader extends RemoteLoader implements Loadable {
	
	var $host;
	var $login;
	var $password;
	var $pasv = false;
	
	protected function downloadFile($link, $save_path) {
		$login_result = false;
		try {
			$conn_id = ftp_connect($this->host);

			if($conn_id) {
				$login_result = ftp_login($conn_id, $this->login, $this->password);
                ftp_pasv($conn_id, true);
			} else {
				throw new ImportException('Ошибка при соединении ftp');
			}
		} catch (\ErrorException $ex) {
			throw new ImportException('Ошибка при соединении ftp: ' . $ex->getMessage(), $ex->getCode(), $ex);
		}
		
		if(!$login_result) {
			throw new ImportException('Ошибка при соединении ftp');
		}
		
		ftp_pasv($conn_id, $this->pasv);
		
		$this->log(1, 'Скачиваем файл: ' . $link);
		$get_result = ftp_get($conn_id, $save_path, $link, FTP_BINARY);
		
		if($get_result) {
			$this->log(1, 'Файл успешно загружен, сохраняем: ' . $save_path);
		} else {
			throw new ImportException('Ошибка при скачивании файла ' . $link . ' по ftp');
		}
		
		ftp_close($conn_id);
		
		if(file_exists($save_path)) {
			return file_get_contents($save_path);
		} else {
			throw new ImportException('Скачанный файл ' . $save_path . ' не существует');
		}
		
		return false;
	}
}
