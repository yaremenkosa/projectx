<?php


namespace PriceParser\Import;

use PriceParser\Controller\Env;
use PriceParser\Import\Exceptions\ImportOldException;

class XmlParserUtils {
	
	static function checkTimestamp($name, $date_stamp) {
		$status_key = $name . '.last_timestamp';
//		$date_stamp = (string)$xml['timestamp'];
		$date_stamp_int = strtotime($date_stamp);
		
		$last = Env::$status->get($status_key);
		
		if($last >= $date_stamp_int) {
			throw new ImportOldException('Файл выгрузки для ' . $name . ' уже был загружен. Дата: ' . date('d.m.Y H:i:s', $date_stamp_int)
					. ', дата последней выгрузки: ' . date('d.m.Y H:i:s', $last));
		}
		
		Env::$status->save($status_key, $date_stamp_int, true);
		
		return $date_stamp_int;
	}
	
}
