<?php

class SiteCatTree extends Tree {
	
	var $cat_binding_count;
	
	public function setCatBindingCount($cat_binding_count) {
		$this->cat_binding_count = $cat_binding_count;
		
		return $this;
	}
	
	public function renderNodes($data = null) {
		if (!isset($data))
			$data = $this->getData();

		if (!is_array($data) && !$data instanceof Traversable)
			throw new PrestaShopException('Data value must be an traversable array');

		$html = '';

		foreach ($data as $item) {
			if (array_key_exists('children', $item)
				&& !empty($item['children'])) {
				$html .= $this->getContext()->smarty->createTemplate(
					$this->getTemplateFile($this->getNodeFolderTemplate()),
					$this->getContext()->smarty
				)->assign(array(
					'children' => $this->renderNodes($item['children']),
					'node'     => $item,
					'bind_count' => (isset($this->cat_binding_count[$item['id']])) ? $this->cat_binding_count[$item['id']] : 0,
				))->fetch();
				} else
				$html .= $this->getContext()->smarty->createTemplate(
					$this->getTemplateFile($this->getNodeItemTemplate()),
					$this->getContext()->smarty
				)->assign(array(
					'node' => $item,
					'bind_count' => (isset($this->cat_binding_count[$item['id']])) ? $this->cat_binding_count[$item['id']] : 0,
				))->fetch();
		}

		return $html;
	}
	
}
