<?php


class AdminPriceParserReferenceMoveController extends ModuleAdminController {
	
	var $bootstrap = true;
	var $backupSQL = Array(); //Массив для хранения запросов для отмены последней операции

	function __construct()
	{
		$this->display = 'edit';
		parent::__construct();
	}


	public function renderForm()
	{
		$this->fields_form = array(
			'legend' => array(
				'title' => $this->l('Дублирование товаров по артикулу'),
				'icon' => 'icon-paper-clip'
			),
			'input' => array(
				array(
					'type' => 'textarea',
					'label' => $this->l('Список артикулов'),
					'name' => 'reference_list',
					'desc' => 'Артикулы через запятую или с новой строки',
					'required' => true,
					'rows' => 20,
					'cols' => 4
				),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Только категории без подкатегорий'),
                    'name' => 'onlyLast',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'onlyLast_1',
                            'value' => 1,
                            'label' => $this->l('Да'),
                        ),
                        array(
                            'id' => 'onlyLast_2',
                            'value' => 0,
                            'label' => $this->l('Нет')
                        )
                    ),
                    'hint' => $this->l('Если укажите, что нет. То ограничение не будет выставлено.')
                ),
			),


			'submit' => array(
				'title' => $this->l('Save'),
				'name' => 'submitReferenceMove'
			)
		);
        $this->fields_value['onlyLast'] = 1;
		$tree_categories_helper = new HelperTreeCategories('associated-categories-tree', 'Выберите категории');
		$tree_categories_helper->setUseCheckBox(1)
			->setUseSearch(1);
		;
		$this->context->smarty->assign('categories_tree', $tree_categories_helper->render());
		return parent::renderForm();
	}



	public function postProcess() {
		if(Tools::getValue('ajax')) {
			$action = Tools::getValue('action');
			$message = '';
			$data = array();
			
			if(method_exists($this, 'ajax' . $action)) {
				try {
					$result = $this->{'ajax' . $action}();
					
					die(json_encode([
						'success' => true,
						'data' => $result,
						'message' => $message,
					]));
					
				} catch(PrestaShopException $ex) {
					if($ex->getCode() === 1) {
						$message = $ex->getMessage();
					}

					die(json_encode([
						'success' => false,
						'data' => false,
						'message' => $message,
					]));
				}
			}
		}
        /*
         * 9_11103608,9_11103612
            9_11103613,9_11103614
         */

		if (Tools::isSubmit('submitReferenceMove')) {
			$reference_list = Tools::getValue('reference_list');
			$references_temp = explode("\n", $reference_list);
            /* ЧТобы понимал при вводе и переносы и запятые */
            $references = Array();
            foreach($references_temp as $row){
                if(explode(",", $row)>1){
                    foreach(explode(",", $row) as $row2){
                        if(trim($row2)) $references[] = $row2;
                    }
                }else $references[] = $row;
            }

			$categoryBox  = Tools::getValue('categoryBox');
			if (isset($categoryBox) && count($categoryBox)) {
				$i= 0 ;
				foreach ($references as $key => &$r) {
					$r = trim($r);
					if (strlen($r)) {
						$id_product = self::searchByReference($r);
						if (!$id_product)
							$this->errors[] = Tools::displayError('Не найден артикул ' . $r);
						else {
							foreach ($categoryBox as $id_category) {
                                    //echo self::categoryHasChild($id_category)."<br/>";
                                if(Tools::getValue('onlyLast')){
                                    if(self::categoryHasChild($id_category)){
                                        $this->errors[] = "Товар <b>" . $r . "</b> не был добавлен в категорию <b>" . $id_category . "</b> поскольку она не родительская.";
                                        continue;
                                    }
                                }
                                if ($this->checkLink($id_category, $id_product)) $i++;
                                else $this->errors[] = "Товар <b>".$r."</b> уже в категории <b>".$id_category."</b>";

							}
						}
					}
				}
				$this->confirmations[] = 'Было добавлено связей: '.$i;
				$tpl = $this->context->smarty->createTemplate(_PS_MODULE_DIR_ . 'priceparser/views/templates/admin/price_parser_reference_move/backupform.tpl');
				$tpl->assign(Array(
					'backupSQL'=> serialize($this->backupSQL)
				));
				$this->content .=$tpl->fetch();
			}else{
				$this->errors[] = Tools::displayError('Не выбраны категории для распределния');
			}
		}
		if(Tools::isSubmit('submitUndo')){
			$backupSQL = Tools::getValue('backupSQL');
			if($backupSQL){ //Подхватываем input[type=hidden] с прошлого запроса submitReferenceMove
				$backupSQLs = unserialize($backupSQL);
				$i = 0;
				foreach($backupSQLs as $data){
					$id_category = $data[0];
					$id_product = $data[1];
					if((int)$id_product && (int)$id_category){
						if(Db::getInstance()->Execute('DELETE  FROM `'._DB_PREFIX_.'category_product` WHERE id_product = '.$id_product.' AND id_category = '.$id_category)) $i++;
					}
				}
				$this->confirmations[] ='В результате отмены действия обработано запросов:'.$i;
			}
		}
		parent::postProcess();
	}

	public static function searchByReference($reference){
		return Db::getInstance()->getValue('SELECT id_product FROM `'._DB_PREFIX_.'product` WHERE TRIM(`reference`) LIKE "'.pSQL($reference).'"');
	}

	public function checkLink($id_category, $id_product, $addIfNot = true, $makeBackup = true){
		$linkExist = Db::getInstance()->getValue('SELECT count(id_category) FROM `'._DB_PREFIX_.'category_product` WHERE id_product = '.(int)$id_product.' AND id_category = '.(int)$id_category);
		if($linkExist) return 0;
		else{
			if($addIfNot){
				if($makeBackup) $this->backupSQL[] = array($id_category, $id_product);
				Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'category_product` (id_product, id_category) VALUES ('.$id_product.', '.$id_category.')');
			}
			return 1;
		}
	}


    public static function categoryHasChild($id_category){
        return Db::getInstance()->getValue('
        SELECT COUNT(c.id_category) as count FROM `'._DB_PREFIX_.'category` c
         '.Shop::addSqlAssociation('category', 'c').' WHERE c.id_parent = '.(int)$id_category);
    }



	/*public function renderList() {



		$site_tree = new HelperTreeCategories('categories-tree', $this->l('Filter by category'));
		$site_tree
			->setTemplateDirectory(_PS_MODULE_DIR_ . 'priceparser/views/templates/admin/price_parser_duplicator/helpers/site_cat_tree/')
			->setUseCheckBox(true)
			->setUseSearch(true);
		
		$site_cat_tree = $site_tree->render();
		
		$modal_tree = new HelperTreeCategories('categories-tree', $this->l('Filter by category'));
		$modal_tree
			->setTemplateDirectory(_PS_MODULE_DIR_ . 'priceparser/views/templates/admin/price_parser_duplicator/helpers/site_cat_tree/')
			->setUseCheckBox(true)
			->setId('modal-categories-tree')
			->setUseSearch(true);
		
		$modal_cat_tree = $modal_tree->render();
		
		$tpl = $this->context->smarty->createTemplate(_PS_MODULE_DIR_ . 'priceparser/views/templates/admin/price_parser_duplicator/list.tpl');
		
		$vendors = Db::getInstance()->executeS('SELECT s.name, v.id_root_category'
				. ' FROM ' . _DB_PREFIX_ . 'price_parser_vendor AS v'
				. ' INNER JOIN ' . _DB_PREFIX_ . 'supplier AS s ON s.id_supplier = v.id_supplier'
				. ' ORDER BY s.name ASC');
		
		$tpl->assign(array(
			'site_cat_tree' => $site_cat_tree,
			'modal_cat_tree' => $modal_cat_tree,
			'vendors' => $vendors,
		));
		
		return $tpl->fetch();
	}*/
	
	public function setMedia() {
		parent::setMedia();

		$this->addJS(array(
		//	_MODULE_DIR_ . 'priceparser/js/admin/duplicator.js'
		));
	}
	
	/*public function ajaxSaveCategoryDuplication() {
		$id_first_cat = Tools::getValue('id_first_cat');
		$ids_duplicate_cats = Tools::getValue('ids_duplicate_cats', array());
		$subaction = Tools::getValue('subaction');

		if(!$id_first_cat || !$ids_duplicate_cats) {
			throw new PrestaShopException('Некорректные параметры запроса', 1);
		}
		
		$result_count = false;
		
		array_walk($ids_duplicate_cats, function(&$el) {$el = (int)$el;});
		
		switch($subaction) {
			case 'bind':
		
				$products = Db::getInstance()->executeS('SELECT id_product FROM `'._DB_PREFIX_.'category_product`'
						. ' WHERE id_category = ' . (int)$id_first_cat);

				$result = true;

				if(!empty($products)) {
					foreach($products as $p_row) {
						$_Product = new Product($p_row['id_product']);
						$result &= $_Product->addToCategories($ids_duplicate_cats);
					}
					
				}
				
				$result_count = count($products);
				
				break;
			
			case 'unbind':
				
				\Db::getInstance()->execute('DELETE s_cp
					FROM ps_category_product AS f_cp
					INNER join ps_category_product AS s_cp ON s_cp.id_product = f_cp.id_product
					WHERE f_cp.id_category = ' . (int)$id_first_cat . ' AND s_cp.id_category IN (' . implode(',', $ids_duplicate_cats) . ')');
				
				$result_count = \Db::getInstance()->Affected_Rows();
				
				break;
			
			default:
				throw new PrestaShopException('Некорректные параметры запроса', 1);
				break;
		}
		
		
		
		return [
			'count' => $result_count,
		];
	}
	*/
}
