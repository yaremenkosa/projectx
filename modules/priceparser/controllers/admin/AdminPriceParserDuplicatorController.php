<?php

require_once dirname(__FILE__) . "/../../classes/VendorCatTree.php";

class AdminPriceParserDuplicatorController extends ModuleAdminController {
	
	var $bootstrap = true;
	
	public function postProcess() {
		if(Tools::getValue('ajax')) {
			$action = Tools::getValue('action');
			$message = '';
			$data = array();
			
			if(method_exists($this, 'ajax' . $action)) {
				try {
					$result = $this->{'ajax' . $action}();
					
					die(json_encode([
						'success' => true,
						'data' => $result,
						'message' => $message,
					]));
					
				} catch(PrestaShopException $ex) {
					if($ex->getCode() === 1) {
						$message = $ex->getMessage();
					}

					die(json_encode([
						'success' => false,
						'data' => false,
						'message' => $message,
					]));
				}
			}
		}
	}
	
	public function renderList() {
		
		$site_tree = new HelperTreeCategories('categories-tree', $this->l('Filter by category'));
		$site_tree
			->setTemplateDirectory(_PS_MODULE_DIR_ . 'priceparser/views/templates/admin/price_parser_duplicator/helpers/site_cat_tree/')
			->setUseCheckBox(true)
			->setUseSearch(true);
		
		$site_cat_tree = $site_tree->render();
		
		$modal_tree = new HelperTreeCategories('categories-tree', $this->l('Filter by category'));
		$modal_tree
			->setTemplateDirectory(_PS_MODULE_DIR_ . 'priceparser/views/templates/admin/price_parser_duplicator/helpers/site_cat_tree/')
			->setUseCheckBox(true)
			->setId('modal-categories-tree')
			->setUseSearch(true);
		
		$modal_cat_tree = $modal_tree->render();
		
		$tpl = $this->context->smarty->createTemplate(_PS_MODULE_DIR_ . 'priceparser/views/templates/admin/price_parser_duplicator/list.tpl');
		
		$vendors = Db::getInstance()->executeS('SELECT s.name, v.id_root_category'
				. ' FROM ' . _DB_PREFIX_ . 'price_parser_vendor AS v'
				. ' INNER JOIN ' . _DB_PREFIX_ . 'supplier AS s ON s.id_supplier = v.id_supplier'
				. ' ORDER BY s.name ASC');
		
		$tpl->assign(array(
			'site_cat_tree' => $site_cat_tree,
			'modal_cat_tree' => $modal_cat_tree,
			'vendors' => $vendors,
		));
		
		return $tpl->fetch();
	}
	
	public function setMedia() {
		parent::setMedia();

		$this->addJS(array(
			_MODULE_DIR_ . 'priceparser/js/admin/duplicator.js'
		));
	}
	
	public function ajaxSaveCategoryDuplication() {
		$id_first_cat = Tools::getValue('id_first_cat');
		$ids_duplicate_cats = Tools::getValue('ids_duplicate_cats', array());
		$subaction = Tools::getValue('subaction');

		if(!$id_first_cat || !$ids_duplicate_cats) {
			throw new PrestaShopException('Некорректные параметры запроса', 1);
		}
		
		$result_count = false;
		
		array_walk($ids_duplicate_cats, function(&$el) {$el = (int)$el;});
		
		switch($subaction) {
			case 'bind':
		
				$products = Db::getInstance()->executeS('SELECT id_product FROM `'._DB_PREFIX_.'category_product`'
						. ' WHERE id_category = ' . (int)$id_first_cat);

				$result = true;

				if(!empty($products)) {
					foreach($products as $p_row) {
						$_Product = new Product($p_row['id_product']);
						$result &= $_Product->addToCategories($ids_duplicate_cats);
					}
					
				}
				
				$result_count = count($products);
				
				break;
			
			case 'unbind':
				
				\Db::getInstance()->execute('DELETE s_cp
					FROM ps_category_product AS f_cp
					INNER join ps_category_product AS s_cp ON s_cp.id_product = f_cp.id_product
					WHERE f_cp.id_category = ' . (int)$id_first_cat . ' AND s_cp.id_category IN (' . implode(',', $ids_duplicate_cats) . ')');
				
				$result_count = \Db::getInstance()->Affected_Rows();
				
				break;
			
			default:
				throw new PrestaShopException('Некорректные параметры запроса', 1);
				break;
		}
		
		
		
		return [
			'count' => $result_count,
		];
	}
}
