<?php

require_once dirname(__FILE__) . "/../../PriceParser/autoloader.php";
require_once dirname(__FILE__) . "/../../classes/VendorCatTree.php";

class AdminPriceParserCategoryBindController extends ModuleAdminController
{
	function __construct() {
        $this->table          = 'price_parser_vendor_categories_bind';
        //$this->className      = 'PWHookSlidersObject';
        $this->explicitSelect = true;
        $this->module         = 'priceparser';
        $this->lang           = false;
        $this->bootstrap      = true;
        $this->need_instance  = 0;
        $this->image_dir 	  = '../modules/pwhooksliders/upload_images';
        $this->context        = Context::getContext();
        $this->db             = Db::getInstance();
		$this->identifier	 = 'id_bind';

		$this->_orderBy	 = 'a.id_vendor';
		$this->_orderWay = 'ASC';

		$this->fields_list = array(
		    'id_bind' => array(
                'title'	 => 'ID',
                'align'	 => 'center',
            ),
			'id_vendor'	 => array(
				'title'	 => 'Поставщик',
				'align'	 => 'center',
			),
            'id_vendor_category'	 => array(
                'title'	 => 'Внешная категория',
                'align'	 => 'center',
                'callback' => 'getNames'
            ),
            'id_category'	 => array(
                'title'	 => 'Внутренняя категория',
                'align'	 => 'center',
                'callback' => 'getCategoryNames'
            ),
		);

		parent::__construct();
	}

    public function getNames($id_category) {
	    $categories = $this->getVendorCategories('intimmoll');
        $result = '';
	    foreach ($categories as $category) {
            if($category['id'] == $id_category) {
                $result = $category['name'];
                break;
            }
        }

        return $result;
    }

    public function getCategoryNames($id_category) {
        $ids = explode(',', $id_category);

        $result = '';
        foreach($ids as $id) {
            $category = new Category((int)$id);
            $result .= $category->getName() . ',';
        }

        return $result;
    }

    public function renderList()
    {
        $this->addRowAction('add');
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        return parent::renderList();
    }

    public function renderForm() {
	    if(Tools::isSubmit('id_bind')) {

	        $sql = "SELECT * FROM `" . _DB_PREFIX_ . "price_parser_vendor_categories_bind` WHERE `id_bind` = '" . Tools::getValue('id_bind') . "'";
	        $bind_info = DB::getInstance()->getRow($sql);

	        $selected_categories = explode(',', $bind_info['id_category']);

        }

        $this->fields_form = array(
            'legend' => array(
                'title' => 'Добавление связи',
            ),
            'input' => array(
                array(
                    'type'  => 'hidden',
                    'name'  => 'id_bind'
                ),
                array(
                    'type' => 'select',
                    'label' => 'Поставщик',
                    'name' => 'id_vendor',
                    'required' => true,
                    'options' => array(
                        'query' => Supplier::getSuppliers(),
                        'id' => 'name',
                        'name' => 'name',
                    ),
                    'default_value' => (isset($bind_info['id_vendor']) ? $bind_info['id_vendor'] : null)
                ),
                array(
                    'type' => 'select',
                    'label' => 'Внешняя категория',
                    'name' => 'id_vendor_category',
                    'required' => true,
                    'options' => array(
                        'query' => $this->getVendorCategories('intimmoll', true),
                        'id' => 'id',
                        'name' => 'name',
                        'default' => array(
                            'label' => $this->trans('--Выберите категорию--', array(), 'Admin.Global'),
                            'value' => 0,
                        ),
                    ),
                    'default_value' => (isset($bind_info['id_vendor_category']) ? $bind_info['id_vendor_category'] : null)

                ),
                array(
                    'type' => 'categories',
                    'label' => 'Внутренния категории',
                    'name' => 'id_category',
                    'required' => true,
                    'tree' => array(
                        'root_category' => 2,
                        'id' => 'id_category',
                        'name' => 'name_category',
                        'use_checkbox' => true,
                        'selected_categories' => (isset($selected_categories) ? $selected_categories : array()),
                    ),
                ),

            ),
            'submit' => array(
                'title' => 'Сохранить',
            )
        );

        return parent::renderForm();
    }

	public function postProcess() {

	    if(Tools::isSubmit('submitAddprice_parser_vendor_categories_bind')) {
            $id_vendor = Tools::getValue('id_vendor');
            $id_vendor_category = Tools::getValue('id_vendor_category');
            $id_categories = Tools::getValue('id_category');

            if (!$id_vendor || !$id_vendor_category || !$id_categories || $id_vendor_category == 0) {
                $this->errors[] = $this->trans('Не все поля заполнены.', array(), 'Admin.Notifications.Error');
            } else {
                if(Tools::isSubmit('id_bind') && Tools::getValue('id_bind') != '') {
                    $sql = "UPDATE `" . _DB_PREFIX_ . "price_parser_vendor_categories_bind` SET 
                    `id_vendor` = '" . $id_vendor . "', 
                    `id_vendor_category` = '" . $id_vendor_category . "', 
                    `id_category` = '" . implode(',', $id_categories) . "' 
                    WHERE `id_bind` = '" . Tools::getValue('id_bind') . "'";
                    DB::getInstance()->execute($sql);
                } else {
                    $sql = "INSERT INTO `" . _DB_PREFIX_ . "price_parser_vendor_categories_bind` SET 
                    `id_category` = '" . implode(',', $id_categories) . "', 
                    `id_vendor` = '" . $id_vendor . "',
                    `id_vendor_category` = '" . $id_vendor_category . "'";
                    DB::getInstance()->execute($sql);
                }
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminPriceParserCategoryBind'));
            }
        }

        if(Tools::isSubmit('deleteprice_parser_vendor_categories_bind')) {
            $sql = "DELETE FROM `" . _DB_PREFIX_ . "price_parser_vendor_categories_bind` WHERE `id_bind` = '" . Tools::getValue('id_bind') . "'";
            DB::getInstance()->execute($sql);
        }
	}
	
	public function ajaxLoadCategoryBinding() {
		$id_vendor = Tools::getValue('id_vendor');
		$id_vendor_cat = Tools::getValue('id_vendor_cat');

		if(!$id_vendor || !$id_vendor_cat) {
			throw new PrestaShopException('Некорректные параметры запроса', 1);
		}

		$cat_binding_ids = array();

		$cat_binding_result = Db::getInstance()->executeS('SELECT id_category'
				. ' FROM ' . _DB_PREFIX_ . 'price_parser_vendor_categories_bind'
				. ' WHERE id_vendor = "' . pSQL($id_vendor) . '"'
				. ' AND id_vendor_category = "' . pSQL($id_vendor_cat) . '"');

		if(!empty($cat_binding_result)) {
			foreach($cat_binding_result as $row) {
				$cat_binding_ids[] = $row['id_category'];
			}
		}
		
		return array(
			'ids_categories' => $cat_binding_ids,
		);
	}
	
	public function ajaxSaveCategoryBinding() {
		$id_vendor = Tools::getValue('id_vendor');
		$id_vendor_cat = Tools::getValue('id_vendor_cat');
		$ids_site_cats = Tools::getValue('ids_site_cats', array());

		if(!$id_vendor || !$id_vendor_cat) {
			throw new PrestaShopException('Некорректные параметры запроса', 1);
		}
		
		array_walk($ids_site_cats, function(&$el) {$el = (int)$el;});
		
		//вытаскиваем текущие связки
		$current_ids = array();
		
		$result = Db::getInstance()->executeS('SELECT id_category FROM ' . _DB_PREFIX_ . 'price_parser_vendor_categories_bind'
				. ' WHERE id_vendor = "' . pSQL($id_vendor) . '"'
				. ' AND id_vendor_category = "' . pSQL($id_vendor_cat) . '"');
		
		if(!empty($result)) {
			foreach($result as $row) {
				$current_ids[] = (int)$row['id_category'];
			}
		}
		
		$to_del_ids = array_diff($current_ids, $ids_site_cats);
		$to_add_ids = array_diff($ids_site_cats, $current_ids);
		
		if(!empty($to_del_ids)) {
			$res_del = Db::getInstance()->delete('price_parser_vendor_categories_bind', 'id_vendor = "' . pSQL($id_vendor) . '"'
				. ' AND id_vendor_category = "' . pSQL($id_vendor_cat) . '"'
				. ' AND id_category IN (' . implode(',', $to_del_ids) . ')');
			
			if(!$res_del) {
				throw new PrestaShopException('Ошибка обработки данных', 1);
			}
		}
		
		if(!empty($to_add_ids)) {
			foreach($to_add_ids as $id_category) {
				$res_insert = Db::getInstance()->insert('price_parser_vendor_categories_bind', array(
					'id_vendor' => pSQL($id_vendor),
					'id_vendor_category' => pSQL($id_vendor_cat),
					'id_category' => (int)$id_category,
				));
				
				if(!$res_insert) {
					throw new PrestaShopException('Ошибка обработки данных', 1);
				}
			}
		}
		
		$new_count = Db::getInstance()->getValue('SELECT COUNT(*) FROM ' . _DB_PREFIX_ . 'price_parser_vendor_categories_bind'
				. ' WHERE id_vendor = "' . pSQL($id_vendor) . '"'
				. ' AND id_vendor_category = "' . pSQL($id_vendor_cat) . '"');
		
		return array(
			'id_vendor_cat'	=> $id_vendor_cat,
			'count'			=> $new_count,
		);
	}

	public function getVendorCategories($id_vendor, $show_info = false) {
	    if(!$id_vendor) return array();

        $runconfig = \PriceParser\Core\Tools::loadConfig($id_vendor . '_config.php');
        $file = $runconfig['import']['params']['loader']['params']['save_path'] . $runconfig['import']['params']['loader']['params']['items']['products']['save_name'];

        if(!file_exists($file)) return array();

        $result = array();

        $data = simplexml_load_file($file);
        $categories = $data->shop->categories;
        foreach($categories->category as $category) {

            $sql = "SELECT `id_bind` FROM `" . _DB_PREFIX_ . "price_parser_vendor_categories_bind` WHERE `id_vendor` = '" . $id_vendor . "' AND `id_vendor_category` = '" . (int)$category->attributes()->id . "'";
            $added = DB::getInstance()->getValue($sql);


            $result[] = array(
                'id' => (int)$category->attributes()->id,
                'name' => ($added ? '+ ' . (string)$category : (string)$category)
            );
        }

        return $result;
    }
}
