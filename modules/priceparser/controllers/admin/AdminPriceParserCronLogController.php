<?php

require_once dirname(__FILE__) . "/../../PriceParser/autoloader.php";
\PriceParser\Autoloader::load('CronRunLog', 'Cron/Model');

class AdminPriceParserCronLogController extends ModuleAdminController {
	
	private $original_filter = '';
	
	function __construct() {
		$this->table	 = 'price_parser_cron_run_log';
		$this->identifier	 = 'id_cron_run_log';
		$this->className = 'CronRunLog';
		$this->lang		 = false;

		$this->explicitSelect	 = false;
		$this->allow_export		 = true;
		$this->deleted			 = false;
		$this->context			 = Context::getContext();
		$this->bootstrap		 = true;
		$this->list_simple_header=false;

		$this->_select = 's.title, a.id_cron_run_log AS view_log_id';

		$this->_join = 'LEFT JOIN ' . _DB_PREFIX_ . 'price_parser_cron_schedule AS s on s.id_cron_schedule = a.id_cron_schedule';

		$this->_orderBy	 = 'a.id_cron_run_log';
		$this->_orderWay = 'DESC';

		$this->fields_list = array(
			'id_cron_run_log'	 => array(
				'title'	 => 'ID записи',
				'align'	 => 'center',
				'width'	 => 40,
			),
			'id_cron_schedule'	 => array(
				'title'	 => 'ID задачи',
				'align'	 => 'center',
				'width'	 => 40,
				'filter_key' => 'a!id_cron_schedule',
			),
			'title'	 => array(
				'title'	 => 'Название задачи',
				'align'	 => 'center',
			),
			'start_date'	 => array(
				'title'	 => $this->l('Старт'),
				'align'	 => 'center',
			),
			'finish_date'	 => array(
				'title'	 => $this->l('Окончание'),
				'align'	 => 'center',
			),
			'status'		 => array(
				'title'	 => $this->l('Статус'),
				'align'	 => 'center',
			),
//			'pid'	 => array(
//				'title'	 => $this->l('PID'),
//				'align'	 => 'center',
//			),
			'date_add'		 => array(
				'title'	 => $this->l('Создан'),
				'align'	 => 'center',
			),
			'view_log_id'	 => array(
				'align' => 'text-center',
				'callback' => 'printViewFileLog',
				'title'	 => $this->l('Лог'),
				'align'	 => 'center',
				'orderby' => false,
				'search' => false,
				'remove_onclick' => true,
			),
		);

		parent::__construct();
	}
	
	public function initProcess() {
		parent::initProcess();
		if (Tools::getIsset('duplicate' . $this->table)) {
			$this->action = 'duplicate';
		}
		
		if(($action = Tools::getValue('submitAction')) !== false) {
			$this->action = $action;
		}
	}
	
	public function initToolbar() {
		if ($this->display != 'list') {
			return parent::initToolbar();
		}
	}
	
	private function getConfigsList() {
		$result = array();
		chdir(dirname(__FILE__) . '/../../PriceParser/Configs');
		
		$files = glob('*_config.*');
		
		if(!empty($files)) {
			foreach($files as $file) {
				$result[] = array(
					'file' => $file,
				);
			}
		}
		
		chdir(dirname(__FILE__));
		
		return $result;
	}
	
	public function printManualRun($id_item, $tr) {
		$this->context->smarty->assign(array(
			'tr' => $tr
		));
		return $this->createTemplate('_print_manual_run.tpl')->fetch();
	}
	
	public function printViewLogs($id_item, $tr) {
		$this->context->smarty->assign(array(
			'tr' => $tr
		));
		return $this->createTemplate('_print_view_logs.tpl')->fetch();
	}
	
	public function processManualRun() {
		$id_item = Tools::getValue('id_item');
		$state = Tools::getValue('state');
		if(($id_item !== false) && ($state !== false)) {
			$obj = new CronSchedule($id_item);
			
			if(Validate::isLoadedObject($obj)) {
				$obj->manual_run = $state;
				$obj->save();
			}
		}
		
		$this->redirect_after = self::$currentIndex.'&token='.$this->token;
	}
	
	public function ajaxProcessStatusPriceParserCronSchedule() {
		if (!$id_item = (int)Tools::getValue('id_cron_schedule')) {
			die(Tools::jsonEncode(array('success' => false, 'error' => true, 'text' => $this->l('Failed to update the status'))));
		} else {
			$obj = new CronSchedule($id_item);
			if (Validate::isLoadedObject($obj)) {
				$obj->is_enabled = $obj->is_enabled == 1 ? 0 : 1;
				$obj->save() ?
					die(Tools::jsonEncode(array('success' => true, 'text' => $this->l('The status has been updated successfully')))) :
					die(Tools::jsonEncode(array('success' => false, 'error' => true, 'text' => $this->l('Failed to update the status'))));
			}
		}
	}
	
	public function printViewFileLog($id_item, $tr) {
		$log_file_name = 'summary_' . $tr['id_cron_run_log'] . '.log';
		
		if(!file_exists(_PS_MODULE_DIR_ . 'priceparser/PriceParser/logs/summary/' . $log_file_name)) {
			return false;
		}
		
		$this->context->smarty->assign(array(
			'tr' => $tr,
			'log_uri' => __PS_BASE_URI__ . 'modules/priceparser/PriceParser/logs/summary/' . $log_file_name,
		));
		return $this->createTemplate('_print_view_file_log.tpl')->fetch();
	}
	
}
