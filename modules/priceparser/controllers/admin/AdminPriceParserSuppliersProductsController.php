<?php

class AdminPriceParserSuppliersProductsController extends ModuleAdminController {


	public $bootstrap = true;

	public function __construct() {
		$this->table = 'supplier';
		$this->className = 'Supplier';
        $this->identifier = "id_supplier";
        $this->addRowAction('copy');
		$this->addRowAction('view');
		$this->addRowAction('edit');
		//$this->addRowAction('delete');
		$this->allow_export = true;
        $this->no_link = true;

		$this->_defaultOrderBy = 'name';
		$this->_defaultOrderWay = 'ASC';

		$this->bulk_actions = array(
			'delete' => array(
				'text' => $this->l('Delete selected'),
				'icon' => 'icon-trash',
				'confirm' => $this->l('Delete selected items?')
			)
		);

        $use_shop_restriction = true;
		Db::getInstance()->execute('create temporary table vendor_cats (id_category INT NOT NULL)');
        $sql = '
			insert into vendor_cats (id_category)
			SELECT c.id_category
				FROM ' . _DB_PREFIX_ . 'category AS c
				'.($use_shop_restriction ? Shop::addSqlAssociation('category', 'c') : '').'
				INNER JOIN (
					SELECT c.nleft
						,c.nright
						,c.level_depth
					FROM ' . _DB_PREFIX_ . 'price_parser_vendor AS ppv
					INNER JOIN ' . _DB_PREFIX_ . 'category AS c ON c.id_category = ppv.id_root_category
					'.($use_shop_restriction ? Shop::addSqlAssociation('category', 'c') : '').'
                    WHERE ppv.id_supplier = ' . (int) Tools::getValue('id_supplier') . '
					) AS vendor_root_c ON c.nleft >= vendor_root_c.nleft
					AND c.nright <= vendor_root_c.nright
					AND c.level_depth >= vendor_root_c.level_depth;';
        //d($sql);
		Db::getInstance()->execute($sql);

		Db::getInstance()->execute('create temporary table holiday_cats (id_category INT NOT NULL)');
				Db::getInstance()->execute('
					insert into holiday_cats (id_category)
					SELECT c.id_category
						FROM ' . _DB_PREFIX_ . 'category AS c
						'.($use_shop_restriction ? Shop::addSqlAssociation('category', 'c') : '').'
						INNER JOIN (
							SELECT c.nleft
								,c.nright
								,c.level_depth
							FROM ' . _DB_PREFIX_ . 'category AS c
							WHERE c.id_category = 4661
							ORDER BY c.id_category ASC
							) AS holiday_root_c ON c.nleft >= holiday_root_c.nleft
							AND c.nright <= holiday_root_c.nright
							AND c.level_depth >= holiday_root_c.level_depth
							;');

        Db::getInstance()->execute('create temporary table holiday_cats2 LIKE holiday_cats;');
        Db::getInstance()->execute('INSERT INTO holiday_cats2 SELECT * FROM holiday_cats');

		$this->fields_list = array(
			/*'id_product' => array(
				'title' => '#',
				//				''
			),*/
			'name' => array(
				'title' => 'Название',
				'align' => 'center',
                'width' => 400
			),
			'reference' => array(
				'title' => 'Артикул',
				'align' => 'center',
			),
			'id_category_default' => array(
				'title' => 'Основная категория',
				'align' => 'center',
			),
			'dubl_cats' => array(
				'title' => 'Категории дублирования',
				'align' => 'center',
				'filter_key' => 'dubl_cat!id_category',
				'order_key' => 'dubl_cat!id_category',
			),
			'hol_cats' => array(
				'title' => 'Праздник',
				'align' => 'center',
				'filter_key' => 'hol_cat_l!name',
				'order_key' => 'hol_cat!id_category',
			),
            'dubl' => array(
                'title' => 'Дублирован',
                'align' => 'center',
                'filter_key' => 'dubl_cat!id_category',
                'order_key' => 'dubl_cat!id_category',
            ),
            'price' => array(
                'title' => 'Цена',
                'type' => 'price',
                'align' => 'center',
            ),
		);
        if (Configuration::get('PS_STOCK_MANAGEMENT'))
            $this->fields_list['sav_quantity'] = array(
                'title' => $this->l('Количество'),
                'type' => 'int',
                'align' => 'text-right',
                'orderby' => true,
                'badge_danger' => true,
                //'hint' => $this->l('This is the quantity available in the current shop/group.'),
            );

        $this->fields_list['orders_list'] = array(
            'title' => $this->l('Заказы'),
            'align' => 'center'
        );

		parent::__construct();

		$this->_select = 'pl.name, sav.`quantity` as sav_quantity, CASE WHEN (dubl_cat.id_category IS NOT NULL) THEN 1 ELSE 0 END AS dubl,
		    CASE WHEN (ppc.reference IS NOT NULL) THEN 1 ELSE 0 END AS green,
		    CASE WHEN (hol_cat.id_category IS NOT NULL) THEN 1 ELSE 0 END AS dublHoliday
		    '
			. ', GROUP_CONCAT(DISTINCT dubl_cat.id_category SEPARATOR \',\') AS dubl_cats'
            . ', GROUP_CONCAT(DISTINCT od.id_order SEPARATOR \',\') AS orders_list'
			. ', GROUP_CONCAT(DISTINCT hol_cat_l.name ORDER BY hol_cat_c.position ASC SEPARATOR \',\') AS hol_cats'
			;
		$this->table = 'product';
		$this->_join = 'LEFT JOIN ' . _DB_PREFIX_ . 'product_lang pl on pl.id_product = a.id_product AND pl.id_lang = ' . (int) $this->context->language->id
			. ' LEFT JOIN ' . _DB_PREFIX_  . 'category_product AS dubl_cat ON dubl_cat.id_product = a.id_product'
				. ' AND dubl_cat.id_category NOT IN (select id_category FROM vendor_cats) AND dubl_cat.id_category NOT IN (select id_category FROM holiday_cats2)'
			. ' LEFT JOIN ' . _DB_PREFIX_  . 'category_product AS hol_cat ON hol_cat.id_product = a.id_product'
				. ' AND hol_cat.id_category IN (select id_category FROM holiday_cats)'
            . ' LEFT JOIN ' . _DB_PREFIX_  . 'price_parser_copy AS ppc ON ppc.reference = a.reference AND ppc.id_supplier = '.(int) Tools::getValue('id_supplier')
            . ' LEFT JOIN ' . _DB_PREFIX_  . 'order_detail AS od ON od.product_id = a.id_product'
            . ' LEFT JOIN ' . _DB_PREFIX_  . 'category_shop AS hol_cat_c ON hol_cat.id_category = hol_cat_c.id_category AND hol_cat_c.id_shop = '.$this->context->shop->id
			. ' LEFT JOIN ' . _DB_PREFIX_ . 'category_lang AS hol_cat_l ON hol_cat_l.id_category = hol_cat.id_category AND hol_cat_l.id_lang = ' . (int) $this->context->language->id.
            ' LEFT JOIN `'._DB_PREFIX_.'stock_available` sav ON (sav.`id_product` = a.`id_product` AND sav.`id_product_attribute` = 0
		'.StockAvailable::addSqlShopRestriction(null, null, 'sav').') ';
		;
		$this->_group = 'GROUP BY a.id_product';
		$this->_where = 'AND a.id_supplier = ' . (int) Tools::getValue('id_supplier');

	}



    public function displayViewLink($token, $id, $name) {
        return '<a target="_blank" href="'.'/'.$id.'-.html'.'" class="btn btn-default">Просмотр</a>';
    }
    public function displayEditLink($token, $id, $name) {
        return '<a target="_blank" href="index.php?controller=AdminProducts&id_product=' . $id . '&updateproduct'
        . '&token=' . Tools::getAdminTokenLite('AdminProducts') . '" class="btn btn-default">Редактирование</a>';
    }

    public function setMedia()
    {
        parent::setMedia(); // TODO: Change the autogenerated stub

        $this->addCSS(array(
            _MODULE_DIR_ . 'priceparser/css/admin.css'
        ));
        $this->addJS(array(
            _MODULE_DIR_ . 'priceparser/js/admin/supplier.js'
        ));
    }

    public function ajaxProcessMarkGreen()
    {
        $references = explode(",", Tools::getValue('ids'));
        $id_supplier = Tools::getValue('id_supplier');
        $i=0;
        if(count($references)){
            foreach($references as $r){
                $r = trim($r);
                $sql = 'SELECT COUNT(*) FROM `'._DB_PREFIX_.'price_parser_copy` WHERE id_supplier = '.(int)$id_supplier.' AND `reference` = "'.$r.'"';
                if(!Db::getInstance()->getValue($sql) && strlen($r)){
                    if(Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'price_parser_copy` (reference, id_supplier) VALUES ("'.$r.'", '.(int)$id_supplier.')')){
                        $i++;
                    }
                }
            }
        }
        // Response
        die(Tools::jsonEncode(array(
            'ids'=>$references,
            'count' => htmlspecialchars($i)
        )));
    }


}