<?php

require_once dirname(__FILE__) . "/../../PriceParser/autoloader.php";
\PriceParser\Autoloader::load('CronSchedule', 'Cron/Model');

class AdminPriceParserCronController extends ModuleAdminController {
	
	private $original_filter = '';
	
	function __construct() {
		$this->table	 = 'price_parser_cron_schedule';
		$this->identifier	 = 'id_cron_schedule';
		$this->className = 'CronSchedule';
		$this->lang		 = false;

		$this->explicitSelect	 = false;
		$this->allow_export		 = true;
		$this->deleted			 = false;
		$this->context			 = Context::getContext();
		$this->bootstrap		 = true;
		$this->list_simple_header=false;

//		$this->addRowAction('view');
		$this->addRowAction('duplicate');
		$this->addRowAction('edit');
		$this->addRowAction('delete');

		$this->_select = 'title AS view_log_name, status AS status_kill';

//		$this->_join = 'LEFT JOIN ' . _DB_PREFIX_ . 'landing_group AS lg on a.id_landing_group = lg.id_landing_group';

		$this->_orderBy	 = 'a.id_cron_schedule';
		$this->_orderWay = 'ASC';

		$this->fields_list = array(
			'id_cron_schedule'	 => array(
				'title'	 => 'ID',
				'align'	 => 'center',
				'width'	 => 25
			),
			'title'	 => array(
				'title'	 => $this->l('Название'),
				'align'	 => 'center',
			),
			'run_config'	 => array(
				'title'	 => $this->l('Конфиг'),
				'align'	 => 'center',
			),
			'period'		 => array(
				'title'	 => $this->l('Период'),
				'align'	 => 'center',
				'width'	 => 50,
			),
			'last_run_date'	 => array(
				'title'	 => $this->l('Последний запуск'),
				'align'	 => 'center',
			),
			'is_enabled'	 => array(
				'title'	 => $this->l('Включен'),
				'align'	 => 'center',
				'active' => 'status',
				'type' => 'bool',
				'class' => 'fixed-width-xs',
				'ajax' => true,
				'orderby' => false
			),
			'status'	 => array(
				'align' => 'text-center',
				'callback' => 'printStatus',
				'title'	 => $this->l('Статус'),
				'align'	 => 'center',
				'orderby' => false,
				'search' => false,
				'remove_onclick' => true,
			),
			'status_kill'	 => array(
				'align' => 'text-center',
				'callback' => 'printKill',
				'title'	 => $this->l('Завершить'),
				'align'	 => 'center',
				'orderby' => false,
				'search' => false,
				'remove_onclick' => true,
			),
			'date_add'		 => array(
				'title'	 => $this->l('Создан'),
				'align'	 => 'center',
			),
			'date_upd'		 => array(
				'title'	 => $this->l('Обновлён'),
				'align'	 => 'center',
			),
			'manual_run'	 => array(
				'align' => 'text-center',
				'callback' => 'printManualRun',
				'title'	 => $this->l('Запуск вручную'),
				'align'	 => 'center',
				'orderby' => false,
				'search' => false,
				'remove_onclick' => true,
			),
			'view_log_name'	 => array(
				'align' => 'text-center',
				'callback' => 'printViewLogs',
				'title'	 => $this->l('Логи'),
				'align'	 => 'center',
				'orderby' => false,
				'search' => false,
				'remove_onclick' => true,
			),
		);

		parent::__construct();
	}
	
	public function renderList() {
		if (isset($this->_filter) && trim($this->_filter) == '') {
			$this->_filter = $this->original_filter;
		}
		
		return parent::renderList();
	}
	
	public function initProcess() {
		parent::initProcess();
		if (Tools::getIsset('duplicate' . $this->table)) {
			$this->action = 'duplicate';
		}
		
		if(($action = Tools::getValue('submitAction')) !== false) {
			$this->action = $action;
		}
	}
	
	public function initToolbar() {
		if ($this->display != 'list') {
			return parent::initToolbar();
		}
	}
	
	public function renderView() {
		return parent::renderView();
	}
	
	public function renderForm() {
		if (!($obj = $this->loadObject(true))) {
			return;
		}

		$this->fields_form = array(
			'legend' => array(
				'title'	 => $this->l('Menus'),
				'icon'	 => 'icon-list-ul'
			),
			'input'	 => array(
				array(
					'type'		 => 'text',
					'label'		 => 'Название',
					'name'		 => 'title',
					'required'	 => true,
				),
				array(
					'type'		 => 'select',
					'label'		 => 'Конфиг',
					'name'		 => 'run_config',
					'options'	 => array(
						'query'	 => \PriceParser\Core\Tools::getConfigsList(),
						'id'	 => 'file',
						'name'	 => 'file',
					),
					'required'	 => true,
				),
				array(
					'type'		 => 'select',
					'label'		 => 'Периодичность',
					'name'		 => 'period',
					'options'	 => array(
						'query'	 => array(
							array('value' => 'never'),
							array('value' => 'hourly'),
							array('value' => 'daily'),
						),
						'id'	 => 'value',
						'name'	 => 'value',
					),
					'required'	 => true,
				),
				array(
					'type'	 => 'checkbox',
					'name'	 => 'is_enabled',
					'values' => array(
						'query'	 => array(
							array(
								'id'	 => 'on',
								'name'	 => 'Включено',
								'val'	 => 1,
							)
						),
						'id'	 => 'id',
						'name'	 => 'name'
					),
				),

			),
			'submit' => array(
				'title' => $this->l('Save'),
			)
		);

		$this->fields_value = array(
			'is_enabled_on'		 => $this->getFieldValue($obj, 'is_enabled'),
		);

		return parent::renderForm();
	}
	public function postProcess() {
		if (Tools::isSubmit('submitAdd' . $this->table)) {
			$_POST['is_enabled']		 = (int) Tools::getValue('is_enabled_on');
		}

		$object = parent::postProcess();
	}
	
	public function printManualRun($id_item, $tr) {
		$this->context->smarty->assign(array(
			'tr' => $tr
		));
		return $this->createTemplate('_print_manual_run.tpl')->fetch();
	}
	
	public function printViewLogs($id_item, $tr) {
		$this->context->smarty->assign(array(
			'tr' => $tr
		));
		return $this->createTemplate('_print_view_logs.tpl')->fetch();
	}
	
	public function printStatus($id_item, $tr) {
		$this->context->smarty->assign(array(
			'tr' => $tr
		));
		return $this->createTemplate('_print_status.tpl')->fetch();
	}
	
	public function printKill($id_item, $tr) {
		$this->context->smarty->assign(array(
			'tr' => $tr
		));
		return $this->createTemplate('_print_kill.tpl')->fetch();
	}
	
	public function processManualRun() {
		$id_item = Tools::getValue('id_item');
		$state = Tools::getValue('state');
		if(($id_item !== false) && ($state !== false)) {
			$obj = new CronSchedule($id_item);
			
			if(Validate::isLoadedObject($obj)) {
				$obj->manual_run = $state;
				$obj->save();
			}
		}
		
		$this->redirect_after = self::$currentIndex.'&token='.$this->token;
	}
	
	public function ajaxProcessStatusPriceParserCronSchedule() {
		if (!$id_item = (int)Tools::getValue('id_cron_schedule')) {
			die(Tools::jsonEncode(array('success' => false, 'error' => true, 'text' => $this->l('Failed to update the status'))));
		} else {
			$obj = new CronSchedule($id_item);
			if (Validate::isLoadedObject($obj)) {
				$obj->is_enabled = $obj->is_enabled == 1 ? 0 : 1;
				$obj->save() ?
					die(Tools::jsonEncode(array('success' => true, 'text' => $this->l('The status has been updated successfully')))) :
					die(Tools::jsonEncode(array('success' => false, 'error' => true, 'text' => $this->l('Failed to update the status'))));
			}
		}
	}
	
	public function processTaskKill() {
		$this->redirect_after = self::$currentIndex.'&token='.$this->token;
		
		$id_item = (int)Tools::getValue('id_item');
		if (!$id_item) {
			return false;
		}
		$obj = new CronSchedule($id_item);
		if (!Validate::isLoadedObject($obj)) {
			return false;
		}
		if(!$obj->status) {
			return false;
		}
		\PriceParser\Autoloader::load('CronRunLog', 'Cron/Model');
		$log = CronRunLog::findLastItemBySchedule($obj->id);

		if($log->pid) {
			$kill_result = posix_kill($log->pid, 15);
			
			if($kill_result) {

				$log->status = 'killed';

				$log->save();

				$obj->status = 0;

				if($obj->manual_run) {
					$obj->manual_run = false;
				}

				$obj->save();
			}
		}
	}
	
}
