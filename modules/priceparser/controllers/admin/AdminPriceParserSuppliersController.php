<?php

/**
 * Description of AdminPriceParserSuppliersController
 *
 * @author konsul
 */
class AdminPriceParserSuppliersController extends ModuleAdminController {

	public $bootstrap = true;



	public function __construct() {
		$this->table = 'supplier';
		$this->className = 'Supplier';

		$this->addRowAction('view');
		$this->addRowAction('edit');
		$this->addRowAction('delete');
		$this->allow_export = true;


		$this->_defaultOrderBy = 'name';
		$this->_defaultOrderWay = 'ASC';

		$this->multishop_context = false;

		$this->bulk_actions = array(
			'delete' => array(
				'text' => 'Delete selected',
				'icon' => 'icon-trash',
				'confirm' => 'Delete selected items?'
			)
		);

		$this->_select = 'COUNT(DISTINCT ps.`id_product`) AS products';
		$this->_join = 'LEFT JOIN `' . _DB_PREFIX_ . 'product_supplier` ps ON (a.`id_supplier` = ps.`id_supplier`)';
		$this->_group = 'GROUP BY a.`id_supplier`';

		$this->fieldImageSettings = array('name' => 'logo', 'dir' => 'su');

		$this->fields_list = array(
			'id_supplier' => array('title' => 'ID', 'align' => 'center', 'class' => 'fixed-width-xs'),
			'logo' => array('title' => 'Logo', 'align' => 'center', 'image' => 'su', 'orderby' => false, 'search' => false),
			'name' => array('title' => 'Name'),
			'products' => array('title' => 'Number of products', 'align' => 'right', 'filter_type' => 'int', 'tmpTableFilter' => true),
			'active' => array('title' => 'Enabled', 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false, 'class' => 'fixed-width-xs')
		);

		parent::__construct();

		if (!Tools::getValue('id_supplier') && in_array($this->context->cookie->priceparsersupplierssupplierOrderby, array('reference','dubl','id_category_default','dubl_cats',))) {
            $this->context->cookie->priceparsersupplierssupplierOrderby = 'name';
            $this->context->cookie->write();
        } elseif (Tools::getValue('id_supplier') && in_array($this->context->cookie->priceparsersupplierssupplierOrderby, array('products'))) {
            $this->context->cookie->priceparsersupplierssupplierOrderby = 'name';
            $this->context->cookie->write();
        }

	}

	public function setMedia($isNewTheme = false) {
		parent::setMedia($isNewTheme);
		$this->addJqueryUi('ui.widget');
		$this->addJqueryPlugin('tagify');
	}

	public function initPageHeaderToolbar() {
		if (empty($this->display))
			$this->page_header_toolbar_btn['new_supplier'] = array(
				'href' => self::$currentIndex . '&addsupplier&token=' . $this->token,
				'desc' => 'Добавить поставщика',
				'icon' => 'process-icon-new'
			);

		parent::initPageHeaderToolbar();
	}

	public function displayViewLink($token, $id, $name) {
		return '<a href="index.php?controller=AdminPriceParserSuppliersProducts&id_supplier=' . $id . '&viewsupplier'
			. '&token=' . Tools::getAdminTokenLite('AdminPriceParserSuppliersProducts') . '" class="btn btn-default">Просмотр</a>';
	}

	public function renderView() {

		Db::getInstance()->execute('create temporary table vendor_cats (id_category INT NOT NULL)');
		Db::getInstance()->execute('
			insert into vendor_cats (id_category)
			SELECT c.id_category
				FROM ' . _DB_PREFIX_ . 'category AS c
				INNER JOIN (
					SELECT c.nleft
						,c.nright
						,c.level_depth
					FROM ' . _DB_PREFIX_ . 'price_parser_vendor AS ppv
					INNER JOIN ' . _DB_PREFIX_ . 'category AS c ON c.id_category = ppv.id_root_category
					WHERE ppv.id_supplier = ' . (int) Tools::getValue('id_supplier') . '
					) AS vendor_root_c ON c.nleft >= vendor_root_c.nleft
					AND c.nright <= vendor_root_c.nright
					AND c.level_depth >= vendor_root_c.level_depth;');

		$this->fields_list = array(
			'id_product' => array(
				'title' => '#',
//				''
			),
			'name' => array(
				'title' => 'Название',
				'align' => 'center',
			),
			'reference' => array(
				'title' => 'Артикул',
				'align' => 'center',
			),
			'dubl' => array(
				'title' => 'Дублирован',
				'align' => 'center',
				'filter_key' => 'dubl_cat!id_category',
				'order_key' => 'dubl_cat!id_category',
			),
			'id_category_default' => array(
				'title' => 'Основная категория',
				'align' => 'center',
			),
			'dubl_cats' => array(
				'title' => 'Категории дублирования',
				'align' => 'center',
				'filter_key' => 'dubl_cat!id_category',
				'order_key' => 'dubl_cat!id_category',
			),
		);

		$this->_select = 'pl.name, CASE WHEN (dubl_cat.id_category IS NOT NULL) THEN 1 ELSE 0 END AS dubl'
			. ', GROUP_CONCAT(DISTINCT dubl_cat.id_category SEPARATOR \',\') AS dubl_cats';
		$this->table = 'product';
		$this->_join = 'LEFT JOIN ' . _DB_PREFIX_ . 'product_lang pl on pl.id_product = a.id_product AND pl.id_lang = ' . (int) $this->context->language->id
			. ' LEFT JOIN ' . _DB_PREFIX_  . 'category_product AS dubl_cat ON dubl_cat.id_product = a.id_product'
				. ' AND dubl_cat.id_category NOT IN (select id_category FROM vendor_cats)';
		$this->_group = 'GROUP BY a.id_product';
		$this->_where = 'AND a.id_supplier = ' . (int) Tools::getValue('id_supplier');

		if (!($this->fields_list && is_array($this->fields_list)))
			return false;
		$this->getList($this->context->language->id);

		// If list has 'active' field, we automatically create bulk action
		if (isset($this->fields_list) && is_array($this->fields_list) && array_key_exists('active', $this->fields_list)
			&& !empty($this->fields_list['active']))
		{
			if (!is_array($this->bulk_actions))
				$this->bulk_actions = array();

			$this->bulk_actions = array_merge(array(
				'enableSelection' => array(
					'text' => 'Enable selection',
					'icon' => 'icon-power-off text-success'
				),
				'disableSelection' => array(
					'text' => 'Disable selection',
					'icon' => 'icon-power-off text-danger'
				),
				'divider' => array(
					'text' => 'divider'
				)
			), $this->bulk_actions);
		}

		/* @var $helper HelperListCore */
		$helper = new HelperList();

		// Empty list is ok
		if (!is_array($this->_list))
		{
			$this->displayWarning('Bad SQL query'.'<br />'.htmlspecialchars($this->_list_error));
			return false;
		}

		$this->setHelperDisplay($helper);
		$helper->currentIndex = $helper->currentIndex . '&viewsupplier';

		//ох
		$this->context->smarty->assign('current', $helper->currentIndex);

//		die($helper->currentIndex);
		$helper->tpl_vars = $this->tpl_list_vars;
		$helper->tpl_delete_link_vars = $this->tpl_delete_link_vars;

		// For compatibility reasons, we have to check standard actions in class attributes
		foreach ($this->actions_available as $action)
		{
			if (!in_array($action, $this->actions) && isset($this->$action) && $this->$action)
				$this->actions[] = $action;
		}
		$helper->is_cms = $this->is_cms;

		return $helper->generateList($this->_list, $this->fields_list);
	}

}
