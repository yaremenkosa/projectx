function copyBuffer(elem){
    var value = $(elem).html().trim();
    console.log(value);
    if(!$(elem).hasClass('markedOrange')){
        copied.push(value);
    }else{
        deleteFromArray(value, copied)
    }
    $(elem).toggleClass('markedOrange');
    $('#copyBuffer').val(copied.join(',') + (copied.length ? ',': ''));
}
$( document ).ready(function() {
    $('#copyBuffer').on('change', function(){
        copied = $(this).val().split(',');
        markReferences(copied);
    });
    $('#resetCopy').on('click', function(){
        copied = [];
        $('#copyBuffer').val('');
        markReferences(copied);
    });

    $('#copyToBuffer').on('click', function(){
        copyToClipBoard();
    });
    $('#resetCopyMark').on('click', function(){
        $.ajax({
            type: 'POST',
            url: moduleTabUrl,

            dataType: 'json',
            data: {
                action : 'MarkGreen',
                ajax : true,
                id_supplier: id_supplier,
                ids : copied.join(",")
            },
            success: function(jsonData)
            {
                if(jsonData) {
                    confirm_modal_my('Товары отмечены', jsonData.count + ' шт.', '', '', '', '');
                }
            }
        });
    });
    $('.markedRed, .markedGreen').on('click', function(){
        var reference = $(this).html().trim();
        confirm_modal_my('Хотите скопировать уже выбранный артикул?', '', 'Да', 'Нет', 'copyBuffer($(\'#r'+reference+'\'));', '');
    })
});

function closeModal(){
    $(this)
}

function markReferences(arr){
    $('.reference').removeClass('markedOrange');
    arr.forEach(function(item, i, arr) {
        if(item.length && $('#r'+item).length){
            $('#r'+item).addClass('markedOrange');
        }
    });
}

function copyToClipBoard(){
    var copyTextarea = document.querySelector('#copyBuffer');
    copyTextarea.select();
    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
    } catch (err) {
        console.log('Oops, unable to copy');
    }
}

function deleteFromArray(item, array){
    while (array.indexOf(item) !== -1) {
        array.splice(array.indexOf(item), 1);
    }
}
/*
Переделал, чтобы кнопки показывались кнопки опционально
 */
function confirm_modal_my(heading, question, left_button_txt, right_button_txt, left_button_callback) {
    var confirmModal =
        $('<div class="bootstrap modal hide fade">' +
        '<div class="modal-dialog">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<a class="close" data-dismiss="modal" >&times;</a>' +
        '<h3>' + heading + '</h3>' +
        '</div>' +
        '<div class="modal-body">' +
        '<p>' + question + '</p>' +
        '</div>' +
        '<div class="modal-footer">' +
        (left_button_txt ? '<a href="#" '+(left_button_callback ? 'onclick="'+left_button_callback+'" ' : '') +'id="confirm_modal_left_button" class="btn btn-primary">' + left_button_txt + '</a>' : '') +
        (right_button_txt ? '<a href="#" id="confirm_modal_right_button" class="btn btn-primary">' + right_button_txt + '</a>' : '') +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>');
    confirmModal.find('#confirm_modal_left_button').click(function () {
       // left_button_callback();
        confirmModal.modal('hide');
    });
    confirmModal.find('#confirm_modal_right_button').click(function () {
        confirmModal.modal('hide');
    });
    confirmModal.modal('show');
}

var copied = [];

