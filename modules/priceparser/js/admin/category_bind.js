(function ($) {
	$.fn.category_bind = function (options) {
		
		var defaults = {
			vendor_tree_id: null,
			site_tree_id: null
		};

		var options = $.extend({}, defaults, options);

		var cache = {};
		
		var modal;
		
		var $vendor_tree;
		
		var $site_tree;
		
		var id_vendor;
		
		
		var methods = {
			init: function() {
				$site_tree = $('#' + options.site_tree_id);
				$vendor_tree = $('#' + options.vendor_tree_id);
				id_vendor = $vendor_tree.data('id-vendor');
				$('.tree-item', $vendor_tree).click(function() {
					var id_vendor_cat = $(this).data('category-id');
					
					if(!id_vendor_cat) {
						alert('Выбрана некорректная категория');
						
						return false;
					}
					
					methods.show(id_vendor_cat, $('.category-title', $(this)).text());
				});
				$('.btn-save', $site_tree).click(function() {
					methods.saveData(function(result) {
						if(result !== false && result.success) {
							methods.setVendorBindCount(result.data.id_vendor_cat, result.data.count);
						} else {
							alert('При загрузке данных возникла ошибка: ' + result.message);
						}
						modal.modal('hide');
					});
				});
			},
			show: function(id_vendor_cat, vendor_cat_title) {
				methods.clearSiteTree();
				modal = $site_tree.modal();
				$('.panel', modal).hide();
				$('.loading', modal).remove();
				$('.modal-title').text(vendor_cat_title);
				$('.modal-body', modal).append('<div class="loading">Идёт загрузка данных</div>');
				
				methods.loadData(id_vendor_cat, function(result) {
					if(result !== false && result.success) {
						methods.setSiteTree(result.data.ids_categories);
						$('.loading', modal).remove();
						$('.panel', modal).show();
						
						modal.data('id_vendor_cat', id_vendor_cat);
					} else {
						modal.modal('hide');
						alert('При загрузке данных возникла ошибка: ' + result.message);
					}
				});
			},
			loadData: function(id_vendor_cat, callback) {
				$.ajax({
					type: 'GET',
					dataType: 'json',
					method: 'post',
					data: {'ajax': 1, 'action': 'LoadCategoryBinding', 'id_vendor_cat': id_vendor_cat, 'id_vendor': id_vendor},
					success: function(result) {
						callback(result);
					},
					error: function() {
						callback(false);
					}
				});
			},
			saveData: function(callback) {
				var id_vendor_cat = modal.data('id_vendor_cat');
				
				if(!id_vendor_cat) {
					console.log('Error: incorrect vendor id category');
					return false;
				}
				
				var ids_site_cats = new Array();
				
				$.each($('input[type=checkbox]:checked', $site_tree), function(key, obj) {
					ids_site_cats.push($(obj).val());
					
				});
				
				$.ajax({
					type: 'GET',
					dataType: 'json',
					method: 'post',
					data: {'ajax': 1, 'action': 'SaveCategoryBinding', 'id_vendor_cat': id_vendor_cat, 'id_vendor': id_vendor, 'ids_site_cats': ids_site_cats},
					success: function(result) {
						callback(result);
					},
					error: function() {
						callback('<div class="alert alert-warning">Произошла ошибка. Повторите действие позже</div>');
					}
				});
			},
			clearSiteTree: function() {
				$('input[type=checkbox]', $site_tree).prop('checked', false);
				$('.tree-folder-name.tree-selected, tree-item-name.tree-selected', $site_tree).removeClass('tree-selected');
			},
			setSiteTree: function(cats_ids) {
				if(cats_ids.length) {
					$.each(cats_ids, function(key, cat_id) {
						$checkbox = $('input[type=checkbox]', $site_tree).filter(function() {
							return parseInt($(this).val()) == parseInt(cat_id);
						});
						
						if($checkbox.length) {
							$checkbox.prop('checked', 'checked');
						}
					});
				}
				$.each($('input[type=checkbox]', $site_tree), function(key, obj) {
					
				});
			},
			setVendorBindCount: function(id_vendor_cat, count) {
				$('#vendor-categories-tree .tree-item').filter(function() {
					return $(this).data('category-id') == id_vendor_cat;
				}).find('.bind-count').text(count);
			}
		};
		
		methods.init();

	};
})(jQuery);