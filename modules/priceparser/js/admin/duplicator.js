(function ($) {
	$.fn.duplicator = function (options) {
		
		var defaults = {
			first_tree_id: null,
			modal_tree_id: null
		};

		var options = $.extend({}, defaults, options);

		var cache = {};
		
		var modal;
		
		var $modal_tree;
		
		var $first_tree;
		
		var methods = {
			init: function() {
				$first_tree = $('#' + options.first_tree_id);
				$modal_tree = $('#' + options.modal_tree_id);
				$('.tree-item a', $first_tree).click(function() {
					var id_first_cat = $(this).data('category-id');
					
					if(!id_first_cat) {
						alert('Выбрана некорректная категория');
						
						return false;
					}
					
					methods.show(id_first_cat, $('label', $(this)).text());
					
					return false;
				});
				$('.btn-bind', $modal_tree).click(function() {
					methods.saveData('bind',function(result) {
						if(result !== false && result.success) {
							if(result.data.count !== false) {
								alert('Привязано ' + result.data.count + ' товаров');
							}
						} else {
							alert('При загрузке данных возникла ошибка: ' + result.message);
						}
						modal.modal('hide');
					});
				});
				
				$('.btn-unbind', $modal_tree).click(function() {
					methods.saveData('unbind',function(result) {
						if(result !== false && result.success) {
							if(result.data.count !== false) {
								alert('Отвязано ' + result.data.count + ' товаров');
							}
						} else {
							alert('При загрузке данных возникла ошибка: ' + result.message);
						}
						modal.modal('hide');
					});
				});
			},
			show: function(id_first_cat, first_cat_title) {
				methods.clearModalTree();
				modal = $modal_tree.modal();
//				$('.panel', modal).hide();
//				$('.loading', modal).remove();
				$('.modal-title').text(first_cat_title);
				modal.data('id_first_cat', id_first_cat);
//				$('.modal-body', modal).append('<div class="loading">Идёт загрузка данных</div>');
				
				/*methods.loadData(id_vendor_cat, function(result) {
					if(result !== false && result.success) {
						methods.setSiteTree(result.data.ids_categories);
						$('.loading', modal).remove();
						$('.panel', modal).show();
						
						modal.data('id_vendor_cat', id_vendor_cat);
					} else {
						modal.modal('hide');
						alert('При загрузке данных возникла ошибка: ' + result.message);
					}
				});*/
			},
			loadData: function(id_vendor_cat, callback) {
				$.ajax({
					type: 'GET',
					dataType: 'json',
					method: 'post',
					data: {'ajax': 1, 'action': 'LoadCategoryBinding', 'id_vendor_cat': id_vendor_cat, 'id_vendor': id_vendor},
					success: function(result) {
						callback(result);
					},
					error: function() {
						callback(false);
					}
				});
			},
			saveData: function(subaction, callback) {
				var id_first_cat = modal.data('id_first_cat');
				
				if(!id_first_cat) {
					console.log('Error: incorrect first id category');
					return false;
				}
				
				var ids_duplicate_cats = new Array();
				
				$.each($('input[type=checkbox]:checked', $modal_tree), function(key, obj) {
					ids_duplicate_cats.push($(obj).val());
				});
				
				if(ids_duplicate_cats.length === 0) {
					alert('Не выбрана ни одна категория!');
					
					return false;
				}
				
				$.ajax({
					type: 'GET',
					dataType: 'json',
					method: 'post',
					data: {'ajax': 1, 'action': 'SaveCategoryDuplication', 'subaction': subaction, 'id_first_cat': id_first_cat, 'ids_duplicate_cats': ids_duplicate_cats},
					success: function(result) {
						callback(result);
					},
					error: function() {
						callback('<div class="alert alert-warning">Произошла ошибка. Повторите действие позже</div>');
					}
				});
			},
			clearModalTree: function() {
				$('input[type=checkbox]', $modal_tree).prop('checked', false);
				$('.tree-folder-name.tree-selected, tree-item-name.tree-selected', $modal_tree).removeClass('tree-selected');
			},
			/*setSiteTree: function(cats_ids) {
				if(cats_ids.length) {
					$.each(cats_ids, function(key, cat_id) {
						$checkbox = $('input[type=checkbox]', $site_tree).filter(function() {
							return parseInt($(this).val()) == parseInt(cat_id);
						});
						
						if($checkbox.length) {
							$checkbox.prop('checked', 'checked');
						}
					});
				}
				$.each($('input[type=checkbox]', $site_tree), function(key, obj) {
					
				});
			},*/
			/*setVendorBindCount: function(id_vendor_cat, count) {
				$('#vendor-categories-tree .tree-item').filter(function() {
					return $(this).data('category-id') == id_vendor_cat;
				}).find('.bind-count').text(count);
			}*/
		};
		
		methods.init();

	};
})(jQuery);