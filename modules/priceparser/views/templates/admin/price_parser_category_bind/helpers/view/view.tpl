{if isset($vendor_tree)}
	<div id="vendor_category_block" data-id-vendor="{$id_vendor}">
		{$vendor_tree}
	</div>
{/if}

<div class="modal fade" id="siteCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				{$site_tree}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
				<button type="button" class="btn btn-primary btn-save">Сохранить изменения</button>
			</div>
		</div>
	</div>
</div>

{literal}
	<script>
		$(document).ready(function() {
			$(document).category_bind({
				vendor_tree_id: 'vendor_category_block',
				site_tree_id: 'siteCategoryModal'
			});
		});
	</script>
{/literal}