{if $tr['manual_run'] == '1'}
	<a class="btn btn-default" href="{$link->getAdminLink('AdminPriceParserCron')|escape:'html':'UTF-8'}&amp;submitAction=manualRun&amp;id_item={$tr.id_cron_schedule}&amp;state=0">
		<i class="icon-stop"></i>
	</a>
{else}
	<a class="btn btn-default" href="{$link->getAdminLink('AdminPriceParserCron')|escape:'html':'UTF-8'}&amp;submitAction=manualRun&amp;id_item={$tr.id_cron_schedule}&amp;state=1">
		<i class="icon-play"></i>
	</a>
{/if}