<script>
	$(document).ready(function() {
		$('.vendors-list > li > a').click(function() {
			var cat_id = $(this).data('cat-id');
			$el = $('#categories-tree input[type=checkbox]').filter(function() {
				return parseInt($(this).val()) === parseInt(cat_id);
			});
			
			if($el) {
				$(window).scrollTo($el.offset().top - $('header .navbar-header').height() - $('#main .page-head').height() - 10);
			}
			
			return false;
		});
		
		$(document).duplicator({
			first_tree_id: 'container_first_tree',
			modal_tree_id: 'siteCategoryModal'
		});
	});
</script>

<style>
#siteCategoryModal .modal-footer {
	position: fixed;
	top: 0;
	background: #fff;
	z-index: 999999;
	right: 0;
}
</style>

<div class="panel col-lg-12">
	<div class="panel-heading">
		Категории сайта
	</div>
	
	{if !empty($vendors)}
		<p>Поставщики:</p>
		<ul class="vendors-list list-unstyled">
			{foreach from=$vendors item=vendor_row}
				<li><a href="#" data-cat-id="{$vendor_row.id_root_category}">{$vendor_row.name}</a></li>
			{/foreach}
		</ul>
	{/if}
	
	<div id="container_first_tree">
		{$site_cat_tree}
	</div>
</div>
	
<div class="modal fade" id="siteCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
		<button type="button" class="btn btn-primary btn-bind">Привязать</button>
		<button type="button" class="btn btn-primary btn-unbind">Отвязать</button>
	</div>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				{$modal_cat_tree}
			</div>
		</div>
	</div>
</div>