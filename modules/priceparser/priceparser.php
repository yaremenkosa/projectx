<?php
if (!defined('_PS_VERSION_')) {
	exit;
}

class priceparser extends Module {

	const CONFIG_KEY_PREFIX = 'PRICE_PARSER_';
	
	protected $tabs = array(
		array(
			'class_name'		 => 'AdminPriceParserCron',
			'title'				 => 'Парсер товаров',
			'profiles_access'	 => array(),
			'_children'			 => array(
				/*array(
					'class_name'		 => 'AdminPriceParserDuplicator',
					'title'				 => 'Дублятор',
					'profiles_access'	 => array(),
				),*/
				array(
					'class_name'		 => 'AdminPriceParserCron',
					'title'				 => 'Cron задачи',
					'profiles_access'	 => array(),
				),
				/*array(
					'class_name'		 => 'AdminPriceParserCronLog',
					'title'				 => 'Логи запуска',
					'profiles_access'	 => array(),
				),*/
				array(
					'class_name'		 => 'AdminPriceParserCategoryBind',
					'title'				 => 'Связывание категорий',
					'profiles_access'	 => array(),
				),
                /*array(
                    'class_name'		 => 'AdminPriceParserReferenceMove',
                    'title'				 => 'Распределятор',
                  'profiles_access'	 => array(),
                ),*/
                array(
                    'class_name'		 => 'AdminPriceParserSuppliers',
                    'title'				 => 'Поставщики',
                    'profiles_access'	 => array(),
                ),/*
                array(
                    'class_name'		 => 'AdminPriceParserSuppliersProducts',
                    'title'				 => 'Товары поставщиков',
                    'profiles_access'	 => array(),
                ),*/
			)
		),
	);
	
	protected $hooks = array('actionProductUpdate');
	
	public function __construct() {
		$this->name			 = 'priceparser';
		$this->tab			 = 'other';
		$this->version		 = 0.1;
		$this->author		 = 'Konsul';
		$this->need_instance = 0;
		$this->bootstrap	 = true;

		parent::__construct();

		$this->displayName	 = $this->l('Парсер товаров и цен');
		$this->description	 = $this->l('Универсальный парсер товаров, цен, остатков');
	}

	
	public function install() {
		if(parent::install()) {
			$result = Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'price_parser_ids` (
					`id_item_site` INT NOT NULL,
					`id_item_type` SMALLINT NULL,
					`id_item_vendor` VARCHAR(255) NULL,
					`id_vendor` varchar(170),
					`update_desc` int(1) DEFAULT 0 COMMENT \'Определяет, обновлялись ли описания в админке\',
					`update_price` int(1) DEFAULT 0 COMMENT \'Определяет, обновлялись ли цена в админке\',
					`price_rr` decimal(20,6) COMMENT \'Для сохранения рекомендуемой цены\',
					PRIMARY KEY (`id_item_site`,`id_item_type`, `id_item_vendor`,`id_vendor`),
					INDEX `index2` (`id_item_type` ASC, `id_item_vendor` ASC),
					KEY `index3` (`id_vendor`));')
				&& Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'price_parser_run_session` (
					`session_hash` VARCHAR(40) NOT NULL,
					`param` VARCHAR(255) NOT NULL,
					`value` VARCHAR(255) NULL,
					PRIMARY KEY (`session_hash`, `param`),
					INDEX `index2` (`param` ASC));')
				&& Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'price_parser_status` (
					`name` VARCHAR(255) NOT NULL,
					`value` VARCHAR(255) NOT NULL,
					`id_vendor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
					PRIMARY KEY (`name`, `id_vendor`),
					KEY `index2` (`id_vendor`));')
				&& Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'price_parser_cron_schedule` (
					`id_cron_schedule` INT NOT NULL AUTO_INCREMENT,
					`title` VARCHAR(100) NULL COMMENT \'Название задачи\',
					`run_config` VARCHAR(255) NULL COMMENT \'название конфига запуска\',
					`period` ENUM(\'never\',\'hourly\',\'daily\') NULL DEFAULT \'never\' COMMENT \'периодичность запуска\',
					`status` TINYINT NULL DEFAULT 0 COMMENT \'текущий статус\',
					`manual_run` TINYINT NULL DEFAULT 0 COMMENT \'единоразовый запуск вручную\',
					`last_run_date` DATETIME NULL,
					`is_enabled` TINYINT NULL DEFAULT 0,
					`date_add` DATETIME NULL,
					`date_upd` DATETIME NULL,
					PRIMARY KEY (`id_cron_schedule`),
					INDEX `index2` (`is_enabled` ASC),
					INDEX `index3` (`period` ASC, `last_run_date` ASC));')
				&& Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'price_parser_cron_run_log` (
					`id_cron_run_log` int(11) NOT NULL AUTO_INCREMENT,
					`id_cron_schedule` int(11) DEFAULT NULL,
					`start_date` datetime DEFAULT NULL,
					`finish_date` datetime DEFAULT NULL,
					`status` enum(\'started\',\'finished\',\'error\',\'killed\') COLLATE utf8_unicode_ci DEFAULT NULL,
					`pid` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
					`date_add` datetime DEFAULT NULL,
					`date_upd` datetime DEFAULT NULL,
					PRIMARY KEY (`id_cron_run_log`),
					KEY `index2` (`date_add`),
					KEY `index3` (`id_cron_schedule`));')
				&& Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'price_parser_vendor_categories_options` (
					`id_vendor` VARCHAR(255) NOT NULL,
					`cat_tree_config` VARCHAR(255) NOT NULL,
					PRIMARY KEY (`id_vendor`, `cat_tree_config`));')
				&& Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'price_parser_vendor_categories_bind` (
					`id_bind` INT NOT NULL AUTO_INCREMENT,
					`id_vendor` varchar(255) NOT NULL COMMENT \'поставщик\',
					`id_vendor_category` varchar(255) NOT NULL COMMENT \'id категории поставщика\',
					`id_category` int(10) unsigned NOT NULL COMMENT \'id категории на сайте\',
					PRIMARY KEY (`id_bind`),
					INDEX `id_vendor_id_vendor_category` (`id_vendor`, `id_vendor_category`));')
				&& Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'price_parser_vendor` (
					`id_vendor` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
					`id_vendor_str` varchar(255) NOT NULL,
					`id_supplier` int NOT NULL,
					`id_root_category` int NOT NULL,
					INDEX `id_vendor_str` (`id_vendor_str`)
				  )')
                && Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `ps_price_parser_copy` (
                  `id_product` int(8) NOT NULL DEFAULT "0"
                 ) ENGINE=InnoDB DEFAULT CHARSET=utf8')
				&& Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `ps_price_parser_product_log` (
                  `id_vendor` VARCHAR(100) NOT NULL,
                  `id_product` INT NOT NULL,
                  `data` TEXT,
                  UNIQUE KEY(`id_vendor`,`id_product`)
                 ) ENGINE=InnoDB DEFAULT CHARSET=utf8')
			;

			
			$tabs_ids = $this->_createTabs();

			$result &=!empty($tabs_ids);

			if(!empty($this->hooks)) {
				foreach($this->hooks as $hook) {
					$result &= $this->registerHook($hook);
				}
			}
			
			return $result;
		}
		
		return false;
	}
	
	private function _createTabs($tabs = null, $parent_id = 0) {
		if($tabs === null) {
			return $this->_createTabs($this->tabs);
		}
		
		$tabs_ids = array();
		
		foreach($tabs as $tab_item) {
			$tab_id = $this->_create_tab($tab_item, $parent_id);
			
			if(isset($tab_item['_children']) && is_array($tab_item['_children'])) {
				$children_ids = $this->_createTabs($tab_item['_children'], $tab_id);
			} else {
				$children_ids = array();
			}
			
			$tabs_ids = array_merge($tabs_ids, array($tab_id), $children_ids);
		}
		
		return $tabs_ids;
	}
	
	private function _create_tab($tab_params, $id_parent = 0) {
		$tab = new Tab(null, Configuration::get('PS_LANG_DEFAULT'));

		$tab->class_name = $tab_params['class_name'];
		$tab->name		 = $tab_params['title'];
		$tab->module	 = $this->name;
		$tab->id_parent	 = $id_parent;

		if ($tab->add()) {
			
			return $tab->id;
		}
		
		return false;
	}
	
	public function uninstall() {
		$result = parent::uninstall();
		
		$db = Db::getInstance();
		
//		$result &= $db->Execute('DROP TABLE `' . _DB_PREFIX_ . 'price_parser_ids`');
//		$result &= $db->Execute('DROP TABLE `' . _DB_PREFIX_ . 'price_parser_run_session`');
//		$result &= $db->Execute('DROP TABLE `' . _DB_PREFIX_ . 'price_parser_status`');
//		$result &= $db->Execute('DROP TABLE `' . _DB_PREFIX_ . 'price_parser_cron_schedule`');
//		$result &= $db->Execute('DROP TABLE `' . _DB_PREFIX_ . 'price_parser_cron_run_log`');
		
		//удаляем табы
		$tabs = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT t.`id_tab`
			FROM `' . _DB_PREFIX_ . 'tab` t
			WHERE t.`module` = "' . $this->name . '"');
		
		$tabs_ids = array();
		
		foreach ($tabs as $_tab_row) {
			$_tab = new Tab($_tab_row['id_tab']);
			$result &= $_tab->delete();
			
			$tabs_ids[] = $_tab_row['id_tab'];

			unset($_tab);
		}
		
		if(!empty($this->hooks)) {
			foreach($this->hooks as $hook) {
				$result &= $this->unregisterHook($hook);
			}
		}
		
		return $result;
	}

	public function getContent() {

/*        $tab = new Tab(null, Configuration::get('PS_LANG_DEFAULT'));

        $tab->class_name = 'AdminPriceParserSuppliers';
        $tab->name		 = 'Поставщики';
        $tab->module	 = $this->name;
        $tab->id_parent	 = 180;
        $tab->add();*/

		$params = [
			['key' => 'OLD_PRODUCTS_DAYS_EXPIRE', 'title' => 'Кол-во дней для устаренивания товара'],
			['key' => 'OLD_PRODUCTS_NOTIFY_EMAILS', 'title' => 'Список email для оповещения (через запятую)'],
            ['key' => 'TEST_PRODUCT', 'title' => 'Список ID товаров, для импорта (через запятую)'],
		];

		if (Tools::getValue('post')) {
			foreach($params as $param) {
				Configuration::updateValue(static::CONFIG_KEY_PREFIX . $param['key'], Tools::getValue($param['key']));
			}
		}


		$html = '<form method="POST">'
			.'<input type="hidden" name="post" value="1" />'
			.'<table><tr><th>Название</th><th>Значение</th></tr>';

		foreach($params as $param) {
			$html .= '<tr>'
				.'<td>' .  $param['title'] . '</td>'
				.'<td><input type="text" name="' . $param['key']  . '" value = "' . Configuration::get(static::CONFIG_KEY_PREFIX . $param['key']) . '" /></td>'
				.'</tr>';
		}

		$html .= '</table>'
			.'<input type="submit" value="Сохранить" class="btn btn-success" />'
			.'</form>';

		return $html;
	}

	public function hookactionProductUpdate($params) {
	    if(Tools::isSubmit('id_product')) {
            $price = Tools::getValue('price_displayed');
            $price_old = Tools::getValue('price_old');

            $sql_price = '';
            if($price != $price_old) // если изменили цену
                $sql_price = ", `update_price` = '1'";

            $sql = "UPDATE `" . _DB_PREFIX_ . "price_parser_ids` SET `update_desc` = '1'" . $sql_price . " WHERE `id_item_site` = '" . $params['id_product'] . "' AND `id_item_type` = '1'";
            DB::getInstance()->execute($sql);


        }
    }
}