<?php
if (!defined('_PS_VERSION_'))
    exit;

class pwphonemask extends Module
{
    public function __construct()
    {
        $this->name = 'pwphonemask';
        $this->tab = 'other';
        $this->version = 0.1;
        $this->author = 'PrestaWeb.ru';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l("Маски для телефонов");
        $this->description = $this->l("Маски для телефонов");
        
        $this->ps_versions_compliancy = array('min' => '1.5.0.0', 'max' => _PS_VERSION_);
    }

    public function install()
    {

        if ( !parent::install() 
			OR !$this->registerHook(Array(
				'displayHeader',
			))
            
        ) return false;
        Configuration::updateValue('PWPHONEMASK_MASK', '7(999) 999-9999');
        return true;
    }

    

    //start_helper
    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Настройки'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Маска для телефона'),
                        'name' => 'PWPHONEMASK_MASK',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Сохранить'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitPWPHONEMASK';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => array('PWPHONEMASK_MASK' => Configuration::get('PWPHONEMASK_MASK')),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getContent()
    {
        if (Tools::isSubmit('submitPWPHONEMASK')) {
            Configuration::updateValue('PWPHONEMASK_MASK', Tools::getValue('PWPHONEMASK_MASK'));
        }
        return $this->renderForm();
    }

	public function hookdisplayHeader($params)
    {
        Media::addJSDef(array('pwmaskphone' => Configuration::get('PWPHONEMASK_MASK')));
        $this->context->controller->addJS($this->_path.'js/jquery.maskedinput.min.js');
		$this->context->controller->addJS($this->_path.'js/'.$this->name.'.js');
	}


}


