$(document).ready(function(){
    $('body').on('focus', 'input#phone, input[name=phone], input#phone_mobile, input[name=phone_mobile], input#tel, input[type=tel]', function(){
        if (typeof pwmaskphone != 'undefined') {
            $(this).mask(pwmaskphone);
        }
    });
});