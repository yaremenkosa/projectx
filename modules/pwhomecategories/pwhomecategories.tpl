<div class="pwhomecategories clearfix">
    <div class="title">{l s='Наш каталог' mod='pwhomecategories'}</div>
    {if isset($categories) AND $categories}
            <ul>
            {foreach from=$categories item=category name=homeCategories}
                {assign var='categoryLink' value=$link->getcategoryLink($category.id_category, $category.link_rewrite)}
                <li class="ajax_block_category {if $smarty.foreach.homeCategories.first}first_item{elseif $smarty.foreach.homeCategories.last}last_item{else}item{/if}">
                    <a href="{$link->getCategoryLink($category.id_category, $category.link_rewrite)}" class="category_image">
                        <img src="{$img_cat_dir}{$category.id_category}-medium_default.jpg" alt="{$category.name}" title="{$category.name}" class="categoryImage" width="{$categorySize.width}" height="{$categorySize.height}" />
                    </a>
                    <a href="{$link->getCategoryLink($category.id_category, $category.link_rewrite)}">{$category.name|truncate:60}</a>
                </li>
            {/foreach}
            </ul>
    {else}
        <p class="alert alert-warning warning">{l s='Категорий нет' mod='pwhomecategories'}</p>
  {/if}
</div>