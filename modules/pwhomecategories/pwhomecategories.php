<?php

class PWHomecategories extends Module
{

    function __construct()
    {
        $this->name = 'pwhomecategories';
        $this->tab = 'front_office_features';
        $this->version = 1.4;
        $this->author = 'Prestaweb.ru';
        $this->need_instance = 0;

        parent::__construct(); // The parent construct is required for translations

        $this->page = basename(__FILE__, '.php');
        $this->displayName = $this->l('Категории на главной странице');
        $this->description = $this->l('');
    }

    function install()
    {
        return (parent::install() AND $this->registerHook('home') AND $this->registerHook('displayCat') AND $this->registerHook('header'));
    }


    public function hookHeader()
    {
        $this->context->controller->addCSS(($this->_path) . 'pwhomecategories.css', 'all');
    }

    function hookHome($params)
    {
        $categories = Category::getChildren(Category::getRootCategory()->id, $this->context->cookie->id_lang);

        $nb = intval(Configuration::get('HOME_categories_NBR'));

        $this->context->smarty->assign(array(
            'categories' => $categories,
            'categorySize' => Image::getSize('medium_default')
        ));
        return $this->display(__FILE__, 'pwhomecategories.tpl');
    }

    function hookdisplayCat($params)
    {
        $categories = Category::getChildren(Category::getRootCategory()->id, $this->context->cookie->id_lang);

        foreach($categories as $key => $category) {
            $child = Category::getChildren($category['id_category'], $this->context->cookie->id_lang);
            if(count($child) > 0)
                $categories[$key]['children'] = $child;
        }

        $nb = intval(Configuration::get('HOME_categories_NBR'));

        $this->context->smarty->assign(array(
            'categories' => $categories,
            'categorySize' => Image::getSize('medium_default')
        ));
        return $this->display(__FILE__, 'pwhomecategories_nav.tpl');
    }

}