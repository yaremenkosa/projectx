$(function() {
    $(document).ready(function() {
        $('.support_feedback-submit').fancybox({
            href: '#uipw-form_call_modal',
            afterClose: function () {
                $('#uipw-form_call_modal').trigger('reset').show();
                $('#uipw-form_call_modal .success').hide();
                $('#uipw-form_call_modal .uipw-modal_form_fields').css({'display':'block'});
                $('#uipw-form_call_modal').hide();
                noError();
            },
            afterLoad: function () {
                $(".fancybox-wrap").attr('id', 'uipw-call_modal');
                $("[name='URL']").attr('value', window.location.href);
            }
        });
    
    });

    if(typeof $.fn.draggable !== 'undefined') {
        $('#pwbackcall').draggable();
    }

    function noError() {
        var fields = $('#uipw-form_call_modal').find('input');
                    
        $.each(fields, function(i, el){
            $(el).removeClass('error').css({'display':'block'});
        });
    }

    $('.backcall-button').fancybox({
        href: '#uipw-form_call_modal',
        afterClose: function () {
            $('#uipw-form_call_modal').trigger('reset').show();
            $('#uipw-form_call_modal .success').hide();
            $('#uipw-form_call_modal .uipw-modal_form_fields').css({'display':'block'});
            $('#uipw-form_call_modal').hide();
            noError();
        },
        afterLoad: function () {
            $(".fancybox-wrap").attr('id', 'uipw-call_modal');
            $("[name='URL']").attr('value', window.location.href);
        }
    });

    $('#uipw-form_call_modal').on('submit', function (e) {
        e.preventDefault();

        var url = $(this).attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: $(this).serialize(),
            success: function (result) {
                if (result.status == 1) {
                    noError();
                    $('#uipw-form_call_modal .uipw-modal_form_fields').hide();
                    $('#uipw-form_call_modal .success').show();

                    $('#uipw-form_call_modal .title').hide();
                    $('#uipw-form_call_modal .info').hide();

					
					$(".uipw-modal_form_fields input[type=text], .uipw-modal_form_fields textarea").val('');//Очистить поля формы
					$(".uipw-modal_form_fields .err").hide();// Скрыть сообщения об ошибках

                    if (typeof PwBackCallJs === 'function') {
                        PwBackCallJs();
                    }
                } else {
                    $.each(result.errors, function (field, error) {
                        $('#call_'+field+'_err').show();
                        $('#uipw-form_call_modal input[name='+field+']').addClass('error');
                    });
                }
            },
            dataType: 'json'
        });
    });
    
    $("#pwbackcall").hover(function() {
        $(this).addClass('pw-hover');
    }, function() {
        $(this).removeClass('pw-hover');
    });
});


