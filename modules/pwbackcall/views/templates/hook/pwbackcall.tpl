{* {if $showbutton}
<div id="pwbackcall" class="pw-phone pw-green pw-show pw-static draggable backcall-button">
    <div class="pw-ph-circle"></div>
    <div class="pw-ph-circle-fill"></div>
    <div class="pw-ph-img-circle"></div>
</div>
{/if} *}

<form method="POST" action="{$pwbackcall.link}" id="uipw-form_call_modal">
    <div class="title">{l s='Enter your phone number' mod='pwbackcall'}</div>
    <div class="info">{l s='and we will call you back' mod='pwbackcall'}</div>
    <div class="success">{l s='Please wait for our call' mod='pwbackcall'}</div>
    <div class="uipw-modal_form_fields">
        <input type="hidden" name="action" value="call"/>
        <input type="hidden" name="URL" value=""/>
        {if $showfieldname}
            <div>
                <span class="err" id="call_name_err">{l s='Enter your name' mod='pwbackcall'}</span>
                <label for="call_name">{l s='Name' mod='pwbackcall'}<sup>*</sup></label>
                <input name="name" id="call_name" value="" type="text" tabindex="1">
            </div>
        {/if}
        <div>
            <span class="err" id="call_phone_err">{l s='Not valid phone number' mod='pwbackcall'}</span>
            <label for="call_phone">{l s='Phone' mod='pwbackcall'}<sup>*</sup></label>
            <input name="phone" id="call_phone" value="" type="tel" tabindex="2">
        </div>
        {if $showfieldemail}
            <div>
                <span class="err" id="call_email_err">{l s='Not valid email address' mod='pwbackcall'}</span>
                <label for="call_email">{l s='Email' mod='pwbackcall'}<sup>*</sup></label>
                <input name="email" id="call_email" value="" type="email" tabindex="3">
            </div>
        {/if}
        {if $showfieldcomment}
            <div>
                <label for="call_comment">{l s='Comment' mod='pwbackcall'}</label>
                <textarea name="comment" id="call_comment" cols="3" tabindex="4"
                    placeholder="{l s='Write your question' mod='pwbackcall'}"></textarea>
            </div>
        {/if}
        <input class="btn_pwbackcall" type="submit" value="{l s='Send' mod='pwbackcall'}" tabindex="5">
    </div>
</form>
