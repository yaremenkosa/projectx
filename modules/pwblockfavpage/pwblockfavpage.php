<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

/** @changelog **
 * 22.04.2016 - все переделали, версия 1.3
 * 27/07 - убрал домен в ссылках, при переносе доставляло неудобство
 */

class PWBlockFavPage extends Module
{
    private $data;
    private $errors;
    private $success;

    public function __construct()
    {
        $this->name          = 'pwblockfavpage';
        $this->tab           = 'front_office_features';
        $this->version       = 1.7;
        $this->author        = 'Prestaweb.ru';
        $this->need_instance = 0;
        $this->bootstrap     = true;
        $this->errors        = array();
        $this->success       = false;

        parent::__construct();

        $this->displayName = $this->l('Простое меню');
        $this->description = $this->l('Добавляет блок ссылок.');

        foreach(Language::getLanguages() as $lang){
            $this->data[$lang['id_lang']] = Configuration::get('PW_BLOCK_FAV_PAGE_LINKS', $lang['id_lang']);
        }
    }

    public function install()
    {
        return parent::install() AND $this->registerHook(array(
            'header', 'actionAdminControllerSetMedia', 'displayNav',
        )) AND $this->makeDefault();
    }

    public function uninstall()
    {
        Configuration::deleteByName('PW_BLOCK_FAV_PAGE_LINKS');
        return parent::uninstall();
    }
    
    private function makeDefault()
    {
        $shops = Shop::getShops();
        foreach($shops as $shop){
            $languages = Language::getLanguages(true, $shop['id_shop']);
            $data = array();
            foreach($languages as $lang){
                $array = array();
                $pages = CMS::getCMSPages($lang['id_lang'], 1, true, $shop['id_shop']);
                foreach ($pages as $page){
                    $array[] = array(
                        'url'  => str_replace('http://'.$this->context->shop->domain, '', $this->context->link->getCMSLink($page['id_cms'], $page['link_rewrite'])),
                        'name' => $page['meta_title'],
                    );
                }
                $data[$lang['id_lang']] = Tools::jsonEncode($array);
            }
            Configuration::updateValue('PW_BLOCK_FAV_PAGE_LINKS', $data, false, null, $shop['id_shop']);
        }
        return true;
    }

    public function getContent()
    {
        if ( Tools::isSubmit('submitPWBlockFavPageModule') )
            $this->postProcess();
        $blocknames = array();
        foreach(Language::getLanguages() as $lang){
            $blocknames[$lang['id_lang']] = Configuration::get('PW_BLOCK_FAV_PAGE_BLOCKNAME', $lang['id_lang']);
        }
		$this->context->smarty->assign(array(
            'data'    => $this->data,
            'errors'  => $this->errors,
            'success' => $this->success,
			'languages'   => Language::getLanguages(),
            'blockName' => $blocknames,
        ));

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
        return $output;
    }

    protected function postProcess()
    {
		foreach(Tools::getValue('shop') as $id_lang => $lang){
            if ( isset($lang['link-name']) AND $lang['link-name'] AND isset($lang['link-url']) AND $lang['link-url'] )
            {
                $links = array();
                foreach ($lang['link-name'] as $key => $link_name)
                {
                    if ( $lang['link-name'][$key] AND $lang['link-url'][$key] )
                        $links[] = array(
                            'name' => $lang['link-name'][$key],
                            'url'  => $lang['link-url'][$key],
                        );
                    elseif ( ($key+1) != count($lang['link-name']) )
                    {
                        // проверка $key потому что в форме есть скрытое поле для клонирования всегда пустое
                        $this->errors[] = Tools::displayError('У ссылки нужно указать и Название и URL для '.$shop['name'].' '.$lang.name);
                        return;
                    }
                }
                $this->data[$id_lang] = Tools::jsonEncode($links);
            }
		}
		Configuration::updateValue('PW_BLOCK_FAV_PAGE_BLOCKNAME', Tools::getValue('blockName'));
		Configuration::updateValue('PW_BLOCK_FAV_PAGE_LINKS', $this->data);
    }

    public function hookActionAdminControllerSetMedia()
    {
        if ( Tools::getValue('module_name') == $this->name || Tools::getValue('configure') == $this->name)
            $this->context->controller->addJS($this->_path.'views/js/back.js');
    }

    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayNav()
    {
		$id_lang = $this->context->language->id;
        $langs = Language::getLanguages();
        $languages = array();
        foreach($langs as $lang){
            $languages[] = $lang['id_lang'];
        }
        if(!in_array($id_lang, $languages)){
            //если язык пользователя вообще не предпологается этим магазином, то отображаем для стандартного языка магазина
            $id_lang = Configuration::get('PS_LANG_DEFAULT');
        }
        $block_name = Configuration::get('PW_BLOCK_FAV_PAGE_BLOCKNAME', $id_lang);
        $this->context->smarty->assign(array('name' => $block_name, 'links' => Tools::jsonDecode($this->data[$id_lang], true)));
        return $this->display(__FILE__, $this->name.'-top.tpl');
    }
	
	public function hookDisplayFooter($params)
	{
		return $this->hookDisplayNav();
	}
}
