{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $errors}
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		{foreach from=$errors item=error name=errors}
			<span>{$error}</span>
		{/foreach}
	</div>
{/if}

{if $success}
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<span>{l s='Сохранено' mod='pwblockfavpage'}</span>
	</div>
{/if}
<div class="panel">
	<form class="form-horizontal" method="post">
		<h3><i class="icon icon-credit-card"></i> {l s='Простое меню' mod='pwblockfavpage'}</h3>
		<div class="form-group">
			<label class="col-sm-2 control-label">Магазин</label>
			<div class="col-sm-4">
				<select id="shop_id">
				{foreach from=$shops item=shop}
					<option {if $shop.id_shop == $id_shop}selected="selected"{/if} value="{$shop.id_shop}">{$shop.name} ({$shop.domain})</option>
				{/foreach}
				</select>
			</div>
			<label class="col-sm-1 control-label">Язык</label>
			<div class="col-sm-4">
			{foreach from=$shops item=shop}
				<select class="shop_list lang_shop_{$shop.id_shop} {if $shop.id_shop != $id_shop}hidden{/if}">
				{foreach from=$shop.langs item=lang}
					<option value="{$lang.id_lang}">{$lang.name} ({$lang.language_code})</option>
				{/foreach}
				</select>
			{/foreach}
			</div>
		</div>
		<hr />
		{foreach from=$shops item=shop key=shop_id}
			{foreach from=$shop.langs item=lang name=langLoop key=id_lang}
			<div class="panel-body panel_shop_{$shop_id}_lang_{$id_lang} {if ($shop_id != $id_shop) || $smarty.foreach.langLoop.index>0}hidden{/if}">
				<div class="form-group">
					<label for="blockName" class="col-sm-2 control-label">{l s='Название блока' mod='pwblockfavpage'}</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="shop[{$shop_id}][lang][{$id_lang}][blockName]" placeholder="{l s='Название блока' mod='pwblockfavpage'}"
							   {if isset($data.$shop_id.$id_lang.blockName) AND $data.$shop_id.$id_lang.blockName}value="{$data.$shop_id.$id_lang.blockName}"{/if}>
					</div>
				</div>
				<hr>
				<table class="table pwblockfavpage-table">
					<caption><h1>{l s='Пункты меню' mod='pwblockfavpage'}</h1></caption>
					<thead>
						<tr>
							<th>{l s='#' mod='pwblockfavpage'}</th>
							<th>{l s='Название' mod='pwblockfavpage'}</th>
							<th>{l s='Ссылка' mod='pwblockfavpage'}</th>
							<th>{l s='Удалить' mod='pwblockfavpage'}</th>
						</tr>
					</thead>
					<tbody>
                        {assign var=links value=$data.$id_lang.$shop_id|@json_decode:true}
						{foreach from=$links item=link name=links}
                            
							<tr>
								<td>{$smarty.foreach.links.iteration}</td>
								<td><input type="text" class="form-control" name="shop[{$shop_id}][lang][{$id_lang}][link-name][]" placeholder="{l s='Название ссылки' mod='pwblockfavpage'}" value="{$link.name}"></td>
								<td><input type="text" class="form-control" name="shop[{$shop_id}][lang][{$id_lang}][link-url][]" placeholder="{l s='URL ссылки' mod='pwblockfavpage'}" value="{$link.url}"></td>
								<td><img class="link-del" src="../img/admin/disabled.gif" /></td>
							</tr>
						{foreachelse}
							<tr>
								<td>1</td>
								<td><input type="text" class="form-control" name="shop[{$shop_id}][lang][{$id_lang}][link-name][]" placeholder="{l s='Название ссылки' mod='pwblockfavpage'}"></td>
								<td><input type="text" class="form-control" name="shop[{$shop_id}][lang][{$id_lang}][link-url][]" placeholder="{l s='URL ссылки' mod='pwblockfavpage'}"></td>
								<td><img class="link-del" src="../img/admin/disabled.gif" /></td>
							</tr>
						{/foreach}
					</tbody>
				</table>
				<button type="submit" value="1" class="linkAdd btn btn-default pull-right">{l s='Добавить еще' mod='pwblockfavpage'}</button>
				<div class="hidden" id="row-template_{$shop_id}_{$id_lang}">
					<table>
						<tbody>
							<tr>
								<td></td>
								<td><input type="text" class="form-control" name="shop[{$shop_id}][lang][{$id_lang}][link-name][]" placeholder="{l s='Название ссылки' mod='pwblockfavpage'}"></td>
								<td><input type="text" class="form-control" name="shop[{$shop_id}][lang][{$id_lang}][link-url][]" placeholder="{l s='URL ссылки' mod='pwblockfavpage'}"></td>
								<td><img class="link-del" src="../img/admin/disabled.gif" /></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			{/foreach}
		{/foreach}
		<div class="panel-footer">
			<button type="submit" value="1" id="submitPWBlockFavPageModule" name="submitPWBlockFavPageModule" class="btn btn-default pull-right">
				<i class="process-icon-save"></i> {l s='Сохранить' mod='pwblockfavpage'}
			</button>
			<a href="index.php?controller=AdminModules" class="btn btn-default pull-right">
				<i class="process-icon-cancel"></i> {l s='Отмена' mod='pwblockfavpage'}
			</a>
		</div>
	</form>
</div>
