<?php

class FavoriteProductGuest extends FavoriteProduct
{
    public $wishlist;
    public $context;
    
    public $id_product;
    public $id_shop;
    public $id_customer;
    
    public function __construct($id_product = null)
    {
        $this->context = Context::getContext();
        if(!empty($this->context->cookie->wishlist))
            $this->wishlist = Tools::jsonDecode($this->context->cookie->wishlist, true);
        else
            $this->wishlist = array();
        if(!empty($id_product)){
            $this->id_product = $id_product;
        }
    }
    
    public function isCustomerFavoriteProduct($id_product, Shop $shop = null)
	{
        return isset($this->wishlist[$id_product]);
	}
    
    public function getFavoriteProduct($id_product, Shop $shop = null)
	{
        if(isset($this->wishlist[$id_product]))
            return new self($id_product);
        return null;
	}
    
    public function delete()
    {
        unset($this->wishlist[$this->id_product]);
        $this->context->cookie->wishlist = Tools::jsonEncode($this->wishlist);
        return $this->context->cookie->write();
    }
    
    public function add($auto_date = true, $null_values = false)
    {
        $this->wishlist[$this->id_product] = 1;
        $this->context->cookie->wishlist = Tools::jsonEncode($this->wishlist);
        return $this->context->cookie->write();
    }
    
    public function getFavoriteProducts()
	{
        $favoriteProducts = array();
        if(is_array($this->wishlist)){
            foreach($this->wishlist as $id_product => $v){
                /*$product = new Product($id_product, true, $this->context->language->id);
                $img = Product::getCover($id_product);
                $product->image = $img['id_image'];
                $product->id_product = $product->id;
                $favoriteProducts[] = get_object_vars($product);*/
                $favoriteProducts[] = $id_product;
            }
        }
        return $favoriteProducts;
    }
    
    //сейчас вызывается при хуке, а вообще можно и в конструкторе вызывать. Особой разницы не будет
    // copied from FavoriteProductAccount )
    public function importFromCookie()
    {
        $context = Context::getContext();
		if(!empty($context->cookie->wishlist)){
			$wishlist = Tools::jsonDecode($context->cookie->wishlist, true);
			if(is_array($wishlist)){
				foreach($wishlist as $id_product => $v){
					$product = new Product($id_product, true, $context->language->id);
					if (!Validate::isLoadedObject($product) || $this->isCustomerFavoriteProduct((int)$product->id))
						continue;
					$favorite_product = new self();
					$favorite_product->id_product = $product->id;
					$favorite_product->id_customer = (int)$this->id_customer;
					$favorite_product->id_shop = (int)$context->shop->id;
					$favorite_product->add();
				}
				unset($context->cookie->wishlist);
				$context->cookie->write();
			}
		}
    }
}
