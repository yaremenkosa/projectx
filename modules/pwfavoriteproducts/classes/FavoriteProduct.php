<?php

require_once('FavoriteProductAccount.php');
require_once('FavoriteProductGuest.php');

abstract class FavoriteProduct extends ObjectModel
{
	public $id_product;

	public $id_customer;
    
    public static function getInstance()
    {
        $context = Context::getContext();
        if(is_object($context->customer) && $context->customer->isLogged()){
            $fv = new FavoriteProductAccount();
            $fv->id_customer = $context->customer->id;
            return $fv;
        }else{
            return new FavoriteProductGuest();
        }
    }
	
}
