<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{pwfavoriteproducts}prestashop>pwfavoriteproducts_c249aeb21294d5e97598462b550e73eb'] = 'Избранные товары';
$_MODULE['<{pwfavoriteproducts}prestashop>pwfavoriteproducts_018770f4456d82ec755cd9d0180e4cce'] = ' ';
$_MODULE['<{pwfavoriteproducts}prestashop>favoriteproducts_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Личный кабинет';
$_MODULE['<{pwfavoriteproducts}prestashop>favoriteproducts_e5090b68524b1bbfd7983bfa9500b7c9'] = 'Избранные товары';
$_MODULE['<{pwfavoriteproducts}prestashop>favoriteproducts_44fd621c6504b8a5346946650682a842'] = 'У вас сейчас нет избранных товаров';
$_MODULE['<{pwfavoriteproducts}prestashop>favoriteproducts_6f16227b3e634872e87b0501948310b6'] = 'Вернуться в личный кабинет';
$_MODULE['<{pwfavoriteproducts}prestashop>favoriteproducts-extra_15b94c64c4d5a4f7172e5347a36b94fd'] = 'Добавить в избранное';
$_MODULE['<{pwfavoriteproducts}prestashop>my-account_e5090b68524b1bbfd7983bfa9500b7c9'] = 'Избранное';
$_MODULE['<{pwfavoriteproducts}prestashop>pwfavorite_button_15b94c64c4d5a4f7172e5347a36b94fd'] = 'Добавить в избранное';
