<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 */
class PWFavoriteproductsActionsModuleFrontController extends ModuleFrontController
{
	/**
	 * @var int
	 */
	public $id_product;

	public function init()
	{
		parent::init();
		$this->id_product = (int)Tools::getValue('id_product');
	}

	public function postProcess()
	{
		if (Tools::getValue('process') == 'remove')
			$this->processRemove();
		else if (Tools::getValue('process') == 'add')
			$this->processAdd();
		exit;
	}

	/**
	 * Remove a favorite product
	 */
	public function processRemove()
	{
		$count = (int)$this->module->getProductCount();
		
		// check if product exists
		$product = new Product($this->id_product);
		if (!Validate::isLoadedObject($product)) {
			$result = 'error';
		} else {
			$favorite_product = $this->module->fav->getFavoriteProduct((int)$product->id);
			if ($favorite_product && $favorite_product->delete()) {
				$result = 'removed';
				$count--;
			} else {
				$result = 'error';
			}
		}
			
		die(json_encode(
			array(
				'result' => $result,
				'count' => $count,
			)
		));
	}

	/**
	 * Add a favorite product
	 */
	public function processAdd()
	{
		$count = (int)$this->module->getProductCount();
		
		$product = new Product($this->id_product);
		// check if product exists
		if (!Validate::isLoadedObject($product)) {
			$result = 'error';
		} else {
			if($this->module->fav->isCustomerFavoriteProduct((int)$product->id)) {
				$result = 'alreadyFaved';
			} else {
				$favorite_product = FavoriteProduct::getInstance();
				$favorite_product->id_product = $product->id;
				$favorite_product->id_shop = (int)Context::getContext()->shop->id;
				if ($favorite_product->add()) {
					$result = 'added';
					$count++;
				} else {
					$result = 'error';
				}
			}
		}
		
		die(json_encode(
			array(
				'result' => $result,
				'count' => $count,
			)
		));
		
	}
}