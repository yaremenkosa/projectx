<?php

class PWFavoriteproductsAccountModuleFrontController extends ModuleFrontController
{
	public $ssl = true;

	public function init()
	{
		parent::init();
	}

	public function initContent()
	{
		parent::initContent();
        $this->addJS(_MODULE_DIR_.'pwfavoriteproducts/account.js');

        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
        	$this->context->smarty->assign('favoriteProducts', $this->renderProducts($this->module->fav->getFavoriteProducts()) );
            $this->setTemplate('module:pwfavoriteproducts/views/templates/front/favoriteproducts-v17.tpl');
        }
        else {
        	$ids = $this->module->fav->getFavoriteProducts();
        	$products = array();
        	foreach ($ids as $id_product){
	            $prod = get_object_vars(new Product($id_product, true, (int)Context::getContext()->language->id));
	            $prod['id_product'] = $prod['id'];
	            $cover = Product::getCover($id_product);
	            $prod['id_image'] = $cover['id_image'];
	            $products[] = $prod;
	        }

	        $products = Product::getProductsProperties((int)Context::getContext()->language->id, $products);

	        $this->context->smarty->assign('favoriteProducts', $products);
	        $this->context->controller->addCSS(_THEME_CSS_DIR_.'product_list.css');
            $this->setTemplate('favoriteproducts-v16.tpl');
        }
	}
	
	//1.7
    private function renderProducts($products)
    {
        $assembler = new ProductAssembler($this->context);
        $presenterFactory = new ProductPresenterFactory($this->context);
        $presentationSettings = $presenterFactory->getPresentationSettings();
        $presenter = new PrestaShop\PrestaShop\Core\Product\ProductListingPresenter(
            new PrestaShop\PrestaShop\Adapter\Image\ImageRetriever(
                $this->context->link
            ),
            $this->context->link,
            new PrestaShop\PrestaShop\Adapter\Product\PriceFormatter(),
            new PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever(),
            $this->context->getTranslator()
        );
        $productsForTemplate = array();

        $presentationSettings->showPrices = true;

        if (is_array($products)) {
            foreach ($products as $productId) {
                $productsForTemplate[] = $presenter->present(
                    $presentationSettings,
                    $assembler->assembleProduct(array('id_product' => $productId)),
                    $this->context->language
                );
            }
        }
        return $productsForTemplate;
    }
}