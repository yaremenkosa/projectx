<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
*
*  Module based on favoriteproducts v1.3.0
*
*/

require_once('classes/FavoriteProduct.php');

class PWFavoriteProducts extends Module
{

    public $fav;

    public function __construct()
    {
        $this->name = 'pwfavoriteproducts';
        $this->tab = 'front_office_features';
        $this->version = '1.6.1';
        $this->author = 'PrestaWeb.ru';
        $this->need_instance = 0;

        $this->controllers = array('account', 'actions');

        parent::__construct();
        $this->bootstrap = true;
        $this->displayName = $this->l('Favorite Products');
        $this->description = $this->l('Display a page featuring the customer\'s favorite products.');
        $this->ps_versions_compliancy = array('min' => '1.5.6.1', 'max' => _PS_VERSION_);
        $this->fav = FavoriteProduct::getInstance();
    }

    public function install()
    {
        if (!parent::install()
            || !$this->registerHook('displayMyAccountBlock')
            || !$this->registerHook('displayCustomerAccount')
            || !$this->registerHook('displayLeftColumnProduct')
            || !$this->registerHook('extraLeft')
            || !$this->registerHook('displayHeader')
            || !$this->registerHook('displayProductButtons')
            || !$this->registerHook('actionAuthentication')
            || !$this->registerHook('productActions')
            || !$this->registerHook('displayProductListFunctionalButtons')
            || !$this->registerHook('displayNav')
            || !$this->registerHook('displayNav2')
            || !$this->registerHook('displayTop')
            || !$this->registerHook('actionCustomerAccountAdd'))
            return false;

        if (!Db::getInstance()->execute('
				CREATE TABLE `'._DB_PREFIX_.'favorite_product` (
				`id_favorite_product` int(10) unsigned NOT NULL auto_increment,
				`id_product` int(10) unsigned NOT NULL,
				`id_customer` int(10) unsigned NOT NULL,
				`id_shop` int(10) unsigned NOT NULL,
				`date_add` datetime NOT NULL,
  				`date_upd` datetime NOT NULL,
				PRIMARY KEY (`id_favorite_product`))
				ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'))
            return false;

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() || !Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'favorite_product`'))
            return false;
        return true;
    }

    public function hookDisplayCustomerAccount($params)
    {
        $this->smarty->assign('in_footer', false);
        return $this->display(__FILE__, 'my-account.tpl');
    }

    public function hookDisplayMyAccountBlock($params)
    {
        $this->smarty->assign('in_footer', true);
        return $this->display(__FILE__, 'my-account.tpl');
    }

    public function hookDisplayLeftColumnProduct($params)
    {
        $this->smarty->assign(array(
            'isCustomerFavoriteProduct' => $this->fav->isCustomerFavoriteProduct(Tools::getValue('id_product')),
            'isLogged' => 1,
        ));
        return $this->display(__FILE__, 'favoriteproducts-extra.tpl');
    }

    public function hookDisplayHeader($params)
    {
        Media::addJsDef(array(
            'favorite_products_add' => $this->l('Добавить в избранное'),
            'favorite_products_remove' => $this->l('Удалить из избранного'),
            'favorite_products_url_add' => $this->context->link->getModuleLink('pwfavoriteproducts', 'actions', array('process' => 'add')),
            'favorite_products_url_remove' => $this->context->link->getModuleLink('pwfavoriteproducts', 'actions', array('process' => 'remove')),
            'favorite_products_id_product' => Tools::getValue('id_product'),
        ));
        $this->context->smarty->assign(array(
            'favorite_products_add' => $this->l('Добавить в избранное'),
            'favorite_products_remove' => $this->l('Удалить из избранного'),
        ));
        $this->context->controller->addCSS($this->_path.'favoriteproducts.css', 'all');
        $this->context->controller->addJS($this->_path.'favoriteproducts.js');
        $this->context->controller->addJqueryPlugin('growl');
    }

    public function hookdisplayProductButtons($params)
    {
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $this->smarty->assign(array(
                'isFavorite' => $this->fav->isCustomerFavoriteProduct($params['product']['id_product']),
                'product' => $params['product']
            ));
            return $this->display(__FILE__, 'pwfavorite_button-v17.tpl');
        }

    }

    public function hookActionAuthentication($params)
    {
        $this->fav->importFromCookie();
    }

    public function hookactionCustomerAccountAdd($params)
    {
        $this->fav->importFromCookie();
    }

    public function hookDisplayProductListFunctionalButtons($params)
    {
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            if (isset($_GET['id_product'])) {
                $this->smarty->assign(array(
                    'isFavorite' => $this->fav->isCustomerFavoriteProduct($_GET['id_product']),
                    'product_id' => $_GET['id_product']
                ));
            } else {
                $this->smarty->assign(array(
                    'isFavorite' => $this->fav->isCustomerFavoriteProduct($params['product']['id_product']),
                    'product_id' => $params['product']['id_product']
                ));
            }
            return $this->display(__FILE__, 'pwfavorite_button-v17.tpl');
        } else {
            $this->smarty->assign(array(
                'isFavorite' => $this->fav->isCustomerFavoriteProduct($params['product']['id_product']),
                'product' => $params['product']
            ));
            return $this->display(__FILE__, 'pwfavorite_button.tpl');
		}
    }

    public function hookDisplayProductAdditionalInfo($params)
    {
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $this->smarty->assign(array(
                'isFavorite' => $this->fav->isCustomerFavoriteProduct($params['product']['id_product']),
                'product' => $params['product']
            ));
            return $this->display(__FILE__, 'pwfavorite_button-v17.tpl');
        }
    }

    public function hookDisplayNav($params)
    {
    	$this->smarty->assign(array(
            'wishlist_products' => $this->getProductCount(),
        ));
        return $this->display(__FILE__, 'nav.tpl');
    }

    public function hookDisplayNav2($params)
    {
    	$this->smarty->assign(array(
            'wishlist_products' => $this->getProductCount(),
        ));
        if (version_compare(_PS_VERSION_, '1.7', '>=')){
            return $this->display(__FILE__, 'nav-v17.tpl');
        }
    }

    public function hookDisplayTop($params)
    {
    	$this->smarty->assign(array(
            'wishlist_products' => $this->getProductCount(),
        ));
        if (version_compare(_PS_VERSION_, '1.7', '>=')){
            return $this->display(__FILE__, 'nav-v17.tpl');
        }
    }

    public function getProductCount()
    {
        $products = $this->fav->getFavoriteProducts((int)Context::getContext()->language->id);
        return count($products);
    }


}
