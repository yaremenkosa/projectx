<div class="wishlist">
	<a class="addToWishlist {if $isFavorite}faved{/if}" href="#" rel="ajax_id_favoriteproduct_{$product.id_product|intval}" title="{l s='Add to my wishlist' mod='pwfavoriteproducts'}">
		<i class="fa fa-heart{if !$isFavorite}-o{/if}"></i>
		<span>{if $isFavorite}{$favorite_products_remove}{else}{$favorite_products_add}{/if}</span>
	</a>
</div>