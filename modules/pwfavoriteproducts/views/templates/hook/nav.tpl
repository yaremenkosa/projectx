<div id="favorite-link">
        <a href="{$link->getModuleLink('pwfavoriteproducts', 'account')}" title="{l s='Избранные товары' mod='pwfavoriteproducts'}">
        	{l s='Избранные товары' mod='pwfavoriteproducts'}
        	{if $wishlist_products >0}
        	<span id="pwfavoriteproducts-count">({$wishlist_products})</span>
        	{else}
        	<span id="pwfavoriteproducts-count"></span>
        	{/if}
        </a>
</div>