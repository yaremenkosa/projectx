<div id="_desktop_contact_link">
    <div id="contact-link">
        <a href="{$link->getModuleLink('pwfavoriteproducts', 'account')}" title="{l s='Избранные товары' mod='pwfavoriteproducts'}">
			{* {l s='Избранные товары' mod='pwfavoriteproducts'} *}
			<svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M21.4875 2.01406C18.7781 -0.264065 14.5922 0.0781223 12 2.71718C9.40782 0.0781223 5.22188 -0.268753 2.51251 2.01406C-1.01249 4.98125 -0.49687 9.81875 2.01563 12.3828L10.2375 20.7594C10.7063 21.2375 11.3344 21.5047 12 21.5047C12.6703 21.5047 13.2938 21.2422 13.7625 20.7641L21.9844 12.3875C24.4922 9.82343 25.0172 4.98593 21.4875 2.01406V2.01406ZM20.3813 10.8031L12.1594 19.1797C12.0469 19.2922 11.9531 19.2922 11.8406 19.1797L3.61876 10.8031C1.90782 9.05937 1.56094 5.75937 3.96094 3.73906C5.78438 2.20625 8.59688 2.43593 10.3594 4.23125L12 5.90468L13.6406 4.23125C15.4125 2.42656 18.225 2.20625 20.0391 3.73437C22.4344 5.75468 22.0781 9.07343 20.3813 10.8031V10.8031Z" fill="#222222" />
			</svg>
        	{if $wishlist_products >0}
        	<span id="pwfavoriteproducts-count">{$wishlist_products}</span>
        	{else}
        	<span id="pwfavoriteproducts-count">0</span>
        	{/if}
        </a>
    </div>
</div>