{extends file=$layout}

{block name='content'}
	<section id="main">
        {block name='product_list_header'}
			<h2 class="h2">{l s='My favorite products.' mod='pwfavoriteproducts'}</h2>
        {/block}

		<section id="products">
            {if $favoriteProducts}
				<div class="products row favoriteproducts_block_account">
                    {foreach from=$favoriteProducts item="product"}
        				{include file="catalog/_partials/miniatures/product.tpl" product=$product}
    				{/foreach}
				</div>
            {else}
				<p class="warning">{l s='No favorite products have been determined just yet. ' mod='pwfavoriteproducts'}</p>
            {/if}
		</section>

	</section>
{/block}
