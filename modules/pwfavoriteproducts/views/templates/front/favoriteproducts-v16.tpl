{if $is_logged}
{capture name=path}
	<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
		{l s='My account' mod='pwfavoriteproducts'}</a>
		<span class="navigation-pipe">{$navigationPipe}</span>{l s='My favorite products.' mod='pwfavoriteproducts'}
{/capture}
{else}
{capture name=path}{l s='My favorite products.' mod='pwfavoriteproducts'}{/capture}
{/if}
<div id="favoriteproducts_block_account">
	<h2>{l s='My favorite products.' mod='pwfavoriteproducts'}</h2>
	{if $favoriteProducts}
		<div class="favoriteproducts_block_account">
			{include file="$tpl_dir./product-list.tpl" products=$favoriteProducts}
		</div>
	{else}
		<p class="warning">{l s='No favorite products have been determined just yet. ' mod='pwfavoriteproducts'}</p>
	{/if}

	<ul class="footer_links">
		<li class="fleft">
            {if $is_logged}
			<a class="btn btn-default icon-left" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}"><span>{l s='Back to your account.' mod='pwfavoriteproducts'}</span></a>
            {else}
            <a class="btn btn-default icon-left" href="{$link->getPageLink('index', true)|escape:'html':'UTF-8'}"><span>{l s='Вернуться на сайт' mod='pwfavoriteproducts'}</span></a>
            {/if}
        </li>
	</ul>
</div>