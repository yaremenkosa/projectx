$(document).ready(function () {

	$('body')
		.on('click', '#pwfavoriteAdd', function (e) {
			e.preventDefault();
			$.ajax({
				url: favorite_products_url_add + '&rand=' + new Date().getTime(),
				type: "POST",
				headers: { "cache-control": "no-cache" },
				async: true,
				dataType: "json",
				data: {
					"id_product": favorite_products_id_product
				},
				success: function (result) {
					if (result.result == 'added') {
						$('#pwfavoriteAdd').prop('id', 'pwfavoriteRemove').text(favorite_products_remove);
						$.growl({ message: "Товар добавлен в избранное", style: 'notice', title: '' });
					} else if (result.result == 'alreadyFaved') {
						$('#pwfavoriteAdd').prop('id', 'pwfavoriteRemove').text(favorite_products_remove);
						$.growl({ message: "Товар уже находится в избранном", style: 'warning', title: '' });
					}
					if (result.count > 0) {
						$('#pwfavoriteproducts-count').html(result.count);
					} else {
						$('#pwfavoriteproducts-count').html('');
					}
				}
			});
		})
		.on('click', '#pwfavoriteRemove', function (e) {
			e.preventDefault();
			$.ajax({
				url: favorite_products_url_remove + '&rand=' + new Date().getTime(),
				type: "POST",
				headers: { "cache-control": "no-cache" },
				async: true,
				dataType: "json",
				data: {
					"id_product": favorite_products_id_product
				},
				success: function (result) {
					if (result.result == 'removed') {
						$('#pwfavoriteRemove').prop('id', 'pwfavoriteAdd').text(favorite_products_add);
						$.growl({ message: "Товар убран из избранного", style: 'notice', title: '' });
					}
					if (result.count > 0) {
						$('#pwfavoriteproducts-count').html(result.count);
					} else {
						$('#pwfavoriteproducts-count').html('');
					}
				}
			});
		});

	$('[rel^=ajax_id_favoriteproduct_]').click(function (e) {
		e.preventDefault();
		var idFavoriteProduct = $(this).attr('rel').replace('ajax_id_favoriteproduct_', '');
		var i = $(this).find('i');
		var action = favorite_products_url_add;
		var ctext = favorite_products_remove;
		var message = "Товар добавлен в избранное";
		var obj = $(this);
		if (favorite_products_id_product && idFavoriteProduct != favorite_products_id_product) idFavoriteProduct = favorite_products_id_product;
		if ($(this).hasClass('faved')) {
			action = favorite_products_url_remove;
			ctext = favorite_products_add;
			message = "Товар убран из избранного";
		}

		$.ajax({
			url: action,
			type: "POST",
			dataType: "json",
			data: {
				'id_product': idFavoriteProduct,
				'ajax': true
			},
			async: true,
		}).done(function (result) {
			if (result.result == 'added') {
				$.growl({ message: "Товар добавлен в избранное", style: 'notice', title: '' });
				i.addClass('fa-heart').removeClass('fa-heart-o');
				obj.addClass('faved');
				obj.find('span').text(ctext);
			}
			if (result.result == 'removed') {
				$.growl({ message: "Товар убран из избранного", style: 'notice', title: '' });
				i.removeClass('fa-heart').addClass('fa-heart-o');
				obj.toggleClass('faved');
				obj.find('span').text(ctext);
				obj.closest('.favoriteproduct').remove();
			}
			if (result.result == 'error') {
				$.growl({ message: "Произошла ошибка", style: 'error', title: '' });
			}

			if (result.count > 0) {
				$('#pwfavoriteproducts-count').html(result.count);
			} else {
				$('#pwfavoriteproducts-count').html('');
			}
		});
	});
});