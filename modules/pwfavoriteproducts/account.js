$().ready(function(){
    $('.product_quantity_up').click(function(e){
        e.preventDefault();
        var inp = $(this).closest('.quantity_wanted_p').find('input[name=qty]');
        inp.val(parseInt(inp.val()) + 1);
    });
    $('.product_quantity_down').click(function(e){
        e.preventDefault();
        var inp = $(this).closest('.quantity_wanted_p').find('input[name=qty]');
        inp.val(parseInt(inp.val()) - 1);
        if(inp.val() < 1)
           inp.val(1);
    });
    if(typeof ajaxCart != 'undefined'){
        $('.pw_add_to_cart_button').click(function(e){
            e.preventDefault();
            var idProduct = $(this).data('id-product');
            var q = $('#quantity_wanted'+idProduct).val();
            ajaxCart.add(idProduct, null, false, this, q);
        });
    }
});