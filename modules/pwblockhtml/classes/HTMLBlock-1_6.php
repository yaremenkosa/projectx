<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2015 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class PWHTMLBlock extends ObjectModel
{
    public $id_pwblockhtml;
    public $name;
    public $hooks;
    public $html;
    public $html_editor;
    public $need_css;
    public $css;
    public $css_editor;
    public $need_js;
    public $js;
    public $js_editor;
    public $order;
    public $active;
    public $smarty;
    public $date_add;
    public $date_upd;

    public $hook_array;

    public static $definition = array(
        'table'     => 'pwblockhtml',
        'primary'   => 'id_pwblockhtml',
        'multilang' => false,
        'multishop' => true,
        'fields'    => array
        (
            'name'        => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'required' => true),
            'hooks'       => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'required' => true),
            'html'        => array('type' => self::TYPE_NOTHING),
            'html_editor' => array('type' => self::TYPE_STRING,  'validate' => 'isString'),
            'need_css'    => array('type' => self::TYPE_BOOL,    'validate' => 'isBool'),
            'css'         => array('type' => self::TYPE_NOTHING),
            'css_editor'  => array('type' => self::TYPE_STRING,  'validate' => 'isString'),
            'need_js'     => array('type' => self::TYPE_BOOL,    'validate' => 'isBool'),
            'js'          => array('type' => self::TYPE_NOTHING),
            'js_editor'   => array('type' => self::TYPE_STRING,  'validate' => 'isString'),
            'order'       => array('type' => self::TYPE_INT,     'validate' => 'isInt'),
            'active'      => array('type' => self::TYPE_BOOL,    'validate' => 'isBool'),
            'smarty'      => array('type' => self::TYPE_BOOL,    'validate' => 'isBool'),
            'date_add'    => array('type' => self::TYPE_DATE,    'validate' => 'isDate'),
            'date_upd'    => array('type' => self::TYPE_DATE,    'validate' => 'isDate'),
        ),
    );


    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        Shop::addTableAssociation(self::$definition['table'], array('type' => 'shop'));
        parent::__construct($id, $id_lang, $id_shop);
        if (!empty($this->hooks)) {
            $this->hook_array = unserialize($this->hooks);
        }
        if (!is_array($this->hook_array)) {
            $this->hook_array = array();
        }
    }

    public function add($auto_date = true, $null_values = false)
    {
        $this->hooks = serialize($this->hook_array);
        return parent::add($auto_date, $null_values);
    }

    public function update($null_values = false)
    {
        $this->hooks = serialize($this->hook_array);
        return parent::update($null_values);
    }

    public function validateController($htmlentities = true)
    {
        if (!empty($_POST['hook_array'])) {
            $_POST['hooks'] = serialize($_POST['hook_array']);
        }
        return parent::validateController($htmlentities);
    }

    public static function getAllHTMLBlocks($id_shop = null, $active = false)
    {
        if(empty($id_shop)){
            $id_shop = Context::getContext()->shop->id;
        }
        $sql = 'SELECT * FROM '._DB_PREFIX_.self::$definition['table'].' t
                LEFT JOIN '._DB_PREFIX_.self::$definition['table'].'_shop ts on(t.'.self::$definition['primary'].' = ts.'.self::$definition['primary'].')
                WHERE id_shop = '.$id_shop.($active?" AND t.active = 1 ORDER BY 'order'":'');

       if (!($blocks = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql))) {
            return array();
        }
        foreach ($blocks as &$block) {
            $block['hook_array'] = unserialize($block['hooks']);
        }

        return $blocks;
    }

    public static function getActiveHTMLBlocksByHook($hook_name, $active = true)
    {
        $html_blocks = self::getAllHTMLBlocks(Context::getContext()->shop->id, $active);
        $active_blocks = array();
        $hook_name = strtolower($hook_name);
        $retro_name = strtolower(Hook::getRetroHookName($hook_name));
        foreach ($html_blocks as $html_block){
            if (in_array($hook_name, $html_block['hook_array'])) {
                $active_blocks[] = $html_block;
            } elseif (in_array($retro_name, $html_block['hook_array'])) {
                $active_blocks[] = $html_block;
            }
        }

        return $active_blocks;
    }

    public static function getBlockHTML($id_pwblockhtml)
    {
        if ( !$id_pwblockhtml ) return array();

        $block_html = new static($id_pwblockhtml);
        return $block_html->getFields();
    }
}
