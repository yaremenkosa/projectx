<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2015 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class AdminPWBlockHTMLController extends ModuleAdminController
{
    public $module;
	public $HTMLBlock;

    public function __construct()
    {
        $this->table         = 'pwblockhtml';
        $this->className     = 'PWHTMLBlock';
        $this->module        = 'pwblockhtml';
        $this->lang          = false;
        $this->bootstrap     = true;
        $this->need_instance = 0;

        $this->context       = Context::getContext();
        $this->errors        = array();
        $this->success       = 0;

        Shop::addTableAssociation($this->table, array('type' => 'shop'));
        parent::__construct();
        
        $this->token = Tools::getAdminTokenLite('Adminpwblockhtml');

        $this->fields_list = array(
            'id_pwblockhtml' => array(
                'title'    => $this->l('ID'),
                'type'     => 'text',
                'orderby'  => true
            ),
            'order' => array(
                'title'    => $this->l('Порядок'),
                'type'     => 'text',
            ),
            'name' => array(
                'title'    => $this->l('Название'),
                'type'     => 'text',
            ),
            'hooks' => array(
                'title'    => $this->l('Хуки'),
                'type'     => 'text',
                'callback' => 'getHookNames',
            ),
            'active' => array(
                'title'    => $this->l('Статус'),
                'type'     => 'bool',
                'align'    => 'center',
                'active'   => 'status',

            ),
        );

        $this->addRowAction('delete');
    }

    public static function getHookNames($hooks)
    {
        if ( !($hook_names = unserialize($hooks)) ) return '';

        return implode(', ', $hook_names);
    }

    public function renderForm()
    {
		$this->loadHTMLBlock();
        $shopAsso = '';
        if (!Tools::version_compare(_PS_VERSION_, '1.6') && Shop::isFeatureActive()){
            $helpShop = new HelperTreeShops($this->id, 'Магазины');
            $helpShop->setSelectedShops($this->tshopList());
            $shopAsso = $helpShop->render();
        }
        $hooks = array();
        foreach (Hook::getHooks() as $h) {
            $hooks[] = $h['name'];
        }
        $this->context->smarty->assign(array(
            'block_html'    => $this->HTMLBlock,
            'hooks'         => $hooks,
            'current_hooks' => Tools::jsonEncode($this->HTMLBlock->hook_array),
            'shopAsso'      => $shopAsso,
        ));
        if (Tools::version_compare(_PS_VERSION_, '1.6')) {
            return $this->context->smarty->fetch(_PS_MODULE_DIR_.'pwblockhtml/views/templates/admin/add_html_form15.tpl');
        }
        return $this->context->smarty->fetch(_PS_MODULE_DIR_.'pwblockhtml/views/templates/admin/add_html_form.tpl');
    }
    
    public function setMedia($isNewTheme = false)
    {
        parent::setMedia();
        $this->addJquery();
        $iso = $this->context->language->iso_code;
        $iso = file_exists(_PS_CORE_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en';
        $ad = __PS_BASE_URI__.basename(_PS_ADMIN_DIR_);
        $this->context->smarty->assign(array(
            'iso' => $iso,
            'ad' => $ad,
        ));
        Media::addJsDef(array(
            'iso' => $iso,
            'ad' => $ad,
        ));
        $this->addJS(_PS_JS_DIR_.'admin/tinymce.inc.js');
        $this->addJS(_MODULE_DIR_.$this->module->name.'/views/js/select2.min.js');
        $this->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/select2.min.css');
        $this->addJS(_PS_CORE_DIR_.'/js/tiny_mce/tiny_mce.js');
        $this->addJS(_MODULE_DIR_.$this->module->name.'/vendor/ace/src/ace.js');
        $this->addJS(_MODULE_DIR_.$this->module->name.'/vendor/emmet/emmet.js');
        $this->addJS(_MODULE_DIR_.$this->module->name.'/vendor/ace/src/ext-emmet.js');
        $this->addJS(_MODULE_DIR_.$this->module->name.'/views/js/back.js');
        $this->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/back.css');
    }
    
    protected function tshopList()
    {
        $res = array();
        if (Tools::isSubmit($this->identifier)) {
            $r = Db::getInstance()->executeS('SELECT `id_shop` FROM `'._DB_PREFIX_.$this->table.'_shop` WHERE '.$this->identifier.' = '.Tools::getValue($this->identifier));
            foreach($r as $b){
                $res[] = $b['id_shop'];
            }
        }
        if(!count($res))
            $res[] = $this->context->shop->id;
        return $res;
    }

    public function postProcess()
    {
        if (Tools::isSubmit('statuspwblockhtml')) {
            $this->changeStatusBlockHTML(Tools::getValue('id_pwblockhtml'));
        }

        if (Tools::isSubmit('deletepwblockhtml')) {
            $this->deleteBlockHTML(Tools::getValue('id_pwblockhtml'));
        }

        if (Tools::getValue('submitAddPWBlockHTML')) {
            $this->saveBlockHTML();
        }
    }

    private function saveBlockHTML()
    {
        $this->loadHTMLBlock();
		
        $this->HTMLBlock->hook_array = Tools::getValue('hook_array');
        $this->errors = $this->HTMLBlock->validateController();
        if (!count($this->errors)) {
            if($this->HTMLBlock->save()) {
                $this->updateAssoShop($this->HTMLBlock->id);
                $this->processHooks($this->HTMLBlock);
            }
        }
    }

    private function changeStatusBlockHTML($id_pwblockhtml)
    {
        $this->loadHTMLBlock();
        $this->HTMLBlock->active = !(bool)$this->HTMLBlock->active;
        return $this->HTMLBlock->update();
    }

    private function deleteBlockHTML($id_pwblockhtml)
    {
        if (!$id_pwblockhtml||!$block = $this->loadObject()) {
            return false;
        }
        return $block->delete();
    }


    private function processHooks($block_html)
    {
        foreach ($block_html->hook_array as $hook) {
            $this->module->registerHook($hook);
        }
    }
	
	// Загружает HTMLBlock
	public function loadHTMLBlock()
	{
		if(!$this->HTMLBlock)//Если блок не загружен
		{
			//Пытаемся найти его id
			$idBlock = (int)Tools::getValue($this->identifier);
			if (!$idBlock||!$this->HTMLBlock = $this->loadObject())// Если id нет или мы не смогли загрузить блок с этим id
			{
				$this->HTMLBlock = new PWHTMLBlock();//Создаём новый блок
			}
		}
	}
}
