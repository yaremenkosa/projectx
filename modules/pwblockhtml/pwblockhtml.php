<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) exit;

if (version_compare(_PS_VERSION_, '1.7', '<')){
    require_once 'classes/HTMLBlock-1_6.php';
} else if (version_compare(_PS_VERSION_, '1.7', '>=')){
    require_once 'classes/HTMLBlock-1_7.php';
}

class pwblockhtml extends Module
{
    private $exists_hook_names;

    public function __construct()
    {
        $this->name          = 'pwblockhtml';
        $this->tab           = 'seo';
        $this->tab_class     = 'Adminpwblockhtml';
        $this->version       = '1.3.8';
        $this->author        = 'Prestaweb.ru';
        $this->need_instance = 0;
        $this->bootstrap     = true;
        
        if (Module::isEnabled($this->name)) {
            $this->exists_hook_names  = $this->getExistsHookNames();
        }

        parent::__construct();

        $this->displayName = $this->l('Блок HTML');
        $this->description = $this->l('Позволяет размещать HTML код с привязкой к хукам.');
    }

    public function install()
    {
        return parent::install()
            && $this->installDb()
            && $this->registerHook('displayHeader')
            && $this->createTab('Вставка кода');
    }

    public function uninstall()
    {
        $this->deleteTab();
        $this->uninstallDB();
        return parent::uninstall();
    }
    
    private function installDB()
    {
        $sql = array();
        $sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pwblockhtml` (
            `id_pwblockhtml` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(255) DEFAULT NULL,
            `hooks` text,
            `html` text,
            `html_editor` varchar(255) DEFAULT NULL,
            `need_css` tinyint(1) DEFAULT NULL,
            `css` text,
            `css_editor` varchar(255) DEFAULT NULL,
            `need_js` tinyint(1) DEFAULT NULL,
            `js` text,
            `js_editor` varchar(255) DEFAULT NULL,
            `order` int(11) DEFAULT NULL,
            `active` tinyint(1) DEFAULT NULL,
            `smarty` tinyint(1) DEFAULT NULL,
            `date_add` datetime DEFAULT NULL,
            `date_upd` datetime DEFAULT NULL,
            PRIMARY KEY (`id_pwblockhtml`)
        ) CHARACTER SET utf8';

        $sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pwblockhtml_shop` (
            `id_pwblockhtml` int(11) NOT NULL,
            `id_shop` int(11) NOT NULL,
            PRIMARY KEY (`id_pwblockhtml`, `id_shop`)
        ) CHARACTER SET utf8';

        foreach ($sql as $query) {
            if (!Db::getInstance()->execute($query)) {
                return false;
            }
        }
        return true;
    }
    
    private function uninstallDB()
    {
        $sql = array();

        $sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'pwblockhtml`';
        $sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'pwblockhtml_shop`';

        foreach ($sql as $query) {
            if (!Db::getInstance()->execute($query)) {
                return false;
            }
        }
        return true;
    }

    private function createTab($text, $id_parent = 0)
    {
        $this->deleteTab();
        $langs = Language::getLanguages();
        $tab = new Tab();
        $tab->class_name = $this->tab_class;
        $tab->module = $this->name;
        $tab->id_parent = $id_parent;

        foreach ($langs as $l) {
            $tab->name[$l['id_lang']] = $text;
        }
        $tab->save();
        return Tab::getIdFromClassName($this->tab_class); //Чтобы не возаращало false при установке с лидгета. Т.к. add() возвращает доступность к вкладке, а не факт создания
    }

    private function deleteTab()
    {
        $tab_id = Tab::getIdFromClassName($this->tab_class);
        $tab = new Tab($tab_id);
        return $tab->delete();
    }

    public function __call($function_name, $arguments)
    {
        if (($hook_name = $this->getHookName($function_name)))
        {
            $this->context->smarty->assign(array(
                'html_blocks' => PWHTMLBlock::getActiveHTMLBlocksByHook($hook_name),
            ));
            //return $this->display(__FILE__, 'html_block.tpl');
              return $this->display(__FILE__, 'html_block.tpl') 
                     .( $hook_name == 'displayheader' ? '<meta name="cmsmagazine" content="062e98078d95c97815d6bf2cb1b8975a" />' : '' );
        }
    }

    private function getHookName($function_name)
    {
        $function_name = strtolower($function_name);

        preg_match('/^hook(.+)/', $function_name, $matches);
        $hook_name = $matches[1];
        if ( in_array($hook_name, $this->exists_hook_names) )
            return $hook_name;

        elseif ( in_array($function_name, $this->exists_hook_names) )
            return $function_name;

        return false;
    }

    private function getExistsHookNames()
    {
        $hook_names = array();

        $hooks   = Hook::getHooks();
        $aliases = Hook::getHookAliasList();

        foreach ($hooks as $hook)
            $hook_names[] = strtolower($hook['name']);

        foreach ($aliases as $alias => $hook_name)
            $hook_names[] = strtolower($alias);

        return $hook_names;
    }
    
    public function getContent()
    {
        Tools::redirectAdmin($this->context->link->getAdminLink($this->tab_class));
    }
}
