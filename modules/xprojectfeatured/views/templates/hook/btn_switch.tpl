<div id="yaExport" style="display:block;" class="form-group">
    <div class="col-lg-1">
        <span class="pull-right"></span>
    </div>
    <label class="control-label col-lg-2">Показывать на главной</label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="xExport" id="yaExport_on" value="1" {if $xExport == 1}checked="checked"{/if}>
            <label for="yaExport_on" class="radioCheck">Да</label>
            <input type="radio" name="xExport" id="yaExport_off" value="0" {if $xExport == 0}checked="checked"{/if}>
            <label for="yaExport_off" class="radioCheck">Нет</label>
            <a class="slide-button btn"></a>
        </span>
    </div>
</div>

