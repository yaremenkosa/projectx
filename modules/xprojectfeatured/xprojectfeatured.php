<?php
/**
* yamarket module main file.
*
* @author    0RS <admin@prestalab.ru>
* @link http://prestalab.ru/
* @copyright Copyright &copy; 2009-2012 PrestaLab.Ru
* @license   http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
* @version 2.0
*/

if (!defined('_PS_VERSION_'))
	exit;

class XprojectFeatured extends Module
{

	public function __construct()
	{
		$this->name = 'xprojectfeatured';
		$this->tab = 'export';
		$this->version = '0.1.0';
		$this->author = 'andrele82';
		$this->need_instance = 0;
		//Ключик из addons.prestashop.com
		$this->module_key = '';
		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('xproject популярные товары');
		$this->description = $this->l('Показ товаров на главной для posfeaturedproduct');
	}

	public function install()
	{
        $sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xfeatured` (
    	`id_product` INT(11) NOT NULL,
        `export` TINYINT(2) NULL DEFAULT NULL,
        PRIMARY KEY (`id_product`)
        );';
		return (parent::install()
            && $this->registerHook(array('DisplayAdminProductsExtra',
                'DisplayAdminProductsMainStepLeftColumnMiddle',
                'ActionProductUpdate', 
                'ActionProductAdd')) 
                && Db::getInstance()->execute($sql)
		);
	}

	public function uninstall()
	{
		return parent::uninstall();
	
		
	}

    public function hookDisplayAdminProductsExtra($params) {

        require_once('classes/xProduct.php');
        $yaProduct = new xProduct(Tools::getValue('id_product'));
        
        
      
        $this->context->smarty->assign(array('yaExport' => $yaProduct->export));
        return $this->display(__FILE__, 'btn_switch.tpl');
    }
    
    public function hookDisplayAdminProductsMainStepLeftColumnMiddle($params) {
        require_once('classes/xProduct.php');
        $yaProduct = new xProduct($params['id_product']);
        $yaProduct->getExport($params['id_product']);
 
        $this->context->smarty->assign(array('xExport' => $yaProduct->export));
        return $this->display(__FILE__, 'btn_switch.tpl');
  

    }
    
    public function hookActionProductUpdate($params)
    {
    if (Tools::getValue('id_product', false) && Tools::getValue('xExport', false)) {
        require_once('classes/xProduct.php');
        $id_product = Tools::getValue('id_product');
        $yaProduct = new xProduct(Tools::getValue('id_product'));
    
        if (!$yaProduct->id) {
            $yaProduct->id = $id_product;
            $yaProduct->id_product = $id_product;
            $yaProduct->force_id = true;
            $yaProduct->export = (bool)Tools::getValue('xExport');
            $yaProduct->add();
        } else {
            $yaProduct->export = (bool)Tools::getValue('xExport');
            $yaProduct->save();
        }
    }
        
    }
    public function hookActionProductAdd($params)
    {
        if (Tools::getValue('id_product', false) && Tools::getValue('xExport', false)) {
            $this->hookActionProductUpdate($params);
        }
    }
}