<?php

class xProduct extends ObjectModel
{
    public $id;
    public $id_product;
    public $export;
    
    public static $definition = array(
        'table' => 'xfeatured',
        'primary' => 'id_product',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array
        (
            'export' => array('type' => self::TYPE_BOOL, 'default' => 0),
        ),
    );
    public function getExport($id = null)
    {
        $exp = Db::getInstance()->executeS('SELECT `export` FROM '._DB_PREFIX_.'xfeatured WHERE id_product = '.(string)$id);
        if (isset($exp[0]['export'])) $this->export = $exp[0]['export'];
        else $this->export = 0;
    }
}