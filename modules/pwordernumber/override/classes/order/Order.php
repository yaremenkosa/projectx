<?php
/**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0) International Registered Trademark & Property of PrestaShop SA
 */

class Order extends OrderCore
{
    public static function generateReference()
    {
        switch(Configuration::get('PWORDERNUMBER_OPTION1')){
            case 1:
                $last_id = Db::getInstance()->getValue('
                  SELECT MAX(id_order)
                  FROM '._DB_PREFIX_.'orders');
                return str_pad((int)$last_id + 1, 7, '0000000', STR_PAD_LEFT);
                break;
            case 2:
                return Tools::passwdGen(9, 'NUMERIC');
                break;
            default: /*Отключено */
                return Tools::strtoupper(Tools::passwdGen(9, 'NO_NUMERIC'));
                break;
        }

    }
}
