<?php
/**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0) International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class PwOrderNumber extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'pwordernumber';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Prestaweb.ru';
        $this->need_instance = 0;

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Make orders by serial number');
        $this->description = $this->l('Replaces the letters on the serial number');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        Configuration::updateValue('PWORDERNUMBER_OPTION1', '1');
        return parent::install();
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    
    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'radio',
                        'label' => $this->l('Type order number'),
                        'name' => 'PWORDERNUMBER_OPTION1',
                        'hint' => $this->l(''),
                        'values' => array(
                            array(
                                'id' => 'disabled',
                                'value' => 0,
                                'label' => $this->l('Disable')
                            ),
                            array(
                                'id' => 'id_order',
                                'value' => 1,
                                'label' => $this->l('Serial number')
                            ),
                            array(
                                'id' => 'random',
                                'value' => 2,
                                'label' => $this->l('Random order')
                            )
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitPWORDERNUMBER';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'PWORDERNUMBER_OPTION1' => Tools::getValue('PWORDERNUMBER_OPTION1', Configuration::get('PWORDERNUMBER_OPTION1'))
        );
    }
    public function getContent()
    {
        $output = '';
        if (Tools::isSubmit('submitPWORDERNUMBER'))
        {
            Configuration::updateValue('PWORDERNUMBER_OPTION1', Tools::getValue('PWORDERNUMBER_OPTION1'));
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&conf=6');
        }
        return $output.$this->renderForm();
    }
}
