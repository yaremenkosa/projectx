<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{pwordernumber}prestashop>pwordernumber_a1cc9f7878ad51c33e655d116348c437'] = 'Сделать заказы порядковым номером';
$_MODULE['<{pwordernumber}prestashop>pwordernumber_fb14d5e2e04887372a85eb421037bcf3'] = 'Заменяет буквы на порядковый номер';
$_MODULE['<{pwordernumber}prestashop>pwordernumber_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{pwordernumber}prestashop>pwordernumber_708009b60260ad3dab3f0f44663d2a8e'] = 'Тип номера заказа';
$_MODULE['<{pwordernumber}prestashop>pwordernumber_bcfaccebf745acfd5e75351095a5394a'] = 'Отключить';
$_MODULE['<{pwordernumber}prestashop>pwordernumber_3460276ef9eb483d13aa31b870077a0d'] = 'Порядковый номер';
$_MODULE['<{pwordernumber}prestashop>pwordernumber_681b35b63483b18bf2b5fb329bbce63e'] = 'В случайном порядке';
$_MODULE['<{pwordernumber}prestashop>pwordernumber_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
