<?php
class PWExpressOrderClass
{
    var $conf;
    var $clientData;

    public static $address_fields = Array(
        'lastname',
        'firstname',
        'address1',
        'address2',
        'city',
        'id_state',
        'id_country',
        'other',
        'phone',
        'phone_mobile'
    );

    public static function getLastAddress($id_customer)
    {
        if(empty($id_customer))
            return array();
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
		SELECT *
		FROM `' . _DB_PREFIX_ . 'address`
		WHERE `id_customer` = ' . (int)($id_customer) . '
		AND `deleted` = 0 ORDER BY `id_address` DESC');
    }



    public static function getSimilarAddress(Address $address, $id_customer){
        $sql = 'SELECT id_address FROM `'._DB_PREFIX_.'address` WHERE 1';
        foreach(self::$address_fields as $field){
            $sql.=' AND '.$field.' = "'.$address->$field.'" ';
        }
        $sql.=' AND id_customer = '.(int)$id_customer.' ORDER BY id_address DESC';
        $result = Db::getInstance()->getValue($sql);
        return $result;
    }

    /**
     * @return bool
     */
    public function cleanCartAddress()
    {
        $context = Context::getContext();
        return Db::getInstance()->Execute('UPDATE `' . _DB_PREFIX_ . 'cart_product` SET id_address_delivery = 0 WHERE id_cart =  ' . $context->cart->id);
    }

}