<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{pwexpressorder}prestashop>form_f50d065821e1d997a32ec402f29cf6ea'] = 'l\'établissement de la commande';
$_MODULE['<{pwexpressorder}prestashop>form_7e63ca937366b63e80d14389922236d4'] = 'Name';
$_MODULE['<{pwexpressorder}prestashop>form_2a5600231e1287383396f4db58b13747'] = 'Comme à vous s\'adresser';
$_MODULE['<{pwexpressorder}prestashop>form_3bf18b050771673272461c55620f70d8'] = 'E-mail';
$_MODULE['<{pwexpressorder}prestashop>form_1a1515da070b116e9d958ee184245893'] = 'Pour les avis du statut de la commande';
$_MODULE['<{pwexpressorder}prestashop>form_d4799e83fd54a7945866068db36c4609'] = 'Phone';
$_MODULE['<{pwexpressorder}prestashop>form_0530bd477953bbbaaeb215f0111eb97b'] = 'Pour la confirmation de la commande';
$_MODULE['<{pwexpressorder}prestashop>form_80148fa5ada7bcd36bf3b351ee3ca3b0'] = 'Address';
$_MODULE['<{pwexpressorder}prestashop>form_783cb853aae6984e51583b3bb80c09d2'] = 'Address (2)';
$_MODULE['<{pwexpressorder}prestashop>form_069c9cb17c0aca1e499f3a00fdeb9b3a'] = 'Ville';
$_MODULE['<{pwexpressorder}prestashop>form_92214e2e52400ef5a61b9ffedc35057c'] = 'Code postale';
$_MODULE['<{pwexpressorder}prestashop>form_8a02e2ef4f9b23b894f7ddc04dc4b0d8'] = 'Pays';
$_MODULE['<{pwexpressorder}prestashop>form_43e670f6d5fc1b265e0243a65ec39ff3'] = 'Introduisez les souhaits sur la commande';
$_MODULE['<{pwexpressorder}prestashop>form_9c389312e17fc238cc52eec49c4c9407'] = 'Le temps de la livraison, les questions au sujet de la marchandise, les souhaits supplémentaires';
$_MODULE['<{pwexpressorder}prestashop>form_dbce3b4f08006c079e53c2b1daeadb47'] = 'Signer sur notre diffusion';
$_MODULE['<{pwexpressorder}prestashop>form_9b4659ede6ccdb76d12d1c6fd1031aca'] = 'Recevoir les propositions spéciales';
$_MODULE['<{pwexpressorder}prestashop>form_724290b51c4d695ac6446f6e80a24fff'] = 'Utiliser une autre adresse pour la livraison';
$_MODULE['<{pwexpressorder}prestashop>form_a3783c62c13e78e57859a5e86b963c16'] = 'Une autre adresse';
$_MODULE['<{pwexpressorder}prestashop>form_c5823d2d5f07a90a44c2bf4bda4dac17'] = 'Passer au choix du paiement';
$_MODULE['<{pwexpressorder}prestashop>form_3e936e4371e149fb24cce3d96a236f71'] = 'Valider Commande';
$_MODULE['<{pwexpressorder}prestashop>pwexpressorder_34a928487b569e1d9e8eebbbaf11ecb7'] = 'Remplissez tous les champs nécessaires';
