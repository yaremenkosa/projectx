{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{if $field.type == 'hidden'}

  <input type="hidden" name="{$field.name}" value="{$field.value}">

{else}

  <div class="form-group {if !empty($field.errors)}has-error{/if}">
    <label class="{if $field.required}required-field{/if}">
      {if $field.type !== 'checkbox'}
        {$field.label}{if $field.required}<sup style="color: red;">*</sup>{/if}
      {/if}
    </label>

      {if $field.type === 'select'}

        <select class="form-control form-control-select {if $field.required}is_required{/if} {if !empty($field.validate)}validate{/if}" name="{$field.name}" {if $field.required}required{/if}>
          <option value disabled selected>{l s='-- please choose --' mod='pwexpressorder'}</option>
          {foreach from=$field.availableValues item="label" key="value"}
            <option value="{$value}" {if $value eq $field.value} selected {/if}>{$label}</option>
          {/foreach}
        </select>

      {elseif $field.type === 'countrySelect'}

        <select
          class="form-control form-control-select js-country {if $field.required}is_required{/if} {if !empty($field.validate)}validate{/if}"
          name="{$field.name}"
          {if $field.required}required{/if}
        >
          <option value disabled selected>{l s='-- please choose --' mod='pwexpressorder'}</option>
          {foreach from=$field.availableValues item="label" key="value"}
            <option value="{$value}" {if $value eq $field.value} selected {/if}>{$label}</option>
          {/foreach}
        </select>

      {elseif $field.type === 'checkbox'}

        <span class="custom-checkbox">
          <input name="{$field.name}" type="checkbox" value="1" {if $field.value}checked="checked"{/if} {if $field.required}required{/if}>
          <span><i class="material-icons checkbox-checked {if $field.required}is_required{/if}">&#xE5CA;</i></span>
          <label>{$field.label nofilter}</label >
        </span>
        
      {elseif $field.type === 'textarea'}
        <textarea name="{$field.name}" placeholder="{if isset($field.availableValues.placeholder)}{$field.availableValues.placeholder}{/if}" class="form-control {if $field.required}is_required{/if}" cols="40" rows="5"></textarea>

      {elseif $field.type === 'date'}

        <input class="form-control {if $field.required}is_required{/if} {if !empty($field.validate)}validate{/if}" {if !empty($field.validate)}data-validate="{$field.validate}"{/if} type="date" value="{$field.value}" placeholder="{if isset($field.availableValues.placeholder)}{$field.availableValues.placeholder}{/if}">
        {if isset($field.availableValues.comment)}
          <span class="form-control-comment">
            {$field.availableValues.comment}
          </span>
        {/if}

      {elseif $field.type === 'password'}

        <div class="input-group js-parent-focus">
          <input
            class="form-control js-child-focus js-visible-password {if $field.required}is_required{/if}"
            name="{$field.name}"
            type="password"
            value=""
            {if !empty($field.validate)}data-validate="{$field.validate}"{/if}
            pattern=".{literal}{{/literal}5,{literal}}{/literal}"
            {if $field.required}required{/if}
          >
        </div>
      {else}

        <input
          class="form-control {if !empty($field.validate)}validate{/if} {if $field.required}is_required{/if}"
          name="{$field.name}"
          type="{$field.type}"
          value="{$field.value}"
          {if !empty($field.validate)}data-validate="{$field.validate}"{/if}
          {if isset($field.availableValues.placeholder)}placeholder="{$field.availableValues.placeholder}"{/if}
          {if $field.maxLength}maxlength="{$field.maxLength}"{/if}
          {if $field.required}required{/if}
        >
        {if isset($field.availableValues.comment)}
          <span class="form-control-comment">
            {$field.availableValues.comment}
          </span>
        {/if}

      {/if}
  </div>

{/if}
