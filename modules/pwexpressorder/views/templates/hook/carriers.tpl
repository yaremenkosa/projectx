<div class="carrier-select">
    {if isset($carriers) && is_array($carriers)}
            <h3>{l s='Выберите способ доставки' mod='pwexpressorder'}</h3>
            {if count($carriers)}
                <table class="table" id="carrier-select">
                    {foreach from=$carriers key=key item=car name=carriers}
                        <tr{if $smarty.foreach.carriers.last} class="last"{/if}>
                            <td class="radio-select"><input type="radio" name="id_carrier" id="carrier{$car.id_carrier}" data-key="{$key}" value="{$car.id_carrier}" data-name="delivery_option[{$id_address|intval}]" {if  $smarty.foreach.carriers.iteration == 1} checked="checked"{/if}{if $cart->id_carrier == $car.id_carrier} checked="checked"{/if}></td>
                            <td class="name"><label for="carrier{$car.id_carrier_carrier}">{$car.name}</label></td>
                            <td class="delay">{$car.delay}</td>
                            <td class="cost">{if $car.price === 0}<span class="free">{l s='Бесплатно' mod='pwexpressorder'}</span>{else}{$car.price}{/if}</td>
                            <td class="extra">{if !empty($car.extraContent)}{$car.extraContent}{/if}</td>
                        </tr>
                    {/foreach}
                </table>
            {else}
                <p class="alert alert-warning">{l s='Нет доступных способов доставки'}</p>
            {/if}
    {/if}
</div>