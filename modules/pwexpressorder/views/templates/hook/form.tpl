<div class="pwexpressorder card" id="pwexpressorder">
    <div class="card-block">
        <h1 class="h1">{l s="Checkout" mod="pwexpressorder"}</h1>
    </div>
    <hr />
    <div class="cart-overview">
        {if isset($errors)}
        {include file="$tpl_dir./errors.tpl"}
        {/if}
        {if !$hideForm}
        <form action="{$pwexpressorder_uri}" method="post" class="std" id="pwexpressorder_form">
            <section class="form-fields">
                {block name='form_fields'}
                    {foreach from=$formFields item="field"}
                        {block name='form_field'}
                            {include file="./form_field.tpl" field=$field}
                        {/block}
                    {/foreach}
                {/block}
                <input type="submit" name="submitPwExpressOrderUpdate" id="submitPwExpressOrderUpdate" value="update carriers" class="btn btn-default {*hidden*}">
            </section>

            {if isset($carriers) && is_array($carriers)}
                <div class="carrier-select">
                    <h3>{l s='Choose shipping method' mod='pwexpressorder'}</h3>
                    {if count($carriers)}
                        <table class="table" id="carrier-select">
                            {foreach from=$carriers key=key item=car name=carriers}
                        <tr{if $smarty.foreach.carriers.last} class="last"{/if}>
                                    <td class="radio-select"><input type="radio" name="id_carrier" id="carrier{$car.id_carrier}" data-key="{$key}" value="{$car.id_carrier}" data-name="delivery_option[{$id_address|intval}]" {if  $smarty.foreach.carriers.iteration == 1} checked="checked"{/if}{if $cart->id_carrier == $car.id_carrier} checked="checked"{/if}></td>
                            <td class="name"><label for="carrier{$car.id_carrier_carrier}">{$car.name}</label></td>
                            <td class="delay">{$car.delay}</td>
                                    <td class="cost">{if $car.price === 0}<span class="free">{l s='Free' mod='pwexpressorder'}</span>{else}{$car.price}{/if}</td>
                            <td class="extra">{if !empty($car.extraContent)}{$car.extraContent}{/if}</td>
                        </tr>
                        {/foreach}
                        </table>
                    {else}
                        <p class="alert alert-warning">{l s='No shipping methods available'}</p>
                    {/if}
                </div>
            {/if}
        
        

            <div class="eo-manager">{l s="After placing the order, our manager will contact you for further details.
"}</div>
            <footer class="form-footer clearfix">
              <input type="hidden" name="submitPwExpressOrder" value="1">
              {block name='form_buttons'}
                <button class="button btn btn-default button-medium pull-right" id="submitPwExpressOrder" name="submitPwExpressOrder" type="submit" class="form-control-submit">
                  <span>{l s='Checkout' mod='pwexpressorder'}</span>
                </button>
              {/block}
            </footer>
        </form>
        {/if}
    </div>
</div>
