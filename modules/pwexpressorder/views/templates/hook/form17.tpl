<div class="pwexpressorder card" id="pwexpressorder">
    <div class="pwexpressorder-block">
        <h1 class="h1">{l s="Checkout" mod="pwexpressorder"}</h1>
    </div>
    <hr />
    <div class="pwexpressorder-overview">
        {if isset($errors)}
        {include "_partials/form-errors.tpl"}
        {/if}
        {if !$hideForm}
        <form action="{$pwexpressorder_uri}" method="post" class="std" id="pwexpressorder_form">
        
            {if !empty($carriers)}
            {block name='delivery_options'}
            <div class="delivery-options">
              {foreach from=$carriers item=carrier key=carrier_id}
                  <div class="delivery-option">
                    <div class="col-sm-1">
                      <span class="custom-radio pull-xs-left">
                        <input type="radio" name="id_carrier" id="delivery_option_{$carrier.id_carrier}" value="{$carrier.id_carrier}"{if $delivery_option == $carrier.id_carrier} checked{/if}>
                        <span></span>
                      </span>
                    </div>
                    <label for="delivery_option_{$carrier.id_carrier}" class="col-sm-10 delivery-option-2">
                      <div class="row">
                        <div class="col-sm-3 col-xs-12">
                          <div class="row">
                            <div class="col-xs-12">
                              <span class="h6 carrier-name">{$carrier.name}</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                          <span class="carrier-delay">{$carrier.delay}</span>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                          <span class="carrier-price">{$carrier.price}</span>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <span class="carrier-extra">{if !empty($carrier.extraContent)}{$carrier.extraContent nofilter}{/if}</span>
                        </div>
                      </div>
                    </label>
                    <div class="clearfix"></div>
                  </div>
              {/foreach}
            </div>
            {/block}
            <hr /><br />
            {/if}
        
        
            <section class="form-fields">
              {block name='form_fields'}
                {foreach from=$formFields item="field"}
                  {block name='form_field'}
                    {form_field field=$field}
                  {/block}
                {/foreach}
              {/block}
            </section>
            <footer class="form-footer clearfix pwexpress_footer">
              <input type="hidden" name="submitPwExpressOrder" value="1">
              {block name='form_buttons'}
                <button class="btn btn-primary pull-xs-right pwexpress_button" type="submit" class="form-control-submit" onclick="ym(74564365,'reachGoal','showlogist'); return true;">
                  {* {l s='Checkout' mod='pwexpressorder'} *}
                  Оформить заказ
                </button>
              {/block}
            </footer>
        </form>
        {/if}
    </div>
</div>
