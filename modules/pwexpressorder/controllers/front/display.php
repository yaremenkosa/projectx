<?php
/* for 1.5 and 1.6 */

require_once _PS_MODULE_DIR_.'pwexpressorder/classes/PWExpressOrderClass.php';

class PWExpressOrderDisplayModuleFrontController extends ModuleFrontController
{

    public $pw_express_order;
    public $pw_express_order_module;
    public $errors;
    public $new_customer;
    public $customer;
    public $address;
    public $delivery_address;
    public $passwd;
    
    public $defaults = array();

    public function __construct()
    {
        parent::__construct();

        $this->pw_express_order        = new PWExpressOrderClass();
        $this->errors                  = array();
        $this->new_customer            = false;
        $this->customer                = null;
        $this->address                 = null;
        $this->delivery_address        = null;
        $this->display_column_left     = false;
        $this->display_column_right    = false;
        //an array with default values, in case the field is optional for us, but mandatory for prestashop
        $this->defaults = array(
            'firstname'  => $this->module->l('Customer'),
            'lastname'   => $this->module->l('Customer'),
            'alias'      => $this->module->l('My address'),
            'id_state'   => 743,
            'id_country' => Configuration::get('PS_COUNTRY_DEFAULT'),
            'city'       => $this->module->l('Moscow'),
        );
    }

    public function initContent()
    {
        parent::initContent();

        if ($this->context->cart->id_address_delivery != 0) {
            $add = new Address($this->context->cart->id_address_delivery);
            if (Tools::getValue('eo_zip') != '') {
                $add->postcode = Tools::getValue('eo_zip');
            }
            if (Tools::getValue('eo_country') != '') {
                $add->id_country = Tools::getValue('eo_country');
            }
            if (Tools::getValue('eo_fname') != '') {
                $add->firstname = Tools::getValue('eo_fname');
            }
            if (Tools::getValue('eo_lname') != '') {
                $add->lastname = Tools::getValue('eo_lname');
            }
            if (Tools::getValue('eo_address') != '') {
                $add->address1 = Tools::getValue('eo_address');
            }
            if (Tools::getValue('eo_city') != '') {
                $add->city = Tools::getValue('eo_city');
            }
            if (Tools::getValue('eo_mobilephone') != '') {
                $add->phone_mobile = Tools::getValue('eo_mobilephone');
            }
            if (Tools::getValue('eo_other') != '') {
                $add->other = Tools::getValue('eo_other');
            }

            $add->save();
        }

        
        if(Tools::isSubmit('confirmOrder') && Tools::getValue('id_carrier') && (Configuration::get('PW_EO_CARRIER') == PwExpressOrder::_CARRIER_ADV_ && Configuration::get('PW_EO_PAYMENT') == PwExpressOrder::_PAYMENT_OFF_)){
            $this->address = new Address($this->context->cart->id_address_delivery);
            $this->customer = $this->context->customer;
            $this->setCarrier();
            $this->processOrder();
        }

        $this->validate(); //check that in the cart everything is according to our conditions
        $this->customer = $this->getCustomer(); //get a customer (create a new one, if not exists)
        $this->context->customer = $this->customer;
        $this->validateCustomer();

        $this->address          = $this->getAddress(); //create address
        $this->delivery_address = $this->getDeliveryAddress(); //create a new address, but for delivery
        //A shipping address is created if there is a checkmark for a separate shipping address.
        $this->validateAddress();
        //address validation (not shipping address)

        if (Tools::isSubmit('submitPwExpressOrder') AND !$this->errors)
        { 
            $this->customer->save(); //save new customer
            if ($this->new_customer)
                $this->sendNewCustomerMail();
            if(isset($this->context->cookie->id_customer) AND $this->context->cookie->id_customer != $this->customer->id) {
                $this->logoutANDResetCart(); //if the username does not match, then log out
            }

            $this->saveAddresses(); //save addresses
            
            $this->pw_express_order->cleanCartAddress(); //some interesting method to clear the preset addresses for each item in the cart
            $this->setCustomerCookie(); //writing our customer to cookies
            $this->updateCart(); //update cart values (customer, address)
            if (Tools::isSubmit('submitPwExpressOrderUpdate') AND !$this->errors){
                $this->updateCarriers();
            }
            $this->setCarrier(); //install the supplier. the method works analogously to the standard one and should not cause problems

            $this->processOrder(); //buy!
        }
        if($this->errors){
            $this->processErrors();
        }
        $this->setTemplate('display.tpl');
    }
    
    // save address
    private function saveAddresses()
    {
        $this->replaceAddressIfSimilarExists();
        
        $this->address->id_customer = $this->customer->id;
        $this->address->save();
        if ($this->delivery_address)
        {
            $this->delivery_address->id_customer = $this->customer->id;
            $this->delivery_address->save();
        }
    }

    private function validate()
    {
        if (empty($this->context->cart->id)){
            $this->addNewCart();
        }
        $this->validateCart();
        $errors = $this->postValidate();
        $this->errors = array_merge($this->errors, $errors);
    }

    private function validateCart()
    {
        $order_total = $this->context->cart->getOrderTotal();
        $cart_count = count($this->context->cart->getProducts());

        $order_total_default = Tools::convertPrice($this->context->cart->getOrderTotal(true, 1), Currency::getCurrency(Configuration::get('PS_CURRENCY_DEFAULT')));
        $minimal_purchase    = Configuration::get('PS_PURCHASE_MINIMUM');

        if ($order_total_default < $minimal_purchase)
            $this->errors[] = Tools::displayError($this->module->l('The total amount of the order must be at least ')).' '.Tools::displayPrice($minimal_purchase, Currency::getCurrency($this->context->cart->id_currency));

        // elseif ($order_total == 0) //what a hell with you?? if order_total = 0 then order_total=0, but you write then cart is empty
        elseif ($cart_count <= 0)
            $this->errors[] = Tools::displayError($this->module->l('Your cart is empty'));
    }

    private function validateCustomer()
    {
        $this->errors = array_merge(
            $this->errors
            ,$this->customer->validateController()
            ,$this->customer->validateFieldsRequiredDatabase()
        );
    }

    private function validateAddress()
    {
        $this->errors = array_merge(
            $this->errors,
            $this->address->validateController()
        );
    }

    private function addNewCart()
    {
        $this->context->cart->add();

        if ($this->context->cart->id)
            $this->context->cookie->id_cart = $this->context->cart->id;
    }
    
    protected function getCustomer()
    {
        $email = Tools::getValue('eo_email', Tools::getValue('email'));
        if(empty($email))
            $email = 'guest' . uniqid() . '@' . 'prestaweb.ru';
        $customers = Customer::getCustomersByEmail($email);
        $this->passwd = Tools::getValue('eo_password', Tools::getValue('passwd')) ? Tools::getValue('eo_password', Tools::getValue('passwd')) : Tools::passwdGen();
        if (isset($customers[0]['id_customer'])){
            $customer = new Customer($customers[0]['id_customer']);
        }
        else
        {
            $this->new_customer = true;
            $customer = new Customer();
            $customer->email      = $email;
            $customer->birthday   = date("Y-m-d", strtotime("-18 years"));
            $customer->passwd     = Tools::encrypt($this->passwd);
            $customer->newsletter = Tools::getValue('newsletter') ? true : false;
            $customer->optin      = Tools::getValue('optin') ? true : false;
        }
        $customer->firstname = Tools::getValue('eo_fname', Tools::getValue('firstname'));
        $customer->lastname  = Tools::getValue('eo_lname', Tools::getValue('lastname'));
        $customer->deleted   = false;
        foreach($customer->validateController() as $field => $error){
            $customer->$field = $this->getDefaultValue($field, 'Customer');
        }
        return $customer;
    }

    private function sendNewCustomerMail()
    {
        Mail::Send(
            $this->context->cookie->id_lang,
            'account',
            Mail::l('Welcome!'),
            array(
                '{firstname}' => $this->customer->firstname,
                '{lastname}'  => $this->customer->lastname,
                '{email}'     => $this->customer->email,
                '{passwd}'    => !empty($this->pw_express_order->clientData['passwd'])?$this->pw_express_order->clientData['passwd']:$this->passwd,
            ),
            $this->customer->email,
            $this->customer->firstname . ' ' . $this->customer->lastname
        );
    }

    private function setCustomerCookie()
    {
        $this->context->cookie->id_customer        = $this->customer->id;
        $this->context->cookie->customer_lastname  = $this->customer->lastname;
        $this->context->cookie->customer_firstname = $this->customer->firstname;
        $this->context->cookie->passwd             = $this->customer->passwd;
        $this->context->cookie->logged             = 1;
        $this->context->cookie->email              = $this->customer->email;
    }

    private function getAddress()
    {
        $address               = new Address();
        
        $address->alias        = $this->getDefaultValue('alias', 'Address');
        $address->phone_mobile = $_POST['phone_mobile'] = Tools::getValue('eo_mobilephone', Tools::getValue('phone_mobile', null));
        $address->lastname     = $this->customer->firstname;
        $address->firstname    = $this->customer->lastname;
        $address->phone        = $_POST['phone'] = Tools::getValue('eo_phone', Tools::getValue('phone', null));
        $address->id_customer  = $this->customer->id;
        $address->city         = $_POST['city'] = Tools::getValue('eo_city', Tools::getValue('city', null));
        $address->id_country   = $_POST['id_country'] = Tools::getValue('eo_country', Tools::getValue('id_country', null));

        $this->context->country = new Country($address->id_country);

        $address->id_state = $_POST['id_state'] = Tools::getValue('eo_state', Tools::getValue('id_state', null));
        $address->other    = $_POST['other'] = Tools::getValue('eo_other', Tools::getValue('other', null));
        $address->postcode = Tools::getValue('eo_zip');
        $address->company  = $_POST['company'] = Tools::getValue('eo_company', Tools::getValue('company', null));
        $address->address1 = $_POST['address1'] = Tools::getValue('eo_address', Tools::getValue('address1', null));
        $address->address2 = $_POST['address2'] = Tools::getValue('eo_address2', Tools::getValue('address2', null));
        if (mb_strlen($address->other) > $address::$definition['fields']['other']['size']) { //cut long
            $address->other = $_POST['other'] = mb_substr($address->other, 0, $address::$definition['fields']['other']['size']);
        }
        foreach($address->validateController() as $field => $error){
            $address->$field = $this->getDefaultValue($field, 'Address');
        }
        if(empty($address->id_country))
            $address->id_country = $this->getDefaultValue('id_country', 'Address'); //to have delivery
        return $address;
    }

    private function getDeliveryAddress()
    {
        if (!Tools::isSubmit('use_delivery_address') OR !(int)Tools::getValue('use_delivery_address')) return null;

        $address              = new Address();
        $address->alias       = self::ADDRESS_ALIAS;
        $address->address1    = Tools::getValue('delivery_address');
        $address->city        = Tools::getValue('delivery_city');
        $address->postcode    = Tools::getValue('delivery_zip') ? Tools::getValue('delivery_zip') : null;
        $address->id_country  = Tools::getValue('delivery_id_country') ? Tools::getValue('delivery_id_country') : Configuration::get('PS_COUNTRY_DEFAULT');
        $address->id_customer = $this->customer->id;
        $address->lastname    = $this->customer->firstname;
        $address->firstname   = $this->customer->lastname;
        $address->phone       = Tools::getValue('phone');
        $address->id_state    = self::ID_STATE;

        $this->context->country = new Country($address->id_country);

        return $address;
    }

    private function replaceAddressIfSimilarExists()
    {
        $id_address_found = PWExpressOrderClass::getSimilarAddress($this->address, $this->customer->id);

        if ($id_address_found) {
            $this->address = new Address($id_address_found, $this->context->cookie->id_lang);
        }
    }

    private function logoutANDResetCart()
    {
        $this->context->cookie->mylogout();
        $this->context->cookie->id_cart = $this->context->cart->id;
    }

    private function updateCart()
    {
        $this->context->cart->delivery_option     = "";
        $this->context->cart->id_customer         = $this->customer->id;
        //$this->context->cart->id_address_delivery = $this->delivery_address->id;
        //$this->context->cart->id_address_invoice  = $this->delivery_address->id;
        $this->context->cart->secure_key          = $this->customer->secure_key;

        if (isset($this->delivery_address) AND $this->delivery_address) {
            $this->context->cart->id_address_delivery = $this->delivery_address->id;
            $this->context->cart->id_address_invoice  = $this->delivery_address->id;
        }


        $this->context->cart->update();

        //We set the correct address for the products, otherwise the wrong address will be set
        $this->context->cart->updateAddressId(
            key($this->context->cart->getPackageList(true)),
            $this->context->cart->id_address_delivery
        );
    }

    private function setCarrier()
    {
        $id_carrier = Tools::isSubmit('id_carrier') ? Tools::getValue('id_carrier') :
            Carrier::getDefaultCarrierSelection(Carrier::getCarriers($this->context->language->id));

        $delivery_option_list = $this->context->cart->getDeliveryOptionList(null, true);
        $key = $id_carrier .','; //WAT?
        $error = true;
        foreach ($delivery_option_list as $id_address => $options)
        {
            if (isset($options[$key]))
            {
                $error = false;
                $this->context->cart->id_carrier = $id_carrier;
                //need to substitute the current address
                $this->context->cart->setDeliveryOption(array($this->address->id => $key));
                if (isset($this->context->cookie->id_country))
                    unset($this->context->cookie->id_country);
                if (isset($this->context->cookie->id_state))
                    unset($this->context->cookie->id_state);
                break;
            }
        }
        if($error && Configuration::get('PW_EO_CARRIER') == PwExpressOrder::_CARRIER_ON_){
            $this->errors['id_carrier'] = $this->module->l('Carrier is not available for your address');
            $this->processErrors();
        }
        $this->context->cart->update();
    }
    
    private function processErrors()
    {
        $this->context->cookie->pw_errors = implode(";;", $this->errors);
        if(version_compare(_PS_VERSION_, '1.7', '>=')){
            Tools::redirect(Context::getContext()->link->getPageLink('cart', null, null, 'action=show'));
        }
        Tools::redirect(Context::getContext()->link->getPageLink('order'));
    }

    private function processOrder()
    {
        if(count($this->errors)){
            $this->processErrors();
        }
        if ((Configuration::get('PW_EO_PAYMENT') == PwExpressOrder::_PAYMENT_OFF_ && Configuration::get('PW_EO_CARRIER') != PwExpressOrder::_CARRIER_ADV_) || Tools::isSubmit('confirmOrder')){
            $total = $this->context->cart->getOrderTotal(true, 3);
            $this->module->validateOrder(
                $this->context->cart->id,
                Configuration::get('PS_OS_PREPARATION'),
                $total,
                $this->module->displayName,
                null,
                array(),
                null,
                false,
                $this->customer->secure_key
            );
            Tools::redirectLink(
                __PS_BASE_URI__ .
                'order-confirmation.php?key=' . $this->customer->secure_key .
                '&id_cart=' . intval($this->context->cart->id) .
                '&id_module=' . intval($this->module->id) .
                '&id_order=' . intval($this->module->currentOrder)
            );
        }
        else{
            $this->autoStep();
            $step = 3;

            if(Configuration::get('PW_EO_CARRIER')  == PwExpressOrder::_CARRIER_ADV_)
                $step = 2;
            $params = array(
                'step' => $step,
                'checksum' => $this->checksum
            );

            Tools::redirectLink($this->context->link->getPageLink('order', null, null, $params));
        }
    }
    
    private function autoStep()
    {
        if(version_compare(_PS_VERSION_, '1.7', '<')){
            return ;
        }
        $session = $this->getCheckoutSession();
        $this->checkoutProcess = new CheckoutProcess(
            $this->context,
            $session
        );
        $translator = $this->getTranslator();
        $step_personal = new CheckoutPersonalInformationStep(
                $this->context,
                $translator,
                $this->makeLoginForm(),
                $this->makeCustomerForm()
            );
        $step_address = new CheckoutAddressesStep(
                $this->context,
                $translator,
                $this->makeAddressForm()
            );
        $step_personal->setComplete(true)->setReachable(true);
        $step_address->setComplete(true)->setReachable(true);
        $this->checkoutProcess
            ->addStep($step_personal)
            ->addStep($step_address);
        if (!$this->context->cart->isVirtualCart()) {
            $checkoutDeliveryStep = new CheckoutDeliveryStep(
                $this->context,
                $translator
            );

            $checkoutDeliveryStep
                ->setRecyclablePackAllowed((bool) Configuration::get('PS_RECYCLABLE_PACK'))
                ->setGiftAllowed((bool) Configuration::get('PS_GIFT_WRAPPING'))
                ->setIncludeTaxes(
                    !Product::getTaxCalculationMethod((int) $this->context->cart->id_customer)
                    && (int) Configuration::get('PS_TAX')
                )
                ->setDisplayTaxesLabel((Configuration::get('PS_TAX') && !Configuration::get('AEUC_LABEL_TAX_INC_EXC')))
                ->setGiftCost(
                    $this->context->cart->getGiftWrappingPrice(
                        $checkoutDeliveryStep->getIncludeTaxes()
                    )
                );
            // if(Configuration::get('PW_EO_CARRIER') == PwExpressOrder::_CARRIER_ADV_){
                 $checkoutDeliveryStep->setCurrent(true);
            // }else{
               //  $checkoutDeliveryStep->setComplete(true);
            // }
            $checkoutDeliveryStep->setReachable(true);
            $this->checkoutProcess->addStep($checkoutDeliveryStep);
        }
        $payment_step = new CheckoutPaymentStep(
                $this->context,
                $translator,
                new PaymentOptionsFinder(),
                new ConditionsToApproveFinder(
                    $this->context,
                    $translator
                )
            );
   
        
        //if(Configuration::get('PW_EO_CARRIER') == PwExpressOrder::_CARRIER_ADV_){
            $payment_step->setReachable(true);
        // }else{
          //  $payment_step->setCurrent(true)->setReachable(true);
            $this->checkoutProcess
                ->addStep($payment_step);
      //  }
        $this->saveDataToPersist($this->checkoutProcess);
    }
    
    private function saveDataToPersist(CheckoutProcess $process)
    {
        $data = $process->getDataToPersist();

        $this->cartChecksum = new CartChecksum(new AddressChecksum());
        $data['checksum'] = $this->cartChecksum->generateChecksum($this->context->cart);

        Db::getInstance()->execute(
            'UPDATE '._DB_PREFIX_.'cart SET checkout_session_data = "'.pSQL(json_encode($data)).'"
                WHERE id_cart = '.(int) $this->context->cart->id
        );

        $this->checksum = $data['checksum'];
    }
    
    private function getCheckoutSession()
    {
        $deliveryOptionsFinder = new DeliveryOptionsFinder(
            $this->context,
            $this->getTranslator(),
            $this->objectPresenter,
            new PrestaShop\PrestaShop\Adapter\Product\PriceFormatter()
        );

        $session = new CheckoutSession(
            $this->context,
            $deliveryOptionsFinder
        );

        return $session;
    }
    
    protected function postValidate()
    {
        $conf = unserialize(Configuration::get('EO_CONFIG'));
        $errors = array();
        foreach($this->module->eo_fields as $name => $field){
            $value = Tools::getValue($name);
            if(!empty($conf[$name.'_required']) && empty($value))
                $errors[] = sprintf($this->module->l('%s is empty'), $field['name']);
            if(Tools::isSubmit($name) && !empty($value) && !empty($field['validate']) && ($validate_method = $field['validate']) && method_exists('Validate', $validate_method) && !Validate::$validate_method($value))
                $errors[] = sprintf($this->module->l('%s is not valid'), $field['name']);
        }
        return $errors;
    }
    
    /*Avoid mistakes*/
    protected function getDefaultValue($field, $classname = null)
    {
        if(isset($this->defaults[$field]))
            return $this->defaults[$field];
        if(!empty($classname) && class_exists($classname) && method_exists($classname, 'getDefinition')){
            $def = $classname::getDefinition($classname);
            if(!empty($def['fields'][$field]['type'])){
                switch($def['fields'][$field]['type']){
                    case(ObjectModel::TYPE_INT):
                        return 0;
                    case(ObjectModel::TYPE_BOOL):
                        return false;
                    case(ObjectModel::TYPE_STRING):
                        return ' ';
                    case(ObjectModel::TYPE_DATE):
                        return date('d-m-Y');
                    default:
                        return false;
                }
            }
        }
        return false;
    }

    protected function updateCarriers()
    {

        if(Configuration::get('PW_EO_CARRIER') == PWExpressOrder::_CARRIER_ON_){
            $carriers = array();
            $availableCarriers = array();
            $allCarriers = Carrier::getCarriers($this->context->language->id, true, false, false, null, Carrier::ALL_CARRIERS);
            $this->context->controller->address = $this->address;
            
            $country = new Country($this->address->id_country);
            $delivery_option_list = $this->context->cart->getDeliveryOptionList(null, true);

            if(!empty($delivery_option_list) && is_array($delivery_option_list)){
                foreach (reset($delivery_option_list) as $key => $option) {
                    foreach ($option['carrier_list'] as $carrier) {
                        $availableCarriers[] = $carrier['instance']->id;
                    }
                }
            }
            foreach($allCarriers as $key => $carrier){
                $price = $this->context->cart->getPackageShippingCost((int)$carrier['id_carrier'], true, $country, null, $country->id_zone);

                $carrier['price'] = ($price == 0)?$this->module->l('Free'):Tools::displayPrice($price);
                $carrier['available'] = in_array($carrier['id_carrier'], $availableCarriers);
                if($carrier['available']){
                    $carriers[] = $carrier;
                }

            }
            $this->context->smarty->assign(array(
                'carriers'        => $carriers,
                'id_address'      => $this->address->id,
//                'delivery_option' => $this->context->cart->id_carrier
            ));
            if ($carriers && $this->address->id){
                die($this->context->smarty->fetch(_PS_MODULE_DIR_.$this->module->name.'/views/templates/hook/carriers.tpl'));
            } else die;
        }
    }
}