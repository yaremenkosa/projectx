<?php

class PWExpressOrderAjaxModuleFrontController extends ModuleFrontController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function initContent()
    {

        parent::initContent();

        $action = Tools::getValue('action');

        if($action == 'getCity'){
            return $this->getCity();
        }

        if($action == 'updateCarrierPayments'){
            return $this->updateCarrierPayments();
        }
        die('0');
    }

    public function getCity()
    {
        $q = Tools::getValue('q');
        $id_country = Tools::getValue('id_country');
        $id_state = Tools::getValue('id_state');
        $url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input='.urlencode($q).'&types=(cities)&language='.$this->context->language->iso_code.'&key='.Configuration::get('PW_GOOGLE_API_KEY');
        $ret = Tools::jsonDecode(file_get_contents($url), true);
        $cities = array();
        if($ret['status'] == 'OK'){
            $predictions = $ret['predictions'];
            foreach($predictions as $prediction){
                $cities[] = $prediction['structured_formatting']['main_text'];
            }
            die(implode("\n", $cities));
        }else{
            die('');
        }
    }

    public function updateCarrierPayments(){

        if ((Tools::isSubmit('delivery_option') || Tools::isSubmit('id_carrier'))) {
            $this->context->cart->setDeliveryOption(Tools::getValue('delivery_option'));

            $result = array('summary' => $this->context->cart->getSummaryDetails());

            foreach ($result['summary']['products'] as $key => &$product) {
                $product['quantity_without_customization'] = $product['quantity'];
                if ($result['customizedDatas']) {
                    if (isset($result['customizedDatas'][(int)$product['id_product']][(int)$product['id_product_attribute']])) {
                        foreach ($result['customizedDatas'][(int)$product['id_product']][(int)$product['id_product_attribute']] as $addresses) {
                            foreach ($addresses as $customization) {
                                $product['quantity_without_customization'] -= (int)$customization['quantity'];
                            }
                        }
                    }
                }
            }

            if ($result['customizedDatas']) {
                Product::addCustomizationPrice($result['summary']['products'], $result['customizedDatas']);
            }
            die(json_encode($result));
        }
    }
}