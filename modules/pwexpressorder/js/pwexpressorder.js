$(document).ready(function()
{
    if (pw_eo_mask) {
        $('input[name=eo_phone]').mask(pw_eo_mask);
        $('input[name=eo_mobilephone]').mask(pw_eo_mask);
    }

	$('.delivery_options table').click(function(){
		$('.delivery_option_radio i').removeClass('fa-check-square-o').addClass('fa-square-o');
		$(this).find('.delivery_option_radio input').prop('checked', true).prev().find('i').removeClass('fa-square-o').addClass('fa-check-square-o');
        var id_address = $('.delivery_option_radio:checked').data('id_address');
        var id_delivery_option = $('.delivery_option_radio:checked').data('key');
        updateExtraCarrier(id_delivery_option, id_address);
        
	});
    if($('#submitPwExpressOrder').length){
        $('.cart_navigation .standard-checkout').unbind('click').on('click', function(e){
            e.preventDefault();
            $('#submitPwExpressOrder').trigger('click');
        });
    }
    if(typeof pw_skip_payment != 'undefined' && pw_skip_payment){
        $('#checkout-payment-step').hide();
        $('[name=confirmDeliveryOption]').click(function(e){
            e.preventDefault();
            var sep = (pwexpressorder_uri.indexOf('?') == -1)?'?':'&';
            location.href = pwexpressorder_uri + sep + 'confirmOrder=1&id_carrier=' + parseInt($('#js-delivery').find('input:checked').val());
        });
        $('[name=processCarrier]').unbind('click').click(function(e){
            e.preventDefault();
            var sep = (pwexpressorder_uri.indexOf('?') == -1)?'?':'&';
            location.href = pwexpressorder_uri + sep + 'confirmOrder=1&id_carrier=' + parseInt($('.delivery_options').find('input:checked').val());
        });
    }
    if (pw_eo_mask) {
        $(document).on('focus', 'input[name=eo_phone], input[name=eo_mobilephone]', function(){
            $(this).mask(pw_eo_mask);
        });
    }

    updateCustomerAndAddresses();

});

$(function(){
	$(".pwexpressorder form").submit(function(){
		var okay = true;
		$(".pwexpressorder form input.is_required").each(function (i) {
			if($(this).val() == "0" || !$(this).val() || ($(this).attr('type') == 'checkbox' && !$(this).is(':checked'))){
				var use_delivery_address = 0;
				var is_delivery_address = $(this).parents('.delivery_address').length;
				if (is_delivery_address) {
					use_delivery_address = $(this).parents('.delivery_address').find('#use_delivery_address').val();
				}

				if (!is_delivery_address || (is_delivery_address && use_delivery_address != '0'))
				{
					okay = false;
					$(this).css('border', '1px solid red');
				}
			}
		});
        $(".pwexpressorder form select.is_required").each(function(i){
            if($(this).val()  == "" || $(this).val() == 0){
                okay = false;
            }
        });
		if(!okay || $('.form-error').length){
			alert(pw_validate_error);
			return false;
		}
	});

    $(document).on('click', '#carrier-select td', function(){
        $(this).closest('tr').find('input[type="radio"]').prop('checked', true);
        if(typeof $.uniform != "undefined") $.uniform.update();
        updateCarrierSelectionAndGift();
    })
});

$(function () {
	$('#pwexpressorder').on('click','#show_delivery_address',function () {
		$('.delivery_address').toggleClass('hidden');

		var use_delivery_address = $('#use_delivery_address');
		var use_delivery_address_val = use_delivery_address.val();

		if (use_delivery_address_val == '0') use_delivery_address.val(1);
		else use_delivery_address.val(0);
	});
});

$(function () {
	var form = $('form[name="carrier_area"]');

	$('.delivery_option_radio').change(function () {
		$.ajax({
			type: 'POST',
			url: pw_order_link+'?step=3',
			dataType: 'json',
			data: form.serialize() + '&processCarrier=1',
			success: function(data) {

			}
		});

		return false;
	});
});

// $(document).on('change', '#carrier-select .radio-select input', function(){
//     updateCarrierSelectionAndGift();
// });

function updateCarrierSelectionAndGift()
{
    var delivery_option_radio = $('#carrier-select .radio-select input');
    var delivery_option_params = '&';

    if (typeof delivery_option_radio != 'undefined'){
        $.each(delivery_option_radio, function(i) {
            if ($(this).prop('checked')){
                delivery_option_params += $(delivery_option_radio[i]).data('name') + '=' + $(delivery_option_radio[i]).val() + ',&';
            }
        });
    }

    if (delivery_option_params == '&') {
        delivery_option_params = '&delivery_option=&';
    }

    $.ajax({
        type: 'POST',
        url: baseUri,
        async: true,
        cache: false,
        dataType : "json",
        data: 'ajax=1&module=pwexpressorder&fc=module&controller=ajax&action=updateCarrierPayments&submitPwExpressOrderUpdate=1' + delivery_option_params + '&token=' + static_token ,
        success: function(jsndata)
        {
            updateCartSummary(jsndata.summary);
        },
    });
}

function updateCustomerAndAddresses(){
    $(document).on('submit', '#pwexpressorder_form', function(e) {
        if ($(document.activeElement).attr('id') == 'submitPwExpressOrderUpdate' || $(document.activeElement).attr('name') == 'eo_country'){
            e.preventDefault();
            var formData = $(e.target).serialize();
            formData += 'ajax=1&module=pwexpressorder&fc=module&controller=display&submitPwExpressOrderUpdate=1';
            $.ajax({
                url: baseUri,
                type: 'POST',
                data: formData,
                success: function (data) {
                    $('.carrier-select').remove();
                    $('section.form-fields').after(data);
                    updateCarrierSelectionAndGift();
                }
            });
        }
    });

    // $(document).on('change', "select[name=eo_country]", function () {
    //     $('#pwexpressorder_form').submit();
    // })
}