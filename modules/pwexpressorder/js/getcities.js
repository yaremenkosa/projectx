$(document).ready(function(){
    if($.browser == undefined) //small hack for autocomplete in new jQuery ver.
        $.browser = Array();
    $('body').on('focus', '#city', function(){
        $('#city').autocomplete(pw_ajax_link+'?action=getCity');
    });
    $('body').on('focus', '[name=eo_city]', function(){
        $('[name=eo_city]').autocomplete(pw_ajax_link+'?action=getCity');
    });
});