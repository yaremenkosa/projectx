<?php
use PrestaShop\PrestaShop\Adapter\StockManager;

require_once _PS_MODULE_DIR_.'pwexpressorder/classes/PWExpressOrderClass.php';
require_once 'func.php';
class PWExpressOrder extends PaymentModule
{
    const _PAYMENT_OFF_ = 0;
    const _PAYMENT_ON_ = 1;
    
    const _CARRIER_OFF_ = 0;
    const _CARRIER_ON_ = 1;
    const _CARRIER_ADV_ = 2;

    public $eo_fields = array();
    
    public $form_values = array();

    public function __construct()
    {
        $this->name = 'pwexpressorder';
        $this->tab = 'Admin';
        $this->version = '1.3.1';

        parent::__construct();

        $this->page = basename(__FILE__, '.php');
        $this->displayName = $this->l('Simple and quick ordering');
        $this->description = $this->l('Making a quick order');
        $this->author = 'PrestaWeb.ru';
        $this->bootstrap = true;
        $this->controllers = array('display', 'ajax');
        
        $countries = Country::getCountries($this->context->language->id);
        $states    = self::getDeliveryStates();
        
        $this->form_values = array(
            'customer' => new Customer(),
            'address'  => new Address(),
        );
        $this->eo_fields = array(
            'eo_fname' => array(
                'name' => $this->l('Name'),
                'type' => 'text',
                'placeholder' => $this->l('Enter your name'),
                'value' => &$this->form_values['customer']->firstname,
                'validate' => 'isName',
            ),
            'eo_lname' => array(
                'name' => $this->l('Last name'),
                'type' => 'text',
                'placeholder' => $this->l('Enter your last name'),
                'value' => &$this->form_values['customer']->lastname,
            ),
            'eo_email' => array(
                'name' => $this->l('E-mail'),
                'type' => 'email',
                'validate' => 'isEmail',
                'placeholder' => $this->l('Enter your e-mail'),
                'value' => &$this->form_values['customer']->email,
            ),
            'eo_country' => array(
                'name' => $this->l('Country'),
                'type' => 'countrySelect',
                'availableValues' => array_combine(array_keys($countries), array_column($countries, 'name')),
                'value' => &$this->form_values['address']->id_country,
            ),
            'eo_company' => array(
                'name' => $this->l('Organization'),
                'type' => 'text',
                'placeholder' => $this->l('Enter your organization'),
                'value' => &$this->form_values['address']->company,
            ),
            'eo_address' => array(
                'name' => $this->l('Address'),
                'type' => 'text',
                'placeholder' => $this->l('Enter your Address'),
                'value' => &$this->form_values['address']->address1,
                'validate' => 'isAddress',
            ),
            'eo_address2' => array(
                'name' => $this->l('Address (2)'),
                'type' => 'text',
                'value' => &$this->form_values['address']->address2,
                'validate' => 'isAddress',
            ),
            'eo_city' => array(
                'name' => $this->l('City'),
                'type' => 'text',
                'placeholder' => $this->l('Enter city'),
                'value' => &$this->form_values['address']->city,
                'validate' => 'isCityName',
            ),
            'eo_zip' => array(
                'name' => $this->l('ZIP'),
                'type' => 'text',
                'placeholder' => $this->l('Enter your postcode'),
                'value' => &$this->form_values['address']->postcode,
                'validate' => 'isPostCode',
            ),

            'eo_state' => array(
                'name' => $this->l('State'),
                'type' => 'select',
                'availableValues' => $states,
                'value' => &$this->form_values['address']->id_state,
            ),

            'eo_phone' => array(
                'name' => $this->l('Home phone'),
                'type' => 'text',
                'placeholder' => $this->l('Enter your phone number'),
                'value' => &$this->form_values['address']->phone,
                'validate' => 'isPhoneNumber',
            ),
            'eo_mobilephone' => array(
                'name' => $this->l('Mobile phone'),
                'type' => 'text',
                'placeholder' => $this->l('Enter your mobile phone'),
                'value' => &$this->form_values['address']->phone_mobile,
                'validate' => 'isPhoneNumber',
            ),

            'eo_password' => array(
                'name' => $this->l('Password'),
                'type' => 'password',
                'placeholder' => $this->l('Enter your password'),
                'value' => '',
            ),
            'eo_other' => array(
                'name' => $this->l('Additional information'),
                'type' => 'textarea',
                'value' => ''
            ),
        );

        $this->PWExpressOrderClass = new PWExpressOrderClass();
    }

    public function install()
    {
        if (!Configuration::get('CONF_'.strtoupper($this->name).'_FIXED')) {
            Configuration::updateValue('CONF_'.strtoupper($this->name).'_FIXED', '0.2');
        }
        if (!Configuration::get('CONF_'.strtoupper($this->name).'_VAR')) {
            Configuration::updateValue('CONF_'.strtoupper($this->name).'_VAR', '2');
        }
        if (!Configuration::get('CONF_'.strtoupper($this->name).'_FIXED_FOREIGN')) {
            Configuration::updateValue('CONF_'.strtoupper($this->name).'_FIXED_FOREIGN', '0.2');
        }
        if (!Configuration::get('CONF_'.strtoupper($this->name).'_VAR_FOREIGN')) {
            Configuration::updateValue('CONF_'.strtoupper($this->name).'_VAR_FOREIGN', '2');
        }

        
        if (!parent::install() OR !$this->registerHook('paymentReturn') OR !$this->registerHook('header') OR !$this->registerHook('displayPayment') OR !$this->registerHook('PaymentOptions'))
            return false;
        if (!$this->registerHook('shoppingCart') || Configuration::updateValue('eo_displayEmail', 0) == false ||
            Configuration::updateValue('eo_displayAgreement', 0) == false
        ) return false;

        /* Set default values (which fields to show) */
        $conf = Array();
        $conf['eo_fname_show'] = 1;
        $conf['eo_email_required'] = 1;
        $conf['eo_phone_show'] = 1;
        $conf['eo_phone_required'] = 1;
        $conf['eo_other_show'] = 1;
        Configuration::updateValue('PW_EO_PAYMENT', self::_PAYMENT_OFF_);
        Configuration::updateValue('PW_EO_CARRIER', self::_CARRIER_OFF_);
        Configuration::updateValue('PW_EO_MASK', '7(999) 999-9999');
        Configuration::updateValue('EO_CONFIG', serialize($conf));
        Configuration::updateValue('PS_CONDITIONS', false); // disable the terms of service
        Configuration::updateValue('PS_ORDER_PROCESS_TYPE', 0); //in 5 steps, not relevant for 1.7
        Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'required_field` WHERE 1 '); //Remove additional required fields to avoid conflicts
        $this->setZone();
    
        return true;
    }

    public function uninstall()
    {
        return parent::uninstall();
    }
    
    private function setZone(){
        $carriers = Carrier::getCarriers($this->context->language->id, true, false, false, null, Carrier::ALL_CARRIERS);
        $country = new Country(Configuration::get('PS_COUNTRY_DEFAULT'));
        $id_zone = $country->id_zone;
        if (!$id_zone) {
            return;
        }
        foreach($carriers as $carrier){
            if(!Carrier::checkCarrierZone($carrier['id_carrier'], (int)$id_zone)){
                Db::getInstance()->Execute('INSERT IGNORE INTO `'._DB_PREFIX_.'carrier_zone` (id_carrier, id_zone) VALUES ('.$carrier['id_carrier'].', '.(int)$id_zone.')');
                $delivery = new Delivery();
                $delivery->id_zone = (int)$id_zone;
                $delivery->id_carrier = $carrier['id_carrier'];
                $delivery->id_range_price = $delivery->id_range_weight = 1;
                $delivery->price = 0;
                $delivery->id_shop = $this->context->shop->id;
                $delivery->id_shop_group = $this->context->shop->id_shop_group;
                $delivery->save();
            }
        }
    }


    public function getContent()
    {
        $this->postProcess();
        return $this->renderForm();
    }
    
    protected function postProcess()
    {
        if (Tools::isSubmit('submit'.$this->name)) {
            $conf = array();
            foreach ($_POST as $k => $v) {
                if (substr($k, 0, 3) == 'eo_') {
                    $conf[$k.'_show']     = (int)((intval($v) == 1) || (intval($v) == 2));
                    $conf[$k.'_required'] = (int)(intval($v) == 2);
                }
            }
            Configuration::updateValue('EO_CONFIG', serialize($conf));
            Configuration::updateValue('PW_EO_CARRIER', Tools::getValue('PW_EO_CARRIER'));
            Configuration::updateValue('PW_EO_PAYMENT', Tools::getValue('PW_EO_PAYMENT'));
            Configuration::updateValue('PW_GOOGLE_API_KEY', Tools::getValue('PW_GOOGLE_API_KEY'));
            if (Validate::isPhoneNumber(Tools::getValue('PW_EO_MASK'))) {
                Configuration::updateValue('PW_EO_MASK', Tools::getValue('PW_EO_MASK'));
            } else {
                $this->context->controller->errors[] = $this->l('Mask is not valid');
            }
            $this->context->controller->confirmations[] = $this->l('Settings updated');
        }
    }
    
    protected function renderForm()
    {
        $fields = array();
        $default_lang = $this->context->language->id;
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = array(
            'save' => array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                '&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );
        $conf = unserialize(Configuration::get('EO_CONFIG'));
        $helper->fields_value = array(
            'PW_EO_PAYMENT' => (int)Configuration::get('PW_EO_PAYMENT'),
            'PW_EO_CARRIER' => (int)Configuration::get('PW_EO_CARRIER'),
            'PW_GOOGLE_API_KEY' => Configuration::get('PW_GOOGLE_API_KEY'),
            'PW_EO_MASK' => Configuration::get('PW_EO_MASK'),
        );
        foreach($this->eo_fields as $field => $params){
            $f = array(
                'type' => 'radio',
                'name' => $field,
                'label' => $params['name'],
                'values' => array(
                    array(
                        'id' => $field.'_empty',
                        'value' => 0,
                        'label' => $this->l('Hide'),
                    ),
                    array(
                        'id' => $field.'_show',
                        'value' => 1,
                        'label' => $this->l('Show'),
                    ),
                    array(
                        'id' => $field.'_required',
                        'value' => 2,
                        'label' => $this->l('Required'),
                    ),
                ),
            );
            if($field == 'eo_email') //Hide for e-mail option "Show"
                unset($f['values'][1]);
            $fields[] = $f;
            $helper->fields_value[$field] = 0;
            if(!empty($conf[$field.'_show']))
                $helper->fields_value[$field] = 1;
            if(!empty($conf[$field.'_required']))
                $helper->fields_value[$field] = 2;
        }
        $fields_form = array(
            'settings' => array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->l('Module settings'),
                    ),
                    'input' => array(
                        array(
                            'type' => 'radio',
                            'label' => $this->l('Delivery'),
                            'name' => 'PW_EO_CARRIER',
                            'values' => array(
                                array(
                                    'id' => 'PW_EO_CARRIER_OFF',
                                    'value' => self::_CARRIER_OFF_,
                                    'label' => $this->l('Hide selection'),
                                ),
                                array(
                                    'id' => 'PW_EO_CARRIER_ON',
                                    'value' => self::_CARRIER_ON_,
                                    'label' => $this->l('Show selection on order form'),
                                ),
                                array(
                                    'id' => 'PW_EO_CARRIER_ADV',
                                    'value' => self::_CARRIER_ADV_,
                                    'label' => $this->l('Show selection in a separate step'),
                                ),
                            ),
                        ),
                        array(
                            'type' => 'radio',
                            'label' => $this->l('Payment'),
                            'name' => 'PW_EO_PAYMENT',
                            'values' => array(
                                array(
                                    'id' => 'PW_EO_PAYMENT_OFF',
                                    'value' => self::_PAYMENT_OFF_,
                                    'label' => $this->l('Hide selection'),
                                ),
                                array(
                                    'id' => 'PW_EO_PAYMENT_ON',
                                    'value' => self::_PAYMENT_ON_,
                                    'label' => $this->l('Show selection in a separate step'),
                                ),
                            ),
                        ),
                        array(
                            'type' => 'text',
                            'label' => $this->l('Google API key'),
                            'name' => 'PW_GOOGLE_API_KEY',
                                'hint' => $this->l('Google API key, Google Maps Places Api for hint of the city'),
                        ),
                        array(
                            'type' => 'text',
                            'label' => $this->l('phone mask'),
                            'name' => 'PW_EO_MASK',
                            'hint' => $this->l('Empty for not required'),
                        ),
                    ),
                    'submit' => array(
                        'title' => $this->l('Save'),
                    ),
                ),
            ),
            'fields' => array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->l('Required field settings'),
                    ),
                    'input' => $fields,
                    'submit' => array(
                        'title' => $this->l('Save'),
                    ),
                ),
            )
        );
        return $helper->generateForm($fields_form);
    }


    public function hookHeader($params)
    {

        if($this->context->controller->php_self == 'order' || $this->context->controller->php_self == 'cart') {
            $this->context->controller->addCSS(($this->_path) . 'css/pwexpressorder.css', 'all');
            $this->context->controller->addJS(($this->_path) . 'js/pwexpressorder.js');
            $this->context->controller->addJS(__PS_BASE_URI__.'js/validate.js');
        }
        if(version_compare(_PS_VERSION_, '1.7', '>=')){
            $this->context->controller->addCSS(($this->_path) . 'css/pwexpressorder17.css', 'all');
        }
        if(Configuration::get('PW_GOOGLE_API_KEY')){
            $this->context->controller->addJqueryPlugin('autocomplete');
            $this->context->controller->addJS(($this->_path) . 'js/getcities.js');
        }
        if (Configuration::get('PW_EO_MASK')) {
            $this->context->controller->addJS(($this->_path) . 'js/jquery.maskedinput.min.js');
        }
        Media::addJsDef(array(
            'pwexpressorder_uri' => $this->context->link->getModuleLink('pwexpressorder', 'display'),
            'pw_ajax_link'       => $this->context->link->getModuleLink('pwexpressorder', 'ajax'),
            'pw_order_link'      => $this->context->link->getPageLink('order'),
            'pw_validate_error'  => $this->l('Fill all necessary fields
'),
            'pw_skip_payment'    => (Configuration::get('PW_EO_CARRIER') == self::_CARRIER_ADV_ && Configuration::get('PW_EO_PAYMENT') == self::_PAYMENT_OFF_),
            'pw_eo_mask'         => Configuration::get('PW_EO_MASK'),
        ));
    }

    function hookshoppingCart($params)
    {
        return $this->hookshoppingCartExtra($params);
    }

    public function hookshoppingCartExtra($params)
    {
        $this->prepareData();
        if(version_compare(_PS_VERSION_, '1.7', '>=')){
            return $this->display(__FILE__, 'form17.tpl');
        }
        return $this->display(__FILE__, 'form.tpl');
    }
    
    //payment page 1.7
    public function hookPaymentOptions($params)
    {
        return;
    }

    //payment page <1.7
    public function hookDisplayPayment($params)
    {
        return;
    }

    public function hookPaymentReturn($params)
    {
        if (!$this->active)
            return;
        return '<div class="success alert alert-success">' . $this->l('Your order is issued. During the working day, our manager will contact you. Thank!') . '</div>';
    }
    
     protected function prepareData()
     {
        $this->conf = unserialize(Configuration::get('EO_CONFIG'));
        $this->context->smarty->assign('conf', $this->conf);
        $form = array();
        if ($this->context->customer->isLogged()) {
            foreach(get_object_vars($this->context->customer) as $field => $value){
                $this->form_values['customer']->$field = $value;
            }
            if ($this->context->cart->id_address_delivery) {
                $address = get_object_vars(new Address($this->context->cart->id_address_delivery));
            } else {
                $address = PWExpressOrderClass::getLastAddress($this->context->customer->id);
            }
            foreach ($address as $field => $value){
                $this->form_values['address']->$field = $value;
            }
            $this->context->smarty->assign('address', $address);
            $this->context->smarty->assign('customer', $this->context->customer);
        }
        foreach($this->eo_fields as $key => $field){
            if(!empty($this->conf[$key.'_show']) || !empty($this->conf[$key.'_required'])){
                $ar = array(
                    'type' => $field['type'],
                    'label' => $field['name'],
                    'name' => $key,
                    'value' => $field['value'],
                    'availableValues' => array(
                        'placeholder' => @$field['placeholder'],
                    ),
                    'required' => !empty($this->conf[$key.'_required']),
                    'maxLength' => 255,
                    'errors' => array(),
                    'validate' => @$field['validate'],
                );
                if(isset($field['availableValues']) && is_array($field['availableValues']))
                    $ar['availableValues'] = $field['availableValues'];
                $form[] = $ar;
            }
        }
        $this->context->smarty->assign(array(
            'formFields' => $form,
        ));
        
        $errors = Array();

        $back = Tools::getValue('back');
        if (!empty($back)) {
            $this->context->smarty->assign('back', Tools::safeOutput($back));
        }

         //if ($this->context->customer->isLogged()) {
        $this->_assignCarriers();
       //  }

        $hideForm = $this->_checkCartAndHideForm($errors);
        $this->context->smarty->assign('hideForm', $hideForm);
        
        if(!empty($this->context->cookie->pw_errors)){
            $errors = array_merge($errors, explode(';;', $this->context->cookie->pw_errors));
            unset($this->context->cookie->pw_errors);
        }
        $this->context->smarty->assign(
            Array(
                'pwexpressorder_uri' => $this->context->link->getModuleLink('pwexpressorder', 'display'),
                'errors' => $errors
            )
        );
    }
    
    protected function _checkCartAndHideForm(&$errors)
    {
        $orderTotalDefaultCurrency = Tools::convertPrice($this->context->cart->getOrderTotal(true, Cart::BOTH), Currency::getCurrency((int)Configuration::get('PS_CURRENCY_DEFAULT')));
        $minimalPurchase = (float)Configuration::get('PS_PURCHASE_MINIMUM');
        if ($orderTotalDefaultCurrency < $minimalPurchase) {
            $errors[] = Tools::displayError($this->l('The total amount of the order must be at least
 ')).' '.Tools::displayPrice($minimalPurchase, Currency::getCurrency((int)$this->context->cart->id_currency));
            return true;
        }
        if (count($this->context->cart->getProducts())== 0) {
//            $errors[] = Tools::displayError($this->l('Your cart is empty'));
            return true;
        }
        return false;
    }
    
    protected function _assignCarriers()
    {
        if(Configuration::get('PW_EO_CARRIER') == self::_CARRIER_ON_){
            $carriers = array();
            $availableCarriers = array();
            $allCarriers = Carrier::getCarriers($this->context->language->id, true, false, false, null, Carrier::ALL_CARRIERS); //all deliveries

            $country = new Country($this->form_values['address']->id_country);
            $delivery_option_list = $this->context->cart->getDeliveryOptionList(null, true);
            if(!empty($delivery_option_list) && is_array($delivery_option_list)){
                foreach (reset($delivery_option_list) as $key => $option) {
                    foreach ($option['carrier_list'] as $carrier) {
                        $price = $this->context->cart->getPackageShippingCost((int)$carrier['instance']->id, true, null, null, $country->id_zone);
                        $availableCarriers[] = $carrier['instance']->id; //deliveries available for current address

                    }
                }
            }
            foreach($allCarriers as $carrier){
                $price = $this->context->cart->getPackageShippingCost((int)$carrier['id_carrier'], true, null, null, $country->id_zone);
                $carrier['price'] = ($price == 0)?$this->l('Free'):Tools::displayPrice($price);
                $carrier['available'] = in_array($carrier['id_carrier'], $availableCarriers);
                if($carrier['available']){
                    $carriers[] = $carrier;
                }
            }
            //this is all done so that you can choose a delivery method for a non-existing address
            $this->context->smarty->assign(array(
                'carriers'        => $carriers,
                'id_address'      => $this->form_values['address']->id,
                'delivery_option' => $this->context->cart->id_carrier
            ));
        }
    }
    
    public function validateOrder($id_cart, $id_order_state, $amount_paid, $payment_method = 'Unknown',
        $message = null, $extra_vars = array(), $currency_special = null, $dont_touch_amount = false,
        $secure_key = false, Shop $shop = null)
    {
        
        if(version_compare(_PS_VERSION_, '1.7', '>=')){
            $cart = Context::getContext()->cart;
            if (!empty($cart)) {
                $cart->getPackageList(true);
                $cart->getDeliveryOptionList(null, true);
            }


            if (self::DEBUG_MODE) {
                PrestaShopLogger::addLog('PaymentModule::validateOrder - Function called', 1, null, 'Cart', (int)$id_cart, true);
            }

            if (!isset($this->context)) {
                $this->context = Context::getContext();
            }
            $this->context->cart = new Cart((int)$id_cart);
            $this->context->customer = new Customer((int)$this->context->cart->id_customer);
            // The tax cart is loaded before the customer so re-cache the tax calculation method
            $this->context->cart->setTaxCalculationMethod();

            $this->context->language = new Language((int)$this->context->cart->id_lang);
            $this->context->shop = ($shop ? $shop : new Shop((int)$this->context->cart->id_shop));
            ShopUrl::resetMainDomainCache();
            $id_currency = $currency_special ? (int)$currency_special : (int)$this->context->cart->id_currency;
            $this->context->currency = new Currency((int)$id_currency, null, (int)$this->context->shop->id);
            if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery') {
                $context_country = $this->context->country;
            }

            $order_status = new OrderState((int)$id_order_state, (int)$this->context->language->id);
            if (!Validate::isLoadedObject($order_status)) {
                PrestaShopLogger::addLog('PaymentModule::validateOrder - Order Status cannot be loaded', 3, null, 'Cart', (int)$id_cart, true);
                throw new PrestaShopException('Can\'t load Order status');
            }

            if (!$this->active) {
                PrestaShopLogger::addLog('PaymentModule::validateOrder - Module is not active', 3, null, 'Cart', (int)$id_cart, true);
                die(Tools::displayError());
            }

            // Does order already exists ?
            if (Validate::isLoadedObject($this->context->cart) && $this->context->cart->OrderExists() == false) {
                if ($secure_key !== false && $secure_key != $this->context->cart->secure_key) {
                    PrestaShopLogger::addLog('PaymentModule::validateOrder - Secure key does not match', 3, null, 'Cart', (int)$id_cart, true);
                    die(Tools::displayError());
                }

                // For each package, generate an order
                $delivery_option_list = $this->context->cart->getDeliveryOptionList();
                $package_list = $this->context->cart->getPackageList();
                $cart_delivery_option = $this->context->cart->getDeliveryOption(null, false, false);

                // If some delivery options are not defined, or not valid, use the first valid option
                foreach ($delivery_option_list as $id_address => $package) {
                    if (!isset($cart_delivery_option[$id_address]) || !array_key_exists($cart_delivery_option[$id_address], $package)) {
                        foreach ($package as $key => $val) {
                            $cart_delivery_option[$id_address] = $key;
                            break;
                        }
                    }
                }

                $order_list = array();
                $order_detail_list = array();

                do {
                    $reference = Order::generateReference();
                } while (Order::getByReference($reference)->count());

                $this->currentOrderReference = $reference;

                $cart_total_paid = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(true, Cart::BOTH), 2);

                foreach ($cart_delivery_option as $id_address => $key_carriers) {
                    foreach ($delivery_option_list[$id_address][$key_carriers]['carrier_list'] as $id_carrier => $data) {
                        foreach ($data['package_list'] as $id_package) {
                            // Rewrite the id_warehouse
                            $package_list[$id_address][$id_package]['id_warehouse'] = (int)$this->context->cart->getPackageIdWarehouse($package_list[$id_address][$id_package], (int)$id_carrier);
                            $package_list[$id_address][$id_package]['id_carrier'] = $id_carrier;
                        }
                    }
                }
                // Make sure CartRule caches are empty
                CartRule::cleanCache();
                $cart_rules = $this->context->cart->getCartRules();
                foreach ($cart_rules as $cart_rule) {
                    if (($rule = new CartRule((int)$cart_rule['obj']->id)) && Validate::isLoadedObject($rule)) {
                        if ($error = $rule->checkValidity($this->context, true, true)) {
                            $this->context->cart->removeCartRule((int)$rule->id);
                            if (isset($this->context->cookie) && isset($this->context->cookie->id_customer) && $this->context->cookie->id_customer && !empty($rule->code)) {
                                Tools::redirect('index.php?controller=order&submitAddDiscount=1&discount_name='.urlencode($rule->code));
                            } else {
                                $rule_name = isset($rule->name[(int)$this->context->cart->id_lang]) ? $rule->name[(int)$this->context->cart->id_lang] : $rule->code;
                                $error = $this->trans('The cart rule named "%1s" (ID %2s) used in this cart is not valid and has been withdrawn from cart', array($rule_name, (int)$rule->id), 'Admin.Payment.Notification');
                                PrestaShopLogger::addLog($error, 3, '0000002', 'Cart', (int)$this->context->cart->id);
                            }
                        }
                    }
                }

                foreach ($package_list as $id_address => $packageByAddress) {
                    foreach ($packageByAddress as $id_package => $package) {
                        /** @var Order $order */
                        $order = new Order();
                        $order->product_list = $package['product_list'];

                        if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery') {
                            $address = new Address((int)$id_address);
                            $this->context->country = new Country((int)$address->id_country, (int)$this->context->cart->id_lang);
                            if (!$this->context->country->active) {
                                throw new PrestaShopException('The delivery address country is not active.');
                            }
                        }

                        $carrier = null;
                        if (!$this->context->cart->isVirtualCart() && isset($package['id_carrier'])) {
                            $carrier = new Carrier((int)$package['id_carrier'], (int)$this->context->cart->id_lang);
                            $order->id_carrier = (int)$carrier->id;
                            $id_carrier = (int)$carrier->id;
                        } else {
                            $order->id_carrier = 0;
                            $id_carrier = 0;
                        }

                        $order->id_customer = (int)$this->context->cart->id_customer;
                        $order->id_address_invoice = (int)$this->context->cart->id_address_invoice;
                        $order->id_address_delivery = (int)$id_address;
                        $order->id_currency = $this->context->currency->id;
                        $order->id_lang = (int)$this->context->cart->id_lang;
                        $order->id_cart = (int)$this->context->cart->id;
                        $order->reference = $reference;
                        $order->id_shop = (int)$this->context->shop->id;
                        $order->id_shop_group = (int)$this->context->shop->id_shop_group;

                        $order->secure_key = ($secure_key ? pSQL($secure_key) : pSQL($this->context->customer->secure_key));
                        $order->payment = $payment_method;
                        if (isset($this->name)) {
                            $order->module = $this->name;
                        }
                        $order->recyclable = $this->context->cart->recyclable;
                        $order->gift = (int)$this->context->cart->gift;
                        $order->gift_message = $this->context->cart->gift_message;
                        $order->mobile_theme = $this->context->cart->mobile_theme;
                        $order->conversion_rate = $this->context->currency->conversion_rate;
                        $amount_paid = !$dont_touch_amount ? Tools::ps_round((float)$amount_paid, 2) : $amount_paid;
                        $order->total_paid_real = 0;

                        $order->total_products = (float)$this->context->cart->getOrderTotal(false, Cart::ONLY_PRODUCTS, $order->product_list, $id_carrier);
                        $order->total_products_wt = (float)$this->context->cart->getOrderTotal(true, Cart::ONLY_PRODUCTS, $order->product_list, $id_carrier);
                        $order->total_discounts_tax_excl = (float)abs($this->context->cart->getOrderTotal(false, Cart::ONLY_DISCOUNTS, $order->product_list, $id_carrier));
                        $order->total_discounts_tax_incl = (float)abs($this->context->cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS, $order->product_list, $id_carrier));
                        $order->total_discounts = $order->total_discounts_tax_incl;

                        $order->total_shipping_tax_excl = (float)$this->context->cart->getPackageShippingCost((int)$id_carrier, false, null, $order->product_list);
                        $order->total_shipping_tax_incl = (float)$this->context->cart->getPackageShippingCost((int)$id_carrier, true, null, $order->product_list);
                        $order->total_shipping = $order->total_shipping_tax_incl;

                        if (!is_null($carrier) && Validate::isLoadedObject($carrier)) {
                            $order->carrier_tax_rate = $carrier->getTaxesRate(new Address((int)$this->context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')}));
                        }

                        $order->total_wrapping_tax_excl = (float)abs($this->context->cart->getOrderTotal(false, Cart::ONLY_WRAPPING, $order->product_list, $id_carrier));
                        $order->total_wrapping_tax_incl = (float)abs($this->context->cart->getOrderTotal(true, Cart::ONLY_WRAPPING, $order->product_list, $id_carrier));
                        $order->total_wrapping = $order->total_wrapping_tax_incl;

                        $order->total_paid_tax_excl = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(false, Cart::BOTH, $order->product_list, $id_carrier), _PS_PRICE_COMPUTE_PRECISION_);
                        $order->total_paid_tax_incl = (float)Tools::ps_round((float)$this->context->cart->getOrderTotal(true, Cart::BOTH, $order->product_list, $id_carrier), _PS_PRICE_COMPUTE_PRECISION_);
                        $order->total_paid = $order->total_paid_tax_incl;
                        $order->round_mode = Configuration::get('PS_PRICE_ROUND_MODE');
                        $order->round_type = Configuration::get('PS_ROUND_TYPE');

                        $order->invoice_date = '0000-00-00 00:00:00';
                        $order->delivery_date = '0000-00-00 00:00:00';

                        if (self::DEBUG_MODE) {
                            PrestaShopLogger::addLog('PaymentModule::validateOrder - Order is about to be added', 1, null, 'Cart', (int)$id_cart, true);
                        }

                        // Creating order
                        $result = $order->add();

                        if (!$result) {
                            PrestaShopLogger::addLog('PaymentModule::validateOrder - Order cannot be created', 3, null, 'Cart', (int)$id_cart, true);
                            throw new PrestaShopException('Can\'t save Order');
                        }

                        // Amount paid by customer is not the right one -> Status = payment error
                        // We don't use the following condition to avoid the float precision issues : http://www.php.net/manual/en/language.types.float.php
                        // if ($order->total_paid != $order->total_paid_real)
                        // We use number_format in order to compare two string
                        if ($order_status->logable && number_format($cart_total_paid, _PS_PRICE_COMPUTE_PRECISION_) != number_format($amount_paid, _PS_PRICE_COMPUTE_PRECISION_)) {
                            $id_order_state = Configuration::get('PS_OS_ERROR');
                        }

                        $order_list[] = $order;

                        if (self::DEBUG_MODE) {
                            PrestaShopLogger::addLog('PaymentModule::validateOrder - OrderDetail is about to be added', 1, null, 'Cart', (int)$id_cart, true);
                        }

                        // Insert new Order detail list using cart for the current order
                        $order_detail = new OrderDetail(null, null, $this->context);
                        $order_detail->createList($order, $this->context->cart, $id_order_state, $order->product_list, 0, true, $package_list[$id_address][$id_package]['id_warehouse']);
                        $order_detail_list[] = $order_detail;

                        if (self::DEBUG_MODE) {
                            PrestaShopLogger::addLog('PaymentModule::validateOrder - OrderCarrier is about to be added', 1, null, 'Cart', (int)$id_cart, true);
                        }

                        // Adding an entry in order_carrier table
                        if (!is_null($carrier)) {
                            $order_carrier = new OrderCarrier();
                            $order_carrier->id_order = (int)$order->id;
                            $order_carrier->id_carrier = (int)$id_carrier;
                            $order_carrier->weight = (float)$order->getTotalWeight();
                            $order_carrier->shipping_cost_tax_excl = (float)$order->total_shipping_tax_excl;
                            $order_carrier->shipping_cost_tax_incl = (float)$order->total_shipping_tax_incl;
                            $order_carrier->add();
                        }
                    }
                }

                // The country can only change if the address used for the calculation is the delivery address, and if multi-shipping is activated
                if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_delivery') {
                    $this->context->country = $context_country;
                }

                if (!$this->context->country->active) {
                    PrestaShopLogger::addLog('PaymentModule::validateOrder - Country is not active', 3, null, 'Cart', (int)$id_cart, true);
                    throw new PrestaShopException('The order address country is not active.');
                }

                if (self::DEBUG_MODE) {
                    PrestaShopLogger::addLog('PaymentModule::validateOrder - Payment is about to be added', 1, null, 'Cart', (int)$id_cart, true);
                }

                // Register Payment only if the order status validate the order
                if ($order_status->logable) {
                    // $order is the last order loop in the foreach
                    // The method addOrderPayment of the class Order make a create a paymentOrder
                    // linked to the order reference and not to the order id
                    if (isset($extra_vars['transaction_id'])) {
                        $transaction_id = $extra_vars['transaction_id'];
                    } else {
                        $transaction_id = null;
                    }

                    if (!$order->addOrderPayment($amount_paid, null, $transaction_id)) {
                        PrestaShopLogger::addLog('PaymentModule::validateOrder - Cannot save Order Payment', 3, null, 'Cart', (int)$id_cart, true);
                        throw new PrestaShopException('Can\'t save Order Payment');
                    }
                }

                // Next !
                $only_one_gift = false;
                $cart_rule_used = array();
                $products = $this->context->cart->getProducts();

                // Make sure CartRule caches are empty
                CartRule::cleanCache();
                foreach ($order_detail_list as $key => $order_detail) {
                    /** @var OrderDetail $order_detail */

                    $order = $order_list[$key];
                    if (isset($order->id)) {
                        if (!$secure_key) {
                            $message .= '<br />'.$this->trans('Warning: the secure key is empty, check your payment account before validation', array(), 'Admin.Payment.Notification');
                        }
                        // Optional message to attach to this order
                        if (isset($message) & !empty($message)) {
                            $msg = new Message();
                            $message = strip_tags($message, '<br>');
                            if (Validate::isCleanHtml($message)) {
                                if (self::DEBUG_MODE) {
                                    PrestaShopLogger::addLog('PaymentModule::validateOrder - Message is about to be added', 1, null, 'Cart', (int)$id_cart, true);
                                }
                                $msg->message = $message;
                                $msg->id_cart = (int)$id_cart;
                                $msg->id_customer = (int)($order->id_customer);
                                $msg->id_order = (int)$order->id;
                                $msg->private = 1;
                                $msg->add();
                            }
                        }

                        // Insert new Order detail list using cart for the current order
                        //$orderDetail = new OrderDetail(null, null, $this->context);
                        //$orderDetail->createList($order, $this->context->cart, $id_order_state);

                        // Construct order detail table for the email
                        $products_list = '';
                        $virtual_product = true;

                        $product_var_tpl_list = array();
                        foreach ($order->product_list as $product) {
                            $price = Product::getPriceStatic((int)$product['id_product'], false, ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null), 6, null, false, true, $product['cart_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}, $specific_price, true, true, null, true, $product['id_customization']);
                            $price_wt = Product::getPriceStatic((int)$product['id_product'], true, ($product['id_product_attribute'] ? (int)$product['id_product_attribute'] : null), 2, null, false, true, $product['cart_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}, $specific_price, true, true, null, true, $product['id_customization']);

                            $product_price = Product::getTaxCalculationMethod() == PS_TAX_EXC ? Tools::ps_round($price, 2) : $price_wt;

                            $product_var_tpl = array(
                                'id_product' => $product['id_product'],
                                'reference' => $product['reference'],
                                'name' => $product['name'].(isset($product['attributes']) ? ' - '.$product['attributes'] : ''),
                                'price' => Tools::displayPrice($product_price * $product['quantity'], $this->context->currency, false),
                                'quantity' => $product['quantity'],
                                'customization' => array()
                            );

                            if (isset($product['price']) && $product['price']) {
                                $product_var_tpl['unit_price'] = Tools::displayPrice($product['price'], $this->context->currency, false);
                                $product_var_tpl['unit_price_full'] = Tools::displayPrice($product['price'], $this->context->currency, false)
                                    .' '.$product['unity'];
                            } else {
                                $product_var_tpl['unit_price'] = $product_var_tpl['unit_price_full'] = '';
                            }

                            $customized_datas = Product::getAllCustomizedDatas((int)$order->id_cart, null, true, null, (int)$product['id_customization']);
                            if (isset($customized_datas[$product['id_product']][$product['id_product_attribute']])) {
                                $product_var_tpl['customization'] = array();
                                foreach ($customized_datas[$product['id_product']][$product['id_product_attribute']][$order->id_address_delivery] as $customization) {
                                    $customization_text = '';
                                    if (isset($customization['datas'][Product::CUSTOMIZE_TEXTFIELD])) {
                                        foreach ($customization['datas'][Product::CUSTOMIZE_TEXTFIELD] as $text) {
                                            $customization_text .= '<strong>'.$text['name'].'</strong>: '.$text['value'].'<br />';
                                        }
                                    }

                                    if (isset($customization['datas'][Product::CUSTOMIZE_FILE])) {
                                        $customization_text .= $this->trans('%d image(s)', array(count($customization['datas'][Product::CUSTOMIZE_FILE])), 'Admin.Payment.Notification').'<br />';
                                    }

                                    $customization_quantity = (int)$customization['quantity'];

                                    $product_var_tpl['customization'][] = array(
                                        'customization_text' => $customization_text,
                                        'customization_quantity' => $customization_quantity,
                                        'quantity' => Tools::displayPrice($customization_quantity * $product_price, $this->context->currency, false)
                                    );
                                }
                            }

                            $product_var_tpl_list[] = $product_var_tpl;
                            // Check if is not a virutal product for the displaying of shipping
                            if (!$product['is_virtual']) {
                                $virtual_product &= false;
                            }
                        } // end foreach ($products)

                        $product_list_txt = '';
                        $product_list_html = '';
                        if (count($product_var_tpl_list) > 0) {
                            $product_list_txt = $this->getEmailTemplateContent('order_conf_product_list.txt', Mail::TYPE_TEXT, $product_var_tpl_list);
                            $product_list_html = $this->getEmailTemplateContent('order_conf_product_list.tpl', Mail::TYPE_HTML, $product_var_tpl_list);
                        }

                        $cart_rules_list = array();
                        $total_reduction_value_ti = 0;
                        $total_reduction_value_tex = 0;
                        foreach ($cart_rules as $cart_rule) {
                            $package = array('id_carrier' => $order->id_carrier, 'id_address' => $order->id_address_delivery, 'products' => $order->product_list);
                            $values = array(
                                'tax_incl' => $cart_rule['obj']->getContextualValue(true, $this->context, CartRule::FILTER_ACTION_ALL_NOCAP, $package),
                                'tax_excl' => $cart_rule['obj']->getContextualValue(false, $this->context, CartRule::FILTER_ACTION_ALL_NOCAP, $package)
                            );

                            // If the reduction is not applicable to this order, then continue with the next one
                            if (!$values['tax_excl']) {
                                continue;
                            }

                            // IF
                            //  This is not multi-shipping
                            //  The value of the voucher is greater than the total of the order
                            //  Partial use is allowed
                            //  This is an "amount" reduction, not a reduction in % or a gift
                            // THEN
                            //  The voucher is cloned with a new value corresponding to the remainder
                            if (count($order_list) == 1 && $values['tax_incl'] > ($order->total_products_wt - $total_reduction_value_ti) && $cart_rule['obj']->partial_use == 1 && $cart_rule['obj']->reduction_amount > 0) {
                                // Create a new voucher from the original
                                $voucher = new CartRule((int)$cart_rule['obj']->id); // We need to instantiate the CartRule without lang parameter to allow saving it
                                unset($voucher->id);

                                // Set a new voucher code
                                $voucher->code = empty($voucher->code) ? substr(md5($order->id.'-'.$order->id_customer.'-'.$cart_rule['obj']->id), 0, 16) : $voucher->code.'-2';
                                if (preg_match('/\-([0-9]{1,2})\-([0-9]{1,2})$/', $voucher->code, $matches) && $matches[1] == $matches[2]) {
                                    $voucher->code = preg_replace('/'.$matches[0].'$/', '-'.(intval($matches[1]) + 1), $voucher->code);
                                }

                                // Set the new voucher value
                                if ($voucher->reduction_tax) {
                                    $voucher->reduction_amount = ($total_reduction_value_ti + $values['tax_incl']) - $order->total_products_wt;

                                    // Add total shipping amout only if reduction amount > total shipping
                                    if ($voucher->free_shipping == 1 && $voucher->reduction_amount >= $order->total_shipping_tax_incl) {
                                        $voucher->reduction_amount -= $order->total_shipping_tax_incl;
                                    }
                                } else {
                                    $voucher->reduction_amount = ($total_reduction_value_tex + $values['tax_excl']) - $order->total_products;

                                    // Add total shipping amout only if reduction amount > total shipping
                                    if ($voucher->free_shipping == 1 && $voucher->reduction_amount >= $order->total_shipping_tax_excl) {
                                        $voucher->reduction_amount -= $order->total_shipping_tax_excl;
                                    }
                                }
                                if ($voucher->reduction_amount <= 0) {
                                    continue;
                                }

                                if ($this->context->customer->isGuest()) {
                                    $voucher->id_customer = 0;
                                } else {
                                    $voucher->id_customer = $order->id_customer;
                                }

                                $voucher->quantity = 1;
                                $voucher->reduction_currency = $order->id_currency;
                                $voucher->quantity_per_user = 1;
                                if ($voucher->add()) {
                                    // If the voucher has conditions, they are now copied to the new voucher
                                    CartRule::copyConditions($cart_rule['obj']->id, $voucher->id);
                                    $orderLanguage = new Language((int) $order->id_lang);

                                    $params = array(
                                        '{voucher_amount}' => Tools::displayPrice($voucher->reduction_amount, $this->context->currency, false),
                                        '{voucher_num}' => $voucher->code,
                                        '{firstname}' => $this->context->customer->firstname,
                                        '{lastname}' => $this->context->customer->lastname,
                                        '{id_order}' => $order->reference,
                                        '{order_name}' => $order->getUniqReference()
                                    );
                                    Mail::Send(
                                        (int)$order->id_lang,
                                        'voucher',
                                        Context::getContext()->getTranslator()->trans(
                                            'New voucher for your order %s',
                                            array($order->reference),
                                            'Emails.Subject',
                                            $orderLanguage->locale
                                        ),
                                        $params,
                                        $this->context->customer->email,
                                        $this->context->customer->firstname.' '.$this->context->customer->lastname,
                                        null, null, null, null, _PS_MAIL_DIR_, false, (int)$order->id_shop
                                    );
                                }

                                $values['tax_incl'] = $order->total_products_wt - $total_reduction_value_ti;
                                $values['tax_excl'] = $order->total_products - $total_reduction_value_tex;
                                if (1 == $voucher->free_shipping) {
                                    $values['tax_incl'] += $order->total_shipping_tax_incl;
                                    $values['tax_excl'] += $order->total_shipping_tax_excl;
                                }
                            }
                            $total_reduction_value_ti += $values['tax_incl'];
                            $total_reduction_value_tex += $values['tax_excl'];

                            $order->addCartRule($cart_rule['obj']->id, $cart_rule['obj']->name, $values, 0, $cart_rule['obj']->free_shipping);

                            if ($id_order_state != Configuration::get('PS_OS_ERROR') && $id_order_state != Configuration::get('PS_OS_CANCELED') && !in_array($cart_rule['obj']->id, $cart_rule_used)) {
                                $cart_rule_used[] = $cart_rule['obj']->id;

                                // Create a new instance of Cart Rule without id_lang, in order to update its quantity
                                $cart_rule_to_update = new CartRule((int)$cart_rule['obj']->id);
                                $cart_rule_to_update->quantity = max(0, $cart_rule_to_update->quantity - 1);
                                $cart_rule_to_update->update();
                            }

                            $cart_rules_list[] = array(
                                'voucher_name' => $cart_rule['obj']->name,
                                'voucher_reduction' => ($values['tax_incl'] != 0.00 ? '-' : '').Tools::displayPrice($values['tax_incl'], $this->context->currency, false)
                            );
                        }

                        $cart_rules_list_txt = '';
                        $cart_rules_list_html = '';
                        if (count($cart_rules_list) > 0) {
                            $cart_rules_list_txt = $this->getEmailTemplateContent('order_conf_cart_rules.txt', Mail::TYPE_TEXT, $cart_rules_list);
                            $cart_rules_list_html = $this->getEmailTemplateContent('order_conf_cart_rules.tpl', Mail::TYPE_HTML, $cart_rules_list);
                        }

                        // Specify order id for message
                        $old_message = Message::getMessageByCartId((int)$this->context->cart->id);
                        if ($old_message && !$old_message['private']) {
                            $update_message = new Message((int)$old_message['id_message']);
                            $update_message->id_order = (int)$order->id;
                            $update_message->update();

                            // Add this message in the customer thread
                            $customer_thread = new CustomerThread();
                            $customer_thread->id_contact = 0;
                            $customer_thread->id_customer = (int)$order->id_customer;
                            $customer_thread->id_shop = (int)$this->context->shop->id;
                            $customer_thread->id_order = (int)$order->id;
                            $customer_thread->id_lang = (int)$this->context->language->id;
                            $customer_thread->email = $this->context->customer->email;
                            $customer_thread->status = 'open';
                            $customer_thread->token = Tools::passwdGen(12);
                            $customer_thread->add();

                            $customer_message = new CustomerMessage();
                            $customer_message->id_customer_thread = $customer_thread->id;
                            $customer_message->id_employee = 0;
                            $customer_message->message = $update_message->message;
                            $customer_message->private = 1;

                            if (!$customer_message->add()) {
                                $this->errors[] = $this->trans('An error occurred while saving message', array(), 'Admin.Payment.Notification');
                            }
                        }

                        if (self::DEBUG_MODE) {
                            PrestaShopLogger::addLog('PaymentModule::validateOrder - Hook validateOrder is about to be called', 1, null, 'Cart', (int)$id_cart, true);
                        }

                        // Hook validate order
                        Hook::exec('actionValidateOrder', array(
                            'cart' => $this->context->cart,
                            'order' => $order,
                            'customer' => $this->context->customer,
                            'currency' => $this->context->currency,
                            'orderStatus' => $order_status
                        ));

                        foreach ($this->context->cart->getProducts() as $product) {
                            if ($order_status->logable) {
                                ProductSale::addProductSale((int)$product['id_product'], (int)$product['cart_quantity']);
                            }
                        }

                        if (self::DEBUG_MODE) {
                            PrestaShopLogger::addLog('PaymentModule::validateOrder - Order Status is about to be added', 1, null, 'Cart', (int)$id_cart, true);
                        }

                        // Set the order status
                        $new_history = new OrderHistory();
                        $new_history->id_order = (int)$order->id;
                        $new_history->changeIdOrderState((int)$id_order_state, $order, true);
                        $new_history->addWithemail(true, $extra_vars);

                        // Switch to back order if needed
                        if (Configuration::get('PS_STOCK_MANAGEMENT') && ($order_detail->getStockState() || $order_detail->product_quantity_in_stock <= 0)) {
                            $history = new OrderHistory();
                            $history->id_order = (int)$order->id;
                            $history->changeIdOrderState(Configuration::get($order->valid ? 'PS_OS_OUTOFSTOCK_PAID' : 'PS_OS_OUTOFSTOCK_UNPAID'), $order, true);
                            $history->addWithemail();
                        }

                        unset($order_detail);

                        // Order is reloaded because the status just changed
                        $order = new Order((int)$order->id);

                        // Send an e-mail to customer (one order = one email)
                        if ($id_order_state != Configuration::get('PS_OS_ERROR') && $id_order_state != Configuration::get('PS_OS_CANCELED') && $this->context->customer->id) {
                            $invoice = new Address((int)$order->id_address_invoice);
                            $delivery = new Address((int)$order->id_address_delivery);
                            $delivery_state = $delivery->id_state ? new State((int)$delivery->id_state) : false;
                            $invoice_state = $invoice->id_state ? new State((int)$invoice->id_state) : false;

                            $data = array(
                                '{firstname}' => $this->context->customer->firstname,
                                '{lastname}' => $this->context->customer->lastname,
                                '{email}' => $this->context->customer->email,
                                '{delivery_block_txt}' => $this->_getFormatedAddress($delivery, "\n"),
                                '{invoice_block_txt}' => $this->_getFormatedAddress($invoice, "\n"),
                                '{delivery_block_html}' => $this->_getFormatedAddress($delivery, '<br />', array(
                                    'firstname'    => '<span style="font-weight:bold;">%s</span>',
                                    'lastname'    => '<span style="font-weight:bold;">%s</span>'
                                )),
                                '{invoice_block_html}' => $this->_getFormatedAddress($invoice, '<br />', array(
                                    'firstname'    => '<span style="font-weight:bold;">%s</span>',
                                    'lastname'    => '<span style="font-weight:bold;">%s</span>'
                                )),
                                '{delivery_company}' => $delivery->company,
                                '{delivery_firstname}' => $delivery->firstname,
                                '{delivery_lastname}' => $delivery->lastname,
                                '{delivery_address1}' => $delivery->address1,
                                '{delivery_address2}' => $delivery->address2,
                                '{delivery_city}' => $delivery->city,
                                '{delivery_postal_code}' => $delivery->postcode,
                                '{delivery_country}' => $delivery->country,
                                '{delivery_state}' => $delivery->id_state ? $delivery_state->name : '',
                                '{delivery_phone}' => ($delivery->phone) ? $delivery->phone : $delivery->phone_mobile,
                                '{delivery_other}' => $delivery->other,
                                '{invoice_company}' => $invoice->company,
                                '{invoice_vat_number}' => $invoice->vat_number,
                                '{invoice_firstname}' => $invoice->firstname,
                                '{invoice_lastname}' => $invoice->lastname,
                                '{invoice_address2}' => $invoice->address2,
                                '{invoice_address1}' => $invoice->address1,
                                '{invoice_city}' => $invoice->city,
                                '{invoice_postal_code}' => $invoice->postcode,
                                '{invoice_country}' => $invoice->country,
                                '{invoice_state}' => $invoice->id_state ? $invoice_state->name : '',
                                '{invoice_phone}' => ($invoice->phone) ? $invoice->phone : $invoice->phone_mobile,
                                '{invoice_other}' => $invoice->other,
                                '{order_name}' => $order->getUniqReference(),
                                '{date}' => Tools::displayDate(date('Y-m-d H:i:s'), null, 1),
                                '{carrier}' => ($virtual_product || !isset($carrier->name)) ? $this->trans('No carrier', array(), 'Admin.Payment.Notification') : $carrier->name,
                                '{payment}' => Tools::substr($order->payment, 0, 255),
                                '{products}' => $product_list_html,
                                '{products_txt}' => $product_list_txt,
                                '{discounts}' => $cart_rules_list_html,
                                '{discounts_txt}' => $cart_rules_list_txt,
                                '{total_paid}' => Tools::displayPrice($order->total_paid, $this->context->currency, false),
                                '{total_products}' => Tools::displayPrice(Product::getTaxCalculationMethod() == PS_TAX_EXC ? $order->total_products : $order->total_products_wt, $this->context->currency, false),
                                '{total_discounts}' => Tools::displayPrice($order->total_discounts, $this->context->currency, false),
                                '{total_shipping}' => Tools::displayPrice($order->total_shipping, $this->context->currency, false),
                                '{total_wrapping}' => Tools::displayPrice($order->total_wrapping, $this->context->currency, false),
                                '{total_tax_paid}' => Tools::displayPrice(($order->total_products_wt - $order->total_products) + ($order->total_shipping_tax_incl - $order->total_shipping_tax_excl), $this->context->currency, false));

                            if (is_array($extra_vars)) {
                                $data = array_merge($data, $extra_vars);
                            }

                            // Join PDF invoice
                            if ((int)Configuration::get('PS_INVOICE') && $order_status->invoice && $order->invoice_number) {
                                $order_invoice_list = $order->getInvoicesCollection();
                                Hook::exec('actionPDFInvoiceRender', array('order_invoice_list' => $order_invoice_list));
                                $pdf = new PDF($order_invoice_list, PDF::TEMPLATE_INVOICE, $this->context->smarty);
                                $file_attachement['content'] = $pdf->render(false);
                                $file_attachement['name'] = Configuration::get('PS_INVOICE_PREFIX', (int)$order->id_lang, null, $order->id_shop).sprintf('%06d', $order->invoice_number).'.pdf';
                                $file_attachement['mime'] = 'application/pdf';
                            } else {
                                $file_attachement = null;
                            }

                            if (self::DEBUG_MODE) {
                                PrestaShopLogger::addLog('PaymentModule::validateOrder - Mail is about to be sent', 1, null, 'Cart', (int)$id_cart, true);
                            }

                            $orderLanguage = new Language((int) $order->id_lang);

                            if (Validate::isEmail($this->context->customer->email)) {
                                Mail::Send(
                                    (int)$order->id_lang,
                                    'order_conf',
                                    Context::getContext()->getTranslator()->trans(
                                        'Order confirmation',
                                        array(),
                                        'Emails.Subject',
                                        $orderLanguage->locale
                                    ),
                                    $data,
                                    $this->context->customer->email,
                                    $this->context->customer->firstname.' '.$this->context->customer->lastname,
                                    null,
                                    null,
                                    $file_attachement,
                                    null, _PS_MAIL_DIR_, false, (int)$order->id_shop
                                );
                            }
                        }

                        // updates stock in shops
                        if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                            $product_list = $order->getProducts();
                            foreach ($product_list as $product) {
                                // if the available quantities depends on the physical stock
                                if (StockAvailable::dependsOnStock($product['product_id'])) {
                                    // synchronizes
                                    StockAvailable::synchronize($product['product_id'], $order->id_shop);
                                }
                            }
                        }

                        $order->updateOrderDetailTax();

                        // sync all stock
                        (new StockManager())->updatePhysicalProductQuantity(
                            (int)$order->id_shop,
                            (int)Configuration::get('PS_OS_ERROR'),
                            (int)Configuration::get('PS_OS_CANCELED'),
                            null,
                            (int)$order->id
                        );
                    } else {
                        $error = $this->trans('Order creation failed', array(), 'Admin.Payment.Notification');
                        PrestaShopLogger::addLog($error, 4, '0000002', 'Cart', intval($order->id_cart));
                        die($error);
                    }
                } // End foreach $order_detail_list

                // Use the last order as currentOrder
                if (isset($order) && $order->id) {
                    $this->currentOrder = (int)$order->id;
                }

                if (self::DEBUG_MODE) {
                    PrestaShopLogger::addLog('PaymentModule::validateOrder - End of validateOrder', 1, null, 'Cart', (int)$id_cart, true);
                }

                return true;
            } else {
                $error = $this->trans('Cart cannot be loaded or an order has already been placed using this cart', array(), 'Admin.Payment.Notification');
                PrestaShopLogger::addLog($error, 4, '0000001', 'Cart', intval($this->context->cart->id));
                die($error);
            }
        }
        else{
            $cart = Context::getContext()->cart;
            if (!empty($cart)) {
                $cart->getPackageList(true);
                $cart->getDeliveryOptionList(null, true);
            }
            parent::validateOrder($id_cart, $id_order_state, $amount_paid, $payment_method,
                $message, $extra_vars, $currency_special, $dont_touch_amount,
                $secure_key, $shop);
        }
    }
    
    // get the list in the states in the array [id_state => name]
    private static function getDeliveryStates() {
        $states = array();
        
        $res = Db::getInstance()->ExecuteS('SELECT s.id_state, s.name
        FROM `'._DB_PREFIX_.'state` s
        WHERE s.active = 1 
        ORDER BY s.name');
        
        foreach ($res as $value) {
            $states[ $value['id_state'] ] = $value['name'];
        }
        
        return $states;
        
    }
}
