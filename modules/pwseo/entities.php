<?php
$id_editorial = null;
if (Module::isEnabled('editorial')) {
    Module::getInstanceByName('editorial');
    $ed = EditorialClass::getByIdShop(Context::getContext()->shop->id);
    $id_editorial = $ed->id;
}
$id_pwcatseo = null;
if (Module::isEnabled('pwcatseo') && Tools::getValue('id_category')) {
    Module::getInstanceByName('pwcatseo');
    $id_pwcatseo = Db::getInstance()->getValue('SELECT `id_pwcatseo` FROM '._DB_PREFIX_.'pwcatseo WHERE id_category='.(int)Tools::getValue('id_category'));
    if (empty($id_pwcatseo)) {
        $cs = new Catseo();
        $cs->id_category = Tools::getValue('id_category');
        $cs->add();
        $id_pwcatseo = $cs->id;
    }
    $_POST['id_pwcatseo'] = $id_pwcatseo; //как это работает??? Все просто! Мы указываем, что id должен браться из id_pwcatseo, и поэтому мы его туда засовываем. Сами себе делаем костыли. Но я буду называть это фичей
}
$result = array(
    'index' => array(
        array(
            'type' => 'module',
            'module' => 'ps_customtext',
            'class' => 'CustomText',
            'fields' => array(
                'text' => array('lang' => true, 'title' => 'Текст', 'rte' => true),
            ),
            'id' => 1,
        ),
        array(
            'type' => 'module',
            'module' => 'editorial',
            'class' => 'EditorialClass',
            'fields' => array(
                'body_title' => array('lang' => true, 'title' => 'H1', 'type' => 'text'),
                'body_subheading' => array('lang' => true, 'title' => 'Subtitle', 'rte' => true),
                'body_paragraph' => array('lang' => true, 'title' => 'Text on the main page', 'rte' => true),
            ),
            'id' => $id_editorial,
        ),
        array(
            'configuration' => array(
                'PS_SHOP_NAME' => 'Shop name',
                'PS_SHOP_EMAIL' => 'Shop email',
                'PS_SHOP_PHONE' => 'Phone',
            ),
        ),
    ),
    'product' => array(
        array(
            'class' => 'Product',
            'fields' => array(
                'name' => array('lang' => true, 'title' => 'Product name', 'type' => 'text'),
                'description' => array('lang' => true, 'title' => 'Description', 'rte' => true, 'type' => 'textarea'),
                'description_short' => array('lang' => true, 'title' => 'Short description', 'rte' => true, 'type' => 'textarea'),
                'meta_title' => Array('title' => 'Page title', 'lang' => true, 'type' => 'text'),
                'meta_description' => Array('title' => 'meta descriprion', 'lang' => true, 'type' => 'text'),
                'meta_keywords' => Array('title' => 'meta keywords', 'lang' => true, 'type' => 'text'),
				'сanonical' => Array('title' => 'Canonical', 'lang' => true, 'type' => 'text'),
                // 'condition' => array('title' => 'State', 'type' => 'select', 'values' => array('new' => 'New', 'used' => 'Used', 'refurbished' => 'Refurbished')),
                // 'active' => array('title' => 'Active', 'type' => 'switch'),
            ),
            'id_entity' => 'id_product',
        ),
    ),
    'category' => array(
        array(
            'class' => 'Category',
            'fields' => array(
                'name' => array('lang' => true, 'title' => 'Category name', 'type' => 'text'),
                'description' => array('lang' => true, 'title' => 'Description', 'rte' => true, 'type' => 'textarea'),
                'meta_title' => Array('title' => 'Page title', 'lang' => true, 'type' => 'text'),
                'meta_description' => Array('title' => 'meta descriprion', 'lang' => true, 'type' => 'text'),
                'meta_keywords' => Array('title' => 'meta keywords', 'lang' => true, 'type' => 'text'),
				'сanonical' => Array('title' => 'Canonical', 'lang' => true, 'type' => 'text'),
            ),
            'id_entity' => 'id_category',
        ),
        array(
            'type' => 'module',
            'module' => 'pwcatseo',
            'class' => 'Catseo',
            'fields' => array(
                'title' => array('lang' => true, 'title' => 'H1', 'type' => 'text'),
                'text' => array('lang' => true, 'title' => 'Second description', 'rte' => true, 'type' => 'textarea'),
            ),
            'id_entity' => 'id_pwcatseo',
        ),
        array(
            'configuration' => array(
                'PS_PRODUCTS_PER_PAGE' => 'products on page',
            ),
        ),
    ),
    'cms' => array(
        array(
            'class' => 'CMS',
            'fields' => array(
                'content' => array('lang' => true, 'title' => 'Content', 'rte' => true, 'type' => 'textarea'),
                'meta_title' => Array('title' => 'Page title', 'lang' => true, 'type' => 'text'),
                'meta_description' => Array('title' => 'Meta descriprion', 'lang' => true, 'type' => 'text'),
                'meta_keywords' => Array('title' => 'Meta keywords', 'lang' => true, 'type' => 'text'),
				'сanonical' => Array('title' => 'Canonical', 'lang' => true, 'type' => 'text'),
            ),
            'id_entity' => 'id_cms',
        ),
        array(
            'class' => 'CMSCategory',
            'fields' => array(
                'description' => array('lang' => true, 'title' => 'Description', 'rte' => true, 'type' => 'textarea'),
                'meta_title' => Array('title' => 'Page title', 'lang' => true, 'type' => 'text'),
                'meta_description' => Array('title' => 'meta descriprion', 'lang' => true, 'type' => 'text'),
                'meta_keywords' => Array('title' => 'meta keywords', 'lang' => true, 'type' => 'text'),
				'сanonical' => Array('title' => 'Canonical', 'lang' => true, 'type' => 'text'),
            ),
            'id_entity' => 'id_cms_category',
        ),
    ),
    'manufacturer' => array(
        array(
            'class' => 'Manufacturer',
            'fields' => array(
                'name' => Array('title' => 'Name', 'lang' => false, 'type' => 'text'),
                'description' => array('lang' => true, 'title' => 'Description', 'rte' => true, 'type' => 'textarea'),
                'short_description' => array('lang' => true, 'title' => 'Short description', 'rte' => true, 'type' => 'textarea'),
                'meta_title' => Array('title' => 'Page title', 'lang' => true, 'type' => 'text'),
                'meta_description' => Array('title' => 'Meta descriprion', 'lang' => true, 'type' => 'text'),
                'meta_keywords' => Array('title' => 'Meta keywords', 'lang' => true, 'type' => 'text'),
				'сanonical' => Array('title' => 'Canonical', 'lang' => true, 'type' => 'text'),
            ),
            'id_entity' => 'id_manufacturer',
        ),
    ),
    'supplier' => array(
        array(
            'class' => 'Supplier',
            'fields' => array(
                'name' => Array('title' => 'Name', 'lang' => false, 'type' => 'text'),
                'description' => array('lang' => true, 'title' => 'Description', 'rte' => true, 'type' => 'textarea'),
                'meta_title' => Array('title' => 'Page title', 'lang' => true, 'type' => 'text'),
                'meta_description' => Array('title' => 'Meta descriprion', 'lang' => true, 'type' => 'text'),
                'meta_keywords' => Array('title' => 'Meta keywords', 'lang' => true, 'type' => 'text'),
				'сanonical' => Array('title' => 'Canonical', 'lang' => true, 'type' => 'text'),
            ),
            'id_entity' => 'id_supplier',
        ),
    ),
    'module-smartblog-details' => array(
        array(
            'class' => 'SmartBlogPost',
            'fields' => array(
                'meta_title' => Array('title' => 'Meta title', 'lang' => false, 'type' => 'text'),
                'title' => Array('title' => 'Description', 'lang' => false, 'type' => 'text'),
                'short_description' => array('lang' => true, 'title' => 'Short description', 'rte' => true, 'type' => 'textarea'),
                'content' => array('lang' => true, 'title' => 'Content', 'rte' => true, 'type' => 'textarea'),
                'meta_description' => Array('title' => 'Meta descriprion', 'lang' => true, 'type' => 'text'),
                'meta_keyword' => Array('title' => 'Meta keywords', 'lang' => true, 'type' => 'text'),
				'сanonical' => Array('title' => 'Canonical', 'lang' => true, 'type' => 'text'),
            ),
            'id_entity' => 'id_post',
        ),
    ),
    'module-smartblog-category' => array(
        array(
            'class' => 'BlogCategory',
            'fields' => array(
                'meta_title' => Array('title' => 'Title', 'lang' => false, 'type' => 'text'),
                'description' => array('lang' => true, 'title' => 'Description', 'rte' => true, 'type' => 'textarea'),
                'meta_description' => Array('title' => 'Meta descriprion', 'lang' => true, 'type' => 'text'),
                'meta_keywords' => Array('title' => 'Meta keywords', 'lang' => true, 'type' => 'text'),
				'сanonical' => Array('title' => 'Canonical', 'lang' => true, 'type' => 'text'),
            ),
            'id_entity' => 'id_category',
        ),
        array(
            'configuration' => array('smartblogmetatitle' => 'Meta title', 'smartblogmetadescrip' => 'Meta descriprion', 'smartblogmetakeyword' => 'Meta keywords', 'smartmainblogurl' => 'Main link'),
        ),
    ),
);

return $result;