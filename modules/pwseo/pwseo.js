$(document).ready(function(){
    $.ajax({
        async: true,
        url: 'index.php',
        method: 'POST',
        data: {
            ajax: true,
            method: 'getForm',
            page_name: window.page_name,
            fc: 'module',
            module: 'pwseo',
            controller: 'ajax',
            adding: pwseo,
        },
        dataType: 'json',
    }).done(function(r){
        if (!!$.fancybox && r.form) {
            $('body').append(r.form);
        }
    });
    $(document).on('click', '#pwseo_editlink', function(){
        $('#pwseo_menu').toggle();
        return false;
    });
    $(document).on('click', '#pwseo_status_edit', function(e){
        e.preventDefault();
        var clas = $(this).data('toggle');
        var id = $(this).data('id');
        $.ajax({
            async: true,
            url: 'index.php',
            method: 'POST',
            data: {
                ajax: true,
                method: 'toggle',
                page_name: window.page_name,
                fc: 'module',
                module: 'pwseo',
                controller: 'ajax',
                clas : clas,
                id: id,
            },
            dataType: 'json',
        }).done(function(resp){
            if (resp.is_error) {
                alert('Error');
                console.log(resp.errors);
            } else {
                if (resp.active) {
                    alert('Enabled');
                } else {
                    alert('Disabled');
                }
            }
        });
        return false;
    });
    $('#pwseo_meta_edit').fancybox();
    $('#pwseo_desc_edit').fancybox();
});

$(document).keyup(function(e){
	var key=[];
	key["c"]=67;
	key["b"]=66;
	key["v"]=86;
	key["f"]=70;
	key["r"]=82;
	key["space"]=32;
	key["enter"]=13;
    if (e.keyCode==key["c"] && (e.altKey))
    {
        $("#pwseo_editlink").click();
        return false;
    }
    if (e.keyCode==key["b"] && (e.altKey))
    {
        $('#pwseo_desc_edit').click()
        return false;
    }
    if (e.keyCode==key["v"] && (e.altKey))
    {
        $('#pwseo_meta_edit').click()
        return false;
    }
});
$(document).on('submit', '#pwseo_meta_form, #pwseo_desc_form', function(e){
    e.preventDefault();
    var form = $(this);
    var elements = Object();
    form.find('[name]').each(function(i, el){
        var params = {
            name: $(el).attr('name'),
            data: $(el).data(),
            value: $(el).val(),
        }
        if ($(el).is(':checkbox')) {
            params.value = $(el).is(':checked') == true?1:0;
        }
        elements[i] = params;
    });
    form.find('.form-error').remove();
    form.find('.form-success').remove();
    $.ajax({
        async: true,
        url: 'index.php',
        method: 'POST',
        data: {
            ajax: true,
            method: 'updateValues',
            page_name: window.page_name,
            fc: 'module',
            module: 'pwseo',
            controller: 'ajax',
            data: elements,
        },
        dataType: 'json',
    }).done(function(resp){
        if (resp.is_error) {
            form.find('.panel-body').prepend('<span class="form-error">Error: '+ resp.errors[0] +'</span>');
            console.log(resp.errors);
        } else {
            console.log(resp.message);
            form.find('.panel-body').prepend('<span class="form-success">'+resp.message+'</span>');
        }
    });
    return false;
});