<?php
if (!defined('_PS_VERSION_'))
    exit;

class pwseo extends Module
{
    public $conf;
    
    public function __construct()
    {
        $this->name = 'pwseo';
        $this->tab = 'other';
        $this->version = "2.0.4";
        $this->author = 'Prestaweb.ru';
        $this->need_instance = 0;

        parent::__construct();
        
        $this->bootstrap = true;
        $this->displayName = "SEO module";
        $this->description = "For quick editing SEO page tags and descriptions";
        $this->conf = require(dirname(__FILE__) . '/entities.php');
    }

    

    public function install(){

        $sql = array();

        $sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'pwseo`';

        $sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pwseo` (
            `id_pwseo` int(11) NOT NULL AUTO_INCREMENT,
            `page` varchar(255) DEFAULT NULL,
            `url` varchar(255) DEFAULT NULL, 
            PRIMARY KEY (`id_pwseo`)
        ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

        foreach ($sql as $query) {
            if (Db::getInstance()->execute($query) == false) {
                return false;
            }
        }
        
        return (parent::install()  AND $this->registerHook('displayHeader') AND $this->fixMetaSmartblog());
    }
    
    //Чтобы не мешали лишние поля у смартблога
    protected function fixMetaSmartblog()
    {
        return Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'meta SET configurable = 0 WHERE page LIKE "module-smartblog-%"');
    }

	public function hookDisplayHeader($params){
        $adminCookie = new Cookie('psAdmin');
		if ($adminCookie->profile) {
			$adding_fields = array();
            $page_name = !empty($this->context->controller->php_self)?$this->context->controller->php_self:$this->context->controller->page_name;
            if (isset($this->conf[$page_name])) {
                foreach ($this->conf[$page_name] as $field) {
                    if (isset($field['id_entity'])) {
                        $adding_fields[$field['id_entity']] = Tools::getValue($field['id_entity']);
                    }
                }
            }
			$this->context->controller->addCSS(($this->_path).$this->name . '.css', 'all');
			$this->context->controller->addJS(($this->_path).$this->name . '.js');
			// $this->context->controller->addJS(_PS_JS_DIR_.'tiny_mce/tinymce.min.js');
            // if (version_compare(_PS_VERSION_, '1.7', '>=')) {
                // $this->context->controller->registerJavascript("tiny-mce", _PS_JS_DIR_.'tiny_mce/tinymce.min.js', array('position' => 'bottom', 'priority' => 100));
            // }
            $this->context->controller->addJqueryPlugin('fancybox');
            $this->context->smarty->assign(array(
                'adding_fields' => $adding_fields,
                'page_name' => $page_name,
            ));
            return $this->display(__FILE__, 'addFields.tpl');
        }
	}


    public function postProcess()
    {
	    if (Tools::getValue('apiSecretKey')) {
		    Configuration::updateValue('apiSecretKey', Tools::getValue('apiSecretKey'));
	    }
    }

    public function getContent()
    {
        $this->postProcess();
        return $this->renderForm();
    }
    
    protected function renderForm()
    {
        $default_lang = $this->context->lanugage->id;
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = array(
            'save' => array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                '&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );
        $fields_form = array(
            array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->l('Settings'),
                    ),
                    'input' => array(
                        array(
                            'type' => 'text',
                            'label' => $this->l('Секретный ключ api'),
                            'name' => 'apiSecretKey',
                            'size' => 20,
                            'required' => true
                        ),
                    ),
                    'submit' => array(
                        'title' => $this->l('Save'),
                        'class' => 'button'
                    )
                ),
            ),
        );
        $helper->fields_value = array(
            'apiSecretKey' => Configuration::get('apiSecretKey'),
        );
        return $helper->generateForm($fields_form);
    }
}