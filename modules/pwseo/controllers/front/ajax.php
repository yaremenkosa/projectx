<?php
@ini_set('display_errors', 'off');
class pwseoAjaxModuleFrontController extends ModuleFrontController {

    protected $page_name;
    protected $editor;
    protected $toggles;

    public function init()
    {
        parent::init();
        $adminCookie = new Cookie('psAdmin');
        if (!$adminCookie->profile) {
            die;
        }
        $this->toggles = array(
            'product' => array('class' => 'Product', 'field' => 'active'),
            'category' => array('class' => 'Category', 'field' => 'active'),
            'cms' => array('class' => 'CMS', 'field' => 'active'),
            'manufacturer' => array('class' => 'Manufacturer', 'field' => 'active'),
            'supplier' => array('class' => 'Supplier', 'field' => 'active'),
        );
        $this->page_name = Tools::getValue('page_name');
        $method = 'action'.Tools::getValue('method');
        if (method_exists($this, $method)) {
            die(Tools::jsonEncode($this->$method()));
        }
        die;
    }
    
    protected function actionUpdateValues()
    {
        $data = Tools::getValue('data');
        $errors = array();
        $clearcache = false;
        foreach ($data as $field) {
            $params = $field['data'];
			
			if($params['class'] == 'link_сanonical') { // index
				if(empty($params['ref'])) {
					if($params['id'] == 0)
						$sql = "INSERT INTO `"._DB_PREFIX_."pwseo` (`page`, `url`) VALUE ('".pSQL($params['id'])."', '".pSQL($field['value'])."')";
					else
						$sql = "UPDATE `"._DB_PREFIX_."pwseo` SET `url` = '".pSQL($field['value'])."' WHERE `id_pwseo` = '".pSQL($params['id'])."'";
				} else {
					if($params['id'] == 0) {
						$sql = "INSERT INTO `"._DB_PREFIX_."pwseo` (`page`, `url`) VALUE ('".pSQL($params['ref'])."','".pSQL($field['value'])."')";
						
					} else {
						$sql = "UPDATE `"._DB_PREFIX_."pwseo` SET `url` = '".pSQL($field['value'])."' WHERE `id_pwseo` = '".pSQL($params['id'])."'";
					}
					
				}
				
				Db::getInstance()->Execute($sql);
				
				
				continue;
			}
			
            if (isset($params['conf'])) {
                Configuration::updateValue($field['name'], $field['value']);
                continue;
            }
            if (!empty($params['module'])) {
                Module::getInstanceByName($params['module']);
                $clearcache = true;
            }
            try{
                $id = $params['id'];
                $object = new $params['class']($id); //можно, конечно, тут передавать id_lang, но могут быть косяки. Например, в product второй параметр не id_lang
                if (empty($object::$definition['fields'])) {
                    continue;
                }
                if (is_array($object->{$field['name']})) {
                    $object->{$field['name']}[$this->context->language->id] = $field['value'];
                } else {
                    if (!empty($object::$definition['fields'][$field['name']]['type'])) {
                        $type = $object::$definition['fields'][$field['name']]['type'];
                        if ($type == ObjectModel::TYPE_BOOL) {
                            if ($field['value'] == 'false' || $field['value'] == '0') {
                                $field['value'] = false;
                            }
                            $field['value'] = (bool)$field['value'];
                        }
                        if ($type == ObjectModel::TYPE_FLOAT) {
                            $field['value'] = (float)str_replace(',', '.', $field['value']);
                        }
                        if ($type == ObjectModel::TYPE_INT) {
                            $field['value'] = (int)$field['value'];
                        }
                        if ($type == ObjectModel::TYPE_STRING) {
                            $field['value'] = (string)$field['value'];
                        }
                    }
                    $object->{$field['name']} = $field['value'];
                }
				
                $object->save();
            } catch (Exception $e) {
                $errors[] = $e->getMessage();
            }
        }
        if ($clearcache) {
            Tools::clearCache();
        }
        if (count($errors)) {
            return array('is_error' => true, 'errors' => $errors);
        }
        return array('is_error' => false, 'message' => 'Edited success');
    }
    
    protected function actionToggle()
    {
        $class = Tools::getValue('clas');
        $id = Tools::getValue('id');
        try{
            $obj = new $class($id);
            $field = $this->toggles[$this->page_name]['field'];
            $obj->{$field} = !$obj->{$field};
            $obj->update();
        } catch (Exception $e) {
            return array('is_error' => true, 'errors' => $e->getMessage());
        }
        return array('active' => $obj->{$field});
    }
    
    protected function actionGetForm()
    {
        $fields = array();
        $adding = Tools::getValue('adding');
        $configurations = array();
        $toggle = false;
        if (isset($this->toggles[$this->page_name])) {
            $toggle = true;
            $toggleId = 0;
        }
        $metas = Meta::getMetaByPage($this->page_name, $this->context->language->id);
		
        if (!empty($metas) && $metas['configurable']) {
			$sql = 'SELECT * FROM `'._DB_PREFIX_.'pwseo` WHERE `page` = "'.$this->page_name.'"';
			$rel_canonical = Db::getInstance()->getRow($sql);
		
            $fields[] = array(
                'name' => 'title',
                'title' => 'Meta title',
                'lang' => true,
                'class' => 'Meta',
                'id' => $metas['id_meta'],
                'type' => 'text',
                'value' => $metas['title'],
            );
            $fields[] = array(
                'name' => 'description',
                'title' => 'Meta description',
                'lang' => true,
                'class' => 'Meta',
                'id' => $metas['id_meta'],
                'type' => 'text',
                'value' => $metas['description'],
            );
        }
		
		
		
        if (!empty($this->module->conf[$this->page_name])) {
			

            $rel_id = 0;
			foreach ($this->module->conf[$this->page_name] as $obj) {
			
                if (isset($obj['fields'])) {
					
                    foreach ($obj['fields'] as $field => $params) {
                        $params['name'] = $field;
                        $params['class'] = $obj['class'];
                        if (!empty($obj['module'])) {
                            if (!Module::isEnabled($obj['module'])) {
                                continue;
                            }
                            $params['module'] = $obj['module'];
                            Module::getInstanceByName($params['module']); //чтобы подгрузить все классы
                        }
                        if (isset($obj['id'])) {
                            $params['id'] = $obj['id'];
                        } elseif (isset($obj['id_entity'])) {
                            $params['id'] = $adding[$obj['id_entity']];
                        }
                        $id = $params['id'];
                        if (empty($id)) {
                            continue;
                        }
                        $params['value'] = '';
                        try{
                            if (!class_exists($obj['class'])) {
                                throw(new Exception(sprintf('Class %s missing', $obj['class'])));
                            }
                            $object = new $obj['class']($id);
                            if (!property_exists($object, $field)) {
                                continue; //просто проигнорируем
                            }
                            $params['value'] = $object->{$field};
                            if (is_array($params['value'])) {
                                $params['value'] = $params['value'][$this->context->language->id]; //мне кажется лучше так, чем сувать id_lang в конструктор. Мало ли как там все написано
                            }
                        } catch (Exception $e) {
                            Tools::dieOrLog($e->getMessage());
                        }
                        if ($toggle && $params['class'] == $this->toggles[$this->page_name]['class']) {
                            $toggleId = $id;
                        }
						$rel_id = $id;
                        $fields[] = $params;
						
                    }
					$sql = 'SELECT * FROM `'._DB_PREFIX_.'pwseo` WHERE `page` = "'.$this->page_name.'_'.$rel_id.'"';
					$rel_canonical = Db::getInstance()->getRow($sql);
                }
                if (isset($obj['configuration'])) {
                    if (isset($obj['module'])) {
                        if (!Module::isEnabled($obj['module'])) {
                            continue;
                        }
                    }
                    $configurations = array_merge($configurations, $obj['configuration']);
                }
            }
			
			$fields[] = array(
				'name' => 'сanonical',
				'title' => 'Сanonical',
				'lang' => true,
				'class' => 'link_сanonical',
				'id' => (isset($rel_canonical['id_pwseo']) ? $rel_canonical['id_pwseo'] : '0'),
				'ref' => $this->page_name.'_'.$rel_id, 
				'type' => 'text',
				'value' => (isset($rel_canonical['url']) ? $rel_canonical['url'] : ''),
			);
			
        }
        $configs = array();
        foreach ($configurations as $k => $config) {
            $configs[$k] = array(
                'title' => $config,
                'value' => Configuration::get($k),
            );
        }
        $this->context->smarty->assign(array(
            'fields' => $fields,
            'page_name' => $this->page_name,
            'toggle'   => $toggle,
            'configurations'   => $configs,
        ));
        if ($toggle) {
            $this->context->smarty->assign(array(
                'toggle_class' => $this->toggles[$this->page_name]['class'],
                'toggle_id' => $toggleId,
            ));
        }
        return array(
            'form' => $this->context->smarty->fetch(_PS_MODULE_DIR_.'pwseo/views/templates/front/form.tpl'),
        );
    }
}
