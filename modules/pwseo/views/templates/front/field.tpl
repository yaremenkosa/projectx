<div class="form-group">
    <label class="control-label" for="{$field.name}">{$field.title}</label>
    {if (isset($field.type) && $field.type == 'textarea') || !empty($field.rte)}
    <textarea data-class="{$field.class}" data-id="{$field.id}" name="{$field.name}" {if !empty($field.module)}data-module="{$field.module}"{/if} class="{if !empty($field.rte)}autoload_rte{/if}">{$field.value}</textarea>
    {elseif (isset($field.type) && $field.type == 'select')}
    <select class="select" id="{$field.name}" {if !empty($field.module)}data-module="{$field.module}"{/if} data-class="{$field.class}" data-id="{$field.id}" name="{$field.name}">
    {foreach from=$field.values item=val key=key}
    <option value="{$key}" {if $key==$field.value}selected{/if}>{$val}</option>
    {/foreach}
    </select>
    {elseif (isset($field.type) && $field.type == 'switch')}
    <label for="{$field.name}_off">
        <input type="checkbox" {if $field.value == 1}checked{/if} class="radio" id="{$field.name}_off" {if !empty($field.module)}data-module="{$field.module}"{/if} data-class="{$field.class}" data-id="{$field.id}" name="{$field.name}" value="1">
    </label>
    {else}
    <input class="text" type="{if !empty($field.type)}{$field.type}{else}text{/if}" id="{$field.name}" {if !empty($field.module)}data-module="{$field.module}"{/if} data-class="{$field.class}" data-id="{$field.id}" data-ref="{if isset($field.ref)}{$field.ref}{/if}" name="{$field.name}" value="{$field.value}" />
    {/if}
</div>