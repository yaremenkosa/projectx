<div id="pwseo_container" class="js-cart-open">
    <a id="pwseo_editlink" href="#">{l s='Edit' mod='pwseo'}</a>
    <div id="pwseo_menu" style="display:none">
        <div class="pwseo_wrapper">
            <a href="#pwseo_meta_popup" id="pwseo_meta_edit">{l s='Edit seo' mod='pwseo'} (Alt+v)</a>
            <a href="#pwseo_description_popup" id="pwseo_desc_edit">{l s='Edit description' mod='pwseo'} (Alt+b)</a>
            {if $toggle}<a id="pwseo_status_edit" class="edit-active" data-id="{$toggle_id}" data-toggle="{$toggle_class}" href="#">{l s='Enable / disable' mod='pwseo'}</a>{/if}
        </div>
    </div>
</div>
<div id="pwseo_meta_popup" class="panel panel-default" style="display:none;">
    <form id="pwseo_meta_form">
        <div class="panel-heading"></div>
        <div class="panel-body">
        {foreach from=$fields item=field}
        {if (isset($field.type) && $field.type != 'textarea') && empty($field.rte)}
            {include file='./field.tpl' field=$field}
        {/if}
        {/foreach}
        {foreach from=$configurations item=configuration key=name}
            {include file='./conffield.tpl' field=$configuration key=$name}
        {/foreach}
        </div>
        <div class="panel-footer">
            <input type="submit" value="Save changes" class="button"/>
            <a class="button" href="javascript:window.location.reload('true')">{l s='Reload page' mod='pwseo'}</a>
        </div>
    </form>
</div>
<div id="pwseo_description_popup" class="panel panel-default" style="display:none;">
    <form id="pwseo_desc_form">
        <div class="panel-heading"></div>
        <div class="panel-body">
        {foreach from=$fields item=field}
        {if (isset($field.type) && $field.type == 'textarea') || !empty($field.rte)}
            {include file='./field.tpl' field=$field}
        {/if}
        {/foreach}
        </div>
        <div class="panel-footer">
            <input type="submit" value="Save changes" class="button"/>
            <a class="button" href="javascript:window.location.reload('true')">{l s='Reload page' mod='pwseo'}</a>
        </div>
    </form>
</div>