<script>
    var dadata_city_field = '{$city_field}';
    var dadata_addr_field = '{$addr_field}';
    var dadata_api_key    = '{$api_key}';

    $(document).ready(function () {

        if(!dadata_city_field) {
            dadata_city_field = 'input[name=city]';
        }
        if(!dadata_addr_field) {
            dadata_addr_field = 'input[name=address1]';
        }

        $(dadata_city_field).suggestions({
            token: dadata_api_key,
            type: "ADDRESS",
            count: 5,
            triggerSelectOnEnter: false,
            formatSelected: formatSelectedCity,
            onSelect: function(suggestion) {

            }
        });

        $(dadata_addr_field).suggestions({
            token: dadata_api_key,
            type: "ADDRESS",
            count: 5,
            triggerSelectOnEnter: false,
            formatSelected: formatSelectedAddr,
            onSelect: function(suggestion) {

            }
        });
    });
</script>