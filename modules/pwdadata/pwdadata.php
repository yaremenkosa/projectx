<?php
if (!defined('_PS_VERSION_'))
    exit;

class pwdadata extends Module
{
    public function __construct()
    {
        $this->name = strtolower(get_class());
        $this->tab = 'other';
        $this->version = 0.1;
        $this->author = 'PrestaWeb.ru';
        $this->need_instance = 1;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l("DaData");
        $this->description = $this->l("Автоматические подсказки при вводе адресе/города в корзине");
        
        $this->ps_versions_compliancy = array('min' => '1.5.0.0', 'max' => _PS_VERSION_);
    }

    public function install()
    {

        if ( !parent::install() 
			OR !$this->registerHook(Array(
				'displayHeader',
			))
            
        ) return false;

        return true;
    }


    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Включен'),
                        'name' => 'PWDADATA_ENABLED',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Селектор города'),
                        'name' => 'PWDADATA_CITY_FIELD',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Слектор адреса'),
                        'name' => 'PWDADATA_ADDR_FIELD',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('API ключ'),
                        'name' => 'PWDADATA_API_KEY',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitPWPAYPAL';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'PWDADATA_ENABLED' => Tools::getValue('PWDADATA_ENABLED', Configuration::get('PWDADATA_ENABLED')),
            'PWDADATA_CITY_FIELD' => Tools::getValue('PWDADATA_CITY_FIELD', Configuration::get('PWDADATA_CITY_FIELD')),
            'PWDADATA_ADDR_FIELD' => Tools::getValue('PWDADATA_ADDR_FIELD', Configuration::get('PWDADATA_ADDR_FIELD')),
            'PWDADATA_API_KEY' => Tools::getValue('PWDADATA_API_KEY', Configuration::get('PWDADATA_API_KEY')),
        );
    }

    public function getContent()
    {
        $output = '';
        if (Tools::isSubmit('submitPWPAYPAL'))
        {
            Configuration::updateValue('PWDADATA_ENABLED', Tools::getValue('PWDADATA_ENABLED'));
            Configuration::updateValue('PWDADATA_CITY_FIELD', Tools::getValue('PWDADATA_CITY_FIELD'));
            Configuration::updateValue('PWDADATA_ADDR_FIELD', Tools::getValue('PWDADATA_ADDR_FIELD'));
            Configuration::updateValue('PWDADATA_API_KEY', Tools::getValue('PWDADATA_API_KEY'));
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&conf=6');
        }
        return $output.$this->renderForm();
    }

	public function hookdisplayHeader($params){
        if (Configuration::get('PWDADATA_ENABLED') && Configuration::get('PWDADATA_API_KEY')) {

            $this->context->controller->addCSS($this->_path.$this->name.'.css', 'all');
            $this->context->controller->addJS('https://cdn.jsdelivr.net/npm/suggestions-jquery@17.12.0/dist/js/jquery.suggestions.min.js');
            $this->context->controller->addJS($this->_path.$this->name.'.js');

            $this->smarty->assign(array(
                'city_field' => Configuration::get('PWDADATA_CITY_FIELD'),
                'addr_field' => Configuration::get('PWDADATA_ADDR_FIELD'),
                'api_key'    => Configuration::get('PWDADATA_API_KEY'),
            ));

            return $this->display(__FILE__, 'pwdadata.tpl');
        }
	}


}


