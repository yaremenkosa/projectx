function formatSelectedCity (suggestion) {
    var addressValue = suggestion.data.city;

    if (suggestion.data.settlement) {
        addressValue = addressValue + ', ' + suggestion.data.settlement_type + ' ' + suggestion.data.settlement;
    }

    return addressValue;
}

function formatSelectedAddr (suggestion) {
    var addressValue = suggestion.data.street_with_type;

    if (suggestion.data.house) {
        addressValue = addressValue + ', ' + suggestion.data.house_type + ' ' + suggestion.data.house;
    }

    $(dadata_city_field).val(formatSelectedCity(suggestion));

    return addressValue;
}