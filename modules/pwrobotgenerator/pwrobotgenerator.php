<?php
if (!defined('_PS_VERSION_'))
    exit;

class pwrobotgenerator extends Module
{

    public static $generated = false;
	
	public static $generatedHtacces = false;

    public function __construct()
    {
        $this->name = strtolower(get_class());
        $this->tab = 'other';
        $this->version = '0.4.5';
        $this->author = 'Prestaweb.ru';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = "Генератор robots.txt";
        $this->description = "Генератор robots.txt (мультимагазинный)";
    }

    public function install()
    {
        if(parent::install()){
            Configuration::updateValue('PW_ROBOTS_TEXT', 'Host: '.(Configuration::get('PS_SSL_ENABLED_EVERYWHERE')?'https://':'').$this->context->shop->domain);
            if(Hook::getIdByName('actionAdminMetaAfterWriteRobotsFile')){
                $this->registerHook('actionAdminMetaAfterWriteRobotsFile');
            }
            if(Hook::getIdByName('actionHtaccessCreate')){
                $this->registerHook('actionHtaccessCreate');
            }
           // AdminMetaController::getController('AdminMetaController', true)->generateRobotsFile();
            
            unlink(_PS_ROOT_DIR_ . '/cache/class_index.php');
			
          //  $this->generateRobots();
            return true;
        }
        return false;
    }
    
    public function uninstall()
    {
        return parent::uninstall();
    }

    public function getContent()
    {
		
        if(Tools::isSubmit('submitPwGenerator')){
            Configuration::updateValue('PW_ROBOTS_TEXT', Tools::getValue('PW_ROBOTS_TEXT'));
            Configuration::updateValue('PW_ROBOTS_ON', Tools::getValue('PW_ROBOTS_ON'));
			//not correct chek ...
			/*
            if(!is_writable(_PS_ROOT_DIR_.'/robots.txt')){
                $this->context->controller->errors[] = 'Нет прав на запись файла '._PS_ROOT_DIR_.'/robots.txt';
            }*/
           AdminMetaController::getController('AdminMetaController', true)->generateRobotsFile();
            if(!Hook::getIdByName('actionAdminMetaAfterWriteRobotsFile')){
                $this->generateRobots();
            }
			
			if (!self::$generatedHtacces) {
				Tools::generateHtaccess(); 
				self::$generatedHtacces=true;
			}
        }
        return $this->renderForm();
    }

    public function hookactionAdminMetaAfterWriteRobotsFile($params){
        $this->generateRobots($params['write_fd']);
    }

    public function hookactionHtaccessCreate($params)
    {

    }

    private function generateRobots(&$handler = null)
    {
        $text = '';
        if(!empty($handler)){
            fclose($handler);
        }
        $text = file_get_contents(_PS_ROOT_DIR_.'/robots.txt');

        /**
         * Ладно, не ржем, всем приходится костыльнуть
         */
        $text = str_replace("\r\n", "\n", $text);
        $text = str_replace("\r", "\n", $text);

        $rows = explode("\n", $text);
        $rgRows = array();
        foreach ($rows as $row) {
            if(strpos($row, 'itemap'))
                continue;

            $rgRows[$row] = $row;
        }


        $text = implode("\r\n", $rgRows);

        if(Shop::isFeatureActive()){
			/*
			in future we could rename old $_SERVER['DOCUMENT_ROOT'].'/robots.txt' into 
			robots_old.txt. In this case we don't have to rename or delete ROOT/robots.txt manually   
			*/
			/*
			if (file_exists($_SERVER['DOCUMENT_ROOT'].'/robots.txt')) {
		rename($path, _PS_ROOT_DIR_.'/robots_old.txt');
			 
	}
	*/
	
	 
            $shops = Shop::getShops();
            foreach($shops as $shop){
                $active = Configuration::get('PW_ROBOTS_ON', null, null, $shop['id_shop']);
                $txt = $active?'':$text;
                $file = '/' . $this->name . '/robots/'.$shop['domain'].'.txt';
                $txt .= "\n#pwrobotgenerator\n";
                $txt .= "\n".Configuration::get('PW_ROBOTS_TEXT', null, null, $shop['id_shop']);
                $handler = fopen(_PS_MODULE_DIR_.$file, 'w');
                fwrite($handler, $txt);
                fclose($handler);
            }
        } else { 
            $active = Configuration::get('PW_ROBOTS_ON');
            $txt = $active?'':$text;
            $txt .= "\n#pwrobotgenerator\n";
            $txt .= "\r\n".Configuration::get('PW_ROBOTS_TEXT');
//            $txt .= "\r\n".'Host: '.$this->context->shop->domain;
            $handler = fopen(_PS_ROOT_DIR_.'/robots.txt', 'w');
            fwrite($handler, $txt);
            fclose($handler);
        }

    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Содержимое robots.txt'),
                        'name' => 'PW_ROBOTS_TEXT',
                        'desc' => $this->l('Содержимое robots.txt'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Заменять содержимое'),
                        'name' => 'PW_ROBOTS_ON',
                        'desc' => $this->l('Заменять ли содержимое robots.txt при генерации.'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table =  $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitPwGenerator';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form))."<script>$('#PW_ROBOTS_TEXT').css('height', 'auto').attr('rows', '100').css('resize', 'vertical');</script>";
    }


    public function getConfigFieldsValues()
    {
        return array(
            'PW_ROBOTS_TEXT' => Tools::getValue('PW_ROBOTS_TEXT', Configuration::get('PW_ROBOTS_TEXT')),
            'PW_ROBOTS_ON' => Tools::getValue('PW_ROBOTS_ON', Configuration::get('PW_ROBOTS_ON')),
        );
    }

}
