<?php

class PWgrastinAjaxModuleFrontController extends ModuleFrontController
{

    protected $templateFinder = null;
	// координаты кольца МКАД
	public  $mkad_points = array( 
		array(37.842663,55.774543),
		array(37.842663,55.774543),
		array(37.84269,55.765129), 
		array(37.84216,55.75589),		
		array(37.842232,55.747672),		
		array(37.841109,55.739098),
		array(37.840112,55.730517),
		array(37.839555,55.721782),
		array(37.836968,55.712173),
		array(37.832449,55.702566),
		array(37.829557,55.694271),
		array(37.831425,55.685214),
		array(37.834695,55.676732),
		array(37.837543,55.66763),
		array(37.839295,55.658535),
		array(37.834713,55.650881),
		array(37.824948,55.643749),
		array(37.813746,55.636433),
		array(37.803083,55.629521),
		array(37.793022,55.623666),
		array(37.781614,55.617657),
		array(37.769945,55.61114),
		array(37.758428,55.604819),
		array(37.747199,55.599077),
		array(37.736949,55.594763),
		array(37.721013,55.588135),
		array(37.709416,55.58407),
		array(37.695708,55.578971),
		array(37.682709,55.574157),
		array(37.668471,55.57209),
		array(37.649948,55.572767),
		array(37.63252,55.573749),
		array(37.619243,55.574579),
		array(37.600828,55.575648),
		array(37.586814,55.577785),
		array(37.571866,55.581383),
		array(37.55761,55.584782),
		array(37.534541,55.590027),
		array(37.527732,55.59166),
		array(37.512227,55.596173),
		array(37.501959,55.602902),
		array(37.493874,55.609685),
		array(37.485682,55.616259),
		array(37.477812,55.623066),
		array(37.466709,55.63252),
		array(37.459074,55.639568),
		array(37.450135,55.646802),
		array(37.441691,55.65434),
		array(37.433292,55.66177),
		array(37.425513,55.671509),
		array(37.418497,55.680179),
		array(37.414338,55.687995),
		array(37.408076,55.695418),
		array(37.397934,55.70247),
		array(37.388978,55.709784),
		array(37.38322,55.718354),
		array(37.379681,55.725427),
		array(37.37483,55.734978),
		array(37.370131,55.745291),
		array(37.369368,55.754978),
		array(37.369062,55.763022),
		array(37.369691,55.771408),
		array(37.370086,55.782309),
		array(37.372979,55.789537),
		array(37.37862,55.796031),
		array(37.387047,55.806252),
		array(37.390523,55.81471),
		array(37.393371,55.824147),
		array(37.395176,55.832257),
		array(37.394476,55.840831),
		array(37.392949,55.850767),
		array(37.397368,55.858756),
		array(37.404564,55.866238),
		array(37.417446,55.872996),
		array(37.429672,55.876839),
		array(37.443129,55.88101),
		array(37.45955,55.882904),
		array(37.474237,55.88513),
		array(37.489634,55.889361),
		array(37.503001,55.894737),
		array(37.519072,55.901823),
		array(37.529367,55.905654),
		array(37.543749,55.907682),
		array(37.559757,55.909418),
		array(37.575423,55.910881),
		array(37.590488,55.90913),
		array(37.607035,55.904902),
		array(37.621911,55.901152),
		array(37.633014,55.898735),
		array(37.652993,55.896458),
		array(37.664905,55.895661),
		array(37.681443,55.895106),
		array(37.697513,55.894046),
		array(37.711276,55.889997),
		array(37.723681,55.883636),
		array(37.736168,55.877359),
		array(37.74437,55.872743),
		array(37.75718,55.866137),
		array(37.773646,55.8577),
		array(37.780284,55.854234),
		array(37.792322,55.848038),
		array(37.807961,55.840007),
		array(37.816127,55.835816),
		array(37.829665,55.828718),
		array(37.836914,55.821325),
		array(37.83942,55.811538),
		array(37.840166,55.802472),
		array(37.841145,55.793925)
	);

	public $city_msk = array(
        'Апрелевка',
        'Балашиха',
        'Бронницы',
        'Верея',
        'Видное',
        'Волоколамск',
        'Воскресенск',
        'Высоковск',
        'Голицыно',
        'Дзержинский',
        'Дмитров',
        'Долгопрудный',
        'Домодедово',
        'Дрезна',
        'Дубна',
        'Егорьевск',
        'Жуковский',
        'Зарайск',
        'Звенигород',
        'Ивантеевка',
        'Истра',
        'Кашира',
        'Климовск',
        'Клин',
        'Коломна',
        'Королев',
        'Котельники',
        'Красмоармейск',
        'Красногорск',
        'Краснозаводск',
        'Краснознаменск',
        'Кубинка',
        'Куровское',
        'Ликино-Дулево',
        'Лобня',
        'Лосино-Петровский',
        'Луховицы',
        'Лыткарино',
        'Люберцы',
        'Можайск',
        'Мытищи',
        'Наро-Фоминск',
        'Ногинск',
        'Одинцово',
        'Озеры',
        'Орехово-Зуево',
        'Павловский Посад',
        'Пересвет',
        'Подольск',
        'Протвино',
        'Пушкино',
        'Пущино',
        'Раменское',
        'Реутов',
        'Рошаль',
        'Руза',
        'Сергиев Посад',
        'Серпухов',
        'Солнечногорск',
        'Старая Купавна',
        'Ступино',
        'Талдом',
        'Фрязино',
        'Химки',
        'Хотьково',
        'Черноголовка',
        'Чехов',
        'Шатура',
        'Щелково',
        'Электрогорск',
        'Электросталь',
        'Электроугли',
        'Юбилейный',
        'Яхрома',
    );

    public function initContent()
    {

        parent::initContent();

        $this->display_header = true;

        $action = Tools::getValue('action');
        if (!empty($action) && method_exists($this, 'ajaxProcess'.Tools::ucfirst(Tools::toCamelCase($action))))
            return $this->{'ajaxProcess'.Tools::toCamelCase($action)}();
        elseif (!empty($action) && method_exists($this, 'process'.Tools::ucfirst(Tools::toCamelCase($action))))
            return $this->{'process'.Tools::toCamelCase($action)}();
    }

    public function getTemplateFinder()
    {
        if (!$this->templateFinder) {
            $this->context->smarty->addTemplateDir(_PS_MODULE_DIR_.'pwgrastin/views/templates/front/');
            $this->templateFinder = new TemplateFinder($this->context->smarty->getTemplateDir(), '.tpl');
        }
        return $this->templateFinder;
    }

    public function ajaxProcessShowModal() {
        // определим какие пвз показывать согласно настройкам зон
        $address = new Address($this->context->cart->id_address_delivery);
        if($address->id_country == null)
            $id_country = Configuration::get('PS_COUNTRY_DEFAULT');
        else
            $id_country = $address->id_country;

        $country = new Country($id_country);
        $grastin_regions = json_decode(Configuration::get('PWGRASTIN_REGION'), true);
        $boxberry_regions = json_decode(Configuration::get('PWGRASTIN_BOXBERRY_REGION'), true);

        if(in_array($country->id_zone, $grastin_regions) && !in_array($country->id_zone, $boxberry_regions))
            $type = 'grastin';
        elseif(!in_array($country->id_zone, $grastin_regions) && in_array($country->id_zone, $boxberry_regions))
            $type = 'boxberry';
        else
            $type = 'all';

        //die(var_dump($country));

        $grastinAPI = new GrastinAPI();
        $listPVZ = $grastinAPI->getListPVZ($type);

        foreach($listPVZ as $key => $list) {
            if(
                ($id_country == 177 && $this->inPoly($list['longitude'], $list['latitude'])) ||
                ($id_country == 245 && !$this->inPoly($list['longitude'], $list['latitude']) && $this->inMSKCity($list['city'])) ||
                ($id_country != 177 && $id_country != 245)
            )
                continue;

            unset($listPVZ[$key]);
        }

        $this->context->smarty->assign([
            'list_pvz'  => $listPVZ
        ]);
        $this->setTemplate('modal.tpl');
    }


    public function ajaxProcessGetPVZInfo () {
        $params = Tools::getValue('params');

        $qty_products = Cart::getNbProducts($this->context->cart->id);
        $summ_product = $this->context->cart->getOrderTotal(true, Cart::ONLY_PRODUCTS);
        $weight = $this->context->cart->getTotalWeight() * 1000; //вес в граммах

        $maxwidth = 0;
        $maxheight = 0;
        $fulldepth = 0;

        foreach ($this->context->cart->getProducts() as $product) {
            $width = ($product['width'] > 0)?$product['width']:1;
            $depth = ($product['depth'] > 0)?$product['depth']:1;
            $height = ($product['height'] > 0)?$product['height']:1;
            $maxwidth = max(array($maxwidth, $width));
            $maxheight = max(array($maxheight, $height));
            $fulldepth += $depth * $product['quantity'];
        }

        switch ($params['type']) {
            case 'Grastin':
            case 'PartnerGrastin':
                // если задан вес по умолчанию берем его
                if(Configuration::get('PWGRASTIN_WEIGHT') > 0)
                    $weight = Configuration::get('PWGRASTIN_WEIGHT') * $qty_products * 1000;

                $grastinAPI = new GrastinAPI();
                $price = $grastinAPI->getCalcShipingCost(
                    $this->context->cart,
                    $params,
                    $summ_product,
                    $weight,
                    $maxwidth,
                    $maxheight,
                    $fulldepth
                );

                $cart = new Cart($this->context->cart->id);
                $total_product = $cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING);
                if($total_product >= Configuration::get('PWGRASTIN_FREEPRICE') && Configuration::get('PWGRASTIN_FREEPRICE') > 0)
                    $price = 0;

                $data = array(
                    'price' => Tools::displayPrice($price),
                    'id_pvz' => $params['id'],
                    'cash_card' => $grastinAPI->getPointsPaymentCard($params['id'])
                );

                $result = array_merge($params, $data);
                break;

            case 'BoxBerry':
                // получаем данные о пвз через api boxberry

                // если задан вес по умолчанию берем его и переводим в граммы
                if(Configuration::get('PWGRASTIN_BOXBERRY_WEIGHT') > 0)
                    $weight = Configuration::get('PWGRASTIN_BOXBERRY_WEIGHT') * $qty_products * 1000;

                $BoxBerry = new BoxBerryApi();
                $data = $BoxBerry->getPointsDescription(array('code' => $params['externalid'], 'photo' => '1'));
                //die(var_dump($data));
                $data_params = array(
                    'weight' => $weight,
                    'target' => $params['externalid'],

                    'ordersum' => $summ_product,
                    'deliverysum' => '0',
                    'targetstart' => $params['externalid'],
                    'height' => $maxheight,
                    'width' => $maxwidth,
                    'depth' => $fulldepth,
                );

                $prices = $BoxBerry->getDeliveryCosts($data_params);

                $cart = new Cart($this->context->cart->id);
                $total_product = $cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING);
                if($total_product >= Configuration::get('PWGRASTIN_FREEPRICE') && Configuration::get('PWGRASTIN_FREEPRICE') > 0)
                    $prices['price'] = 0;

                if (count($data) > 0 or !$data['err']) {
                    $result = array(
                        'id' => $params['id'],
                        'id_pvz' => $params['externalid'],
                        'address' => $data['Address'],
                        'city' => $data['CityName'],
                        'phone' => $data['Phone'],
                        'desc' => $data['TripDescription'],
                        'type' => 'BoxBerry',
                        'image' => (count($data['photos']) > 0 ? 'data:image/gif;base64,' . $data['photos'][0] : 'null'), // картинка приходит в base64
                        'timetable' => $data['WorkShedule'],
                        'timedelivery' => '',
                        'price' => Tools::displayPrice($prices['price']),
                        'data_params' => $data_params,
                        'cash_card' => $data['Acquiring'],
                        'deliveryperiod' => $prices['delivery_period']
                    );
                } else {
                    $result = Tools::getValue('params');
                }
                break;


            default:
                $result = array();
        }

        die(json_encode($result));
    }

    public function ajaxProcessGetListPVZ () {
		
        // определим какие пвз показывать согласно настройкам зон
        $address = new Address($this->context->cart->id_address_delivery);
		if($address->id_country == null)
        	$id_country = Configuration::get('PS_COUNTRY_DEFAULT');
		else
			$id_country = $address->id_country;
		
		$country = new Country($id_country);
        $grastin_regions = json_decode(Configuration::get('PWGRASTIN_REGION'), true);
        $boxberry_regions = json_decode(Configuration::get('PWGRASTIN_BOXBERRY_REGION'), true);

		

        if(in_array($country->id_zone, $grastin_regions) && !in_array($country->id_zone, $boxberry_regions))
            $type = 'grastin';
        elseif(!in_array($country->id_zone, $grastin_regions) && in_array($country->id_zone, $boxberry_regions))
            $type = 'boxberry';
        else
            $type = 'all';

        //die(var_dump($country));

        $grastinAPI = new GrastinAPI();
        $listPVZ = $grastinAPI->getListPVZ($type);
		
		


        //die(var_dump($listPVZ));
        $features = array();
        foreach($listPVZ as $list) {
            if(!isset($list['id'])) $id = null; else $id = $list['id'];
            if(!isset($list['timetable'])) $timetable = $list['schedule']; else $timetable = $list['timetable'];
            if(!isset($list['address'])) $address = $list['name']; else $address = $list['address'];
            if(isset($list['addres'])) $address = $list['addres'];
			
			// определим если регион москва, отображение до мкад или за мкад
			if(
				($id_country == 177 && $this->inPoly($list['longitude'], $list['latitude'])) ||
				($id_country == 245 && !$this->inPoly($list['longitude'], $list['latitude']) && $this->inMSKCity($list['city'])) ||
				($id_country != 177 && $id_country != 245)
			)
            $features[] = array(
                'type' => 'Feature',
                'id' => $id,
                'geometry' => array(
                    'type' => 'Point',
                    'coordinates' => array($list['latitude'], $list['longitude'])
				),
                'properties' => array(
                    'id' => $list['id'],
                    'timetable' => $timetable,
                    'city' => (isset($list['city']) ? $list['city'] : 'null'),
                    'address' => $address,
                    'image' => (isset($list['linkdrivingdescription']) ? '' : 'null'),
                    'phone' => (isset($list['phone']) ? $list['phone'] : 'null'),
                    'type' => $list['type'],
                    'externalid' => (isset($list['externalid']) ? $list['externalid'] : 'null'),
                    'desc' => (isset($list['drivingdescription']) ? $list['drivingdescription'] : 'null')
                ),
                'options' => array(
                    'iconLayout' => 'default#image',
                    'iconImageHref' =>  'http://zterra.leadget.ru/modules/pwgrastin/img/object.png',
                    'iconImageSize' => array(25, 33),
					'iconImageOffset' => array(-5, -38)
                )
            );

        }

        $result = array('type' => 'FeatureCollection', 'features' => $features);
        die(json_encode($result));
    }

	

    public function ajaxProcessSetCookieAddres()
    {

        $params = json_decode(Tools::getValue('params'), true);

        if(!is_array($params))  die(json_encode(array('error'=>'true')));

        // удалим картинку если боксберри, base64 не получается сохранить в куку
        $image = $params['image'];
        if ($params['type'] == 'BoxBerry')
            $params['image'] = null;

        $image = $params['image'];
        $params['desc'] = '';
        $params['image'] = $image;



        //$boxberry_details = Tools::jsonDecode($details);
        $this->context->cookie->__set('pwgrastin', json_encode($params));


        //$details = $this->context->cookie->pwboxberry;

        $dOptions = $this->context->cart->getDeliveryOption();

        $carrier = new Carrier($dOptions[$this->context->cart->id_address_delivery]);

        if(!empty($params['address']) && $carrier->external_module_name == 'pwgrastin'){
            if($this->context->cart->id_address_delivery > 0) {
                $address = new Address($this->context->cart->id_address_delivery);
                $address->city = $params['city'];
                $address->address1 = $params['address'] . ' (' . $params['type'] . ')';

                $address->save();
            }
            // обновляем хэш сумму
            $rawData  = Db::getInstance()->getValue(
                'SELECT checkout_session_data FROM ' . _DB_PREFIX_ . 'cart WHERE id_cart = ' . (int)$this->context->cart->id
            );
            $data     = json_decode($rawData, true);

            $cartChecksum = new CartChecksum(new AddressChecksum());
            $checksum = $cartChecksum->generateChecksum($this->context->cart);
            $data['checksum'] = $checksum;

            Db::getInstance()->execute(
                'UPDATE ' . _DB_PREFIX_ . 'cart SET checkout_session_data = "' . pSQL(json_encode($data)) . '"
                WHERE id_cart = ' . (int)$this->context->cart->id
            );

            //die(json_encode(array('old_add' => $this->context->cart->id_address_deliver, 'new' => $address->id)));
        }

        die(json_encode($params));
    }

    public function ajaxProcessSetCustomerContactsData()
    {
        $name = Tools::getValue('name');
        $value = Tools::getValue('value');

        $this->context->cookie->$name = $value;
        die();
    }

    public function ajaxProcessGetCustomerContactsData()
    {
        $data = array(
            'name' => $this->context->cookie->pw_customer_firstname,
            'phone' => $this->context->cookie->pw_delivery_phone_mobile,
            'email' => $this->context->cookie->pw_customer_email,
            'addr' => $this->context->cookie->pw_delivery_address1,
        );
        die(json_encode($data));
    }

    public function ajaxProcessGetProductsWeightAndPrice()
    {
        $cartTotal = $this->context->cart->getOrderTotal(true, Cart::ONLY_PRODUCTS);
        $weight = $this->context->cart->getTotalWeight() * 1000;
        die(json_encode(array(
            'price' => $cartTotal,
            'weight' => $weight,
        )));
    }

    public function ajaxProcessUnsetCoockie() {
        $this->context->cookie->pwgrastin = null;
        die(true);
    }
	
	/*
     * Функция, проверяющая, находится ли указанная точка внутри МКАД
     * @param x широта
     * @param y долгота
     */
	public function inPoly( $x, $y ) {
		$j = count($this->mkad_points) - 1;
		$c = false; // true/false - внутри или вне полигона
		for ($i = 0; $i < count($this->mkad_points); $i++) {
            if (
                (
                    (($this->mkad_points[$i][1] <= $y) && ($y < $this->mkad_points[$j][1])) ||
                    (($this->mkad_points[$j][1] <= $y) && ($y < $this->mkad_points[$i][1]))
                ) && ($x > ($this->mkad_points[$j][0] - $this->mkad_points[$i][0]) * ($y - $this->mkad_points[$i][1]) / ($this->mkad_points[$j][1] - $this->mkad_points[$i][1]) + $this->mkad_points[$i][0])
            ) {
                $c = !$c;
            }
            $j = $i;
        }
	    return $c;
    }


    public function inMSKCity($city) {
	    if(in_array($city, $this->city_msk))
	        return true;
    }

}