<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2015 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class AdminPWGrastinController extends ModuleAdminController
{
    public $module;
    public $PWClass;

    public function __construct()
    {
        $this->table         = 'ps_pwgrastin_details';
        $this->className     = 'GrastinAPI';
        $this->module        = 'PWGrastin';
        $this->lang          = false;
        $this->bootstrap     = true;
        $this->need_instance = 0;

        $this->context       = Context::getContext();
        $this->errors        = array();
        $this->success       = 0;

        parent::__construct();

        $this->token = Tools::getAdminTokenLite('AdminPWGrastin');


    }

    public function initContent() {

        $action = Tools::getValue('action');
        if (!empty($action) && method_exists($this, 'ajaxProcess'.Tools::ucfirst(Tools::toCamelCase($action))))
            return $this->{'ajaxProcess'.Tools::toCamelCase($action)}();
        elseif (!empty($action) && method_exists($this, 'process'.Tools::ucfirst(Tools::toCamelCase($action))))
            return $this->{'process'.Tools::toCamelCase($action)}();

    }



}
