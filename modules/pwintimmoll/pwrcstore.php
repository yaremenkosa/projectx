<?php
if (!defined('_PS_VERSION_'))
    exit;

include_once ('classes/RCStoreAPI.php');

class pwrcstore extends Module
{
    public $api;

    public function __construct()
    {
        $this->name = strtolower(get_class());
        $this->version = 0.1;
        $this->author = 'PrestaWeb.ru';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l("PW RCStore");
        $this->description = $this->l("Модуль выгрузки товаров с rcstore.ru");

        $this->ps_versions_compliancy = array('min' => '1.5.0.0', 'max' => _PS_VERSION_);

        $this->api = new RCStoreAPI();
    }

    public function install()
    {
        //Configuration::updateValue('PWGRASTIN_CARRIER_ID', $this->installCarrier());
        if ( !parent::install()
            //|| !$this->installCarrier()
            //|| !$this->registerHook('actionCarrierUpdate')
            || !$this->registerHook('displayHeader')
            //|| !$this->registerHook('actionValidateOrder')
            //|| !$this->registerHook('displayAdminOrder')
            //|| !$this->registerHook('displayCarrierExtraContent')
            || !$this->installTables()
        ) return false;

        return true;
    }

    public function uninstall()
    {
        if(!parent::uninstall()
            //|| !$this->uninstallCarrier(Configuration::get('PW_RUSSIAN_POST_CARRIER_ID'))
            || !$this->uninstallTables()
        ) return false;

        return true;
    }

    private function installTables() {
        $result = true;

        $sql[] = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."pwrcstore_categories` (
                `id_category_site` INT(11) NOT NULL,
                `id_category` TEXT NOT NULL,
                `price` decimal(20,6) NOT NULL DEFAULT '0.000000'
            ) DEFAULT CHARACTER SET='utf8' COLLATE='utf8_general_ci';";

        foreach($sql as $query){
            if(!Db::getInstance()->execute($query)) $result = false;
        }

        return $result;
    }

    private function uninstallTables() {
        return (
        Db::getInstance()->execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'pwrcstore_categories')
        );
    }

    public function renderForm()
    {
        $categories = $this->api->getCategories();
        $cat = array();
        if(count($categories) > 0) {
            foreach ($categories as $key => $category) {
                $cat[] = $category;
                if (isset($category['children'])) {
                    foreach ($category['children'] as $k => $child) {
                        $child['name'] = '&nbsp;&nbsp;&nbsp;&nbsp;' . $child['name'];
                        $cat[] = $child;
                    }

                }
            }
        }

        //die(var_dump($cat));

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Настройки'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('API Токен'),
                        'name' => 'PWRCSTORE_TOKEN',
                    ),
                    array(
                        'label' => $this->l('Наценка на товар'),
                        'name' => 'PWRCSTORE_PRICE',
                        'cast' => 'floatval',
                        'type' => 'text',
                        'maxlength' => 10,
                        'suffix' => '%',
                    ),
                    array(
                        'label' => $this->l('E-Mail для уведомлений'),
                        'name' => 'PWRCSTORE_EMAIL',
                        'type' => 'text',
                        'desc' => $this->l('Список e-mail через запятую'),
                    ),
                    array(
                        'label' => $this->l('Текст, для товара в наличии'),
                        'name' => 'PWRCSTORE_AVAILABLE_NOW',
                        'type' => 'text',
                    ),
                    array(
                        'label' => $this->l('Текст, для товара не в наличии'),
                        'name' => 'PWRCSTORE_AVAILABLE_LATER',
                        'type' => 'text',
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Обновить все цены?'),
                        'name' => 'PWRCSTORE_RESETALLPRICE',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Да'),
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Нет'),
                            )
                        ),
                    ),
                    /*array(
                        'type' => 'select',
                        'multiple' => 'true',
                        'label' => $this->l('Категории'),
                        'name' => 'PWRCSTORE_CATEGORIES[]',
                        'desc' => 'Выберите категории, товары которых необходимо выгружать',
                        'options' => array(
                            'id' => 'id',
                            'name' => 'name',
                            'query' => $cat
                        ),
                    ),
                    /*array(
                        'type' => 'text',
                        'label' => $this->l('ID магазина'),
                        'name' => 'PWGRASTIN_IM_ID',
                    ),*/
                ),
                'submit' => array(
                    'title' => $this->l('Сохранить'),
                )
            ),
        );

        if(Module::isEnabled('shopimultitabs')) {
            $id_shop = Context::getContext()->shop->id;
            $id_lang = Context::getContext()->language->id;
            $array = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                SELECT hs.`id_shopimultitab` as id_shopimultitab,
                           hss.`position`, hss.`active`,
                           hssl.`title`
                FROM '._DB_PREFIX_.'shopimultitab_shop hs
                LEFT JOIN '._DB_PREFIX_.'shopimultitab hss ON (hs.id_shopimultitab = hss.id_shopimultitab)
                LEFT JOIN '._DB_PREFIX_.'shopimultitab_lang hssl ON (hss.id_shopimultitab = hssl.id_shopimultitab)
                WHERE (id_shop = '.(int)$id_shop.')
                AND hssl.id_lang = '.(int)$id_lang. ' AND hss.`active` = 1 ORDER BY hss.position'
            );
            if(count($array) > 0)
                $fields_form['form']['input'][] = array(
                    'label' => $this->l('Выберите TAB, отвечающий за видео'),
                    'name' => 'PWRCSTORE_TABVIDEO',
                    'type' => 'select',
                    'options' => array(
                        'query' => $array,
                        'id' => 'id_shopimultitab',
                        'name' => 'title',
                        'default' => array(
                            'value' => '',
                            'label' => $this->l('-- Choose --')
                        )
                    )
                );
        }

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitPWRCStore';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function renderFormAssoc()
    {
        $cat_site = Category::getSimpleCategories($this->context->language->id);

        foreach($cat_site as &$catss) {
            $catss['link'] = Context::getContext()->link->getCategoryLink($catss['id_category']);
        }

        $sql = "SELECT * FROM `"._DB_PREFIX_."pwrcstore_categories`";
        $categories_j = DB::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        $categories = $this->api->getCategories();
        $cat = array();
        if(count($categories) > 0) {
            foreach ($categories as $key => $category) {

                foreach($categories_j as $cats) {
                    if ($category['name'] == $cats['id_category']) {
                        $category['id_category_site'][] = $cats['id_category_site'];
                        $category['price'] = $cats['price'];
                        //break;
                    }
                }
                $cat[] = $category;
                if (isset($category['children'])) {
                    foreach ($category['children'] as $k => $child) {
                        $child['name_orig'] = $child['name'];
                        foreach($categories_j as $cats) {
                            if ($child['name_orig'] == $cats['id_category']) {
                                $child['id_category_site'][] = $cats['id_category_site'];
                                $child['price'] = $cats['price'];
                                //break;
                            }
                        }

                        $child['name'] = '&nbsp;&nbsp;&nbsp;&nbsp;' . $child['name'];
                        $cat[] = $child;
                    }

                }
            }
        }

        $this->context->smarty->assign(array(
            'categories' => $cat_site,
            'cat' => $cat,
            'url_form' => $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token=' . Tools::getAdminTokenLite('AdminModules')
        ));
        return $this->context->smarty->fetch(_PS_MODULE_DIR_.$this->name.'/views/templates/admin/form.tpl');
    }

    public function getConfigFieldsValues()
    {
        $categories = json_decode(Tools::getValue('PWRCSTORE_CATEGORIES', Configuration::get('PWRCSTORE_CATEGORIES')), true);

        return array(
            'PWRCSTORE_TOKEN' => Tools::getValue('PWRCSTORE_TOKEN', Configuration::get('PWRCSTORE_TOKEN')),
            'PWRCSTORE_PRICE' => Tools::getValue('PWRCSTORE_PRICE', Configuration::get('PWRCSTORE_PRICE')),
            'PWRCSTORE_EMAIL' => Tools::getValue('PWRCSTORE_EMAIL', Configuration::get('PWRCSTORE_EMAIL')),
            'PWRCSTORE_AVAILABLE_NOW' => Tools::getValue('PWRCSTORE_AVAILABLE_NOW', Configuration::get('PWRCSTORE_AVAILABLE_NOW')),
            'PWRCSTORE_AVAILABLE_LATER' => Tools::getValue('PWRCSTORE_AVAILABLE_LATER', Configuration::get('PWRCSTORE_AVAILABLE_LATER')),
            'PWRCSTORE_RESETALLPRICE' => 0,
            'PWRCSTORE_TABVIDEO' => Tools::getValue('PWRCSTORE_TABVIDEO', Configuration::get('PWRCSTORE_TABVIDEO')),
            'PWRCSTORE_CATEGORIES[]' => $categories,
        );
    }

    public function getContent()
    {
        $output = '';
        if (Tools::isSubmit('submitPWRCStore'))
        {
            // обновим категории
            if(Tools::isSubmit('PWRCSTORE_CATASS') > 0) {

                $sql = "TRUNCATE TABLE `"._DB_PREFIX_."pwrcstore_categories`;";
                Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
                foreach (Tools::getValue('PWRCSTORE_CATASS') as $key => $category) {
                    if(!empty($category)) {
                        $prices = Tools::getValue('PWRCSTORE_CATPRICE');

                        $sql = "INSERT INTO `" . _DB_PREFIX_ . "pwrcstore_categories` 
                            SET `id_category_site` = '" . $key . "',
                            `id_category` = '" . trim($category) . "',
                            `price` = '" . $prices[$key] . "'";
                        Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
                    }
                }

            } else {
                Configuration::updateValue('PWRCSTORE_TOKEN', Tools::getValue('PWRCSTORE_TOKEN'));
                Configuration::updateValue('PWRCSTORE_PRICE', Tools::getValue('PWRCSTORE_PRICE'));
                Configuration::updateValue('PWRCSTORE_EMAIL', Tools::getValue('PWRCSTORE_EMAIL'));
                Configuration::updateValue('PWRCSTORE_AVAILABLE_NOW', Tools::getValue('PWRCSTORE_AVAILABLE_NOW'));
                Configuration::updateValue('PWRCSTORE_AVAILABLE_LATER', Tools::getValue('PWRCSTORE_AVAILABLE_LATER'));
                Configuration::updateValue('PWRCSTORE_TABVIDEO', Tools::getValue('PWRCSTORE_TABVIDEO'));
                Configuration::updateValue('PWRCSTORE_CATEGORIES', json_encode(Tools::getValue('PWRCSTORE_CATEGORIES')));
            }

            // сбросим обновление цены, если выбрано
            if(Tools::getValue('PWRCSTORE_RESETALLPRICE') != 1) {
                foreach (Tools::getValue('PWRCSTORE_RESETPRICE') as $key => $result) {
                    $sql = 'SELECT p.`id_product`
                    FROM `' . _DB_PREFIX_ . 'product` p
                    LEFT JOIN `' . _DB_PREFIX_ . 'category_product` c ON (c.`id_product` = p.`id_product`)
                    LEFT JOIN `' . _DB_PREFIX_ . 'price_parser_ids` pp ON (pp.`id_item_site` = p.`id_product`)
                    WHERE c.`id_category` = ' . (int)$key;
                    $ids_product = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

                    $ids = array();
                    foreach ($ids_product as $id)
                        $ids[] = $id['id_product'];

                    $ids = implode(',', $ids);

                    $sql = "UPDATE `" . _DB_PREFIX_ . "price_parser_ids` SET `update_price` = '0' WHERE `id_item_site` IN(" . $ids . ") AND `id_vendor` = 'RCStore' AND `id_item_type` = '1'";
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);

                }
            } else {
                $sql = "UPDATE `" . _DB_PREFIX_ . "price_parser_ids` SET `update_price` = '0' WHERE `id_vendor` = 'RCStore' AND `id_item_type` = '1'";
                Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
            }


            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&conf=6');

        }
        return $output.$this->renderForm().$this->renderFormAssoc();
    }

    // получаем id используемых категорий
    public function getCategoriesIDs() {
        $sql = "SELECT `id_category` FROM `" . _DB_PREFIX_ . "pwrcstore_categories`";
        $categories = DB::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        $ids = array();
        foreach($categories as $category) {
            $ids[] = $category['id_category'];
        }
        return implode(',' , $ids);
    }

    // получаем связи категорий
    public function getCategoriesBind($ids) {
        $sql = "SELECT * FROM `" . _DB_PREFIX_ . "pwrcstore_categories` WHERE `id_category` = '" . $ids[0] . "'";
        var_dump($sql);
        return DB::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    }
}


