<?php


class RCStoreAPI extends ObjectModel {

    public $api_url = 'https://rcstore.ru/api/run/';
    public $api_key;
    protected $tmp_path;
    public $debug;

    public function __construct($id = null, $id_lang = null, $id_shop = NULL) {

        $this->api_key = Configuration::get('PWRCSTORE_TOKEN');
        $this->debug = true;
        $this->tmp_path = dirname(__FILE__) . DIRECTORY_SEPARATOR . '../tmp';
        //parent::__construct($id, $id_lang, $id_shop);

    }

    private function getRequest($method, $params = array()) {
        $url = $this->api_url . $method . '?token=' . $this->api_key . (isset($params) ? '&' . http_build_query($params) : '');
        $handle = @fopen($url, "rb");
        if($handle) {
            $contents = stream_get_contents($handle);
            fclose($handle);
        } else {
            $contents = '';
        }
        $data = json_decode($contents, true);

        return $data;
    }

    private function sendRequest($method, $params) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->api_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            'token' => $this->api_key,
            'method' => $method,
            'sdata' => json_encode($params)
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = json_decode(curl_exec($ch),1);
        curl_close($ch);
        return $data;
    }

    // получение товаров
    public function getProducts($params = array()) {
        return $this->joinProducts();
    }

    // получение категорий
    public function getCategories($params = array()) {
        $result = $this->getRequest('exportCategory', $params);

        $categories = $this->formatedCategories($result['result']['DATA']);

        return $categories;
    }

    // определение страницы
    public function getPage($request) {
        if(isset($request['result']['NEXT_HREF'])) {
            $str = explode('&' , $request['result']['NEXT_HREF']);
            $str = explode('=', $str[count($str)-1]);
            return $str[count($str)-1];
        }
        return false;
    }

    // получаем id категории, к которой прикреплять
    public function getCategoryDefault($id) {
        $sql = "SELECT `id_category` FROM `"._DB_PREFIX_."category` WHERE `rcstore` = '" . $id . "'";
        $id_category = Db::getInstance()->getValue($sql);

        if($id_category > 0)
            return $id_category;
        else
            return Configuration::get('PS_HOME_CATEGORY');
    }

    // сбор товаров в один массив
    private function joinProducts(&$products_array = array(), $offset = 1) {
        $request = $this->getRequest('exportProducts', array('offset' => $offset));
        $page = $this->getPage($request); // определим какая страница след.

        $products_array = array_merge($products_array, $request['result']['DATA']);


        if($page > $offset && $page < 1)
            $this->joinProducts($products_array, $page);
        die(var_dump($products_array));
        return $products_array;
    }

    private function formatedCategories($categories = array()) {
        if(count($categories) > 0) {
            foreach ($categories as $key => $category) {
                $categories[$key]['id'] = $category['CODE'];
                $categories[$key]['name'] = $category['NAME'];
                if (isset($category['CHILDS'])) {
                    $categories[$key]['children'] = $this->formatedCategories($category['CHILDS']);
                    unset($categories[$key]['CHILDS']);
                }
                unset($categories[$key]['CODE']);
                unset($categories[$key]['NAME']);
            }
        }

        return $categories;
    }

    private function formatedImages($images = array()) {
        if(count($images) > 0) {
            $result = array();
            if(isset($images['PREVIEW_PICTURE']))
                $result[] = array(
                    'url' => $images['PREVIEW_PICTURE'],
                    'cover' => 1
                );

            if(isset($images['MORE_PHOTO']) && count($images['MORE_PHOTO']) > 0) {
                foreach($images['MORE_PHOTO'] as $img) {
                    $result[] = array(
                        'url' => $img,
                        'cover' => 0
                    );
                }
            }
            return $result;
        }
        return $images;
    }

    public function filterCategory($products = array()) {
        if(count($products) > 0) {
            $categories = $this->getCategories();
            $categories_setting = json_decode(Configuration::get('PWRCSTORE_CATEGORIES'), true);

            $cat_name = array();
            if(count($categories) > 0) {
                foreach ($categories as $key => $category) {

                    if(in_array($category['id'], $categories_setting))
                        $cat_name[$category['id']] = $category['name'];

                    if (isset($category['children'])) {
                        foreach ($category['children'] as $k => $child)
                            if(in_array($child['id'], $categories_setting))
                                $cat_name[$child['id']] = $child['name'];

                    }
                }
            }

            foreach($products as $key => $product) {
                if(!in_array($product['CATEGORY'], $cat_name))
                    unset($products[$key]);
                else
                    $products[$key]['ID_CATEGORY'] = array_search ($product['CATEGORY'], $cat_name);
            }

            return $products;
        }
    }

    // проверка, был ли добавлен товар ранее
    public function checkProduct($id_product) {
        $sql = "SELECT `id_product` FROM `"._DB_PREFIX_."pwrcstore_product` WHERE `id_product_rcstore` = '" . $id_product . "'";
        $id = Db::getInstance()->getValue($sql);
        return $id;
    }

    public function saveImages($images = array(), $productSite) {
        $this->log('Начинаем обработку изображений');
        $images = $this->formatedImages($images);
        $this->log($images);
        //загружаем изображения
        foreach ($images as $i => $image) {

            $imgFile = $this->loadImage($image['url']);

            if ($imgFile === null) {
                //если не удалось загрузить изображение - пропускаем его
                $this->log('ERROR: Не удалось загрузить изображение: ' . $image['url']);
                continue;
            } else {
                $this->log('Изображение: ' . $image['url'] . ' загружено');
            }

            //если изображение первое в списке, то удаляем cover у других изображений товара
            if ($i === 0) {
                Image::deleteCover($productSite->id);
            }

            $imageSite             = new Image();
            $imageSite->id_product = $productSite->id;
            $imageSite->cover      = ($i === 0) ? 1 : 0;
            $imageSite->position   = $i;

            if (!$imageSite->save()) {
                $this->log('Не удалось сохранить модель изображения в БД: ' . $image['url']);
            }

            if (!$newImgPath = $imageSite->getPathForCreation()) {
                $this->log('Ошибка при получении каталога сохранения изображения на сайте');
            }

            $error = 0;

            if (!ImageManagerCore::resize($imgFile, $newImgPath . '.' . $imageSite->image_format, null, null, 'jpg',
                false, $error)
            ) {
                switch ($error) {
                    case ImageManagerCore::ERROR_FILE_NOT_EXIST :
                        $this->log('ERROR: An error occurred while copying image, the file does not exist anymore.');
                        break;

                    case ImageManagerCore::ERROR_FILE_WIDTH :
                        $this->log('ERROR: An error occurred while copying image, the file width is 0px.');
                        break;

                    case ImageManagerCore::ERROR_MEMORY_LIMIT :
                        $this->log('ERROR: An error occurred while copying image, check your memory limit.');
                        break;

                    default:
                        $this->log('ERROR: An error occurred while copying image.');
                        break;
                }
            } else {
                $imagesTypes = ImageTypeCore::getImagesTypes('products');
                foreach ($imagesTypes as $imageType) {
                    if (!ImageManagerCore::resize($imgFile,
                        $newImgPath . '-' . stripslashes($imageType['name']) . '.' . $imageSite->image_format,
                        $imageType['width'], $imageType['height'], $imageSite->image_format)
                    ) {
                        $this->log('ERROR: An error occurred while copying image:' . stripslashes($imageType['name']));
                    }
                }
            }

            HookCore::exec('actionWatermark', ['id_image' => $imageSite->id, 'id_product' => $productSite->id]);

            if (!$imageSite->update()) {
                $this->log('ERROR: Error while updating image status');
            }

            // Associate image to shop from context
            $shops = ShopCore::getContextListShopID();
            $imageSite->associateTo($shops);

            unlink($imgFile);

            $this->log('Изображение сохранено успешно');
        }
    }

    // загрузка изображения
    public function loadImage($url) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,				$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,	true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT,	5);
        curl_setopt($curl, CURLOPT_TIMEOUT,			30);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION,	true);

        $result = curl_exec($curl);

        $err_no = curl_errno($curl);

        curl_close($curl);

        if($err_no !== 0) {
            return null;
        }
        $filename = explode('.', $url);
        $filename = uniqid() . '.' . $filename[count($filename)-1];

        $tmpDir = $this->tmp_path . DIRECTORY_SEPARATOR . 'img';

        if (!file_exists($tmpDir)) {
            if (!is_writable($this->tmp_path)) {
                $this->log('Директория ' . $tmpDir . ' недоступна для записи');
            }

            mkdir($tmpDir);
        }

        $filePath = $tmpDir . DIRECTORY_SEPARATOR . $filename;

        file_put_contents($filePath, $result);

        return $filePath;
    }


    public function log($message) {
        file_put_contents(dirname(__FILE__) .'/../logs/log.txt', "====================" . date("d.m.Y H:s", time()) . "=================\n" . dirname(__FILE__) . "\n", FILE_APPEND);
        if(is_array($message))
            file_put_contents(dirname(__FILE__) .'/../logs/log.txt', print_r($message, 1) . "\n" . dirname(__FILE__) . "\n", FILE_APPEND);
        else
            file_put_contents(dirname(__FILE__) .'/../logs/log.txt', $message . "\n" . dirname(__FILE__) . "\n", FILE_APPEND);
    }
}