{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    SeoSA <885588@bk.ru>
*  @copyright 2012-2017 SeoSA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


{block name="after"}
    <div class="panel">
        <div class="panel-heading">{l s='Настройка ассоциаций категорий'}</div>
        <form action="{$url_form}" method="post">
            <input type="hidden" name="submitPWRCStore" value="1">
        <div class="form-group clearfix">
            <div class="col-lg-12">
                <table class="table table_full_width">
                    <thead>
                        <th>{l s='Категория сайта'}</th>
                        <th>{l s='Перезаписать цены'}</th>
                        <th style="text-align: center">{l s='Категория rcstore.ru'}</th>
                        <th style="width: 20%">Наценка на товар</th>
                    </thead>
                    <tbody>
                    {if is_array($categories) && count($categories)}
                        {foreach from=$categories item=category}
                            {assign var="price" value="0"}
                            <tr>
                                <td>
                                    {$category.id_category|escape:'quotes':'UTF-8'}|<a href="{$category.link}">{$category.name|escape:'quotes':'UTF-8'}</a>
                                </td>
                                <td><input type="checkbox" name="PWRCSTORE_RESETPRICE[{$category.id_category|intval}]" value="1"></td>
                                <td align="center">
                                    <select name="PWRCSTORE_CATASS[{$category.id_category|intval}]">
                                        <option value="">-- Выберите категорию --</option>
                                        {foreach from=$cat item=it}
                                            {if $category.id_category == $it.id_category_site}
                                                {assign var="price" value=$it.price}
                                            {/if}
                                            <option value="{$it.name_orig}" {if in_array($category.id_category, $it.id_category_site)}selected{/if}>{$it.name}</option>
                                        {/foreach}
                                    </select>
                                </td>
                                <td class="input-group">

                                    <input type="text" name="PWRCSTORE_CATPRICE[{$category.id_category|intval}]" value="{$price}" class="" maxlength="10">
                                    <span class="input-group-addon">
										   %
										</span>

                                </td>
                            </tr>
                        {/foreach}
                    {/if}
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" value="1" id="module_form_submit_btn" name="submitPWRCStore" class="btn btn-default pull-right">
                <i class="process-icon-save"></i> Сохранить
            </button>
        </div>
        </form>
    </div>

{/block}