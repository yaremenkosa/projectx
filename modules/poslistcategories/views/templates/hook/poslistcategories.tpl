{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="list_categories">
	<div class="container">
		<h2 class="list_categories-title">Каталог секс-игрушек</h2>
		<div class="flex_items">
			{foreach from=$categories item=category name=myLoop}

				<div class="flex_item">
					<div class="flex_item-body">
						{if $category.image}
							<div class="flex_item-img">
								<a href="{$link->getCategoryLink($category['id_category'])}">
									<img src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`poslistcategories/images/`$category.image|escape:'htmlall':'UTF-8'`")}"
										alt="" />
								</a>
							</div>
						{/if}

						{if $category.category_name}
							<div class="flex_item-title">
								<a href="{$link->getCategoryLink($category['id_category'])}">{$category.category_name}</a>
							</div>
						{/if}
					</div>
				</div>

			{/foreach}
		</div>
	</div>
</div>