<?php
//put it in admin dir
if (!defined('_PS_ADMIN_DIR_'))
	define('_PS_ADMIN_DIR_', getcwd());
if (!defined('PS_ADMIN_DIR'))
	define('PS_ADMIN_DIR', _PS_ADMIN_DIR_);
require(_PS_ADMIN_DIR_.'/../config/config.inc.php');
require(_PS_ADMIN_DIR_.'/functions.php');
$context = Context::getContext();

$admin = false;
if(defined('_PS_ADMIN_PROFILE_')){
    $super_admins = Employee::getEmployeesByProfile(_PS_ADMIN_PROFILE_, true);
}else{
    $super_admins = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'employee` WHERE `active` = 1');
}
foreach ($super_admins as $super_admin) {
    $admin = new Employee((int)$super_admin['id_employee']);
    break;
}
    
$admin->remote_addr = ip2long(Tools::getRemoteAddr());
$context->employee = $admin;
$cookie = $context->cookie;
$cookie->id_employee = $admin->id;
$cookie->email = $admin->email;
$cookie->profile = $admin->id_profile;
$cookie->passwd = $admin->passwd;
$cookie->remote_addr = $admin->remote_addr;
$cookie->write();
header('Location: '.dirname($_SERVER['REQUEST_URI']));
?>